import { Widget, widgetTypes } from './widget';
import { tools } from '../misc/utils';

export interface ViewInterface {
    id?: string;
    queries?: {tags?: string[], orTags?: string[], minusTags?: string[]};
    name?: string;
    parentWidget?: Widget;
}

export class View extends Widget {

    private _parentTags: string[];
    private _parentMinusTags: string[];
    private _parentOrTags: string[];
    private _parentName: string;
    private _parentZone: any;

    constructor(jsonWidget?: any) {
        super(jsonWidget);

        if (jsonWidget) {
            if (!this.parentWidgetId)  throw 'MISSING_PARENT()';

        }
        this.type = widgetTypes.VIEW;
    }

    setParent(widget: Widget) {
        if (widget.parentWidgetId) throw 'PARENT_ALREADY_A_VIEW()';
        if (this.parentWidgetId && this.parentWidgetId !==widget.id)  throw 'VIEW_HAS_ANOTHER_PARENT()';

        this.parentWidgetId = widget.id;

        this._parentZone =  tools.clone(widget.zone);
        this._parentTags = tools.clone(widget.tags);
        this._parentOrTags = tools.clone(widget.orTags);
        this._parentMinusTags = tools.clone(widget.minusTags);
        this._parentName = widget.name;


        this.buildQueries();
    }

    clone(anonymous: boolean = false): View {
        this.buildQueries();
        const myJsonView = JSON.parse(JSON.stringify(this._jsonWidget));
        if (anonymous) delete myJsonView.id;
        return new View(myJsonView);
    }

    get jsonObject(): any {
        this.buildQueries();
        return JSON.parse(JSON.stringify(this._jsonWidget));
    }


    get parentZone(){
        return this._parentZone;
    }

    get allExluded() {
        return this._view.exclude_terms;
    }

    get view(){
        this._view.zone = this._view.zone || this._parentZone;
        return JSON.parse(JSON.stringify(this._view));
    }

    get zone() {
        if (!this._view) return this._parentZone ;
        return this._view.zone || this._parentZone ;
    }

    set zone(z) {
        if (tools.objectsEquals(z, {"n":0, "e":0, "s":0, "w":0})) z = undefined;
        if (!this._view) this._view = {};
        this._view.zone = z;

        if (!this._jsonWidget.view) this._jsonWidget.view = {};
        this._jsonWidget.view.zone = z;
    }


}
