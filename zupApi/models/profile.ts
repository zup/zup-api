import { _attributionTypes} from '../listOfValues';
import * as utils from '../misc/utils';
import { Label } from './labels';

export type JsonProfile = {
    attribution: string;
    b2b_followers: number,
    blocked?: boolean,
    category?: string,
    cover?: string,
    created?: number,
    description?: string,
    email?: string, // for members
    extern?: boolean,
    followers?: number,
    followings?: number,
    format?: number,
    gender?: string,
    id: string,
    interests: string[],
    interests_details?: {intensity: number, tags: string}[];
    is_profile_engaged?: boolean;
    isError?: boolean,
    kind: string,
    last_seen?: number,
    last_source?: string,
    last_widget_id?: string,
    lead_score?: number
    live_in?: any,
    name: string,
    profile_label_ids?: string[],
    screen_name: string,
    search_terms?: string [],
    thumbnail?: string,
    total_published?: number,
    updated?: number,
    xscore?: number,
}

export type SimpleProfile = {
    id: string,
    name: string,
    screenName: string,
    thumbnail: string
}

export interface ProfileInterface {
    id: string;
    attributionLink: string; // https://twitter.com/user
    attributionKind: string; // 'tw'
    cover?: string;
    customValue?: any;
    description?: string;
    interests?: string[];
    followers?: number;
    format?: number;
    labels?: Label[];
    lastSeen?: number;
    leadScore?: number;
    livesIn?: any;
    name: string;
    nbPosts?: number;
    screenName: string;
    thumbnail: string;
    widgetId?: string;

}


export class Profile implements ProfileInterface {
    id: string;
    isError: boolean;
    kind: string;
    name: string;
    screenName: string;
    email: string;
    thumbnail: string;
    description: string;
    attributionLink: string;
    attributionKind: string;
    cover: string;
    interests: string[] = [];
    interestsV2: {
        name: string,
        intensity?: number,
        tags?: string[]
    }[];
    followers: number = 0;
    format?: number;
    gender: string;
    leadScore: number = 0;
    lastSeen: number = utils.dates.toZIFromDate(new Date());
    nbPosts: number = 0;
    widgetId: string;
    labels: any[] = [];
    livesIn?: any;
    isBanned: boolean;
    isEngaged: boolean;
    customValue: any;

    constructor(private juser: JsonProfile, private allLabels?: Label[]) {
        if (!juser) throw 'BAD_CONSTRUCTOR(User)';

        if (juser.isError) {
            this.id = juser.id;
            this.isError = true;
        }

        this.id = juser.id;
        this.kind = juser.kind;
        this.thumbnail = juser.thumbnail;
        this.description = juser.description;
        this.attributionLink = juser.attribution;
        this.attributionKind = juser.kind === _attributionTypes.PLACE ? _attributionTypes.FACEBOOK : juser.kind;
        this.cover = juser.cover;
        this.isEngaged = juser.is_profile_engaged;
        this.gender = juser.gender;
        this.livesIn = juser.live_in;

        // name vs screen name has to be defined (for me, the name is the id, the screen name is the understandable name:
        //if (this.attributionKind !== _attributionTypes.FACEBOOK) {
        this.screenName = juser.screen_name;
        this.name = juser.name;
        /*} else {
         this.screenName = juser.name;
         this.name = juser.screen_name;
         //} */
        // TODO: check for 500px

        //labels
        if (allLabels && juser.profile_label_ids) this.labels = juser.profile_label_ids.map(id => allLabels.find(l => l.id === id));

        // tobe decided :
        this.leadScore = juser.lead_score;
        this.widgetId = juser.last_widget_id
        this.nbPosts = juser.total_published;
        this.lastSeen = juser.last_seen;
        this.followers = juser.xscore || juser.b2b_followers || 0;
        this.format = juser.format;
        this.interests = juser.interests && juser.interests.filter(interest => !!interest);
        if (juser.interests_details) {
            this.interestsV2 = [];
            juser.interests.filter(interest => !!interest).forEach((name: string, i: number) => {
                if (i >= juser.interests_details.length ) return;
                this.interestsV2.push({
                    name: name,
                    intensity: juser.interests_details[i].intensity,
                    tags: juser.interests_details[i].tags.split(' ').map((t: string) => t.indexOf('#') === 0 ? t.substr(1) : t)
                });
            });
        }

        // correct tw thumbnail
        if (this.kind === _attributionTypes.TWITTER) this.thumbnail = this.thumbnail.replace('_normal.', '_200x200.');


    }

    toInfluencer() {
        const I = new Influencer(this.juser, this.labels);

        if (I.attributionKind === _attributionTypes.TWITTER) {
            I.thumbnail = I.thumbnail.replace(/_200x200\./, '_400x400.')
        } else if (I.attributionKind === _attributionTypes.INSTAGRAM) {
            //I.thumbnail = I.thumbnail.replace(/s150x150/, 's640x640')
        }

        return I;
    }
}



export type JsonInfluencer = JsonProfile & {
    engagement_rate?: number; //0-100
    followings?: number
}

export class Influencer extends Profile {
    engagementRate: number;
    followings: number;

    campaignRank: number;
    engagements: number;

    communityGender: {name, count}[];
    communityInterests: {name, count}[];
    communityAge: {name, count}[];
    communityLanguage: {name, count}[];
    communityLivesIn: {name, count}[];

    constructor(private jinfluencer: JsonInfluencer, private allILabels?: Label[]) {
        super(jinfluencer, allILabels);

        this.engagementRate = jinfluencer.engagement_rate;
        this.followings = jinfluencer.followings;
    }
}