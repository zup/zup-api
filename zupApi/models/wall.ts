/*
export interface wallJsonPost {
    id: string;
    created: number;
    attributionKind: string;
    position: {geo_name?: string};
    text: string;
    thumbnail?: string;
    video?: string;
    user: {
        id: string,
        name: string,
        screenName: string,
        thumbnail: string
    }
}
*/

import { SimpleProfile } from './profile';
import { toLOV } from '../listOfValues';
import { tools, dates } from '../misc/utils';

const options = [
    {name: 'embedded', text: 'Embed this wall', value: false, type: 'boolean'},
    {name: 'replay', text: 'Use replay mode', value: false, type: 'boolean'},
    {name: 'replay-from', text: 'From: ', parent: 'replay', type: 'date-time', value: new Date(2017, 0, 1)},
    {name: 'replay-to', text: 'To: ', parent: 'replay', type: 'date-time'}
]

export enum wallPage {
    EMPTY = 'EMPTY',
    POSTWALL = 'POSTWALL',
    TOPUSERS = 'TOPUSERS',
    TOPHASHTAGS = 'TOPHASHTAGS',
    FAVPOSTS = 'FAVPOSTS',
    COVERPAGE = 'COVERPAGE',
    LIVESTATS = 'LIVESTATS',
    FULLSCREEN = 'FULLSCREEN'
}

export const wallConfOptions = [
    {name: 'sandBoxDelay', value: 0, text: 'Sandbox delay (delay between a post creation and its apparition in the wall ) : $1 minutes', type: 'int'},
    {name: 'entryDelay', value: 0, text: 'Entry Delay (before the entry of the next post, wait for) : $1 seconds', type: 'int'},
    //{name: 'pinnedPostDelay', value: 0, text: 'PinnedPost frequency : $1 seconds', type: 'int'},
    {name: 'replay', text: 'Use replay mode', value: false, type: 'boolean'},
    {name: 'replay-timeRange', text: 'Replay period', parent: 'replay', type: 'time-range'},
    {name: 'noRT', text: 'Hide shares from the wall', value: true, type: 'boolean'},
    {name: 'onlyMedia', text: 'Only show post with photo or video', value: false, type: 'boolean'},
    {name: 'embedded', text: 'Embed this wall', value: false, type: 'boolean'},
    {name: 'hideAttribution', text: 'Hide attribution icon', value: false, type: 'boolean'}
];

const pages = {

    POSTWALL: {
        value: 'mainWall',
        text: 'Wall of posts',
        description: 'Shows in real time all post done in the place',
        options: [
            //{name: 'sandBoxDelay', value: 0, text: 'Sandbox delay (delay between a post creation and its apparition in the wall ) : $1 minutes', type: 'int', defaultConf: true},
            //{name: 'entryDelay', value: 0, text: 'Entry Delay (before the entry of the next post, wait for) : $1 seconds', type: 'int', defaultConf: true},
            //{name: 'pinnedPostDelay', value: 0, text: 'PinnedPost frequency : $1 seconds', type: 'int', defaultConf: true},
            //{name: 'replay', text: 'Use replay mode', value: false, type: 'boolean', defaultConf: true},
            //{name: 'replay-interval', text: 'Replay the last $1 hours', parent: 'replay', type: 'int', value: 24},
            //{name: 'replay-timeRange', text: 'Replay period', parent: 'replay', type: 'time-range', defaultConf: true},
            {name: 'hideText', text: 'Hide text on posts with image or video', value: false, type: 'boolean'},
            //{name: 'onlyMedia', text: 'Only show post with photo or video', value: false, type: 'boolean', defaultConf: true},
            //{name: 'noRT', text: 'Hide retweets from the wall', value: true, type: 'boolean'},
            {name: 'sideBar', text: 'Display side bar', value: false, type: 'boolean'},
            {name: 'sideBar-option', text: 'Nothing', parent: 'sideBar', value: 'nothing', type: 'radio', default: true},
            {name: 'sideBar-option', text: 'Side bar stats', parent: 'sideBar', value: 'stats', type: 'radio'},
            //{name: 'sideBar-option', text: 'Side bar pinned posts', parent: 'sideBar', value: 'pinnedPosts', type: 'radio'},
            {name: 'bottomBar', text: 'Display bottom bar', value: false, type: 'boolean'},
            {name: 'colsNum', text: 'Number of columns', value: 4, choices: [1, 2, 3, 4, 6]},
            {name: 'avatarBubblesInterval', value: 0, text: 'Display Avatar bubbles every $1 seconds', type: 'int'},
            {name: 'avatarBubblesDuration', value: 0, text: 'Keep Avatar animation during $1 seconds', type: 'int'},
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512}
        ]
    },
    TOPUSERS: {
        value: 'topUsers',
        text: 'Top Users',
        description: 'Shows the top contributors in the last hour',
        options: [
            {name: 'timeInterval', value: 2, text: 'Display top users of the $1 last hours', type: 'int'},
            {name: 'excludedProfiles', text: 'Profiles to Exclude from top users (enter person\'s name or user name without the @)', type: 'freelist'},
            //{name: 'useBackgroundImage', text: 'Use background image', value: true, type: 'boolean'},
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512}
        ]
    },
    TOPHASHTAGS: {
        value: 'topHashtags',
        text: 'Top Hashtags',
        description: 'Top hashtags used',
        options: [
            {name: 'timeInterval', value: 12, text: 'Display top hashtags of the $1 last hours', type: 'int'},
            {name: 'nbTags', value: 40, text: 'Number of tags to display : ', type: 'int'},
            //{name: 'tagsColor', value: 'black', text: 'Color of the tags', type: 'input', size: 10},
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512}
        ]
    },
    /*
    AVATARWALL: {
        value: 'avatarWall',
        text: 'Avatar Wall',
        description: 'Shows the contributors',
        options: [
            {name: 'type', value: 'cube', choices: ['cube', 'sphere', 'bubbles']}
        ]
    },*/
    FAVPOSTS: {
        value: 'favoritePage',
        text: 'Highlights',
        description: 'Animates highlighted posts',
        options: [
           // {name: 'useBackgroundImage', text: 'Use background image', value: true, type: 'boolean'},
            {name: 'favInterval', value: 0, text: 'Display each post for $1 seconds. <br><b>if set to 0</b>, the time used ' +
            'will be calculated from the "page duration". <br>' +
            '<b>if NOT set to 0</b>, the "page duration" will be ignored.', type: 'int'},
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512}
        ]
    },
    COVERPAGE: {
        value: 'coverPage',
        text: 'Cover Page',
        description: 'Presentation page',
        options: [
            {name: 'image-url', text: 'Url of a background image', type: 'input', maxLength: 512},
        ]
    },
    LIVESTATS: {
        value: 'liveStats',
        text: 'Statistics',
        description: 'Shows the evolution of the number of posts',
        options: [
            {name: 'timeInterval', value: 12, text: 'Display post evolution of the $1 last hours', type: 'int'},
            {name: 'startCount', text: 'Start counting from', type: 'time-picker', value: dates.ZInow - 3600 * 24},
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512}
        ]
    },
    FULLSCREEN: {
        value: 'fullScreen',
        text: 'Full screen post',
        description: 'Display a specific post on stand alone',
        options: [
            {name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512},
            {name: 'postId', type: 'hidden'}
        ]
    }
    /*,
    PINNEDPOSTS: {
        value: 'pinnedPost',
        text: 'Pinned posts Page',
        decription: 'Shows Pinned posts'
    }*/

}

//export const wallPages = toLOV(pages);
export const wallPages = pages;
export const confOptions = options;

export type wallStyles = {
    postStyles: {
        post: {'background-color', color},
        postUserScreenName: {color},
        postUserName: {color},
        highlight: {color},
        postText: {color},
        postAttribution: {'background-color'}
    },
    pageStyles: {'background-color'}
}


export type wallConf = {
    id,
    frontPage?: string | boolean, //pageId
    refreshDt?: number,
    styles?: any,
    options?: any,// {optionKey: value}
    pages?: wallPageConf[]
}

export type wallPageConf = {
    id: wallPage,
    pageId?: string,
    duration?: number,
    styles?: {pageStyles?, postsStyles?},
    options?: any // page options ex: {timeIntervall: 12, startCount: xxx}
}

export type wallJsonPost = {
    id: string;
    isShared: boolean;
    created: number;
    attributionKind: string;
    attributionLink?: string;
    position: {geo_name?: string};
    title?: string;
    text: string;
    thumbnail?: string;
    video?: string;
    videoType?: string;
    user: SimpleProfile
}

export interface jsonWall {
    id?: string;
    name?: string;
    pinnedPosts?: wallJsonPost[];
    sponsoredPosts?: wallJsonPost[];
    favoritePosts?: string[];
    overlayPost?: {postId?: string, endAt?: number};
    delays?: {
        mainPosts: number,
        pinnedPosts: number,
        sponsoredPosts: number,
        sandbox: number
    };
    title?: string;
    description?: string;
    thumbnail?: string;
    poweredBy?: string;
    styles?: any;
    moderated?: {
        posts?: string[],
        users?: string[]
    };
    version: number;
    widgetIds?: string[];
    //pagesConfig?: any; // pageName -> {options: any, duration: number, show: boolean}
    configurations?: any[] // array of "pageConfig"
    wallConfs?: wallConf[],
    defaultPageConf?: wallPageConf[]
    defaultOptions?: any// {optionKey: value}[]
}


export interface zWall {
    created: number;
    deleted: number;
    id: string;
    metas: string;
    name: string;
    partner_id?: string;
    widgets: string[];
}

export class Wall {
    private jsonWall: jsonWall;

    public needModeration: boolean;

    constructor(private wall?: zWall) {
        if (wall) {
            this.jsonWall = wall.metas ? JSON.parse(wall.metas) : {};
            this.jsonWall.id = wall.id;
            this.jsonWall.name = wall.name;
            this.jsonWall.widgetIds = wall.widgets;
        } else {
            this.jsonWall = { widgetIds: [], version: 0};
        }

        this.initPagesConfig();
    }

    get configurations(): wallConf[] {
        /*if (!this.jsonWall.configurations) this.jsonWall.configurations = [];
        return this.jsonWall.configurations;*/
        //if (this.jsonWall.configurations) throw "TODO" //return this.convertConf(this.jsonWall.configurations);
        if (!this.jsonWall.wallConfs) this.jsonWall.wallConfs = [];
        return this.jsonWall.wallConfs;
    };
    set configurations(pc: wallConf[]) {
        //this.jsonWall.configurations = pc;
        this.jsonWall.wallConfs = pc;
    };

    get delays(): any {
        if (!this.jsonWall.delays) {
            this.jsonWall.delays = {mainPosts: 30, pinnedPosts: 30, sponsoredPosts: 30, sandbox: 0};
        }
        return this.jsonWall.delays;
    };
    set delays( delays: any) {
        this.jsonWall.delays = delays;
    };
    get description(): string { return this.jsonWall.description; };
    set description( description: string) {
        this.jsonWall.description = description;
    };
    get favoritePosts(): string[] {
        if (!this.jsonWall.favoritePosts) {
            this.jsonWall.favoritePosts = [];
        }
        return this.jsonWall.favoritePosts;
    };
    set favoritePosts(fp: string[]) {
        this.jsonWall.favoritePosts = fp;
    }
    get id(): string { return this.jsonWall.id; };
    get moderated(): any {
        if (!this.jsonWall.moderated) {
            this.jsonWall.moderated = {};
        }
        return this.jsonWall.moderated;
    };
    set moderated( moderated: any) {
        this.jsonWall.moderated = moderated;
    };
    get name(): string { return this.jsonWall.name; };
    set name( name: string) {
        this.jsonWall.name = name;
    };
    get overlayPost(): {postId?: string, endAt?: number} {
        if (!this.jsonWall.moderated) {
            this.jsonWall.overlayPost = {};
        }
        return this.jsonWall.overlayPost;
    };
    set overlayPost( postId: {postId?: string, endAt?: number}) {
        this.jsonWall.overlayPost = postId;
    };
    get pagesConfig(): any {
        throw "OBSOLET"
      //  return this.jsonWall.pagesConfig;
    };
    set pagesConfig(pc: any) {
        throw "OBSOLET"
        //this.jsonWall.pagesConfig = pc;
    };

    get wallConfs(): wallConf[] {
        if (!this.jsonWall.wallConfs) this.jsonWall.wallConfs = [];
        return this.jsonWall.wallConfs
    }

    set wallConfs( conf: wallConf[]) {
        this.jsonWall.wallConfs = conf;
    }

    get defaultPageConf(): wallPageConf[] {
        if (!this.jsonWall.defaultPageConf) this.jsonWall.defaultPageConf = [];
        return this.jsonWall.defaultPageConf ;
    }

    set defaultPageConf(conf: wallPageConf[]) {
        this.jsonWall.defaultPageConf = conf;
    }

    get defaultOptions(): any {
        if (!this.jsonWall.defaultOptions) this.jsonWall.defaultOptions = {};
        return this.jsonWall.defaultOptions ;
    }

    set defaultOptions(option: any) {
        this.jsonWall.defaultOptions = option;
    }

    get poweredBy(): string {
        return this.jsonWall.poweredBy;
    };
    set poweredBy(img: string) {
        this.jsonWall.poweredBy = img;
    };
    get pinnedPosts(): wallJsonPost[] {
        if (!this.jsonWall.pinnedPosts) {
            this.jsonWall.pinnedPosts = [];
        }
        return this.jsonWall.pinnedPosts;
    };
    set pinnedPosts(pp: wallJsonPost[]) {
        this.jsonWall.pinnedPosts = pp.filter(p => !!p);
    }
    get sponsoredPosts(): wallJsonPost[] {
        if (!this.jsonWall.sponsoredPosts) {
            this.jsonWall.sponsoredPosts = [];
        }
        return this.jsonWall.sponsoredPosts;
    };
    set sponsoredPosts( sp: wallJsonPost[]) {
        this.jsonWall.sponsoredPosts = sp.filter(p => !!p);;
    };
    get styles(): any {
        if (!this.jsonWall.styles) {
            this.jsonWall.styles = {};
        }
        return this.jsonWall.styles;
    };
    set styles( styles: any) {
        this.jsonWall.styles = styles;
    };
    get thumbnail(): string {
        return this.jsonWall.thumbnail;
    };
    set thumbnail(img: string) {
        this.jsonWall.thumbnail = img;
    };
    get title(): string { return this.jsonWall.title; };
    set title( title: string) {
        this.jsonWall.title = title;
    };
    /*get version(): string {
        //initialising setters :
        let a: any = this.pinnedPosts;
        a = this.sponsoredPosts;
        a = this.favoritePosts;
        a = this.moderated;
        a = this.delays;
        a = this.styles;
        a = this.overlayPost;

        return JSON.stringify(this.jsonWall);
    };*/
    get version(): number { return this.jsonWall.version; };
    set version(v: number) { this.jsonWall.version = v; };

    get widgetIds(): string[] {
        if (!this.jsonWall.widgetIds) this.jsonWall.widgetIds = [];
        return this.jsonWall.widgetIds;
    };
    set widgetIds(ws: string[]) {
        this.jsonWall.widgetIds = ws;
    };

    addPinnedPost(p: wallJsonPost): boolean {
        if (!this.pinnedPosts.find(pp => pp.id === p.id)) {
            this.pinnedPosts.push(p);
            return true;
        } else {
            return false;
        }
    }
    isBlocked(post: wallJsonPost): boolean {
        return (this.moderated.posts || []).findIndex(p => p === post.id) >= 0 ||
            (this.moderated.users || []).findIndex(u => u === post.user.id) >= 0;
    }

    public isOkPost(p: wallJsonPost, conf: wallConf): boolean {
        if (!p) return false;
        let ok: boolean = true;

        if (
            tools.deepGet(conf, 'options','noRT') !== false // default = true!
            && p.isShared
        ) return false;


        if (
            conf &&
            tools.deepGet(conf, 'options', 'onlyMedia')
            && !p.thumbnail
        ) return false;

        if (
            p.attributionKind === 'tw'
            && !p.thumbnail
            && !p.text
        ) return false;

        if (this.moderated.posts && this.moderated.posts.length > 0) {
            ok = ok && this.moderated.posts.indexOf(p.id) < 0;
        }
        if (this.moderated.users && this.moderated.users.length > 0) {
            ok = ok && this.moderated.users.indexOf(p.user.id) < 0;
        }
        return ok;

    }
    /*
    moderatePost(id): void {
        if (!this.moderated.posts || this.moderated.posts.indexOf(id) < 0) {
            this.moderated.posts = (this.moderated.posts || []).push(id);
        }
    }
    moderateUser(id): void {
        if (!this.moderated.users || this.moderated.users.indexOf(id) < 0) {
            this.moderated.users = (this.moderated.users || []).push(id);
        }
    }
*/
    updatePinnedPost(p: wallJsonPost) {
        const i = this.pinnedPosts.findIndex(pp => pp.id === p.id);
        if (i < 0) {
            return false;
        } else {
            this.pinnedPosts.splice(i, 1, p);
            return true;
        }
    }

    toString(): string {
        return JSON.stringify(this.jsonWall);
    }
    toJson(): any {
        return {
            wall_id: this.id,
            name: this.name,
            metas: JSON.stringify(this.jsonWall)
        };
    }

    clone(): Wall {

        const jw = <any>this.toJson();
        jw.created = 0;
        jw.deleted = 0;
        jw.id = jw.wall_id;
        jw.widgets = this.jsonWall.widgetIds;

        const clonedWall = new Wall(jw);
        clonedWall.needModeration = this.needModeration;

        return clonedWall;
    }

    private initPagesConfig() {

        if (this.wallConfs.length > 0) return; // already new format
        if (!this.jsonWall.configurations || this.jsonWall.configurations.length === 0) {

        } else {
            const wallPagesEntries = tools.objectEntries(wallPages);
            this.jsonWall.configurations.forEach((c, i) => {//pageName -> {options: any, duration: number, show: boolean}
                const pages: wallPageConf[] = [];

                for (let p in c) {
                    const oldConf = c[p];
                    const pageEntry = wallPagesEntries.find(([k,v]) => v.value === p);
                    if (!pageEntry) continue;
                    const pageConf: wallPageConf = {id: <wallPage>pageEntry[0]};
                    pageConf.duration = oldConf.duration;
                    pageConf.options = oldConf.options;
                    pages.push(pageConf);
                }

                //fix bug for mutliple conf with same name
                if (this.wallConfs.find(co => co.id === c.name)) c.name += Math.floor(Math.random()*1000)

                this.wallConfs.push({
                    id: c.name,
                    pages: pages,
                    refreshDt: c.refreshDt,
                    frontPage: c.frontPage
                });
            })
            delete this.jsonWall.configurations;
        }

        if (this.defaultPageConf.length === 0) {
            const defaultConfig: wallPageConf[] = []

            Object.keys(wallPages).forEach(k => defaultConfig.push({id: <wallPage>k, duration: 0}));
            this.defaultPageConf = defaultConfig;
        }



        /*

        const defaultConfig = {}
        wallPages.keys.forEach(k => defaultConfig[k] = {duration: 0});
        if (!this.jsonWall.pagesConfig) this.jsonWall.pagesConfig = {};
        this.jsonWall.pagesConfig = tools.deepAssign({}, defaultConfig, this.jsonWall.pagesConfig);

        // new config format
        if (!this.jsonWall.configurations || this.jsonWall.configurations.length === 0) {
            const defaultConf = tools.clone(this.jsonWall.pagesConfig);
            defaultConf.name = 'default'
            this.jsonWall.configurations = [defaultConf];
        } else {
            this.jsonWall.configurations.forEach((c, i) => {
                this.jsonWall.configurations[i] = tools.deepAssign({}, defaultConfig, c);
            })
        }
        */

    }
}

