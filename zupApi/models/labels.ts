import { FilterQuery} from './';


export interface inLabel {
    id?: string;
    auto: boolean;
    auto_rules?: any;
    count?: number;
    exclude_profiles?: string[];
    include_profiles?: string[];
    name: string;
    partner_id?: string;
    is_auto_publish_campaign?: boolean;
}

export class Label {

    id: string;
    name: string;
    rules = new FilterQuery();
    auto: boolean;
    count: number;
    isCampaign: boolean;

    constructor(label?: inLabel) {
        if (label) {
            this.id = label.id;
            this.name = label.name;
            this.rules = new FilterQuery(label.auto_rules);
            this.auto = label.auto;
            this.count = label.count;
            this.isCampaign = label.is_auto_publish_campaign;
        }
    }

    forExport() {
        return {
            auto: this.auto,
            auto_rules: this.rules,
            id: this.id,
            name: this.name,
            is_auto_publish_campaign: this.isCampaign

        }
    }

    clone(): Label {
        const l = new Label();
        l.id = this.id;
        l.name = this.name;
        l.rules = new FilterQuery(this.rules.toJSON());
        l.auto = this.auto;
        l.count = this.count;
        l.isCampaign = this.isCampaign;
        return l;
    }
}

//export const tmpLabel = 1; // needed for correct export and compilation (buf typescript ?)
