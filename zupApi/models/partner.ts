import { Widget, View, Profile } from './';

import { tools } from '../misc/utils';

export type JsonPartner = {
  created: number,
  description: string,
  email: string,
  metas: string,
  name: string,
  partner_id: string,
  rights: string[],
  updated: number
}

export type Member = {
  created: number,
  email: string,
  id: string,
  locale: string,
  name: string,
  updated: string
}
/**
 * A partner is a client, he can have several Widgets and Members
 */
export class Partner {
  advancedQueries: {name: string, query: any, type?: string}[] = [];
  logo: string;
  name: string;
  email: string;
  description: string;
  widgets: Widget[] = [];
  members: Member[];
  socialAccounts: Profile[] = [];
  fbProfilesLoaded: boolean;
  rights: string[];
  defaultLimits: any =  {
    userDailyThreshold: undefined,
    influencerThreshold: undefined,
    customerLeadThreshold: undefined,
    loyaltyNbThreshold: undefined,
    loyaltyDaysThreshold: undefined
  };
  defaultWordsAlert: string[] = [];
  recordedMessages: {attribution: string, message: string, name?: string}[] = [];
  roUsers: string[] = [];
  woUsers: string[] = [];
  widgetAndViews: Map<string, Widget> = new Map<string, Widget>();
  private _id: string;
  private _metas: any = {};

  constructor(partner?: JsonPartner, widgets?: Widget[], members?: Member[]) {

    this._id = partner.partner_id;
    this.name = partner.name || 'Noname';
    this._metas = !!partner.metas ? JSON.parse(partner.metas) : {};
    this.email = partner.email;
    this.description = partner.description;
    this.rights = partner.rights;

    // TODO: parse meta while no other info:
    if (this._metas.hasOwnProperty('logo')) this.logo = this._metas.logo;
    if (this._metas.hasOwnProperty('defaultLimits')) Object.assign(this.defaultLimits, this._metas.defaultLimits);
    if (this._metas.hasOwnProperty('defaultWordsAlert')) this.defaultWordsAlert = this._metas.defaultWordsAlert;
    if (this._metas.hasOwnProperty('roUsers')) this.roUsers = this._metas.roUsers;
    if (this._metas.hasOwnProperty('woUsers')) this.woUsers = this._metas.woUsers;
    if (this._metas.hasOwnProperty('advancedQueries')) this.advancedQueries = this._metas.advancedQueries;
    if (this._metas.hasOwnProperty('recordedMessages')) this.recordedMessages = this._metas.recordedMessages;


    this.widgets = widgets;
    this.members = members;

    widgets.forEach((w: Widget) => {
      w.widgetViews.forEach((v: View) => {
        this.widgetAndViews.set(v.id, v);
      });
      this.widgetAndViews.set(w.id, w);
    });
  }

  get id(): string {
    return this._id;
  }

  get metas(): string {
    const metas =  {
      logo: this.logo,
      defaultLimits: this.defaultLimits,
      defaultWordsAlert: this.defaultWordsAlert,
      roUsers: this.roUsers,
      woUsers: this.woUsers,
      advancedQueries: this.advancedQueries,
      recordedMessages: this.recordedMessages,

      inSuite: this._metas.inSuite,
      
      tmpInfo: this._metas.tmpInfo
    };
    return JSON.stringify(metas);
  }

  removeMember(id: string) {
    const i = this.members.findIndex(u => { return u.id === id; });

    if (i >= 0) {
      this.members.splice(i, 1);
    };
  }
  setInsuiteRights(hasRights: boolean) {
    this._metas.inSuite = hasRights;
  }

  get hasInsuite(): boolean {
    return this._metas.inSuite !== false;
  }
/*
  get sources(): string[] {
    if (!this._metas.sources) this._metas.sources = Object.keys(attributionSources);
    return this._metas.sources;
  }

  set sources(sources: string[])  {
    this._metas.sources = sources;
  }*/

  getTmpInfo(key) {
    return tools.deepGet(this._metas, 'tmpInfo', key);
  }
  setTmpInfo(key, value) {
    this._metas.tmpInfo = this._metas.tmpInfo || {};
    this._metas.tmpInfo[key] = value;
  }
}
