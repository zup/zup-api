export const lexicalCategories = {
    ALERT: 'Alert',
    CLS: 'CLS',
    SOV: 'SOV',
    INF: 'INF',
    STANDARD: 'Standard'

}
export const lexicalServices = {
    STANDARD: 'Standard',
    WORD_ALERT: 'B2B_word_alert_lexical_name',
    SOV: 'B2B_SOV',
    SOV_MYBRAND: 'B2B_SOV_MYBRAND',
    SOV_COMPETITOR: 'B2B_SOV_COMPETITOR',
    CLS_INTEREST_HI: 'B2B_customer_lead_interest_high',
    CLS_INTEREST_LO: 'B2B_customer_lead_interest_low',
    CLS_BRAND: 'B2B_customer_lead_brand_screennames',
    CLS_COMPETITOR: 'B2B_customer_lead_concurrent_screennames',
    CLS_LINKS: 'B2B_customer_lead_link',
    INF_CAMPAIGN: 'B2B_SOV_INF_campaign'
}

export type NewLexical = {
    displayName: string;
    mult?: number;
    partnerId?: string;
    service: string;
    values: string[];
    widgetId?: string;
}

export type Lexical = NewLexical & {id: string, category: string};

export const toLexical = function(l: any): Lexical {
    return <Lexical>{
        category: l.category,
        displayName: l.display_name,
        id: l.id,
        mult: l.mult,
        partnerId: l.partner_id,
        service: l.service,
        values: l.values,
        widgetId: l.widget_id
    };
}

export const fromLexical = function(l: NewLexical | Lexical): any {
    return {
        id: (<Lexical>l).id,
        display_name: l.displayName,
        mult: l.mult,
        partner_id: l.partnerId,
        service: l.service,
        values: l.values,
        widget_id: l.widgetId
    };

}
