import { Subject } from  'rxjs';

import { dates, ids, tools, bigbang } from '../misc/utils';

import { toLOV, attributionSources } from '../listOfValues';
import { View, Profile, PostFilterRules, ProfileFilterRules, PublishAction } from './';
import {FilterQuery} from "./filters";



export const widgetTypes: any = {
    PLACE:     { value: 1, menuIndex: 1, label: 'Places', label2: 'Place',    path: 'places',    icon: 'icon-map', name: 'place' },
    LISTENING: { value: 2, menuIndex: 2, label: 'Listening', path: 'listening', icon: 'icon-globe', name: 'listening' },
    VIEW: { value: 3, label: 'View', path: 'view' },
    PROFILES: { value: 4, label: 'Profile List', path: 'profile-list' }
};
toLOV(widgetTypes, {
    findFromPath: function(path: string){
        const myT = this.values.find(t => t.path === path);
        return myT && myT.value;
    }
});


export const widgetCategories: any = {
    MY_PLACE:   {
        value: 1,
        type: widgetTypes.PLACE,
        label: 'My Places',
        icon: 'icon-location-pin',
        description: 'Belongs to my places'
    },
    WATCH:      {
        value: 2,
        type: widgetTypes.PLACE,
        label: 'Watch',
        icon: 'icon-chart',
        description: 'Place to watch'
    },
    COMPETITOR: {
        value: 3,
        type: widgetTypes.PLACE,
        label: 'Competitors',
        icon: 'icon-magnifier',
        description: 'Competitor'
    },
    LISTENING_ME: {
        value: 4,
        type: widgetTypes.LISTENING,
        label: 'My Listenings',
        icon: 'icon-globe',
        description: 'Is one of my listenings'
    },
    LISTENING_WATCH: {
        value: 5,
        type: widgetTypes.LISTENING,
        label: 'Watch',
        icon: 'icon-chart',
        description: 'A keyword to watch'
    },
    LISTENING_COMPETITOR: {
        value: 6,
        type: widgetTypes.LISTENING,
        label: 'Competitors',
        icon: 'icon-magnifier',
        description: 'Belongs to my competitors'
    },
    PROFILE_LIST: {
        value:7,
        type: widgetTypes.PROFILES,
        label: 'List of influencers',
        description: 'Influencers I want to monitor'
    },
    PROFILE_MY_BRAND: {
        value:8,
        type: widgetTypes.PROFILES,
        label: 'My brand',
        description: 'Social accounts of my brand'
    },
    PROFILE_COMPETITOR: {
        value:9,
        type: widgetTypes.PROFILES,
        label: 'My competitors',
        description: 'Social accounts of my competitors'
    },
    PROFILE_CAMPAIGN: {
        value:10,
        type: widgetTypes.PROFILES,
        label: 'Influencer Campaign',
        description: 'Influencers for one of my campaigns'
    }
};
toLOV(widgetCategories, {
    filter: function(type: number){
        return this.values.filter(c => c.type === type);
    }
});

export type WidgetView = {
    ends_at?: number,
    starts_at?: number,
    exclude_sources?: string[],
    zone?: {
        n: number,
        s: number,
        e: number,
        w: number
    },
    tags?: string[],
    or_tags?: string[],
    queries?: {
        and_terms?: string[],
        or_terms?: string[]
    }[],
    exclude_terms?: string[],
    exclude_shares?: boolean,
    exclude_reply?: boolean,
    lang?: string,
    profiles_label_id?: string
}

export class Widget {
    wordsAlert: string[]; // Actually should not be here it comes form lexical
    widgetViews: View[] = [];


    protected _metas: any = {};
    protected _jsonWidget: any;

    protected _view: WidgetView = {};
    protected _mandatoryKeywords: string[] = [];
    protected _associatedKeywords: string[] = [];
    protected _excludeKeywords: string[] = [];
    protected originalQueries;


    private _useDefaultThresholds: boolean = true;

    public options: any = {};

    constructor(widget?: any, type?: number) {
        if (widget) {
            this._jsonWidget = widget;
            if (widget) {
                this.originalQueries = tools.clone(widget.view.queries || []);
            }


            this._view = widget.view;
            this._view.ends_at = this._view.ends_at || 0;
            this._view.starts_at = this._view.starts_at || 0;
            // transform new format to old format:
            // this.view.queries = widget.view.queries;

            if (!widget.name || widget.name.length === 0) {
                throw 'INCORRECT_FORMAT()';
            }

            if (widget.metas) {
                this._metas = JSON.parse(widget.metas);

                // tmp dev parentWidgetId
                this._jsonWidget.widget_parent = this._jsonWidget.widget_parent || this._metas.parent_widget_id;

                // tmp bug du type laisse a 1
                if (this._jsonWidget.widget_parent) {
                    this._jsonWidget.type = 3;
                }

            } else {
                throw 'INCORRECT_FORMAT()';
            }

            this._useDefaultThresholds = this._metas.useDefaultThresholds;


            // convert queries to mandatory / associated keywords :
            if (this._view.queries && this._view.queries.length > 0) {
                this.view.queries.forEach(q => {
                    if (!q.and_terms) throw `BAD_VIEW_FORMAT('and_terms',${this.id})`;
                    this._mandatoryKeywords = this._mandatoryKeywords.concat(q.and_terms);

                    if (
                        q.or_terms && q.or_terms.length > 0 &&
                        this._associatedKeywords.length > 0 &&
                        q.or_terms.toString() !== this._associatedKeywords.toString()
                    ) {
                        throw `BAD_VIEW_FORMAT('or_terms',${this.id})`;
                    } else if ( q.or_terms && q.or_terms.length > 0) {
                        this._associatedKeywords = q.or_terms;
                    }
                });
                this._mandatoryKeywords = tools.unduplicate(this._mandatoryKeywords);
                this._associatedKeywords = tools.unduplicate(this._associatedKeywords);
            }

            this._excludeKeywords = this._view.exclude_terms || [];

            this.buildQueries(); // for consistance inempty exclude_terms
        } else {
            this._metas = {useDefaultThresholds: true};
            this._jsonWidget = {
                metas: JSON.stringify(this._metas),
                view: this._view
            };
        }

        if (type) {
            this.type = type;
            if (this.type === widgetTypes.PROFILES) {
                this.options = {
                    "no_bootstrap":true,
                    "no_auto_search_places":true,
                    "without_mission":true,
                }
            }
        }
    }

    get associatedLexical(): string {
        return this._metas.associatedLexical;
    }

    set associatedLexical(lid: string) {
        this._metas.associatedLexical = lid;
    }

    get category(): number | string {
        if ( this._jsonWidget.category && typeof this._jsonWidget.category !== 'number') {
            console.log("CATEGORY=", this._jsonWidget.category);
            throw 'INVALID CATEGORY FORMAT';
        }
        return <number>this._jsonWidget.category;
    }

    set category(category: number | string){
        if (typeof category === 'string') category = parseInt(<string>category, 10);
        this._jsonWidget.category = category;
    }

    get customerLeadThreshold(): number {
        return this._jsonWidget.customer_lead_threshold || undefined;
    }

    set customerLeadThreshold(t: number) {
        this._jsonWidget.customer_lead_threshold = t;
    }

    get dateRangeDt(): {from?: Date, to?: Date} {
        if (!this._view) return {};
        return {
            from: this._view.starts_at && dates.toDateFromZI(this._view.starts_at),
            to: this._view.ends_at && dates.toDateFromZI(this._view.ends_at),
        };
    }

    set dateRangeDt(r: {from?: Date, to?: Date}) {
        this.startsAtDt = r.from;
        this.endsAtDt = r.to;
    }


    get endsAt(): number {
        return this._view.ends_at || 0;
    }

    set endsAt(e: number) {
        this._view.ends_at = e || 0;
    }

    get endsAtDt(): Date {
        if (this._view.ends_at) return dates.toDateFromZI(this._view.ends_at);
    }

    set endsAtDt(e: Date) {
        this._view.ends_at = dates.toZIFromDate(e);
    }

    get excludedPlaces(): string[] {
        if (!this._jsonWidget.excludedPlaces) { this._jsonWidget.excludedPlaces = []; };
        return this._jsonWidget.excludedPlaces;
    }

    set excludedPlaces(profiles: string[]) {
        this._jsonWidget.excludedPlaces = profiles;
    }

    get excludedProfiles(): string[] {
        if (!this._jsonWidget.excludedProfiles) { this._jsonWidget.excludedProfiles = []; };
        return this._jsonWidget.excludedProfiles;
    }

    set excludedProfiles(profiles: string[]) {
        this._jsonWidget.excludedProfiles = profiles;
    }

    get id() {
        return this._jsonWidget.id;
    }

    set id(id: string){
        if (this._jsonWidget.id) throw 'ID_ALREADY_EXISTS()';
        this._jsonWidget.id = id;
    }

    get influencerThreshold(): number {
        return this._jsonWidget.influencer_threshold || undefined;
    }

    set influencerThreshold(t: number) {
        this._jsonWidget.influencer_threshold = t;
    }

    get isProfileListWidget(): boolean {
        return !!this._view.profiles_label_id;
    }

    get brandListeningId(): string {
        if (!this.isProfileListWidget) return;

        return this._metas.brandListeningId;
    }

    set brandListeningId(id: string) {
        this._metas.brandListeningId = id;
    }

    get brandLexicalId(): string {
        if (!this.isProfileListWidget) return;

        return this._metas.brandLexicalId;
    }

    set brandLexicalId(id: string) {
        this._metas.brandLexicalId = id;
    }

    get excludedSources(): string[] {
        return this._view.exclude_sources
    }

    set excludedSources(s: string[]) {
        this._view.exclude_sources = s;
    }

    /**
     * @returns {any} a clone of the _jsonWidget (not the original)
     */
    get jsonObject(): any {
        return JSON.parse(JSON.stringify(this._jsonWidget));
    }

    get labelId(): string {
        return this._view.profiles_label_id;
    }

    set labelId(id: string) {
        this._view.profiles_label_id = id;
    }

    get lang(): string {
        return this._view && this._view.lang
    }

    set lang(l: string) {
        this._view.lang = l || '';
    }

    get loyaltyDaysThreshold(): number {
        if (this._jsonWidget.loyalty_threshold) return this._jsonWidget.loyalty_threshold[1];
    }

    set loyaltyDaysThreshold(t: number) {
        if (this._jsonWidget.loyalty_threshold) {
            this._jsonWidget.loyalty_threshold[1] = t;
            if (this._jsonWidget.loyalty_threshold[0] === this._jsonWidget.loyalty_threshold[1] &&
                this._jsonWidget.loyalty_threshold[0] === undefined) {
                this._jsonWidget.loyalty_threshold = undefined;
            }
        } else {
            this._jsonWidget.loyalty_threshold = [undefined, t];
        }
    }

    get loyaltyNbThreshold(): number {
        if (this._jsonWidget.loyalty_threshold) return this._jsonWidget.loyalty_threshold[0];
    }

    set loyaltyNbThreshold(t: number) {
        if (this._jsonWidget.loyalty_threshold) {
            this._jsonWidget.loyalty_threshold[0] = t;
            if (this._jsonWidget.loyalty_threshold[0] === this._jsonWidget.loyalty_threshold[1] &&
                this._jsonWidget.loyalty_threshold[0] === undefined) {
                this._jsonWidget.loyalty_threshold = undefined;
            }
        } else {
            this._jsonWidget.loyalty_threshold = [t, undefined];
        }
    }

    get mainWidgetId(): string {
        return this.parentWidgetId || this.id;
    }

    get metas(){
        return JSON.stringify(this._metas);
    }

    get minusTags() {
        return this._excludeKeywords;
    }

    set minusTags(ts: string[]) {
        this._excludeKeywords = ts || [];
        this.buildQueries();
    }

    get name(): string {
        return this._jsonWidget.name;
    }

    set name(n: string) {
        this._jsonWidget.name = n;
    }

    set noMission(b: boolean) {
        this._jsonWidget.without_geo_missions = b;
    }

    get noMission(): boolean {
        return this._jsonWidget.without_geo_missions;
    }

    get noReplies(): boolean {
        return !!(this._view && this._view.exclude_reply)
    }

    set noReplies(s: boolean) {
        this._view.exclude_reply = s;
    }

    get noShares(): boolean {
        return !!(this._view && this._view.exclude_shares)
    }

    set noShares(s: boolean) {
        this._view.exclude_shares = s;
    }

    get orTags() {
        return this._associatedKeywords;
    }
    set orTags(ts: string[]) {
        this._associatedKeywords = ts || [];
        this.buildQueries();
    }

    get parentWidgetId(): string {
        return this._jsonWidget.widget_parent;
    }
    set parentWidgetId( id: string) {
        this._jsonWidget.widget_parent = id;
    }

    get publishActions(): PublishAction[] {
        return this._jsonWidget.publishActions || [];
    }

    set publishActions(publishActions: PublishAction[]) {
        this._jsonWidget.publishActions = publishActions;
    }

    get queryTags(): {tags?: string[], orTags?: string[], minusTags?: string[]} {
        return {tags: this.tags, orTags: this.orTags, minusTags: this.minusTags};
    }
    set queryTags( queryTags: {tags?: string[], orTags?: string[], minusTags?: string[]}) {
        this._mandatoryKeywords = queryTags.tags || [];
        this._associatedKeywords = queryTags.orTags || [];
        this._excludeKeywords = queryTags.minusTags || [];
        this.buildQueries();
    }

    get startsAt(): number {
        return this._view.starts_at || 0;
    }

    set startsAt(e: number) {
        this._view.starts_at = e;
    }

    get startsAtDt(): Date {
        if (this._view.starts_at) return dates.toDateFromZI(this._view.starts_at);
    }

    set startsAtDt(e: Date) {
        this._view.starts_at = dates.toZIFromDate(e);
    }

    get sources(): string[] {
        return tools.arrayDelta(tools.objectValues(attributionSources), this._view.exclude_sources || [])[0];
    }

    set sources(sources: string[]) {

        this._view.exclude_sources = tools.arrayDelta(tools.objectValues(attributionSources), sources)[0];

    }

    get tags() {
        return this._mandatoryKeywords;
    }
    set tags(ts: string[]) {
        this._mandatoryKeywords = ts || [];
        this.buildQueries();
    }

    get type(): number {
        return this._jsonWidget.type;
    }

    set type(t: number) {
        this._jsonWidget.type = t;
    }

    get useDefaultThresholds(): boolean {
        return !!this._useDefaultThresholds;
    }

    set useDefaultThresholds(useDefault: boolean) {
        this._useDefaultThresholds = useDefault;
        this._metas.useDefaultThresholds = this._useDefaultThresholds;
        this._jsonWidget.metas = JSON.stringify(this._metas);
    }

    get userDailyThreshold(): number {
        return this._jsonWidget.user_daily_threshold || undefined;
    }

    set userDailyThreshold(t: number) {
        this._jsonWidget.user_daily_threshold = t;
    }

    get view(){
        return JSON.parse(JSON.stringify(this._view));
    }

    get zone() {
        if (!this._view) return;
        return this._view.zone;
    }
    set zone(z: any) {
        // TODO test format
        if (tools.objectsEquals(z, {"n":0, "e":0, "s":0, "w":0})) z = undefined;
        if (!this._view) this._view = {};
        this._view.zone = z;

        if (!this._jsonWidget.view) this._jsonWidget.view = {};
        this._jsonWidget.view.zone = z;
    }

    get queries(): {and_terms?: string[], or_terms?: string[]}[] {
        return this._view.queries;
    }

    get search() {
        return {
            tags: this.tags,
            or_tags: this.orTags
        };
    }


    get safeId() {
        return ids.toSafe(this._jsonWidget.id);
        //return encodeURIComponent(this._id).replace('(', '%28').replace(')', '%29');
    }

    clone(anonymous?: boolean): Widget {
        const myJsonWidget = JSON.parse(JSON.stringify(this._jsonWidget));
        if (anonymous) delete myJsonWidget.id;
        const w = new Widget(myJsonWidget);
        w.widgetViews = this.widgetViews;
        return w;
    }

    getView(viewId: string): View {
        return this.widgetViews.find((v: View) => v.id === viewId);
    }

    delView(viewId: string) {
        const i =  this.widgetViews.findIndex((v: View) => v.id === viewId);
        if (i >= 0) {
            this.widgetViews.splice(i, 1);
        }
    }

    setView(view: View) {
        const i =  this.widgetViews.findIndex((v: View) => v.id === view.id);
        if (i >= 0) {
            this.widgetViews[i] = view;
        } else {
            this.widgetViews.push(view);
        }
    }

    toString() {
        return JSON.stringify(this._jsonWidget);
    }

    protected buildQueries() {
        if (!this._view) this._view = {};
        // make queries from mandatory and associated kwds :
        const queries = [];
        tools.unduplicate(this._mandatoryKeywords).forEach(k => {
            const q: any = {and_terms: [k]};
            if (this._associatedKeywords.length > 0) {
                q.or_terms = tools.unduplicate(this._associatedKeywords);
            };

            queries.push(q);
        });

        if (!this._view) this._view = {};
        this._view.queries = queries;
        this._view.exclude_terms = tools.unduplicate(this._excludeKeywords);

        if (!this._jsonWidget.view) this._jsonWidget.view = {};
        this._jsonWidget.view.queries = queries
        this._jsonWidget.view.exclude_terms = this._excludeKeywords;
    }

}



export class WidgetContext {
    catId: number = undefined;
    _after: number;
    _before: number;
    type: number;
    change: Subject<boolean> = new Subject<boolean>();
    public filterRulesOptions: {dates: boolean, widgets: boolean};
    public beforeChange: (any?) => Promise<boolean>;


    private _filterRules: PostFilterRules | ProfileFilterRules;
    private _persistantsRules: PostFilterRules | ProfileFilterRules | FilterQuery = < PostFilterRules | ProfileFilterRules | FilterQuery>{};

    private _widget: Widget;

    private _widgetId: string;

    constructor(
        widget?: Widget,
        private options: {after?: number, before?: number, category?: number, type?: number} = {},
        private filter?:  PostFilterRules | ProfileFilterRules
    ) {
        if (widget && widget.id) this._widget = widget;
        // bug of deleted widgets still in notifications (had to return {name:'uncknown'} )

        this._filterRules = filter || {};

        // default date range = current day
        const today = dates.today;
        if (options.after === 0) options.after = 1;
        this._after = options.after || this._filterRules.after || dates.toZIFromDate(today.from);
        this._before = options.before || this._filterRules.before || dates.toZIFromDate(today.to);


        this.catId = options.category || tools.deepGet(this, '_filterRules','widget_category');
        this.type = options.type || tools.deepGet(this, '_filterRules','widget_type');

        /*
         if (this._filterRules) {
         if (!this._widget) {
         this._widgetId = this._filterRules.widget_id;
         this.catId = this._filterRules.widget_category;
         this.type = this._filterRules.widget_type;
         }
         }*/
    }


    private get _iframe(): number {
        return;
        /*const delta = (this.before - this.after);
         if (delta <= 24 * 3600) {
         return; // default : 1h
         } else if (delta <= 24 * 3600 * 31) {
         return 24 * 3600;
         } else if (delta <= 24 * 3600 * 180) {
         return 24 * 3600 * 7;
         }else {
         return 24 * 3600 * 30;
         };*/
    }
    private get _frame(): string {
        const delta = (this.before - this.after);
        if (delta <= 24 * 3600) {
            return 'h'; // default : 1h
        } else if (delta <= 24 * 3600 * 31) {
            return 'd';
        } else if (delta <= 24 * 3600 * 180) {
            return 'w';
        }else {
            return 'm';
        };
    }

    get category(): number | string {
        return <number>this.catId;
    }

    set category(c: number | string) {
        if (c) {
            if (typeof c === 'string') c = parseInt(c, 10);
            this.type = widgetCategories.detail(c).type;
        }
        this.catId = <number>c;
    }

    /**
     *
     * @param rules
     * @param options: {dates?: boolean, widgets?: boolean, silent?: boolean}
     *          if dates = true, rules.after over rights context.after (or set to context.after to 0)
     *          if widgets, delete widget and set widget (id/category / type) of the rule
     */
    setFilterRules(
        rules: ProfileFilterRules | PostFilterRules | FilterQuery,
        options?: {dates?: boolean, widgets?: boolean, silent?: boolean, persistent?: boolean}
    ) {
        options = Object.assign({}, this.filterRulesOptions, options);
        //if (!options.hasOwnProperty('dates') || !options.hasOwnProperty('widgets')) throw 'FILTER_OPTIONS_REQUIRED()';


        const init = JSON.stringify(this.getContext());

        if (options.persistent) {
            this._persistantsRules = rules;
        }

        if (options.dates) {
            this._after = rules.after //|| 1;
            this._before = rules.before //|| dates.toZIFromDate(dates.today.to);
        }

        if (options.widgets) {
            delete this.widget;
            this._widgetId = rules.widget_id;
            this.category = rules.widget_category;
            this.type = rules.widget_type;

        }

        this._filterRules = <ProfileFilterRules | PostFilterRules>{...(rules || {}), ...this._persistantsRules};
        //
        if (init !== JSON.stringify(this.getContext()) && !options.silent) this.triggerChange();
    }

    get frame(): string {
        return this._frame;
    }

    get filterRules(): any {
        return this._filterRules;
    }


    get after(): number {
        return this._after;
    }

    get before(): number {
        return this._before;
    }

    get widget(): Widget {
        return this._widget;
    }

    get widgetId(): string {
        if (this._widget) {
            return this._widget.id;
        } else {
            return this._widgetId;
        }
    }

    get zone(): {n: number, s: number, e: number, w: number } {
        return this._widget && this._widget.zone;
    }

    get view() {
        return this._widget && this._widget.view;
    }

    get tags(): string[] {
        return this._widget && this._widget.tags;
    }

    get orTags(): string[] {
        return this._widget && this._widget.orTags;
    }

    get minusTags() {
        return this._widget && this._widget.minusTags;
    }

    get dateRange():  {from: Date, to: Date} {
        return {from: dates.toDateFromZI(this._after), to: dates.toDateFromZI(this._before)};
    }
    set dateRange(dts: {from: Date, to: Date}){
        const init = JSON.stringify(this.getContext());

        if (typeof dts.from === 'string') dts.from = new Date(dts.from);
        if (typeof dts.to === 'string') dts.to = new Date(dts.to);

        this._after = dates.toZIFromDate(dts.from);
        this._before = dates.toZIFromDate(dts.to);

        if (init !== JSON.stringify(this.getContext())) this.triggerChange();

    }


    /*
     * @parameter Object : options
     *                     extendedOptions: Options to add to the context (assign),
     *                                      ex : limit witch is on the card level and not the page level
     * */
    // TODO : manage category and all partners widget !
    getContext(options: {extendedOptions?: Object, filter?: boolean, timeline?: boolean} = {}) {
        const c: any = {};

        if (this._filterRules && options.filter !== false) {
            Object.assign(c, this._filterRules);
            //CLEAN object with unused fields :
            delete c.widgets
        }


        c.after = this._after;
        c.before = this._before;


        if (this._widget) {
            c.widget_id = this._widget.id;
        }
        if (this.catId) {
            c.widget_category = this.catId;
        }
        if (this.type) {
            c.widget_type = this.type;
        }

        if (options.timeline !== false) {
            // frame managment :
            const delta = (this.before - this.after) / 3600 / 24
            let frame: string;
            if (delta <= 1 ) {
                frame = 'h';
            } else if ( delta <= 31) {
                frame = 'd';
            } else if (delta <= 70 ) {
                frame = 'w';
            } else {
                frame = 'm';
            }
            c.frame = frame;
            c.hour_offset = -1 * new Date().getTimezoneOffset() /   60;

        }


        if (options.extendedOptions) {
            Object.assign(c, options.extendedOptions);
        }




        return c;
    }

    get isWidget(): boolean {
        return !!this._widget || !!this._widgetId;
    }

    set after(after: number) {
        this._after = after;
    }

    set before(before: number) {
        this._before = before;
    }

    set widget(widget: Widget) {
        const init = JSON.stringify(this.getContext());
        this._widget = widget;
        if (init !== JSON.stringify(this.getContext())) this.triggerChange();
    }

    allRange(): WidgetContext {
        const ar = dates.all;
        this._after = undefined//dates.toZIFromDate(ar.from);
        this._before = undefined//dates.toZIFromDate(ar.to);
        return this;
    }

    clone(): WidgetContext {
        const wc = new WidgetContext(
            this._widget,
            {category: this.catId, type: this.type},
            Object.assign({}, this._filterRules)
        )
        // I need to set before and after after, otherwhise allrange => today
        wc.after = this.after;
        wc.before = this.before;
        return wc;
    }

    isInContext(o: any) {
        if (o.created || o.date) {
            if (this.after > (o.created  || o.date) || this.before < (o.created  || o.date)) return false;
        }
        if (o.widgetId && this.widgetId && o.widgetId !== this.widgetId) return false;
        if (o.catId && this.catId && o.catId !== this.catId) return false;


        return true;
    }

    setFilterRule(
        ruleName: string,
        ruleValue: any,
        options: {dates?: boolean, widgets?: boolean, silent?: boolean, persistent?: boolean, persistentForce?: boolean} = {}
    ) {
        if (this._persistantsRules.hasOwnProperty(ruleName) && !options.persistentForce) return;
        const currentRules = tools.clone(this._filterRules);
        currentRules[ruleName] = ruleValue;
        this.setFilterRules(<PostFilterRules | ProfileFilterRules>currentRules, options);// RISQUE DE GROS BUGS, ce ne sont pas toutes les regles qui sont persistenetes, juste celle la <= tout refaire
    }

    /***
     *
     * Does not trigger change
     * @param dr
     * @returns {WidgetContext}
     */
    setRange(dr: {from?: Date, to?: Date}) {
        this._after = dates.toZIFromDate(dr.from);
        this._before = dates.toZIFromDate(dr.to);
        return this;
    }

    triggerChange() {
        if (!this.beforeChange) {
            this.change.next(true);
        } else {
            this.beforeChange().then((change) => { if (change) this.change.next(true); });
        }
    }
}
