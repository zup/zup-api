import { Profile, JsonProfile } from './';

import * as utils from '../misc/utils';
import { wallJsonPost } from './';


export type Location = {
    lat: number,
    lng: number,
    geo_name?: string
}

export type FullJsonPost = {
    id: string,
    isError?: boolean,
    kind: string,
    authorid: string,
    approximate: boolean,
    content: {
        attribution: string,
        cites: string[],
        is_share: boolean,
        lang: string,
        links: string[],
        medias: string[],
        short_text: string,
        tags: string[],
        text: string,
        thumbnail: string,
        title: string,
        sentiment_fetched: number,
        sentiment_score: number
    },
    crawled_at: number,
    created: number,
    filtered: string
    likes: number,
    position: Location,
    updated: number,
    xcomments: number,
    xlikes: number,
    xscore: number,
    xshares: number
}

export interface PostInterface {
    id: string;
    created: number;
    attributionType: string; // tw,fb,rive,...
    attributionLink: string;
    authorId: string;
    position: {lat: number, lng: number, geo_name?: string};
    lang?: string;
    tags?: string[];
    isShared?: boolean;
    text: string;
    thumbnail: string;
    user: Profile;
    video?: string;
    videoType?: string;
    xlikes?: number;
    xcomments?: number;
    xshares?: number;
    sentimentScore?: number;
    lexicalIds?: string[];
    engagements?: number;
}

export class Post implements PostInterface {
    id: string;
    isError: boolean;
    isShared: boolean;
    created: number;
    tags?: string[];
    text: string;
    thumbnail: string;
    attributionType: string; // tw,fb,rive,...
    attribution: string; // should not be used, use attributionLink instead
    attributionLink: string;
    authorId: string;
    user: Profile;
    position: {lat: number, lng: number, geo_name?: string};
    video?: string;
    videoType?: string;
    xlikes?: number;
    xcomments?: number;
    xshares: number;
    sentimentScore?: number;
    lexicalIds?: string[];
    lang: string;
    engagements?: number;

    constructor(post: FullJsonPost | any, user: Profile | JsonProfile) {
        if (post.isError || user.isError) {
            this.id = post.id;
            this.isError = true;
            return;
        }

        // manage temporarly 2 formats :
        if (post.author_id) { // format rive
            //throw 'BAD_FORMAT(Post)';
            this.id = post.id;
            this.created = post.created;
            this.text = post.text || post.short_text;
            const media = utils.objectFormatTools.parseMedia(post.medias);
            //this.thumbnail = utils.objectFormatTools.getImageUrl(post.thumbnail, post.medias);
            this.thumbnail = media.imgUrl || post.thumbnail;
            this.video = media.videoUrl;
            this.videoType = media.videoType;

            this.attributionType = post.attribution ?
                utils.objectFormatTools.getAttributionTypeFromAttribution(post.attribution) : utils.objectFormatTools.getAttributionTypeFromId(post.id);
            this.authorId = post.author_id
            this.attribution = post.attribution;
            this.attributionLink =  post.attribution;
            this.position = {lat: post.lat, lng: post.lng, geo_name: post.geo_name};
            this.xcomments = post.xcomments;
            this.xlikes = post.xlikes;
            this.xshares = post.xshares;

        } else { // format B2B
            this.id = post.id;
            this.created = post.created;
            this.text = post.content.text;
            //this.thumbnail = utils.objectFormatTools.getImageUrl(post.content.thumbnail);
            const media = utils.objectFormatTools.parseMedia(post.content.medias);
            this.thumbnail = media.imgUrl || post.content.thumbnail;
            this.video = media.videoUrl;
            this.videoType = media.videoType;
            this.attributionType = post.kind;
            this.authorId = post.authorid
            this.attribution = post.content.attribution;
            this.attributionLink = post.content.attribution;
            this.tags = post.content.tags;
            this.position = post.position;
            this.xcomments = post.xcomments;
            this.xlikes = post.xlikes;
            this.xshares = post.xshares;
            this.lang = post.content.lang;
            this.sentimentScore = post.content.sentiment_score;
            this.lexicalIds = post.lexical_ids || [];
            this.engagements = post.xscore;
            this.isShared = post.content.is_share;
        }



        if (user instanceof Profile) {
            this.user = user;
        } else {
            this.user = new Profile(<JsonProfile>user);
        }
    }

    get elapsedTime() {
        return utils.dates.toElapsedTimeFromZIDate(this.created);
    }

    get wallJsonPost(): wallJsonPost {
        return {
            id: this.id,
            created: this.created,
            isShared: this.isShared,
            attributionKind: this.attributionType,
            attributionLink: this.attributionLink,
            position: {geo_name: this.position.geo_name},
            text: this.text,
            thumbnail: this.thumbnail,
            video : this.video,
            videoType: this.videoType,
            user: {
                id: this.user.id,
                name: this.user.name,
                screenName: this.user.screenName,
                thumbnail: this.user.thumbnail
            }
        };
    }

    get engagement(): number {
        if (!utils.tools.deepGet(this.user, 'followers')) return;

        return (this.xlikes + this.xcomments) / this.user.followers;
    }
}
