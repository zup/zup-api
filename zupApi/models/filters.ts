import { dates, tools, applicationLanguages } from '../misc/utils';
import { WidgetContext } from './';
import { lexicalCategories } from '../models/lexical';


export enum FILTER_RULES_CATEGORIES {
    PROFILE =  'profile',
    CONTENT = 'content',
    AUDIENCE = 'audience',
    CONTEXT = 'context'
}

export const FILTER_RULES_CATEGORIES_DESC = {
    profile: {name: 'Profile', icon: 'fa fa-user'},
    content: {name: 'Content', icon: 'icon-speech'},
    audience: {name: 'Audience', icon: 'fa fa-handshake-o'},
    context: {name: 'Context', icon: 'fa fa-calendar'}
}

export const FILTER_RULES = {
    order_by: {
        id: 'order_by',
        value: 'order_by',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    author_id: {
        id: 'author_id',
        value: 'author_id',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['post']
    },
    zone: {
        id: 'zone',
        value: 'zone',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['post']
    },
    date: {
        id: 'date',
        text: 'Was detected between',
        datePicker: true,
        //initValue: dates['yesterday'],
        valueToRule: (v: any, query?: FilterQuery) => {
            if (!v && query) {
                query.after = query.before = undefined;
                return;
            } else if (v && query) {
                query.after = dates.toZIFromDate(v.from);
                query.before = dates.toZIFromDate(v.to);
            } else if (v && !query) {
                return {
                    after: dates.toZIFromDate(v.from),
                    before: dates.toZIFromDate(v.to)
                }
            } else {
                return {};
            }

        },
        ruleToValue: (query: FilterQuery) => {
            if (query.after === 1) {
                return undefined;
            }

            if (query.after || query.before) {
                return {
                    from: dates.toDateFromZI(query.after),
                    to: dates.toDateFromZI(query.before)
                };
            }
        },
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    engagement_rate_more_than: {
        id: 'engagement_rate_more_than',
        text: 'post engagement rate is greater than',
        text2: '%',
        inputSize: 5,
        max: 100,
        defaultValue: 0,
        isInt: true,
        valueToRule: (v: number, query: FilterQuery) => {
            if (query) {
                query.engagement_rate_more_than = v / 100;
            } else if (v) {
                return {engagement_rate_more_than: v / 100}
            } else {
                return {};
            }
        },
        ruleToValue: (query: FilterQuery) => { return (query.engagement_rate_more_than || 0) * 100},
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    community_engagement_rate_more_than: {
        id: 'community_engagement_rate_more_than',
        text: 'Average author engagement rate is greater than',
        text2: '%',
        inputSize: 5,
        max: 100,
        defaultValue: 0,
        isInt: true,
        valueToRule: (v: number, query: FilterQuery) => {
            if (query) {
                query.community_engagement_rate_more_than = v / 100;
            } else if (v) {
                return {community_engagement_rate_more_than: v / 100}
            } else {
                return {};
            }
        },
        ruleToValue: (query: FilterQuery) => { return (query.community_engagement_rate_more_than || 0) * 100},
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    filter_shares: {
        id: 'filter_shares',
        text: 'exclude shares ',
        checkbox: true,
        defaultValue: false,
        isValue: true, // to bypass 'strict rules' validation
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    filter_reply: {
        id: 'filter_reply',
        text: 'exclude replies ',
        checkbox: true,
        defaultValue: false,
        isValue: true, // to bypass 'strict rules' validation
        forLabel: true,
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['label']
    },
    follower_less_than: {
        id: 'follower_less_than',
        text: 'Has less followers than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    follower_more_than: {
        id: 'follower_more_than',
        text: 'Has more followers than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    gender : {
        id: 'gender',
        text: 'Filter by gender',
        choicePicker: true,
        choices: [
            {value: 'F', text: 'Female'},
            {value: 'M', text: 'Male'},
            {value: 'H', text: 'Human'},
            {value: 'O', text: 'Organisation'}
        ],
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    has_interests: {
        id: 'has_interests',
        text: 'Is interested in ',
        interestPicker: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    lang: {
        id: 'lang',
        text: 'Who writes in  ',
        choicePicker: true,
        choices: applicationLanguages,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    lang2: {
        id: 'lang',
        text: 'Who writes in  ',
        choicePicker: true,
        choices: applicationLanguages,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer']
    },
    lead_score_more_than: {
        id: 'lead_score_more_than',
        text: 'Has a Customer Lead Score over ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    not_profile_label_id: {
        id: 'not_profile_label_id',
        text: 'Exclude profiles from ...',
        labelPicker: true,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    name_like: {
        id: 'name_like',
        text: 'Whose name contains ',
        inputSize: 12,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    profile_label_id: {
        id: 'profile_label_id',
        text: 'Is in my list or campaign ',
        labelPicker: true,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    post_label_id: {
        id: 'post_label_id',
        text: 'Has spoken on brand or theme ',
        sovPicker: true,
        lexicalCategories: [lexicalCategories.STANDARD, lexicalCategories.SOV] ,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    published_more_than: {
        id: 'published_more_than',
        text: 'Has published more than ',
        text2: ' times',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile']
    },
    source: {
        id: 'source',
        text: 'Filter by social network ',
        kindPicker: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    terms: {
        id: 'terms',
        text: 'Has used the words ',
        inputSize: 12,
        //defaultValue: '',
        multiple: true,
        valueToRule: (ks, query?: FilterQuery) => {
            if (typeof ks === 'string') ks = ks.split(/\s*,\s*/);
            if(ks && query) {
                query.terms = ks.map(k => tools.formatkwd(k));
            } else if (!ks && query) {
                delete query.terms;
            } else if(ks && !query) {
                return {terms: ks.map(k => tools.formatkwd(k))};
            } else {
                return {};
            }
        },
        ruleToValue: (query: FilterQuery) => {
            return query.terms && query.terms.map(k => tools.unformatKwd(k));
        },
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    description_terms: {
        id: 'description_terms',
        text: 'Has the words in his description',
        inputSize: 12,
        //defaultValue: '',
        multiple: true,
        valueToRule: (ks, query?: FilterQuery) => {
            if (typeof ks === 'string') ks = ks.split(/\s*,\s*/);
            if(ks && query) {
                query.description_terms = ks.map(k => tools.formatkwd(k));
            } else if (!ks && query) {
                delete query.description_terms;
            } else if(ks && !query) {

                return {description_terms: ks.map(k => tools.formatkwd(k))};
            } else {
                return {};
            }
        },
        ruleToValue: (query: FilterQuery) => {
            return query.description_terms && query.description_terms.map(k => tools.unformatKwd(k));
        },
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['label']
    },
    xscore_more_than: {
        id: 'xscore_more_than',
        text: 'post has more engagements than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    widgets: {
        id: 'widgets',
        text: 'Was detected in',
        widgetPicker: true,
        ruleToValue:(query: FilterQuery) => {
            if (query.widget_id) return {type: 'widget', id: query.widget_id};
            if (query.widget_category) return {type: 'category', id: query.widget_category};
            if (query.widget_type) return {type: 'type', id: query.widget_type};
        },
        valueToRule: (v: string | {type:string, id: string | number}, query?: FilterQuery) => {
            if (!v && query) {
                query.widget_id = query.widget_type = query.widget_category = undefined;
                return;
            } else if (v) {
                let value: {type:string, id: string | number};
                if (typeof v === 'string') {
                    value = {type: 'widget', id: <string>v};
                } else {
                    value = <{type:string, id: string | number}>v;
                }

                if (query) {
                    switch (value.type) {
                        case 'widget':
                            query.widget_id = <string>value.id;
                            delete query.widget_category;
                            delete query.widget_type;
                            break;
                        case 'category':
                            query.widget_category = <number>value.id;
                            delete query.widget_id;
                            delete query.widget_type;
                            break;
                        case 'type':
                            query.widget_type = <number>value.id;
                            delete query.widget_category;
                            delete query.widget_id;
                            break;
                    }
                } else {
                    switch (value.type) {
                        case 'widget':
                            return {widget_id: value.id};
                        case 'category':
                            return {widget_category: value.id};
                        case 'type':
                            return {widget_type: value.id};
                    }
                }
            } else {
                return {};
            }
        },
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    community_gender: {
        id: 'community_gender',
        text: 'Gender of the community : ',
        payload: {
            type: 'gender',
            default: 0.6
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_interests: {
        id: 'community_interests',
        text: 'Interests of the community :',
        payload: {
            type: 'interests',
            default: 0.2
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_age: {
        id: 'community_age',
        text: 'Age rank of the community :',
        payload: {
            type: 'age',
            default: 0.3
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_language: {
        id: 'community_language',
        text: 'Language of the community :',
        payload: {
            type: 'language',
            default: 0.5
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },

    /*
 postSentiment :{
 key: 'sentiment_value',
 value: 'sentiment_value',
 text: 'post sentiment is ',
 choicePicker: true,
 choices: [
 {value: 1, text: 'Positive'},
 {value: 2, text: 'Negative'}
 ],
 defaultValue: '',
 category: 'content'
 },*/
    /*
     tag: {
     key: 'tag',
     value: 'tag',
     text: 'Has used the hashtag ',
     inputSize: 12,
     defaultValue: '',

     category: 'content'
     },*/
    /*
     has_interest_details: {
     key: 'has_interest_details',
     value: 'has_interest_details',
     text: 'Talks about ',
     inputSize: 12,
     defaultValue: '',
     multiple: true,
     },*/
};

export class FilterQuery {
    after?: number;
    before?: number;

    widget_id?: string;
    widget_category?: number;
    widget_type?: number;

    widgets?: {type: string, id: string | number};

    author_id?: string;
    published_by_widget_more_than?: number;
    published_more_than?: number;
    published_by_widget_more_days_than?: number;
    engagement_rate_more_than?: number;
    community_engagement_rate_more_than?: number;

    terms?: string[];
    description_terms?: string[]
    source?: string;
    follower_more_than?: number;
    follower_less_than?: number;
    lead_score_more_than?: number;
    name_like?: string;
    post_content_contains?: string;
    profile_kind?: string;
    has_interest?: string;
    has_interest_detail?: string;
    gender: string;
    lang: string;



    order_by?: string;  // posts :  +created/-created/+-xscore/+-xlike/+-xcomment/+-xshare/+-sentiment
                        // profiles: +/-published", "+/-follower", "+/-leadscore", "+/-lastseen


    constructor(rules?, ruleType?, private ruleOptions?) {
        if (!rules) return;
        for (let k of Object.keys(rules)) {
            if (ruleType && (tools.deepGet(FILTER_RULES, k, 'for') || []).indexOf(ruleType) < 0 ) continue; //will not work on  widget and dates
            if (rules[k]) {
                this[k] = rules[k];
            }
        }
        if (this.widget_id) {
            this.widgets = {type: 'widget', id: this.widget_id}
        } else if (this.widget_category) {
            this.widgets = {type: 'category', id: this.widget_category}
        } else if (this.widget_type) {
            this.widgets = {type: 'type', id: this.widget_type}
        };
    }

    clear(persistent: string[]) {
        Object.keys(FILTER_RULES).forEach(k => {
            if (persistent && persistent.indexOf(k) >= 0) return;
            const rule = FILTER_RULES[k];
            if (rule.valueToRule) {
                rule.valueToRule(rule.defaultValue, this);
            } else {
                delete this[rule.id];
            };
        })
    }

    fromContext(widgetContext?: WidgetContext, ruleOptions?: any): FilterQuery {
        this.ruleOptions = ruleOptions
        if (widgetContext) {
            const jwc = widgetContext.getContext({timeline: false});
            Object.assign(this, jwc);

            // test if dates are allRange to delete them
            if (JSON.stringify(widgetContext.dateRange) === JSON.stringify(dates.all)) {
                delete this.after;
                delete this.before;
            }
        }
        return this;
    }

    getActiveRules(category?: FILTER_RULES_CATEGORIES, withHidden?: boolean): any[] {
        const activeRules: {id: string, value: any}[] = []
        Object.keys(FILTER_RULES).forEach(k => {
            if (category && FILTER_RULES[k].category !== category) return;
            if (withHidden === false && FILTER_RULES[k].hidden) return;
            let currentVal = this[k];

            if (FILTER_RULES[k].ruleToValue) {
                currentVal = FILTER_RULES[k].ruleToValue(this);
            }
            const defaultValue = tools.deepGet(this.ruleOptions, k, 'defaultValue') ||  FILTER_RULES[k].defaultValue;
            if (currentVal && JSON.stringify(currentVal) !== JSON.stringify(defaultValue)) {
                activeRules.push(FILTER_RULES[k]);
            };
        })

        return activeRules;
    }

    isActive(rid: string): boolean {
        if (!FILTER_RULES[rid]) {
            console.log(`UNKNOW_RULE(${rid})`);return false;
        }
        let currentVal = this[rid];

        if (FILTER_RULES[rid].ruleToValue) {
            currentVal = FILTER_RULES[rid].ruleToValue(this);
        }
        let defaultValue = tools.deepGet(this.ruleOptions, rid, 'defaultValue') || FILTER_RULES[rid].defaultValue;

        return currentVal && JSON.stringify(currentVal) !==  JSON.stringify(defaultValue);
    }

    ruleValue(rid: string) {
        if (!FILTER_RULES[rid]) {
            console.log(`UNKNOW_RULE(${rid})`);return;
        }
        let val = this[rid];
        if (!val) val = FILTER_RULES[rid].defaultValue;
        if (FILTER_RULES[rid].ruleToValue) val = FILTER_RULES[rid].ruleToValue(this);

        if (FILTER_RULES[rid].multiple && val) {
            return val.toString();
        }


        return val;

    }

    toJSON() {
        const activeRules = this.getActiveRules();
        let json: any = {};

        activeRules.forEach(r => {
            let rv = {};
            if (r.valueToRule) {
                rv = r.valueToRule(this.ruleValue(r.id));
            } else {
                rv[r.id] = this.ruleValue(r.id);
            }

            json = {...json,...rv}
        })
        return json;
    }

    get length(): number {
        return this.getActiveRules().length;
    }

}

export class FilterRules {
    /*after?: number;
     before?: number;

     published_by_widget_more_than?: number;
     published_more_than?: number;
     published_by_widget_more_days_than?: number;

     term?: string;
     source?: string;
     follower_more_than?: number;
     follower_less_than?: number;
     lead_score_more_than?: number;
     name_like?: string;
     post_content_contains?: string;
     profile_kind?: string;
     has_interest?: string;
     has_interest_detail?: string;
     gender: string;
     lang: string;
     */
    widget_id?: string;
    widget_category?: number;
    widget_type?: number;

    widgets?: {type: string, id: string | number};

    constructor(rules?) {
        if (!rules) return;
        for (let k of Object.keys(rules)) {
            if (rules[k]) {
                this[k] = rules[k];
            }
        }
        if (this.widget_id) {
            this.widgets = {type: 'widget', id: this.widget_id}
        } else if (this.widget_category) {
            this.widgets = {type: 'category', id: this.widget_category}
        } else if (this.widget_type) {
            this.widgets = {type: 'type', id: this.widget_type}
        };
    }
}

export class PostFilterRules extends FilterRules {
    order_by?: string; //  +created/-created/+-xscore/+-xlike/+-xcomment/+-xshare/+-sentiment

    after?: number;
    before?: number;

    tag?: string;
    terms?: string[];
    source?: string;
    lang?: string;
    post_label_id?: string;
    author_id?: string;
    sentiment_value?: number; // 1: positive, 2 negative
    xscore_more_than?: number;
    xlike_more_than?: number
    xcomment_more_than?: number;
    xshare_more_than?: number
    engagement_rate_more_than?: number;

    widget_id?: string;
    widget_category?: number;
    widget_type?: number;

    widgets?: {type: string, id: string | number};

    constructor(rules) {super(rules);}
}

export class ProfileFilterRules extends FilterRules {
    order_by?: string; // "+published", "+follower", "+leadscore", "+lastseen"

    after?: number;
    before?: number;

    published_by_widget_more_than?: number;
    published_more_than?: number;
    published_by_widget_more_days_than?: number;

    terms?: string[];
    source?: string;
    follower_more_than?: number;
    follower_less_than?: number;
    lead_score_more_than?: number;
    name_like?: string;
    post_content_contains?: string;
    profile_kind?: string;
    has_interest?: string;
    has_interest_detail?: string;
    gender: string;
    lang: string;
    profile_label_id: string;

    widget_id?: string;
    widget_category?: number;
    widget_type?: number;

    widgets?: {type: string, id: string | number};

    constructor(rules) {super(rules);}
}

