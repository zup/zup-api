import { Observable } from 'rxjs';
import { EventEmitter } from "@angular/core";


export type OLOptions = {
    unduplicateFunction?: any, // (T, T[]) => boolean, true if object not duplicate
    filterFunction?: (T) => boolean,
    afterGMFunction?: (o:ObjectsList<any>) => Observable<ObjectsList<any>> // This function is applied asyncly on the loaded objects before they are added to the list on GetMore
}
/**
 * ObjectsList is an list of objects (that should be iterable, but it does not work currently)
 * copulated with an API call (getMore methode) to get next page of objects
 */
export class ObjectsList<T> {
    isLoading: EventEmitter<boolean> = new EventEmitter<boolean>();
    loading: boolean = false;

    // should be protected but need a tric for converting notificaitons to users
    getMoreObservable: Observable<ObjectsList<T>> | Observable<ObjectsListLC<T>>;

    protected objects: T[];
    protected _cursor: string;
    protected filterFunction:  (T) => boolean;
    protected options: OLOptions = {};
    // getMore is the functions (observable) that loads the next page
    /**
     *
     * @param {T[]} list
     * @param {Observable<ObjectsList<T>>} getMore Observable that when subscribes send the next page of elements
     * (eventually after an API call)
     */
    constructor(
        list: T[],
        getMore?: Observable<ObjectsList<T>> | Observable<ObjectsListLC<T>>,
        options?: OLOptions
    ) {

        this.getMoreObservable = getMore;
        this.options = options || {};

        let myList = list;
        if (this.options.filterFunction) {
            this.filterFunction = this.options.filterFunction;
            myList = list.filter(o => this.filterFunction(o));
        }

        if (this.options.unduplicateFunction) {
          this.objects = [];
          myList.forEach(o => {
            if (this.options.unduplicateFunction(o, this.objects)) this.objects.push(o)
          });
        } else {
          this.objects = myList;
        }

    }

    [Symbol.iterator]() {
        let index = 0;
        return {
            next: () => {
                if (index < this.objects.length) {
                    index += 1;
                    return {done: false, value: this.objects[index - 1]};
                }
                return { value: undefined, done: true };
            }
        };
    }

    addObject(o: T, index: number = 0) {
        this.objects.splice(index, 0, o);
    }

    cleanList() {
        if (this.filterFunction) {
            this.objects = this.objects.filter(o => this.filterFunction(o));
        }
    }
    /**
     * Deletes an object of the list identified by is id property
     * @param id
     */
    deleteById(id: string) {
        const i = this.objects.findIndex(n => {
            return n['id'] === id;
        });

        if (i < 0) {
            console.log('UNKNOWN ID for ObjectsList.deleteById');
            return;
        }
        this.objects.splice(i, 1);
    }

    /**
     * Deletes an object from the list
     * @param o
     */
    deleteObject(o: T) {
        const i = this.objects.findIndex(n => {
            return n === o;
        });

        if (i < 0) {
            console.log('UNKNOWN Object for ObjectsList.deleteObject');
            return;
        }
        this.objects.splice(i, 1);
    }

    /**
     * Returns an object from the list, identified by its id property
     * @param id
     * @returns {T}
     */
    getById(id: string): T {
        if (!id) return;
            return this.objects.find(n => {
                return n['id'] === id;
            });
    }

    getShiftedObject(id: string, shift: number): Observable<T> {
        const i = this.objects.findIndex(n => {
            return n['id'] === id;
        }) + shift;
        if (i < 0) {
            return Observable.of(null);
        } else if ( i < this.objects.length) {
            return Observable.of(this.objects[i]);
        } else if (this.hasMore) {
            const j = i + 1 - this.objects.length;
            const lastId = this.objects[this.objects.length - 1]['id'];
            return this.getMore().flatMap(() => this.getShiftedObject(lastId, j));
        } else {
            return Observable.of(null);
        }
    }

    getObject(o: T): T {
        return this.objects.find(n => {
            return n === o;
        });
    }

    /**
     * Returns the length of the list
     * @returns {number}
     */
    get length(): number {
        return (this.objects && this.objects.length) || 0;
    }

    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(T) => boolean)} rulesFn function to filter the list
     * @param {boolean} replace : replace existing objects
     * @returns {T[]}
     */
    filter(rulesFn: (T) => boolean, replace?: boolean) { // local filter
        const filtered = this.objects.filter( o => { return rulesFn(o); });
        if (replace) {
            this.filterFunction = rulesFn;
            this.objects = filtered;
        }
        return filtered;
    }

    /**
     *
     * @param {any} filters object of type { prop: value,... } to be matched by the posts
     * @returns {T[]}
     */
    filterByProp(filters?: any) {
        const fFn = function(o) {
            if (!filters) return true;
            let ok: boolean = true;
            for (let p in filters) {
                ok = ok && o[p] === filters[p];
            }

            return ok;
        };

        return this.filter(fFn);
    }

    /**
     *
     * @returns {T[]}
     */
    get list(): T[] {
        return this.objects;
    }

    /**
     * Returns if a "next page" exists
     * @returns {boolean}
     */
    get hasMore(): boolean {
        return !!this.getMoreObservable;
    }

    /**
     * Returns an OBservable that when subscribed returns the "next page" of elements and add them to the current list
     * @returns {Observable<T[]>}
     */
    getMore(): Observable<T[]>  {
        if (!this.hasMore) return Observable.from([]);
        this.loading = true;
        this.isLoading.emit(this.loading);

        let getMoreObservable = (<Observable<ObjectsList<T>>>this.getMoreObservable);

        if (this.options.afterGMFunction) getMoreObservable = getMoreObservable.flatMap((res) => this.options.afterGMFunction(res));

        return getMoreObservable.map(
            res => {
                if (this.filterFunction) {
                    res.filter(this.filterFunction, true);
                };


                if (this.options.unduplicateFunction) {
                    res.list.forEach((no: T) => {
                        if (this.options.unduplicateFunction(no, this.objects)) this.objects.push(no);
                    });
                } else {
                    this.objects = this.objects.concat(res.list);
                }



                this.getMoreObservable = <Observable<ObjectsList<T>>>(res.getMoreObservable);
                this.loading = false;
                this.isLoading.emit(this.loading);
                if (this.filterFunction) {
                    return res.list.filter(o => this.filterFunction(o));
                } else {
                    return res.list;
                }
            }
        );
    };

    setOptions(optionName: string, optionValue) {
        this.options[optionName] = optionValue;
    }


}

// list with LocalCache (no cursor)
export class ObjectsListLC<T> extends  ObjectsList<T> {
    private pageLimit: number;
    private page: number = 1;
    private _list: T[] = [];
    protected objects: any[];

    // !list is can be something else than T[] if a load Detail is provided
    constructor(
      list: any[],
      limit?: number,
      private loadDetail?: (ids: any[]) => Observable<T[]>,
      getMore?: Observable<ObjectsListLC<T>>,
      options?: OLOptions) {
        super(list, getMore, options);
        this.pageLimit = limit || 10;
        this.loadList().subscribe();
    };

    [Symbol.iterator]() {
        let index = 0;
        return {
            next: () => {
                if (index < this._list.length) {
                    index += 1;
                    return {done: false, value: this._list[index - 1]};
                }
                return { value: undefined, done: true };
            }
        };
    }


    get cacheLength(): number {
        return this.objects.length;
    }

    get ids(): any[] {
        return this.objects;
    }

    get length(): number {
        return (this._list && this._list.length) || 0;
    }

    get list(): T[] {
        return this._list;
    }

    get hasMore(): boolean {
        return this.objects.length > (this.pageLimit * this.page) || !!this.getMoreObservable;
    }

    addObject(object: T, index: number = 0, key?: any)  {
        this.objects.splice(index, 0, key)
        this.list.splice(index, 0, object);
    }

    contains(key: any): boolean {
        return this.objects.indexOf(key) >= 0;
    }

    deleteById(id: string) {
        const i = this.objects.indexOf(id);

        if (i < 0) {
            console.log('UNKNOWN ID for ObjectsList.deleteById');
            return;
        }
        this.objects.splice(i, 1);
        this._list.splice(i, 1);
    }

    deleteObject(key: any) {
        const i = this.objects.indexOf(key);
        if (i >= 0) {
            this.objects.splice(i, 1);
            this._list.splice(i, 1); // what if objects and list are not synchronized ?????
        }
    };

    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(id) => boolean)} rulesFn function to filter the list, will be applied on ids, not on <T>
     * @param {boolean} replace : replace existing objects
     * @returns {id[]}
     */
    filter(rulesFn: (T) => boolean, replace?: boolean) { // local filter
        const filtered = this.objects.filter( o => { return rulesFn(o); });
        if (replace) {
            this.filterFunction = rulesFn;
            this.objects = filtered;
            this._list = this._list.filter((o: any) => this.objects.indexOf(o.id) >= 0);
        }
        return filtered;
    }

    getMore(): Observable<T[]> {
        if (!this.hasMore) {
            return Observable.from([]);
        } else if (this.objects.length > (this.pageLimit * (this.page + 1) )) {
            // still enough objects in localcache for nextPage
            this.page++;
            this.loading = true;
            this.isLoading.emit(this.loading);
            return this.loadList();
        } else if (!!this.getMoreObservable) {
            this.loading = true;
            this.isLoading.emit(this.loading);
            return (<Observable<ObjectsListLC<T>>>this.getMoreObservable).flatMap(
                res => {

                  if (this.options.unduplicateFunction) {
                      res.objects.forEach((no: T) => {
                          if (this.options.unduplicateFunction(no, this.objects)) this.objects.push(no);
                      });
                  } else {
                      this.objects = this.objects.concat(res.objects);
                  }

                  if (this.filterFunction) {
                      this.objects = this.objects.filter(o => this.filterFunction(o));
                  }

                    this.getMoreObservable = <Observable<ObjectsListLC<T>>>(res.getMoreObservable);
                    return this.getMore();
                }
            );
        } else {
            // no more in back, need to finish loading detail
            this.page++;
            this.loading = true;
            this.isLoading.emit(this.loading);
            return this.loadList();
        }
    }

    /**
     * Loads in // all the details in cache
     */
    getCacheDetail(): Observable<ObjectsListLC<T>> {
        if (!this.loadDetail || this.list.length >= this.objects.length) return Observable.from([this]);

        this.loading = true;
        this.isLoading.emit(this.loading);

        const todos = [];
        let allIds = this.objects.slice(this.list.length);
        while (allIds.length > 0) {
            const ids = allIds.slice(0, this.pageLimit);
            allIds = allIds.slice(this.pageLimit);
            todos.push(this.loadDetail(ids));
        };

        return Observable.forkJoin(...todos).map((res: T[][]) => {
            res.forEach((details: T[]) => {
                this._list = this._list.concat(details);
            });
            this.loading = false;
            this.isLoading.emit(this.loading);
            return this;
        });
    }

    getShiftedObject(id: string, shift: number): Observable<T> {
        const i = this.list.findIndex(n => {
                return n['id'] === id;
            }) + shift;
        if (i < 0) {
            return Observable.of(null);
        } else if ( i < this.list.length) {
            return Observable.of(this.list[i]);
        } else if (this.hasMore) {
            const j = i + 1 - this.list.length;
            const lastId = this.list[this.list.length - 1]['id'];
            return this.getMore().flatMap(() => this.getShiftedObject(lastId, j));
        } else {
            return Observable.of(null);
        }
    }

    setObjects(objects: T[], keys: any[]) {
        this._list = objects;
        this.objects = keys;
    }

    getCacheObjects(): any[] {
        return this.objects;
    }

    hasInCache(id: any): boolean {
        return this.list.indexOf(id) >= 0;
    }

    private loadList(): Observable<T[]> {
        if (!this.objects) return Observable.from([[]]);

        this.loading = true;
        this.isLoading.emit(this.loading);
        const l = this.objects.slice(this.pageLimit * (this.page - 1), this.pageLimit * this.page);
        if (this.loadDetail) {
            return this.loadDetail(l).map( (res: T[]) => {
                if (this.filterFunction) res = res.filter((t) => this.filterFunction((<any>t).id));
                this._list = this._list.concat(res);
                this.loading = false;
                this.isLoading.emit(this.loading);
                return this._list; });
        } else {
            this._list = this._list.concat(l);
            this.loading = false;
            this.isLoading.emit(this.loading);
            return Observable.from([this._list]);
        }
    }
}
