/*
'new_follower'
'following_action'
'new_citation'
'action_my_post'
'comment_on_commented_post'
'geo_alert'
'b2b_user_daily'
'b2b_influencer'
'b2b_loyalty'
'b2b_word'
*/
import { Profile } from './profile';
import { Post, Location } from './post';
//import { ObjectsList } from './objectsList';
import { objectFormatTools } from '../misc/utils';
import { toLOV } from '../listOfValues';

export const alertTypes: any = {
  WORD: {
    text: 'Word Alert',
    color: 'red',
    title: 'The sensitive word "$0" was used in a user\'s post',
    description: 'Be notified when a post containing a specific word is detected',
    variable: 'word',
    value: 'b2b_word'
  },
  INFLUENCER: {
    text: 'Influencer Alert',
    color: 'blue',
    title: 'An influencer with more than $0 followers has posted in this place',
    description: 'Be notified when an influencer publishes content',
    variable: 'profile_x_score',
    variableTitle: 'Number of Followers',
    value: 'b2b_influencer'
  },
  LOYALTY: {
    text: 'Top Loyalty',
    color: 'purple',
    title: 'A user has returned $0 times in this place',
    description: 'Be notified when a user comes back several times in the same place',
    variable: 'day_count',
    variableTitle: 'Number of times detected',
    value: 'b2b_loyalty'
  },
  USER_DAILY: {
    text: 'Top users',
    color: 'green',
    title: 'A user has postedd $0 times in one day',
    description: 'Be notified when someone publishes content multiple times in the same place',
    variable: 'score',
    variableTitle: 'Number of posts per day',
    value: 'b2b_user_daily'
  },
  CUSTOMER_LEAD: {
    text: 'Customer Lead',
    color: 'orange',
    title: 'A strong <span>cutomer lead</span> has been detected in this place ',
    description: 'Be notified when a strong potential customer is detected',
    variable: 'score',
    variableTitle: 'score',
    value: 'b2b_customer_lead'
  }
};

toLOV(alertTypes);

export type JsonNotification = {
  id: string,
  type: string,
  follower: string,
  action: string,
  action_author: string,
  action_type: string,
  action_text: string,
  post: string,
  post_attribution: string,
  post_author: string,
  post_location: Location,
  created: number,
  closed_at: number,
  is_closed: boolean,
  payload: any,
  widget_id: string,
}

export interface NotificationInterface {
  id: string;
  type: string;
  isError?: boolean;
  user?: Profile;
  post?: Post;
  postId: string;
  created: number;
  payload: any;
  widgetId: string;
  text: string;
  attributionType: string; // to be used instead of attr
  attributionLink: string; // post attribution (user in the future if alerts on users)
  closedAt: number;
  isClosed: boolean;
}

export class Notification implements NotificationInterface{
  id: string;
  type: string;
  isError: boolean;
  user?: Profile;
  post?: Post;
  postId: string;
  created: number;
  payload: any;
  widgetId: string;
  text: string;
  attr: string;
  attributionType: string; // to be used instead of attr
  attributionLink: string; // post attribution (user in the future if alerts on users)
  closedAt: number;
  isClosed: boolean = false;

  constructor(alert: JsonNotification, user?: Profile, post?: Post) {
    if (alert.action_type !== 'post_create') {
      throw `BAD_ACTION_TYPE(${alert.action_type})`;
    }
    this.id = alert.id;
    this.attributionLink = alert.post_attribution;
    this.type = alert.type;
    this.user = user;
    this.post = post;
    this.postId = alert.post
    this.created = alert.created;
    this.payload = JSON.parse(alert.payload);
    this.widgetId = alert.widget_id;
    this.isClosed = alert.is_closed
    this.closedAt = alert.closed_at;
    this.text = alert.action_text;
    this.attr = objectFormatTools.getAttributionTypeFromId(this.postId);
    this.attributionType = objectFormatTools.getAttributionTypeFromId(this.postId);
  }
}


/*
export class NotificationsList extends ObjectsList<Notification> {
  constructor(notifications: any[], jsonProfiles: any[], posts: Post[], getMore: any) {
    const usersMap: Map<string, Profile> = new Map<string, Profile>();
    const postsMap: Map<string, Post> = new Map<string, Post>();
    jsonProfiles.forEach(p => { usersMap.set(p.id, new Profile(p)); });
    posts.forEach(p => { postsMap.set(p.id, p); });
    const notificationsList = [];

    notifications.forEach(n => {
      if (!n.post) return;
      notificationsList.push(new Notification(n, usersMap.get(n.action_author), postsMap.get(n.post)));
    });

    super(notificationsList, getMore);
  }


  filter(filters?: any) {
    const fFn = function(o) {
      if (!filters) return true;
      let ok: boolean = true;
      for (let p in filters) {
        ok = ok && o[p] === filters[p];
      }

      return ok;
    };

    return super.filter(fFn);
  }
}*/
