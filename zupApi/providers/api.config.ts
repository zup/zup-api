
export type jsonConfig = {
    apiUrls?: any,
    oauth?: any,
    loginRoute?: any,
    keys?: any,
    application?: {id, location?, referrer?, rights: string[], loginRoute: string[]}
}

/**
 * Configration of the Api
 *
 *
 */
export class ApiConfig {
    apiUrls: any = {
        coldapi: 'default-dot-exp-lkl.appspot.com',
        hotapi: 'hotapi-dot-exp-lkl.appspot.com',
        b2bapi: 'b2b-dot-exp-lkl.appspot.com'
    };

    oauth: any = {};

    loginRoute: any[];// = ['login'];

    keys: any;

    application: {id, location?, referrer?, rights: string[], loginRoute: string[]};
    /**
     *
     * @param {any} apiUrls access url to the different apis (cold, hot, b2b). Default: dev apis
     * @param {any} oauth oAuth information of linked social network for publish functionalities
     * @param {any} loginRoute route for the redirection of unlogged users.
     */
    constructor(config: jsonConfig = {}) {
        if (config.apiUrls) this.apiUrls = config.apiUrls;
        if (config.oauth) this.oauth = config.oauth;
        if (config.loginRoute) {
            this.loginRoute = config.loginRoute;
        }
        if (config.keys) this.keys = config.keys;

        if (config.application) this.application = config.application;
    }

}
