import { Injectable } from "@angular/core";
import { ZupApiService } from './zupApi.service';
import {
    ActionLogDetail,
    ActionLog,
    Campaign,
    ObjectsList,
    Profile,
    WidgetContext
} from '../models/';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { dates } from '../misc/utils';



export const enum STATUS {
    NONE = 0,
    INITED = 1
}

@Injectable()
export class HistoLogsService {
    private _status = STATUS.NONE;
    private isInited: Subject<boolean> = new Subject<boolean>();
    private histoLogs: Map<string, ActionLog> = new Map<string, ActionLog>();
    private created: {id: string, date: number, campaignId: string, profileId: string}[] = [];
    private logsByPost: Map<string, string[]> = new Map<string, string[]>();
    private logsByProfile: Map<string, string[]> = new Map<string, string[]>();
    private logsByCampaign: Map<string, string[]> = new Map<string, string[]>();

    private autoPulishToCampaign: Map<string, string> = new Map<string, string>();

    private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    private logFrom: number = Infinity;
    private logTo: number = 0;

    public get status() {return this._status};

    constructor(private api: ZupApiService){
    }

    init() {
        this.api.campaigns.list().subscribe(aps => {
            aps.forEach(ap => {this.autoPulishToCampaign.set(ap.id, ap.profiles_label_id)});
            const initDR = dates.last30Days;
            //const initDR = dates.all;
            this.loadHistoLogs(dates.toZIFromDate(initDR.from), dates.toZIFromDate(initDR.to)).subscribe(() => {
                this._status = STATUS.INITED;
                this.isInited.next(true);
            });
        });
    }

    onInit(callback: Function): void {
        if (this._status === STATUS.INITED) {
            callback();
        } else {
            this.isInited.subscribe(() => callback());
        }
    }


    loadHistoLogs(after?: number, before?: number) {
        this._status = STATUS.NONE;

        this.logFrom = Math.min(this.logFrom, after);
        this.logTo = Math.max(this.logTo, before);

        const wc = new WidgetContext(null, {after: after, before: before})



        return this.api.logs.list(wc, {}, {limit: 1000}).flatMap(this.api.getNextPageFunction()).map((res: ObjectsList<ActionLog>) => {
           Array.from(res).forEach((l: ActionLog) => {
               if (!this.histoLogs.has(l.id)) {
                   this.histoLogs.set(l.id, l);
                   this.created.push({id: l.id, date: l.created, campaignId: l.campaignId, profileId: l.profileId});
               } else {
                   return;
               }

               if (l.postId) {
                   if (!this.logsByPost.has(l.postId)) this.logsByPost.set(l.postId, []);
                   const logs = this.logsByPost.get(l.postId);
                   if (logs.indexOf(l.postId) <0) logs.push(l.id);
               }

               if (l.profileId) {
                   if (!this.logsByProfile.has(l.profileId)) this.logsByProfile.set(l.profileId, []);
                   const logs = this.logsByProfile.get(l.profileId);
                   if (logs.indexOf(l.profileId) <0) logs.push(l.id);
               }

               if (l.campaignId) {
                   if (!this.logsByCampaign.has(l.campaignId)) this.logsByCampaign.set(l.campaignId, []);
                   const logs = this.logsByCampaign.get(l.campaignId);
                   if (logs.indexOf(l.campaignId) <0) logs.push(l.id);
               }

           })

            this.created.sort((a, b) => b.date - a.date);

        });
    }

    getCampaignProfiles(id: string): Observable<ObjectsList<Profile>> {
  /*      let profiles = [];
        const profileEngaged: Map<string, number> = new Map<string, number>()

        campaign.autoPublish.forEach(p => {
            profiles = profiles.concat(this.logsByCampaign.get(p.id))
        })
*/
        if (!this.logsByCampaign.has(id)) return Observable.of(new ObjectsList<Profile>([]));
        const profileEngaged: Map<string, number> = new Map<string, number>()
        const profileIds = this.logsByCampaign.get(id).sort(this.sortLogs).map(lid => {
            const l = this.histoLogs.get(lid);
            profileEngaged.set(l.profileId, l.created)
            return l.profileId;
        });
        return this.api.profiles.getMultiple(profileIds).map(profiles => {
            return new ObjectsList<Profile>(profiles.map(p => {
                p.customValue = profileEngaged.get(p.id);
                return p;
            }));
        })

    }

    isProfileInCampaign(pid, campaign: Campaign): boolean {
        const profileLogs = this.logsByProfile.get(pid);
        if (!profileLogs || profileLogs.length === 0) return false;

        const paIds = campaign.publishActions.map(pa => pa.id);

        for (let i = 0; i < profileLogs.length; i++) {
            if (paIds.indexOf(this.histoLogs.get(profileLogs[i]).campaignId) >= 0) return true;
        }
        return false;

    };

    getProfileLogs(pid: string, last: boolean = true): ActionLog[] {
        if (!this.logsByProfile.has(pid)) return [];
        const lids = this.logsByProfile.get(pid).sort(this.sortLogs);
        const myIds = last? lids.slice(0, 1) : lids;
        return myIds.map(id => this.histoLogs.get(id)).filter(l => !!l);
    }

    private sortLogs = (a, b) => this.histoLogs.get(a).created - this.histoLogs.get(b).created;

}
