import { Injectable, EventEmitter } from '@angular/core';
import { PartnerService } from './partner.service';
import { ZupApiService } from './zupApi.service';

import { Wall, wallJsonPost, Post, SimpleProfile, Profile, WidgetContext, wallPages, ObjectsList } from '../models';
import { dates, tools } from '../misc/utils';

import { Observable } from 'rxjs';

import Timer = NodeJS.Timer;
import { objectFormat } from './storage';
import {wallPage, wallPageConf, wallConf} from "../models/wall";






@Injectable()
export class WallService {

    public displayedPosts: Set<string> = new Set<string>();
    public wallFrontPage: string;

    private wallName: string;
    private wallPartnerName: string;

    private wallId: string;
    public confId: string;
    private _wall: Wall;
    private _currentWallConf: wallConf;
    private dateRange: {from: Date, to: Date};
    private _posts: wallJsonPost[];
    private _postStack: wallJsonPost[] = [];
    private intervals: {
        main?: Timer,
        //pinned?: Timer,
        config: Timer
    }
    private pinnedPostTick: number = 0;
    private sponsoredPostTick: number = 0;
    private dateInitConfig: number = dates.toZIFromDate(new Date());
    private lastSentPP: wallJsonPost;


    serviceError: EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Emits a post every every [wall.delays.mainPosts] seconds
     * @type {EventEmitter<wallJsonPost>}
     *
     *
     * current algo to emit new Post:
     * 1 - try use stack (fst-in/fst-out)
     * 2 - ask for new
     * 3 - take a random in the list (older)
     */
    newPost: EventEmitter<{post: wallJsonPost, isRecycled?: boolean}> = new EventEmitter<{post: wallJsonPost, isRecycled?: boolean}>();
    /**
     * Emits a pinnedPost every every [wall.delays.pinnedPosts] seconds (if any)
     * @type {EventEmitter<wallJsonPost>}
     */
    //newPinnedPost: EventEmitter<wallJsonPost> = new EventEmitter<wallJsonPost>();
    /**
     * Emits a sponsoredPost every every [wall.delays.sponsoredPosts] seconds (if any)
     * @type {EventEmitter<wallJsonPost>}
     */
    //newSponsoredPost: EventEmitter<wallJsonPost> = new EventEmitter<wallJsonPost>();

    /**
     * Emits true when wall has changed, the display should reload everything (general info, posts (moderation might have changed),...)
     * @type {EventEmitter<boolean>}
     */
    configChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    // refreshWall: EventEmitter<boolean> = new EventEmitter<boolean>();

    analytics = {
        nbPosts: (dtFrom?: number, dtTo?: number): Observable<number> => {
            return this.api.wall.nbPosts(this._wall.id, dtFrom, dtTo, {cache: false});
        },
        topHashTags: (nb?: number, dtFrom?: number, dtTo?: number): Observable<{payload: string, total: number}[]> => {
            //return this.api.wall.topTags(this.wall.widgetIds[0], nb, dtFrom, dtTo);
            return this.api.wall.topTags(this._wall.id, nb, dtFrom, dtTo);
        },
        topUsers: (nb?: number, dtFrom?: number, dtTo?: number): Observable<Profile[]> => {
            return this.api.wall.topUsers(this._wall.id, nb, dtFrom, dtTo);
        },
        nbActions: (dtFrom?: number, dtTo?: number): Observable<{likes: number, comments: number, shares: number}> => {
            return this.api.wall.nbActions(this._wall.id, dtFrom, dtTo);
        }
    }

    get currentWallConf(): wallConf {
        return tools.clone(this._currentWallConf);
    }

    get confName(): string {
        return this.confId;
    }

    constructor(private partner: PartnerService, private api: ZupApiService) {}

    /**
     * Returns custom styles to be used in the wall
     * @returns {any}
     */
    getCustomStyles(): any {
        return this._wall.styles;
    }

    /**
     * Returns the new post according to the "newPost" algorithm
     * @returns {wallJsonPost}
     */
    getNewPost(): Observable<{post: wallJsonPost, isRecycled?: boolean}> {

        if (!this.dateRange) return Observable.of(this.getRandomPost());
        const sandBoxDelay = this.getOption('sandBoxDelay') || 0;

        const now = new Date(new Date().valueOf() - sandBoxDelay * 60 *1000);
        const nowZI = dates.nowZI - sandBoxDelay * 60;

        if (!this._wall) {
            console.log('NOWALL')
            return Observable.of(undefined);
        }

        const isReplay = this.getOption('replay');
        if (!isReplay && this._postStack.length > 0) {
            while ( //go to the first post tounger than 2 mins
                this._postStack.length > 0
                && this._posts.unshift(this._postStack.pop())
                &&  (nowZI - this._posts[0].created >= 120))
            {}
            var test = nowZI - this._posts[0].created;
            if (this._postStack.length === 0 && (nowZI - this._posts[0].created >= 120)) {
                //the last is oldest than 2 mins; ask for new ones
                const dtFrom = new Date(now.valueOf() - 15 * 60 * 1000);

                this.dateRange = {from: dtFrom, to: now};
                return this.api.wall.posts(this._wall.id, this.dateRange, {limit: 50}).map( res => {
                    if (res.list.length > 0) {
                        const ids = this._posts.slice(0, 100).map(p => p.id);
                        this._postStack = this.moderate(res.list)
                            .filter(p => ids.findIndex(id => id === p.id) < 0);
                    }

                    if (this._postStack.length > 0) {
                        while ( //go to the first post tounger than 2 mins
                        this._postStack.length > 0
                        && this._posts.unshift(this._postStack.pop())
                        &&  (dates.nowZI - this._posts[0].created >= 120))
                        {}

                    }

                    return {post: this._posts[0]};


                }, (err) => {console.log(err); return Observable.of({post: this._posts[0]});});
            } else {
                return Observable.of({post: this._posts[0]});
            }

        } else  if (this._postStack.length > 0 && isReplay) {
            this._posts.unshift(this._postStack.pop())
            return Observable.of({post: this._posts[0]});
        }
        else if ( isReplay ) {

            let newFrom = this.dateRange.to; // we add 1h

            const maxTo = dates.min(dates.toDateFromZI(
                tools.deepGet(this.getOption('replay-timeRange'), 'to')
                || dates.toZIFromDate(new Date())
            ), now)

            let newTo = dates.min(maxTo, new Date(this.dateRange.to.valueOf() + 3600 * 1000));

            if (newFrom.valueOf() + 59 * 1000 >= maxTo.valueOf()) { // we go back from the begining

                this._postStack = this._posts;
                this._posts = [];
                this._posts.unshift(this._postStack.pop())
                return Observable.of({post: this._posts[0]});
            }

            this.dateRange = {from: this.dateRange.to, to: newTo};

            return this.api.wall.posts(this._wall.id, this.dateRange, {limit: 300}).map( res => {
                if (res.list.length > 0) {
                    const ids = this._posts.map(p => p.id);
                    this._postStack = this.moderate(res.list)
                        .filter(p => ids.findIndex(id => id === p.id) < 0);
                }

                if (this._postStack.length > 0) {
                    this._posts.unshift(this._postStack.pop())
                    return {post: this._posts[0]};
                } else {
                    return this.getRandomPost();
                }

            }, (err) => {console.log(err); return Observable.of(this.getRandomPost());});
        }
        else if ((now.valueOf() - this.dateRange.to.valueOf()) > 14 * 1000) { // no more than one query each 15s

            const dtFrom = new Date(now.valueOf() - 15 * 60 * 1000);

            this.dateRange = {from: dtFrom, to: now};
            return this.api.wall.posts(this._wall.id, this.dateRange, {limit: 50}).map( res => {
                if (res.list.length > 0) {
                    const ids = this._posts.slice(0, 100).map(p => p.id);
                    this._postStack = this.moderate(res.list)
                        .filter(p => ids.findIndex(id => id === p.id) < 0);
                }

                if (this._postStack.length > 0) {
                    this._posts.unshift(this._postStack.pop())
                    return {post: this._posts[0]};
                } else {
                   return this.getRandomPost();
                }

            }, (err) => {console.log(err); return Observable.of(this.getRandomPost());});
        } else {
            return Observable.of(this.getRandomPost());
        }
    }

    getRandomPost() {
        if (!this._posts) return;
        //*
        // pick a random
        let r = Math.floor(Math.random() * this._posts.length)
        if (this.displayedPosts) {
            let cpt = 0;
            while (cpt++ < 5 && this.displayedPosts.has(this._posts[r].id)) {
                r = Math.floor(Math.random() * this._posts.length);
            }
            if (cpt === 6) {
                console.log('Could not find a post not already displayed');
            }
        }

        return {post: this._posts[r], isRecycled: true};
        //*/
    }
    /**
     * Returns the next pinnedPost to display
     * @returns {wallJsonPost}
     */
    getPinnedPost(force?: boolean): Observable<wallJsonPost> {
        let pp: wallJsonPost;
        if (this._wall.pinnedPosts.length > 0) {
            if (this._wall.pinnedPosts.length === 1 && this.pinnedPostTick > 0 && !force) return null;
            pp = this._wall.pinnedPosts[this.pinnedPostTick++ % this._wall.pinnedPosts.length];
        }
        return Observable.of(pp);
    }

    /**
     * Returns the nb ljsast posts of the wall
     * @param {number} [nb] default = 20
     * @returns {Observable<wallJsonPost[]>}
     */
    private retry: number = 0;
    getPosts(nb: number = 20, min: number = nb / 2, pageId?: wallPage | string): Observable<wallJsonPost[]> {

        let isReplay = false;
        const sandBoxDelay = this.getOption('sandBoxDelay') || 0;
        const now  = new Date(new Date().valueOf() - sandBoxDelay * 60 * 1000);

        if (this.getOption('replay')) {
            isReplay = true;
            //const interval = tools.deepGet(this._wall.pagesConfig, <string>wallPage.POSTWALL, 'options', 'replay-interval') || 24
            const interval = this.getOption('replay-timeRange')
                || { from: dates.toZIFromDate(now) - 24 * 3600, to: dates.toZIFromDate(now) } // default: last 24h
            this.dateRange = {
                from: dates.toDateFromZI(interval.from),
                to :  dates.min(dates.toDateFromZI(interval.from + 3600), now)
            };

        } else {
            const dtFrom = new Date(new Date().valueOf() - 1000 * 3600 * 24 * 30);
            this.dateRange = {from: dtFrom, to: now};
        }

        this._postStack = [];
        return this.api.wall.posts(this._wall.id, this.dateRange, {limit: nb})
            .flatMap((res) => {

                const getMore: () => Observable<wallJsonPost[]> = () => {
                    const moderated = this.moderate(res.list);
                    if (moderated.length < min && res.hasMore) {
                        return res.getMore().flatMap(() => {
                            return getMore();
                        })
                    } else {
                        this._posts = moderated;
                        if (isReplay) {
                            const nbPosts = Math.min(nb, 20); //if mode == replay I just need the last 20 to init the wall
                            this._postStack = this._posts.slice(0, -(nbPosts + 1));
                            const returnPosts = this._posts.slice(-nbPosts);
                            this._posts = [];
                            return Observable.from([returnPosts]);

                        } else {
                            return Observable.from([this._posts]);
                        }
                    }
                }

                return getMore();

            })
            .catch(err => {
                // if error in post recuperation, warn front and try again for 1 mintues
                if (this.retry++ >= 4) {
                    this.retry = 0;
                    return Observable.throw(err);
                }

                this.serviceError.emit(true)
                return <Observable<wallJsonPost[]>>Observable.create((observer) => {
                    setTimeout(() => {
                        console.log('try again')
                        this.getPosts(nb).subscribe((ps: wallJsonPost[])  => {
                            this.serviceError.emit(false);
                            observer.next(ps);
                        });
                    }, 15 * 1000);
                });
            });
    }

    getPostDetail(id: string): wallJsonPost {
        return this._posts.find(p => p.id === id);
    }

    getPostsDetail(ids: string[]): Observable<wallJsonPost[]> {
        return this.api.posts.getMultiple(ids, objectFormat.XS).map((posts: Post[]) => posts.map((p: Post) => p.wallJsonPost));
    }

    getProfiles(nb?: number): Observable<SimpleProfile[]> {
        if (!this._posts || this._posts.length === 0) return Observable.from([[]]);

        let profiles = tools.unduplicate(
            this._posts.map((post: wallJsonPost) => post.user),
            (u: SimpleProfile) => u.id
        );

        if (nb) profiles = profiles.slice(0, nb - 1);

        return Observable.from([profiles]);
    }

    getPostObjectList(nb: number): Observable<ObjectsList<wallJsonPost>> {
        if(this._currentWallConf) throw 'DOIT ETRE MODIFIE'
        const sandBoxDelay = this.getOption('sandBoxDelay') || 0;
        const now =  new Date(new Date().valueOf() - sandBoxDelay * 60 *1000);
        if (this.getOption('replay') ) {
            const interval = this.getOption('replay-timeRange')
                || {from: dates.toZIFromDate(now) - 24 * 3600, to: dates.toZIFromDate(now)} // default: last 24h
            this.dateRange = {
                from: dates.toDateFromZI(interval.from),
                to :  dates.min(dates.toDateFromZI(interval.to), now)
            };
        } else {
            this.dateRange = {from: new Date('2010/01/01'), to: now};
        }

        return  this.api.wall.posts(
            this._wall.id,
            this.dateRange,
            {limit: nb},
            {filterFunction: (p) => {return this._wall.isOkPost(p, this._currentWallConf);}}
        ).map(res => {
            this._posts = tools.clone(res.list);
            return res;
        });
    }


    /**
     * Loads wall configuration (delays, pinned and sponsored posts, moderated posts and users,...)
     * @param {string} wid wall id
     * @returns {Observable<void>}
     */
    init(name: string, partnerName: string, confName: string = 'default'): Observable<boolean> {
        this.wallName = name;
        this.wallPartnerName = partnerName;
        this.confId = confName;

        this.intervals = {
            config: setInterval(() => this.getConfig(), 15 * 1000)
        };

        return this.loadWall(name, partnerName).map((wall) => {
            this._wall = wall;
            const ok = this._getCurrentConf();
            this.setIntervals();
            return ok;
        }).catch((err) => Observable.from([false]));
    }

    getPages(): wallPageConf[] {
        if (!this._currentWallConf) return;

        let conf = tools.clone(this._currentWallConf.pages);

        if (this._currentWallConf.frontPage) {
            const p = conf.find(p => p.pageId === this._currentWallConf.frontPage);
            if (p) return [p];
        };

        // remove pages with no duration
        conf = conf.filter(p => {
            if ( p.id === wallPage.FAVPOSTS) {
                if (this._wall.favoritePosts.length === 0) {
                    return false;
                } else if (!p.duration && !tools.deepGet(p, 'options', 'favInterval')) {
                    return false;
                }
            } else if (p.id === wallPage.FULLSCREEN) {
                // if not post, do not display
                if (!p.duration || !tools.deepGet(p, 'options', 'postId')) { return false;}
            } else {
                if (!p.duration) {
                    return false;
                }
            }

            return true;
        })
        /*
        // remove favorites if no favposts
        if (this._wall.favoritePosts.length === 0) {
            let i = conf.findIndex(p => p.id === wallPage.FAVPOSTS);
            while(i  >= 0) {
                conf.splice(i, 1);
                i = conf.findIndex(p => p.id === wallPage.FAVPOSTS);
            }
        }*/

        if (conf.length === 0) {
            conf.push({
                id: wallPage.COVERPAGE,
                pageId: `(wp)404`
            })
        }
        return conf;
    }


    getWallStyles() {
        if (!this._currentWallConf) return;
        // TODO: send the styles for the currentconf
        const styles: any = tools.clone(this._wall.styles || {});
        return styles.wallStyles;
    }

    getPageConfig(pageId: string | wallPage): wallPageConf {
        if ((<string>pageId).indexOf('(wp)') === 0) {
            const conf = this._currentWallConf.pages.find(p => p.pageId === pageId);
            if (!conf) {
                console.log('NO CONF FOR '+pageId);
                return
            }
            return tools.clone(conf);
        } else {
            return tools.clone(this._wall.defaultPageConf.find(p => p.id === <wallPage>pageId));
        }
    }

    getPageStyles(pageId: string | wallPage): {pageStyles?, postsStyles?} {
        let pageConf =  this.getPageConfig(pageId);

        const pageStyles = tools.clone(tools.deepGet(pageConf, 'styles') || {});
        const confStyles = tools.clone(tools.deepGet(this._currentWallConf,'styles') || {});
        const wallStyles = tools.clone(tools.deepGet(this._wall, 'styles') || {});
        wallStyles.pageStyles = wallStyles.wallStyles || {};
        wallStyles.postStyles = wallStyles.postStyles || {};
        delete wallStyles.wallStyles;

        return tools.deepAssign2(wallStyles, confStyles, pageStyles);
    }

    getOption(option: string, pageOrDefault?: wallPage | string | boolean) {
        if (typeof pageOrDefault === 'boolean') {
            if (pageOrDefault) {
                return tools.deepGet(this._wall, 'defaultOptions', option);
            } else {
                return tools.deepGet(this._currentWallConf, 'options', option) || tools.deepGet(this._wall, 'defaultOptions', option);
            }
        }
         else if (typeof pageOrDefault === 'string') {
            const myConf = this.getPageConfig(pageOrDefault);
            return tools.deepGet(myConf, 'options', option);
        } else {
            return tools.deepGet(this._currentWallConf, 'options', option) || tools.deepGet(this._wall, 'defaultOptions', option);
        }

    }

    get wall(): Wall {
        return this._wall.clone();
    }

    private _getCurrentConf(): boolean {
        if (this._wall.wallConfs.length > 0) {
            let i = this._wall.wallConfs.findIndex(c => c.id === this.confId);
            if (i < 0) {
                console.log(`Could not find configuration for '${this.confId}', using Default configuration`);
                i = 0;
            }
            this._currentWallConf =  this._wall.wallConfs[i];
            return true;
        } else {
            return false;
        }
    }
    /**
     * removes moderated posts from input list
     * @param {} posts
     * @returns {any[]}
     */
    private moderate(posts: wallJsonPost[]): wallJsonPost[] {
        return posts.filter(p => this._wall.isOkPost(p, this._currentWallConf));
    }

    private loadWall(name: string, partnerName: string): Observable<Wall> {
        return this.api.wall.search(name, partnerName).map(wall => {
            this._wall = wall;
            return this._wall;
        });
    }

    private getConfig(): void {
        // tmp while waiting for wall/get to be on defaultapi
        /*this.api.wall.search(this.wallName, this.wallPartnerName).subscribe(resWall => {
            const nw =  resWall;
            if (this._wall.version !== nw.version) {
                this._wall = nw;
                this.setIntervals()
                // TODO: moderate posts
            };
            this.configChanged.emit(true);
        });*/
        // TODO when wall/get on default api :
         this.api.wall.get(this._wall.id).subscribe((resWall: Wall) => {
             const nw: Wall =  resWall;
             console.log('get wall config')
             if (this._wall.version !== nw.version) {
                 const oldWall = this._wall;
                 this._wall = nw;

                 this._getCurrentConf();

                 // Do we have to hard refresh ?
                 if (this._currentWallConf.refreshDt && this.dateInitConfig < this._currentWallConf.refreshDt) {
                     window.location.reload(true);
                     return;
                 };

                 let doModerate = false;
                 if (JSON.stringify(this._wall.moderated) !== JSON.stringify(oldWall.moderated)) {
                     doModerate = true;
                     this._wall.needModeration = true;
                 }

                 if (doModerate) {
                     this._postStack = this.moderate(this._postStack);
                     this._posts = this.moderate(this._posts);
                 }

                 this.setIntervals();
                 console.log('new config: ', this._wall);
                 this.configChanged.emit(true);


             };
         }, (err) => {console.log(err);});
    }

    private setIntervals() {
        if (this.intervals.main) clearInterval(this.intervals.main);

        const mainDelay = (this.getOption('entryDelay') || 15) * 1000;
        this.intervals.main = setInterval(() => this.sendNewPost(), mainDelay)
    }

    private sendNewPost() {
        this.getNewPost().subscribe(p => {
            this.newPost.emit(p)
        });
    }

}
