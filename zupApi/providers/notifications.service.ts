import { Injectable, EventEmitter } from '@angular/core';

import { ZupApiService } from './zupApi.service';
// need to be access directly or is not instanced :
import  { PartnerService } from './partner.service';

import { Notification, ObjectsList } from '../models';

import { Observable } from 'rxjs'; // is required for typescript (even if not explicitly used)




@Injectable()
export class NotificationsService {

  counters: any[];
  countChange: EventEmitter<string> = new EventEmitter<string>();

  private _list: ObjectsList<Notification>;
  private maxNot = 99;

  constructor(private zupApi: ZupApiService, private partner: PartnerService) {
    partner.onInit().subscribe(
      () => this._countNotifications()
    );
  }

  /**
   *
   * @param count can be a number or an object
   * @returns
   */
  private toMax(count: any): any {
    if (typeof count === 'number') {
      if (count > this.maxNot) {
        return `${this.maxNot}+`;
      }
    } else {
      const returnObject = {};
      for (let p in count) {
        returnObject[p] = this.toMax(count[p]);
      }
      return returnObject;
    }
  }


  get count(): string {
    if (!this.counters) return;
    return this.toMax(this.counters.reduce((previous, current) => {
      return previous + current.count;
    }, 0));
  }

  get countByWidget() {
    if (!this.counters) return {};
    const alerts = {};
    this.counters.forEach(c => {
      const wid = c.widget_id;
      alerts[wid] = (alerts[wid] || 0) + c.count;
    })
    return this.toMax(alerts);
  }

  countByWidgetAndType(type: string) {
    if (!this.counters) return {};
    const alerts = {};
    this.counters.filter((c) => c.notification_type === type).forEach(c => {
      const wid = c.widget_id;
      alerts[wid] = (alerts[wid] || 0) + c.count;
    })
    return this.toMax(alerts);
  }

  assign(notif: Notification, memberId: string, message?: string, options?: {widgetId?: string, profileId?: string, postId?: string} ) {
    return this.zupApi.notifications.assign(notif, memberId, message, options).map(
        () => {
          notif.isClosed = true;
          this._countNotifications();
        }
    );
  }

  close(id) {
    return this.zupApi.notifications.close(id).map(
      () => {
        this._countNotifications();
      }
    );
  };

  countByType(widgetId?: string, toMax?: boolean) {
    if (!this.counters) return {};
    const types = {};
    this.counters.forEach(c => {
      if (widgetId && c.widget_id !== widgetId) return;
      const t = c.notification_type;
      types[t] = (types[t] || 0) + c.count;
    })

    return types;
  }

  countByFilter(filter: {widgetId?: string, widgetType?: number, alertType?: string}, toMax?: boolean): string | number {
    let sum = 0
    if (!this.counters) return toMax ? this.toMax(sum) : sum;
    const types = {};
    this.counters.filter(c => {
      const w = this.partner.getWidget(c.widget_id)
      if (!w) return false;
      if (filter.widgetId && c.widget_id !== filter.widgetId) return false;
      if (filter.widgetType && w.type !== filter.widgetType) return false;
      if (filter.alertType && c.notification_type !== filter.alertType) return false;

      return true;
    }).forEach(c => {
      sum += c.count;
    })
    return toMax ? this.toMax(sum) : sum;
  }

  getNotifications(limit?: number, options?: any) {
    return this.zupApi.notifications.list({limit: limit}, options).map(
      (res) => {
        this._list = res;
        return this._list;
      }
    );
  }

  private _getNotifications() {
    this.zupApi.notifications.list({}).subscribe(
      (res) => {
        this._list = res;
      }
    );
    this._countNotifications();
  };

  private _countNotifications() {
    this.zupApi.notifications.count().subscribe(
      res => {
        this.counters = res;
        const c = this.count;
        if (this.count) {
          this.countChange.emit(this.count.toString());
        } else {
          this.countChange.emit(undefined);
        }

      }
    );
  }
}
