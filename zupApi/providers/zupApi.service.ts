import { Injectable, Optional } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';

import { ApiAuthService } from './apiAuth.service';

import { ApiConfig } from './api.config';

import {
    ActionLog,
    ActionLogDetail,
    alertTypes,
    Campaign,
    FilterRules,
    fromLexical,
    Label,
    Lexical,
    Influencer,
    Member,
    NewLexical,
    Notification,
    ObjectsList,
    ObjectsListLC,
    Partner,
    Post,
    Profile,
    PublishAction,
    screenTypes,
    SimpleProfile,
    Task,
    TaskEvent,
    toActionLog,
    toLexical,
    //toPublishAction,
    View,
    Wall,
    wallJsonPost,
    Widget,
    WidgetContext
} from '../models';

import { InStorage , objectFormat } from './storage';

import { _attributionTypes } from '../listOfValues';

import { objectFormatTools, dates, tools } from '../misc/utils';


export enum INFLUENCER_ANALYTICS {
    NB_PUB = 1,
    FOLLOWERS = 2,
    FOLLOWINGS = 3,
    GENGER = 4,
    INTERESTS = 5,
    AGE = 6,
    LIVESIN = 7,
    LANGUAGE = 8,
    ENGAGEMENT_RATE = 9
}

@Injectable()
export class ZupApiService {
    private oauthKeys;
     localCache = new InStorage(this);
    private _config;

    constructor(private api: ApiAuthService, @Optional() config: ApiConfig) {
        if (config) {
            if (typeof config === 'function') {
                config = (<() => any>config)();
            }
            this.oauthKeys = config.oauth;
        }
        this._config = config;
    }

    getConfig(key: string): any {
        if (!this._config) return null;
        if (!key) return this._config;
        return this._config[key];
    }

    getNextPageFunction(maxResults: number = Infinity, maxQueries: number = 20): (ol: ObjectsList<any>) => Observable<ObjectsList<any>> {
        let cpt = 0;
        const getNextPage = (res: ObjectsList<any>): Observable<ObjectsList<any>> => {
            if (res.hasMore && res.length < maxResults && cpt++ < maxQueries) {
                return res.getMore().flatMap((resTmp: any[]) => getNextPage(res));
            } else {
                return Observable.from([res]);
            }
        }

        return getNextPage;
    }

    loadFiles(files: {id?, url}[]): Observable<{id, url}[]>{
        if (!files || files.length === 0) return Observable.of([]);
        const mediaUrl = '/tokens/medias/add';

        let  media =  files.filter(f => !!f.id);

        const mediaFiles = files.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));

        if (mediaFiles.length > 0) {
            const mediaMessage = {
                add: {
                    medias:  mediaFiles
                }
            };

            return this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return <{id, url}[]>media.concat(res.json().urls.map((u, i) => { return {id: res.json().media_ids[i], url: u}}));
            });

        } else {
            return Observable.of(<{id, url}[]>files)
        }
    }

    actions = {
        parent: <ZupApiService>this,
        api: this.api,
        likePost: function(
            postId: string,
            like: boolean = true,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                userId?: string,
                socialAccount?: string
            } = {}
        ): Observable<boolean>{
            const provider = objectFormatTools.getAttributionTypeFromId(postId);
            let url;

            switch (provider) {
                case  _attributionTypes.TWITTER:
                    url = '/publish/twitter/like';
                    break;
                case _attributionTypes.INSTAGRAM:
                    url = '/publish/instagram/like';
                    break;
                case _attributionTypes.FACEBOOK:
                    url = '/publish/facebook/like';
                    break;
                default:
                    return Observable.throw(`SOURCE_TYPE_UNINTERACTABLE(${objectFormatTools.getAttributionTypeFromId(postId)})`);
            }

            const message = {
                like: {
                    post_id: postId,
                    like: like,
                    notification_id: options.notificationId,
                    task_id: options.taskId,
                    screen: options.screen,
                    widget_id: options.widgetId,
                    author_id: options.userId,
                    xid: options.socialAccount
                }
            };
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] })
                .map(res => { return true; })
                .catch( err => {
                    if (err.toString() === 'Not found Token for partner') return Observable.throw(`NO_TOKEN(${_attributionTypes.detail(provider).text})`);
                    return Observable.throw(err);
                });
        },
        commentPost: function(
            postId: string,
            comment: string,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                userId?: string,
                socialAccount?: string,
                profileName?: string,
                files?: any[]
            } = {}
        ): Observable<boolean>{
            const provider = objectFormatTools.getAttributionTypeFromId(postId);
            let url;
            let message;
            switch (provider) {
                case  _attributionTypes.TWITTER:
                    if (!this.tweetIsOk({text: comment, files: options.files})) return Observable.throw('TWEET_TOO_LONG()');
                    if (!options.profileName) return Observable.throw('MISSING_FIELD(profile name)');
                    url = '/publish/twitter/create';
                    message = {
                        create: {
                            post_id: postId,
                            message: comment,
                            notification_id: options.notificationId,
                            task_id: options.taskId,
                            screen: options.screen,
                            widget_id: options.widgetId,
                            author_id: options.userId,
                            xid: options.socialAccount,
                            name: options.profileName
                        }
                    };
                    break;
                case  _attributionTypes.INSTAGRAM:
                    url = '/publish/instagram/comment';
                    message = {
                        comment: {
                            post_id: postId,
                            comment: comment,
                            notification_id: options.notificationId,
                            task_id: options.taskId,
                            screen: options.screen,
                            widget_id: options.widgetId,
                            author_id: options.userId,
                            xid: options.socialAccount
                        }
                    };
                    break;
                case  _attributionTypes.FACEBOOK:
                    url = '/publish/facebook/comment';
                    message = {
                        comment: {
                            post_id: postId,
                            comment: comment,
                            notification_id: options.notificationId,
                            task_id: options.taskId,
                            screen: options.screen,
                            widget_id: options.widgetId,
                            author_id: options.userId,
                            xid: options.socialAccount
                        }
                    };
                    break;
                default:
                    return Observable.throw(`SOURCE_TYPE_UNINTERACTABLE(${objectFormatTools.getAttributionTypeFromId(postId)})`);
            }


            return this.parent.loadFiles(options.files).flatMap((res) => {
                if (message.create) message.create.media_ids = res.map(f => f.id); // twitter only
                return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] })
                   .map(res => { return true; })
                   .catch( (err) => {
                       return Observable.throw(err);
                   });
            }) ;
        },
        retweetPost: function(
            postId: string,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                userId?: string,
                socialAccount?: string
            } = {}
        ): Observable<boolean>{
            const url = '/publish/twitter/repost';
            const message = {
                repost: {
                    post_id: postId,
                    repost: true,
                    notification_id: options.notificationId,
                    task_id: options.taskId,
                    screen: options.screen,
                    widget_id: options.widgetId,
                    xid: options.socialAccount
                }
            };
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => {
                return true;
            });

        },
        tweet: function(
            tweet: string,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                socialAccount?: string,
                userId?: string,
                files?: any[]
            } = {}
        ): Observable<boolean>{
            if (!this.tweetIsOk({text: tweet, files: options.files})) return Observable.throw('TWEET_TOO_LONG()');
            // TODO: chect tweet length (if url or without)
            const url = '/publish/twitter/create';
            const message: any = {
                create: {
                    message: tweet,
                    notification_id: options.notificationId,
                    task_id: options.taskId,
                    screen: options.screen,
                    widget_id: options.widgetId,
                    xid: options.socialAccount,
                    author_id: options.userId
                }
            };
            return this.parent.loadFiles(options.files).flatMap((res) => {
                message.create.media_ids = res.map(f => f.id);
                return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                    return true;
                });
            });
        },
        followUser: function(
            userId: string,
            follow: boolean = true,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                postId?: string,
                socialAccount?: string
            } = {}
        ): Observable<boolean>{
            let url;
            switch (objectFormatTools.getAttributionTypeFromId(userId)) {
                case  _attributionTypes.TWITTER:
                    url = '/publish/twitter/follow';
                    break;
                case  _attributionTypes.INSTAGRAM:
                    url = '/publish/instagram/follow';
                    break;
                default:
                    return Observable.throw(`SOURCE_TYPE_UNINTERACTABLE(${objectFormatTools.getAttributionTypeFromId(userId)})`);
            }

            const message = {
                follow: {
                    profile_id: userId,
                    follow: follow,
                    notification_id: options.notificationId,
                    task_id: options.taskId,
                    screen: options.screen,
                    widget_id: options.widgetId,
                    post_id: options.postId,
                    xid: options.socialAccount
                }
            };
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => { return true; });
        },
        clearAlert: function(alertId: string){
            return this.parent.notifications.close(alertId);
        },
        tweetIsOk(message: {text: string, files: any[]}): boolean {
            //replace links with 23 characters:
            if (message.text) return tools.tweetLength(message.text) <= 280;

            return !!message.files && message.files.length > 0;
        },
        mention: function(
            tweet: string,
            profileName: string,
            options: {
                notificationId?: string,
                taskId?: string,
                screen?: string,
                widgetId?: string,
                socialAccount?: string,
                userId?: string,
                files?: any[]
            } = {}
        ): Observable<boolean>{
            if (!this.tweetIsOk({text: tweet, files: options.files})) return Observable.throw('TWEET_TOO_LONG()');
            if (!profileName) return Observable.throw('MISSING_FIELD(profile name)');
            // TODO: chect tweet length (if url or without)
            const url = '/publish/twitter/create';
            const message: any = {
                create: {
                    message: tweet,
                    notification_id: options.notificationId,
                    task_id: options.taskId,
                    screen: options.screen,
                    widget_id: options.widgetId,
                    xid: options.socialAccount,
                    author_id: options.userId,
                    name: profileName,
                }
            };
            return this.parent.loadFiles(options.files).flatMap((res) => {
                message.create.media_ids = res.map(f => f.id);
                return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                    return true;
                });
            })
        },
        multiple(publishes: any[]): Observable<boolean> {
          const url = '/publish/multipublish';

          const todo = [];

          publishes.forEach(p => {
            const message = {
              multi_publish: {
                  like: p.like,
                  comment: p.comment,
                  follow: p.follow,
                  token_xid: p.tokenId,
                  message: p.message,
                  profile_ids: p.profileIds,
                  profile_names: p.profileNames,
                  medias: p.files ? p.files.map(f => f.substring(f.indexOf(',') + 1)) : undefined
              }
            }

            todo.push(this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}))
          })

          return Observable.forkJoin(...todo).map(() => true)


        }
        /*list: function(filters: any): Observable<Action[]>{
         // fake actions
         const fakeActions = [

         ];

         return Observable.from(fakeActions);
         }*/
    };

    campaigns = {
        api: this.api,
        parent: <ZupApiService>this,
        add: (campaign: Campaign): Observable<Campaign> => {
            const todos = [];
            campaign.publishActions.forEach(p => {
                const url = '/tokens/autopublish/add';

                const message: any = {
                    add: {
                        like: p.like,
                        comment: p.comment,
                        follow: p.follow,
                        mention: p.mention,
                        token_xid: p.tokenId,
                        message: p.message,
                        mention_message: p.mentionMessage,
                        profiles_label_id: campaign.label.id,
                        since: (p.since || 0) * 60,
                        once_for_parter: p.once,
                        rate_limit: p.rateLimit
                       // medias: p.files ? p.files.map(f => f.substring(f.indexOf(',') + 1)) : undefined
                    }
                };

                const mediaObservable = [];

                if ( p.files && p.files.length > 0) {
                    const mediaUrl = '/tokens/medias/add';

                    let  mediaIds =  p.files.filter(f => !!f.id).map(f => f.id);

                    const files = p.files.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));

                    if (files.length > 0) {
                        const mediaMessage = {
                            add: {
                                medias:  files
                            }
                        }


                        mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                            message.add.media_ids = mediaIds.concat(res.json().media_ids);
                                return '';
                            })
                        )
                    }

                }

                if ( p.mentionFiles && p.mentionFiles.length > 0) {
                    const mediaUrl = '/tokens/medias/add';

                    let  mediaIds =  p.mentionFiles.filter(f => !!f.id).map(f => f.id);

                    const files = p.mentionFiles.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));

                    if (files.length > 0) {
                        const mediaMessage = {
                            add: {
                                medias:  files
                            }
                        }


                        mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                            message.add.mention_media_ids = mediaIds.concat(res.json().media_ids);
                                return '';
                            })
                        )
                    }

                }

                if (mediaObservable.length === 0) mediaObservable.push(Observable.of(''));

                todos.push(
                    Observable.forkJoin(...mediaObservable).flatMap(() => {
                        return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']})
                            .map(res => {
                                return {
                                    id: res.json().widget_auto_publish.id,
                                    files: (res.json().widget_auto_publish.media_urls || []).map((u, i) => {
                                        return {id:res.json().widget_auto_publish.media_ids[i], url: u};
                                    }),
                                    mentionFiles: (res.json().widget_auto_publish.mention_media_urls || []).map((u, i) => {
                                        return {id:res.json().widget_auto_publish.mention_media_ids[i], url: u};
                                    })
                                };
                            })
                    })
                );
            });


           return Observable.forkJoin(...todos).map((res: {id: string, files: {id, url}[], mentionFiles: {id, url}[]}[])=> {
              campaign.publishActions.forEach((p, i) => {
                  p.id = res[i].id;
                  p.files = res[i].files;
                  p.mentionFiles = res[i].mentionFiles;
              });

               return campaign;
           });
        },
        delete: (campaign: Campaign, deleteLabel: boolean = true): Observable<boolean> => {
            const todos = [];
            campaign.publishActions.forEach(p => {
                if (!p.id) return; // when updating a campaign, som publishAction can be new
                const url = '/tokens/autopublish/delete';

                const message = {
                    delete: {id: p.id}
                }

                todos.push(this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}));
            });

            if (deleteLabel) todos.push(this.labels.delete(campaign.label.id));
            if (todos.length > 0) {
                return Observable.forkJoin(...todos).map(res => true);
            } else {
                return Observable.from([true]);
            }

        },
        list: (): Observable<any[]> => {

            const url = '/tokens/autopublish/list';
            const message = {
                list: {
                }
            }

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                return (res.json().items || []);
            });

        },
        listProfiles: (
            campaign: Campaign,
            options: {after?: number, before?: number} = {},
            //listOptions: {limit?: number, cursor?: string[]} = {}
        ): Observable<ObjectsListLC<Profile>> => {
            const wc = new WidgetContext();
            wc.allRange();
            if (options.after) wc.after = options.after;
            if (options.before) wc.before = options.before;

            const todos = campaign.publishActions.map(p => {
                return this.logs.list(wc, {campaignId: p.id}, {limit: 1000})
            })

            return Observable.forkJoin(...todos).map((res) => {
                //*
                //let profilesCreated: any[] = [];
                const profile2Log = new Map<string, ActionLog>();

                res.forEach((ol: ObjectsList<ActionLog>) => {
                    //profilesCreated = profilesCreated.concat(ol.list.map(l => {return {id: l.profileId, created: l.created}}));
                    ol.list.forEach(l => {
                        if (l.profileId && !profile2Log.has(l.profileId)) profile2Log.set(l.profileId, l);
                    })
                })

                //profilesCreated = tools.unduplicate(profilesCreated, (o) => o.id);
                const profileIds = Array.from(profile2Log.keys());

                return new ObjectsListLC<Profile>(
                    profileIds,
                    20,
                    (ids) => {
                        return this.profiles.getMultiple(ids).map(profiles => {
                            profiles.forEach(p => p.customValue = profile2Log.get(p.id));
                            return profiles;
                        })
                    }
                )
                //*/
            })

                /*
                .flatMap((res: ObjectsList<ActionLog>[]) => {
                const profile2Log = new Map<string, ActionLog>();


                res.forEach((ol: ObjectsList<ActionLog>) => {
                    ol.list.forEach( l => {
                        if (!profile2Log.has(l.profileId)) profile2Log.set(l.profileId, l);
                    });
                })

                return this.profiles.getMultiple(Array.from(profile2Log.keys())).map(profiles => {
                    profiles.forEach(p => p.customValue = profile2Log.get(p.id));

                    return new ObjectsListLC(
                        profiles,
                        20,
                        (profiles) => Observable.of(profiles)
                    )

                })
            });
            //*/
        },
        update:(campaign: Campaign, partnerId?: string): Observable<Campaign> => {
            const todos = [];
            campaign.publishActions.forEach(p => {
                const url = '/tokens/autopublish/update';

                const message: any = {
                    update: {
                        id: p.id,
                        like: p.like,
                        comment: p.comment,
                        follow: p.follow,
                        mention: p.mention,
                        token_xid: p.tokenId,
                        message: p.message,
                        mention_message: p.mentionMessage,
                        profiles_label_id: campaign.label.id,
                        since: (p.since || 0) * 60,
                        once_for_parter: p.once,
                        rate_limit: p.rateLimit,
                        partner_id: partnerId
                    }
                };

                const mediaObservable = [];
                mediaObservable.push(this.loadFiles(p.files).map((res) => {
                    message.update.media_ids = res.map(f => f.id);
                    message.update.media_urls = res.map(f => f.url);
                }));
                mediaObservable.push(this.loadFiles(p.mentionFiles).map((res) => {
                    message.update.mention_media_ids = res.map(f => f.id);
                    message.update.mention_media_urls = res.map(f => f.url);
                }));

                /*
                if ( p.files && p.files.length > 0) {
                    const mediaUrl = '/tokens/medias/add';

                    let  mediaIds =  p.files.filter(f => !!f.id).map(f => f.id);

                    const files = p.files.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));

                    if (files.length > 0) {
                        const mediaMessage = {
                            add: {
                                medias:  files
                            }
                        }


                        mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                                message.add.media_ids = mediaIds.concat(res.json().media_ids);
                                return '';
                            })
                        )
                    }

                }
*/
                /*
                if ( p.mentionFiles && p.mentionFiles.length > 0) {
                    const mediaUrl = '/tokens/medias/add';

                    let  mediaIds =  p.mentionFiles.filter(f => !!f.id).map(f => f.id);

                    const files = p.mentionFiles.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));

                    if (files.length > 0) {
                        const mediaMessage = {
                            add: {
                                medias:  files
                            }
                        }


                        mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                                message.add.mention_media_ids = mediaIds.concat(res.json().media_ids);
                                return '';
                            })
                        )
                    }

                }
                */

                if (mediaObservable.length === 0) mediaObservable.push(Observable.of(''));

                todos.push(
                    Observable.forkJoin(...mediaObservable).flatMap(() => {
                        return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']})
                            .map(res => {
                                return {
                                    id: res.json().widget_auto_publish.id,
                                    files: (res.json().widget_auto_publish.media_urls || []).map((u, i) => {
                                        return {id:res.json().widget_auto_publish.media_ids[i], url: u};
                                    }),
                                    mentionFiles: (res.json().widget_auto_publish.mention_media_urls || []).map((u, i) => {
                                        return {id:res.json().widget_auto_publish.mention_media_ids[i], url: u};
                                    })
                                };
                            })
                    })
                );
            });


            return Observable.forkJoin(...todos).map((res: {id: string, files: {id, url}[], mentionFiles: {id, url}[]}[])=> {
                campaign.publishActions.forEach((p, i) => {
                    p.id = res[i].id;
                    p.files = res[i].files;
                    p.mentionFiles = res[i].mentionFiles;
                });

                return campaign;
            });
        }
    }

    influencers = {
        api: this.api,
        parent: <ZupApiService>this,
        analytics: function(iid: string): Observable<{gender?, interests?, followings?, age?, language?, livesin?, engagementRate?, engagements?, nbPosts?}> {
            const url = '/influence/influencer_last_version';
            const message = {
                    influencer_id: iid,
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                const analytics = res.json().analytics;
                if (!analytics) return {};
                const influencerAnalytics: {gender?, interests?, followings?, age?,  language?, livesin?, engagementRate?, engagements?, nbPosts?} = {};
                // gender :
                const g = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.GENGER)
                if (g) influencerAnalytics.gender = g.analytics.map(a => { return {name: a.payload, count: a.score}});
                /*if (g) influencerAnalytics.gender = [
                    {name: 'F', count: (g.analytics[0].score) * 1000},
                    {name: 'M', count : (1 - g.analytics[0].score) * 1000},
                    {name: 'O', count: 0}
                ];*/

                // interests :
                const i = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.INTERESTS);
                if (i) influencerAnalytics.interests = i.analytics.map(a => { return {name: a.payload, count: Math.floor(a.score * 1000)}; });

                //followings:
                const f = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.FOLLOWINGS)
                if (f) influencerAnalytics.followings = f.analytics[0].score;

                //engagement:
                const e = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.ENGAGEMENT_RATE)
                if (e) influencerAnalytics.engagements = tools.deepGet(e.analytics.find(a => a.payload === 'total_engagements'),'score') || 0;
                if (e) influencerAnalytics.engagementRate = tools.deepGet(e.analytics.find(a => a.payload === 'engagement_rate'),'score') || 0;
                if (e) influencerAnalytics.nbPosts = tools.deepGet(e.analytics.find(a => a.payload === 'total_posts'),'score') || 0;

                //age:
                const age = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.AGE)
                if (age) influencerAnalytics.age = age.analytics.map(a => { return {name: a.payload, count: Math.floor(a.score * 1000)}; });
                // transform desc :


                //language
                const la = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.LANGUAGE)
                if (la) influencerAnalytics.language = la.analytics.map(a => { return {name: a.payload, count: Math.floor(a.score * 1000)}; });


                //linvesin:
                const l = analytics.find(a => a.analytic_type === INFLUENCER_ANALYTICS.LIVESIN)
                if (l) influencerAnalytics.livesin = l.analytics.map(a => { return {name: a.payload, count: Math.floor(a.score * 1000)}; });

                return influencerAnalytics;
            })
        },
        analyticHistory(iid: string, widgetContext: WidgetContext, analyticType: INFLUENCER_ANALYTICS): Observable<{date: number, analytics: any}[]>{
            const url = '/influence/analytic_historic';

            const message = {
                analytic_historic: widgetContext.getContext({extendedOptions: {
                    influencer_id: iid,
                    analytic_type: analyticType
                }})
            }

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                const ha = [];
                const histo = res.json().historic;
                histo.forEach(h => {
                    ha.push({date: h.version, analytics: h.analytics});
                })
                return ha.sort((a,b) => a.date - b.date);
            })
        },
        get: function(iid: string, onI4U?: boolean): Observable<Influencer> {
            //TODO: check in storage if we already have him

            const todos = [
                this.parent.profiles.getMultiple([iid], objectFormat.XL, {onI4U: onI4U}),
                this.analytics(iid)
            ]
            if (onI4U) {
                todos.push(this.parent.profiles.getMultiple([this._profileId]).map(res => res[0]));
            }

            return Observable.forkJoin(...todos).map(
                (res: [
                    Profile[],
                    {gender, interests, followings, age?, language?, livesin?, engagementRate?, engagements?, nbPosts?},
                    Profile
                    ]) => {
                    const I: Influencer = (<Profile>res[0][0]).toInfluencer();
                    I.communityGender = res[1].gender;
                    I.communityInterests = res[1].interests;
                    I.communityAge = res[1].age;
                    I.communityLanguage = res[1].language;
                    I.communityLivesIn = res[1].livesin;
                    I.followings = res[1].followings;
                    I.engagementRate = res[1].engagementRate;
                    I.engagements = res[1].engagements;
                    I.nbPosts = res[1].nbPosts;

                    if (res[2]) {
                        I.labels = res[2].labels;
                        I.leadScore = res[2].leadScore; // TODO: replace by influencer score
                    }
                    return I;
            })
        },
        multiProfiles: function(iid: string): Observable<Profile[]> {
            const url = '/influence/influencer_multi_profiles';
            const message = {
                profile_id: iid
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).flatMap(res => {
               return this.parent.profiles.getMultiple(res.json().profiles_id);
            }).catch((err) => {
                return this.parent.profiles.getMultiple([iid]);
            });
        }
    }

    /**
     * Exposes methodes to access and manage labels
     *
     * @Method add
     *
     *
     */
    labels = {
        api: this.api,
        parent: <ZupApiService>this,
        add: function(label: Label): Observable<Label> {
            const url = 'label/add';

            const message = {
                add: label.forExport()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => {
                return new Label(res.json().add);
            });
        },
        addProfileToLabel: function(
            userId: string | string[],
            labelId: string,
            options: {widgetId?: string, screen?: string, postId?: string} = {}
        ){
            const url = 'label/profile/include';

            const message: any = {
                include: {
                    label_id: labelId,
                    widget_id: options.widgetId,
                    screen: options.screen,
                    postId: options.postId
                }
            };

            if (typeof userId === 'string') {
                message.include.profile_id = userId;
            } else {
                message.include.profile_ids = userId;
            }
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => {
                const label = this.parent.localCache.getJsonObjects([labelId], 'label')[0];
                if (label) label.count++;

                const profile = this.parent.localCache.getJsonObjects([userId], 'profile')[0];
                if (profile) {
                    profile.profile_label_ids = profile.profile_label_ids || [];
                    profile.profile_label_ids.push(labelId);
                }
            });
        },
        addProfilesToLabel: function(profilIds: string[], labelId: string): Observable<boolean> {
            return this.addProfileToLabel(profilIds, labelId);
        },
        delete: function(id: string): Observable<boolean> {
            return this.remove(id);
        },
        get: function(id: string): Observable<Label> {
            const ls = this.parent.localCache.getJsonObjects([id], 'label');
            if (ls.length === 0 ) {
                return Observable.throw({message: 'Unckown Label', noParse: true})
            } else {
                return Observable.of(new Label(ls[0]));
            }
        },
        list: function(
            listOptions: {limit?: number, cursor?: string} = {}
        ): Observable<ObjectsList<Label>>{
            const url = 'label/list';

            const message = {
                list: {
                    limit: listOptions.limit || 100,
                    cursor: listOptions.cursor
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi' }).map(res => {
                this.parent.localCache.addObjects(res.json().list.labels, 'label');

                return new ObjectsList<Label>(
                    res.json().list.labels.map((l) => new Label(l)),
                    res.json().list.cursor
                        ? this.list({limit: listOptions.limit, cursor: res.json().label.cursor})
                        : undefined
                );
            });
        },
        // to be improved
        listAll: function(): Observable<ObjectsList<Label>>{
            const url = 'label/list';

            const message = {
                list: {
                    limit: 1000
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi' }).map(res => {
                this.parent.localCache.addObjects(res.json().list.labels, 'label');
                return new ObjectsList<Label>(
                    res.json().list.labels.map((l) => new Label(l))
                );
            }).catch(err => {
                console.log(err.toString());
                return Observable.of(new ObjectsList<Label>(
                    []
                ));
            });
        },
        profileLabels: function(userId: string): Observable<Label[]>{
            const url = 'label/profile/list';

            const message = {
                list: {
                    profile_id: userId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi' })
                .map(res => res.json().list.profile_labels.map(l => new Label(l)));
        },
        profilesList: function(
            labelId: string,
            limitOptions: {limit?: number, cursor?: string} = { limit: 50},
            options: {onlyIds?: boolean, simpleProfiles?: boolean, onI4U?: boolean} = {}
        ): Observable<ObjectsList<Profile | string>> {
            const url = '/label/profiles';
            const message = {
                list: {
                    limit: limitOptions.limit,
                    cursor: limitOptions.cursor,
                    label_id: labelId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', noCache: true}).map( (res) => {
                const jsonRes = res.json();

                const getMore = jsonRes.list.cursor ?
                    this.profilesList(labelId, {limit: limitOptions.limit, cursor: jsonRes.list.cursor}, options) : undefined;
                const profiles =  tools.unduplicate(jsonRes.list.profiles, (p) => p.id);
                const profilesIds =  profiles.map(p => p.id);

                if (options.onlyIds) {
                    return new ObjectsList(
                        profilesIds,
                        getMore,
                        {unduplicateFunction: (id, ids) => ids.indexOf(id) < 0}
                    );

                } else if (options.simpleProfiles) {
                    return new ObjectsList(
                        profiles.map(p => new Profile(p)),
                        getMore,
                        {unduplicateFunction: (p, ps) => ps.findIndex((u) => u.id === p.id) < 0}
                    );
                } else {
                    return new ObjectsListLC(
                        profilesIds,
                        10,
                        // if there is a limitoption.cursor, it is a "getMore" call, no need to look for details now
                        limitOptions.cursor ? null : (ids) => this.parent.profiles.getMultiple(ids, objectFormat.L, {onI4U: options.onI4U}),
                        getMore,
                        {unduplicateFunction: (id, ids) => ids.indexOf(id) < 0}
                    );
                }

            });
        },
        remove: function(id: string): Observable<boolean> {
            const url = 'label/remove';

            const message = {
                remove: {
                    label_id: id
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => true);
        },
        removeProfileFromLabel: function(userId: string, labelId: string): Observable<boolean> {
            const url = 'label/profile/exclude';

            const message = {
                remove: {
                    profile_id: userId,
                    label_id: labelId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => {
                const label = this.parent.localCache.getJsonObjects([labelId], 'label')[0];
                if (label) label.count--;

                const profile = this.parent.localCache.getJsonObjects([userId], 'profile')[0];
                if (profile) {
                    profile.profile_label_ids = profile.profile_label_ids || [];
                    profile.profile_label_ids.splice(profile.profile_label_ids.indexOf(labelId), 1);
                }
            });
        },
        removeProfilesFromLabel: function(userIds: string[], labelId: string): Observable<boolean> {
            const todos = [];
            userIds.forEach(uid => {
                todos.push(this.removeProfileFromLabel(uid, labelId, {}));
            });

            return tools.forkJoin2FlatMap(todos).map(res => true);
        },
        update: function(label: Label): Observable<boolean> {
            const url = 'label/update';

            const message = {
                update: label.forExport()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => true);
        }

    };

    lexical = {
        api: this.api,
        self: <ZupApiService>this,
        delete: function(id: string): Observable<boolean> {
            return this.delete_multiple([id]);
        },
        delete_multiple: function(ids: string[]): Observable<boolean> {
            const url = '/lexical/delete';
            const message = {
                lexicals: ids
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return true;
            });
        },
        get: function(ids: string[]): Observable<Lexical[]> {
            const url = '/lexical/get';

            const message = {
                lexicals: ids
            };

            return this.api.call(url, message, {apiid: 'b2bapi', noCache: true}).map(res => {
                // todo : handle errors
                return res.json().lexicals.ok;
            });
        },
        list: function(category?: string): Observable<Lexical[]> {
            const url = '/lexical/list';

            const message = {
                lexical_category: category
            };
            return this.api.call(url, message, {apiid: 'b2bapi', noCache: true}).map(res => {
                const lexicals = res.json().lexicals.lexicals;
                return lexicals.map(l => toLexical(l))
            });
        },
        set: function(lexicals: NewLexical[]): Observable<Lexical[]>{
            const url = 'lexical/create';

            const message = {
                lexicals: lexicals.map(l => fromLexical(l))
            }

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return res.json().lexicals.map(l => toLexical(l));
            })
        },
        create: function(lexical: NewLexical): Observable<Lexical> {
            return this.set([lexical]).map(res => res[0]);
        },
        update: function(lexicals: Lexical[]): Observable<boolean> {

            const url = '/lexical/update';
            const message = {
                lexicals: lexicals.map(l => fromLexical(l))
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return true;
            });
        }
    };

    logs = {
        api: this.api,
        parent: <ZupApiService>this,
        get(actionLog: ActionLog): Observable<ActionLogDetail> {
            const pids = [
                actionLog.publishAccountId,
                actionLog.memberId,
                actionLog.profileId
            ].filter(s => !!s);

            const returnLog = <ActionLogDetail>Object.assign({}, actionLog);
            return this.parent.profiles.getMultiple(pids).map((profiles: Profile[]) => {
                const P: Map<string, Profile> = new Map<string, Profile>();
                profiles.forEach(p => P.set(p.id, p));
                if (actionLog.publishAccountId) { returnLog.publishAccount = P.get(actionLog.publishAccountId); }
                if (actionLog.memberId) { returnLog.member = P.get(actionLog.memberId); }
                if (actionLog.profileId) { returnLog.profile = P.get(actionLog.profileId); }
                return returnLog;
            });
        },
        list(
            widgetContext: WidgetContext,
            filters: {
                memberId?: string,
                profileId?: string,
                postId?: string,
                campaignId?: string,
                token?: string,
                publishType?: string,
            } = {},
            listOptions: {limit?: number, cursor?: string} = {}
        ): Observable<ObjectsList<ActionLog>> {
            const url = 'history_log/list';
            const message = {
                list: widgetContext.getContext({
                    extendedOptions: {
                        limit: listOptions.limit,
                        cursor: listOptions.cursor,
                        by_member: filters.memberId,
                        by_token: filters.token,
                        by_campaign_id: filters.campaignId,
                        on_profile: filters.profileId,
                        //on_post: filters.postId,
                        type: filters.publishType
                    }
                })
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                /*
                 {
                 ..ApiResponse
                 ..list: {
                 ....history_log_entries:[]HistoryLogEntries
                 ....tasksk:[]Task;
                 ....notifications:[]Notification;
                 ....profiles:[]Profile
                 ..}
                 }
                 */
                this.parent.localCache.addObjects(res.json().list.profiles.filter(p => !!p.id), 'profile', objectFormat.S);
                this.parent.localCache.addObjects(res.json().list.posts, 'post', objectFormat.S);
                this.parent.localCache.addObjects(res.json().list.notifications, 'notification', objectFormat.S);
                this.parent.localCache.addObjects(res.json().list.tasks, 'task', objectFormat.S);


                const getMore = res.json().list.cursor ?
                    this.list(widgetContext, filters, Object.assign(listOptions, {cursor:  res.json().list.cursor})) :
                    undefined;

                let list = res.json().list.history_log_entries.map(l => toActionLog(l));

                if (filters.postId) {
                    list = list.filter((l: ActionLog) => !l.postId || l.postId === filters.postId);
                }

                return new ObjectsList(list, getMore);

            });
        },
        listDetail(
            widgetContext: WidgetContext,
            options: {memberId?: string, profileId?: string, postId?: string} = {},
            listOptions: {limit?: number, cursor?: string} = {}
        ): Observable<ObjectsList<ActionLogDetail>> {
            const url = 'history_log/list';
            const message = {
                list: widgetContext.getContext({
                    extendedOptions: {
                        limit: listOptions.limit,
                        cursor: listOptions.cursor,
                        by_member: options.memberId,
                        on_profile: options.profileId,
                        on_post: options.postId,
                    }
                })
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap(res => {

                /*
                 {
                 ..ApiResponse
                 ..list: {
                 ....history_log_entries:[]HistoryLogEntries
                 ....tasksk:[]Task;
                 ....notifications:[]Notification;
                 ....profiles:[]Profile
                 ..}
                 }
                 */
                this.parent.localCache.addObjects(res.json().list.profiles, 'profile');
                this.parent.localCache.addObjects(res.json().list.posts, 'post');
                this.parent.localCache.addObjects(res.json().list.notifications, 'notification');
                this.parent.localCache.addObjects(res.json().list.tasks, 'task');

                const profileIds = tools.unduplicate(
                    (res.json().list.posts || []).map(p => p.profile_id)
                        .concat((res.json().list.history_log_entries || []).map(h => h.member_id).filter(m => m !== 'auto'))
                        .concat((res.json().list.history_log_entries || []).map(h => h.by_token))
                        .concat((res.json().list.history_log_entries || []).map(h => h.profile_id))
                ).filter(id => !!id);

                return this.parent.profiles
                    .getMultiple(profileIds, objectFormat.XS)
                    .map(() => {
                        const getMore = res.json().list.cursor ?
                            this.listDetail(widgetContext, options, Object.assign(listOptions, {cursor:  res.json().list.cursor})) :
                            undefined;

                        return new ObjectsList(res.json().list.history_log_entries.map(l => {
                            const al: ActionLogDetail = <ActionLogDetail>toActionLog(l);

                            al.member = al.memberId && <Profile>this.parent.localCache.profileObject(al.memberId);
                            al.profile = al.profileId && <Profile>this.parent.localCache.profileObject(al.profileId);
                            al.publishAccount = al.publishAccountId && <Profile>this.parent.localCache.profileObject(al.publishAccountId);
                            al.task = al.taskId && <Task>this.parent.localCache.taskObject(al.taskId)

                            return al;

                        }), getMore);
                    });

            });
        }
    };
    /**
     *
     * Exposes member functions : invite, list, login, logout, remove
     */
    members = {
        api: this.api,
        parent: <ZupApiService>this,
        invite(mail: string): Observable<boolean> {

            const url = '/member/add';
            const message = {
                add: {
                    member_id: '0aL4mg(m)216'
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return true;
            });
        },
        list(): Observable<Member[]> {
            const url = '/member/list';

            const message = {
                list: {
                    limit: 50
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                this.parent.localCache.addObjects(res.json().list.members, 'profile');
                return res.json().list.members;
            });
        },
        /*
         login(username?: string, password?: string, rememberMe: boolean = false) {
         return this.api.login(username, password, rememberMe)
         },
         logout() {
         return this.api.logout();
         },
         */
        remove(id: string): Observable<boolean> {
            const url = '/member/remove';
            const message = {
                remove: {
                    member_id: id
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return true;
            });
        }

    };

    misc = {
        api: this.api,
        parent: <ZupApiService>this,
        uploadPhoto: function(data: string): Observable<string> {
            if (data.indexOf('data:image') !== 0) return Observable.throw('BAD_FORMAT(image)')

            const apiurl = 'photos/upload';

            const message = {
                    photo_upload: {
                        share: true,
                        photo: data.substring(data.indexOf(';base64,') + 8)
                    },
                };

            return this.api.call(apiurl, message, {apiid: 'coldapi', rights: ['write']}).map(res => JSON.parse(res.photo_upload.upload_url).url);
        },
        listPhoto: function(): Observable<any> {
            const apiurl = 'photos/list';
            const message = {
                    photo_list: {
                        num_per_page: 5
                    }
                };

            return this.api.call(apiurl, message, {apiid: 'coldapi'}).map(res => console.log(res.json().photo_list.photos.map(p => JSON.parse(p.url))));
        },
        mstores: {
            api: this.api,
            parent: <ZupApiService>this,
            list: function(): Observable<any>{
                const url = 'mstores/list'
                const message = {
                    mstore_list: {}
                }

                return this.api.call(url, message, {apiid: 'coldapi'}).map(res => {
                    const mstores = {};

                    (res.json().mstore_list.mstore_list || []).forEach((m: {key: string, value: string}) => {
                        mstores[m.key] = JSON.parse(m.value);
                    });

                });
            },
            get: function(key: string): Observable<any> {
                const url = 'mstores/get';
                const message = {
                    mstore_get: {
                        key: key
                    }
                };
                return this.api.call(url, message, {apiid: 'coldapi'}).map(res => {
                    return JSON.parse(res.json().mstore_get.value);
                }).catch(err => {
                    if (err.toString() === 'DataNotFound') {
                        return Observable.from([undefined]);
                    } else {
                        return Observable.throw(err);
                    }
                });
            },
            put: function(key: string, value: any): Observable<boolean> {
                const url = 'mstores/put'
                const message = {
                    mstore_put: {
                        key: key,
                        value: JSON.stringify(value)
                    }
                };
                return this.api.call(url, message, {apiid: 'coldapi', rights: ['write']}).map(res => true);
            },
            delete: function(key: string): Observable<boolean> {
                const url = 'mstores/delete';
                const message = {
                    mstore_delete: {
                        key: key
                    }
                };
                return this.api.call(url, message, {apiid: 'coldapi', rights: ['write']}).map(res => true);
            }
        }
    };

    notifications = {
        api: this.api,
        parent: <ZupApiService>this,
        assign: function(
            notification: Notification,
            memberId: string,
            assignmentMessage?: string,
            options?: {widgetId?: string, profileId?: string, postId?: string}
        ): Observable<boolean>{
            const url = '/notifications/assign';
            const message = {
                assign: {
                    notification_id: notification.id,
                    assign_to: memberId,
                    assignment_message: assignmentMessage,
                    widget_id: options.widgetId,
                    profile_id: options.profileId,
                    post_id: options.postId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).flatMap(() => {
                if (notification.isClosed) return Observable.from([true]);
                // we can reassign a closed notification (via tasks workflow)
                return this.close(notification.id);
            });

        },
        close: function(id: string): Observable<boolean> {
            const url = '/notifications/close';
            const message = {
                close: {
                    notification_id: id
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map(res => {
                return true;
            });
        },
        closeMultiple: function(notificationIds: string[]): Observable<boolean> {
            const url = 'notifications/close_multi';
            if (notificationIds.length === 0) {
                return Observable.from([true]);
            }

            const message = {
                close_multi: {
                    ids: notificationIds
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => true);

            /*const message = {
             close_multi: {
             ids: notificationIds.slice(0, 10)
             }
             };
             let returnO = this.api.call(url, message, {apiid: 'b2bapi'});

             for (let i = 1; i <= Math.floor(notificationIds.length - 1 / 10); i++) {
             const tmpIds  = notificationIds.slice(i * 10, (i + 1) * 10);
             if (tmpIds.length > 0) {
             const messageSup = {
             close_multi: {
             ids: tmpIds
             }
             };
             returnO = returnO.flatMap(() => this.api.call(url, messageSup, {apiid: 'b2bapi'}));
             };
             };

             return returnO.map(() => true);*/
        },
        count: function(after?: number): Observable<any[]> {
            const url = '/notifications/counters';
            const message = {};

            return this.api.call(url, message, {apiid: 'b2bapi' }).map(res => {
                return res.json().counters.widgets;
            });
        },
        get: function(id: string): Observable<any> {
            const url = '/notifications/get';
            const message = {
                get: {
                    notification_id: id
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap( res => {
                const notification = res.json().get.notification;

                const pid =  res.json().get.notification.post;

                const getUser = this.parent.profiles.get(res.json().get.profile.id);
                const getPost = this.parent.posts.getMultiple([pid]);

                return Observable.forkJoin(getUser, getPost).map(resDetail => {
                    return new Notification(notification, <Profile>resDetail[0], <Post>resDetail[1][0]);
                });
            });
        },
        list: function(
            listOptions: {limit?: number, cursor?: string},
            options?: {widgetId?: string, type?: string, after?: number, before?: number, isClosed?: boolean, widgetType?: number}
        ): Observable<any> {
            const url = '/notifications/list';
            const message: any = {
                list: {
                    limit: listOptions.limit || 10,
                    cursor: listOptions.cursor,
                    after: options && options.after,
                    before: options && options.before,
                    type: options && options.type,
                    widget_id: options && options.widgetId,
                    widget_type: options && options.widgetType
                }
            };

            if (options && options.isClosed) {
                message.list.hide_opened_notifications = true;
            } else {
                message.list.hide_closed_notifications = true;
            }

            return this.api.call(url, message, {apiid: 'b2bapi' })
            // tmp: we need to get the posts attribution :
                .flatMap(resNotifications => {
                    const postIds = resNotifications.json().list.notifications.map( n => n.post);

                    //const profiles = resNotifications.json().list.profiles.map(p => p.id);

                    this.parent.localCache.addObjects(resNotifications.json().list.profiles, 'profile', objectFormat.S);
                    const profiles = resNotifications.json().list.profiles.map(p => this.parent.localCache.profileObject(p.id, objectFormat.S));

                    return Observable.forkJoin(
                        //this.parent.profiles.getMultiple(profiles),
                        this.parent.posts.getMultiple(postIds)
                    ).map( resDetail => {
                        const profilesMap: Map<string, Profile> = new Map<string, Profile>();
                        profiles.forEach(p => {profilesMap.set(p.id, p); });


                        const resPosts = <Post[]>resDetail[0];
                        const postsMap: Map<string, Post> = new Map<string, Post>();
                        resPosts.forEach(p => {postsMap.set(p.id, p); });

                        const notifications: Notification[] = [];

                        resNotifications.json().list.notifications.forEach( n => {
                            if (!n.post) { console.log('NOTIFICATION WITHOUT POSTID'); return; }
                            const post = postsMap.get(n.post);
                            const profile = profilesMap.get(n.post_author);
                            if (!profile && !post) {
                                console.log('notification in error :', n);
                                return;
                            }
                            notifications.push(new Notification(n, profile, post));
                        })

                        return new ObjectsList(
                            notifications,
                            resNotifications.json().list.cursor ?
                                this.list({limit: listOptions.limit, cursor: resNotifications.json().list.cursor}, options) : undefined
                        );
                    });
                }).catch((err) => {console.log(err)
                    return Observable.from([new ObjectsList([], undefined)]);
                });
        },
        setLimits: function(limits: any): Observable<any> {
            let setOrDelete;
            if (limits.lexical.length > 0) {
                setOrDelete = this.parent.lexical.set({
                    name: 'word_alert_lexical_name',
                    values: limits.lexical.toLowerCase().split(/\s*,\s*/),
                    mult: 1500
                });
            } else {
                setOrDelete = this.parent.lexical.delete('word_alert_lexical_name');
            }
            return Observable.forkJoin(
                setOrDelete
            ).map( () => {
                return true;
            });
        },
        // //////////////////////////////////////////////////// //
        // ///////////       SUBSCRIPTIONS      /////////////// //
        // //////////////////////////////////////////////////// //
        addSubscribtion: function(emailOrPhone: string, type: string = 'M', owner?: string): Observable<boolean> {
            const url = '/notifications/tokens/add';

            const message = {
                notification_token: {
                    type: type,
                    token: emailOrPhone,
                    owner: owner
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => true);
        },
        listSubscriptions: function(): Observable<any[]> {
            const url = '/notifications/tokens/list';

            return this.api.call(url, {}, {apiid: 'b2bapi'}).map(res => res.json().list.tokens || [])
                .catch(err => Observable.from([[]]));
        },
        toggleNotification: function(emailOrPhone: string, notificationType: string, enable: boolean =  true): Observable<boolean> {
            const url = '/notifications/tokens/toggle';

            const message = {
                toggle: {
                    token: emailOrPhone,
                    notification_type: notificationType,
                    enable: enable
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => true);
        }

    };

    partner = {
        api: this.api,
        parent: <ZupApiService>this,
        addInstagramToken: function(token): Observable<Profile>{
            // TODO : tester si pas deja un compte twitter
            const url = '/tokens/insert_ig';
            const message = {
                insert: {
                    access_token: token,
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] })
                .map( res => {
                    if (res.json().insert.profile) {
                        return new Profile(res.json().insert.profile);
                    } else if (res.json().insert.profiles) {
                        return new Profile(res.json().insert.profiles[0]);
                    }
                });
        },
        addOauthToken: function(token, verifier, provider): Observable<any>{
            const url = 'tokens/insert_oauth';
            const message = {
                insert: {
                    oauth_token: token,
                    oauth_verifier: verifier,
                    provider: provider
                }
            };
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( res => {
                if (res.json().insert.profile) {
                    return new Profile(res.json().insert.profile);
                } else if (res.json().insert.profiles) {
                    return new Profile(res.json().insert.profiles[0]);
                }
            });
        },
        addTwitterToken: function(token, secret): Observable<Profile>{
            // TODO : tester si pas deja un compte twitter
            const url = '/tokens/insert_tw';
            const message = {
                insert: {
                    access_token: token,
                    access_secret: secret
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( res => {
                if (res.json().insert.profile) {
                    return new Profile(res.json().insert.profile);
                } else if (res.json().insert.profiles) {
                    return new Profile(res.json().insert.profiles[0]);
                }
            });
        },
        addFacebookToken: function(token): Observable<Profile[]>{
            // TODO : tester si pas deja un compte twitter
            const url = '/tokens/insert_fb';
            const message = {
                insert: {
                    access_token: token
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( res => {
                return res.json().insert.profiles.map((p: any) => new Profile(p));
            }).catch(err => {
                if (err.toString() === 'No Any Page Found') {
                    return Observable.throw('FACEBOOK_PAGE_NEEDED()');
                } else {
                    return Observable.throw(err);
                }
            });
        },
        deleteToken: function(tokenUserId: string){
            const url = '/tokens/delete';
            const message = {
                delete: {
                    xid: tokenUserId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( res => { return true; });
        },
        get(): Observable<any> {
            const url = '/partner/get';
            const getDetail = this.api.call(url, { }, {apiid: 'b2bapi' }).map(res => {
                return res.json().partner;
            });

            return getDetail;

        },
        getOAuthUrl(provider: string): Observable<string> {
            if (provider === _attributionTypes.INSTAGRAM) {
                const redirectUrl = encodeURIComponent( window.location.origin + this.parent.oauthKeys.INSTAGRAM.REDIRECT);
                const IG_CLIENTID = this.parent.oauthKeys.INSTAGRAM.CLIENT_ID;
                const url = `https://api.instagram.com/oauth/authorize/?client_id=${IG_CLIENTID}&redirect_uri=${redirectUrl}&response_type=token&scope=public_content+likes+comments+relationships`;
                return Observable.from([url]);
            }

            var url = '/auth/oauth/urls',
                message = {
                    providers: [{
                        provider: provider,
                        callback_url: window.location.origin + '/callback/tw'
                    }]
                };

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                return res.json().auth_url[0].redirect_url;
            });
        },
        listTokens: function(): Observable<any[]>{
            const url = '/tokens/list';
            const message = {};

            return this.api.call(url, message, {apiid: 'b2bapi' })
                .map( res => { return res.json().tokens.profiles.map( p => {
                    this.parent.localCache.addObject(p, 'profile');
                    return new Profile(p);
                }).filter((p: Profile) => p.attributionKind !== _attributionTypes.FACEBOOK); })
                .catch((err) => { Observable.from([[]]); });
        },
        setAccessToken(data: any): Observable<Profile> {

            switch (data.provider) {
                case _attributionTypes.TWITTER:
                    return this.addOauthToken(data.oauth_token, data.oauth_verifier, data.provider);
                case  _attributionTypes.INSTAGRAM:
                    return this.addInstagramToken(data.access_token);
                default:
                    return Observable.throw('PROVIDER_UNKNOWN()');
            }
        },
        update(partner: Partner): Observable<boolean> {
            const url = '/partner/update';
            const message = {
                partner: {
                    description: partner.description,
                    name: partner.name,
                    metas: partner.metas
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {return true; });
        },
        widgetsList(limit?: number): Observable<Widget[]> {
            return this.parent.widget.list(limit);
        }
    };

    places = {
        api: this.api,
        parent: <ZupApiService>this,
        exploreZone(zone: {e: number, w: number, s: number, n: number}): Observable<boolean> {
            const url = 'places/search';
            const message = {
                box: zone
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']});
        },
        exclude(placeId: string, widgetId: string[], ban: boolean = true): Observable<boolean> {
            const url = 'widget/filters/update';
            const message: any = {
                filter: {
                    widget_id: widgetId,
                }
            };
            if (ban) {
                message.filter.filter_places = [placeId];
            } else {
                message.filter.unfilter_places = [placeId];
            }

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
        },
        getMultiple(ids: string[]): Observable<SimpleProfile[]> {
            if (ids.length === 0) return Observable.of([])
            const url = 'places'
            const message = {
                    place_getmulti: {
                        ids: ids
                    }
            };

            return this.api.call(url, message, { urlParameters: 'v=2'}).map(res => {
                return res.json().place_getmulti.ok;
            });
        },
        listExcluded(widgetId: string): Observable<Profile[]> {
            const url = 'widget/filters'
            const message = {
                filter_get: {
                    widget_id: widgetId
                }
            }
            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap( res => {
                    const placeIds = (res.json().filter_get.places || []);
                    return this.parent.profiles.loadToCache(placeIds)
                        .map(() => {
                            return this.parent.localStorage.getProfiles(placeIds);
                        });
               }
            ).catch(err => {
                return Observable.of([]);
            });;
        },
        searchInZone: function(
            zone: {e: number, w: number, s: number, n: number},
            listOptions: {limit?: number, cursor?: number} = {limit: 50}
        ): Observable<ObjectsList<Profile>> {
            const url = 'places';
            const message = {
                zone: zone,
                limit: listOptions.limit,
                skip: listOptions.cursor
            };

            return this.api.call(url, message, {apiid: 'hotapi', urlParameters: 'v=2'}).flatMap(res => {
                this.parent.localCache.addObjects(
                    res.json().places.map(p => { p.isError = false; return p; }),
                    'profile'
                );

                const getMore = res.json().cursor ?
                    this.searchInZone(zone, {
                        limit: listOptions.limit,
                        cursor: res.json().cursor
                    }) : undefined
                return this.parent.localCache.getProfiles(res.json().places.map(p => p.id), true)
                    .map((resProfiles: Profile[]) => {
                        return new ObjectsList<Profile>(resProfiles, getMore);
                    });
            });
        }
    };

    posts = {
        api: this.api,
        parent: <ZupApiService>this,
        list: function(
            widgetContext: WidgetContext,
            options: {
                author_id?: string,
                source?: string,
                tag?: string,
                terms?: string[],
                lang?: string,
                post_label_id?: string,
                engagement_rate_more_than?: number,
                sentiment_value?: number, // 1: postif, 2 negatif,
                order_by?: string,
                xscore_more_than?: number
            } = {},
            listOptions: {limit?: number, cursor?: string} = { limit: 5}
        ): Observable<{posts: ObjectsList<Post>, total: number}> {
            if (widgetContext === null) {
                widgetContext = new WidgetContext(null, {after: 0, before: dates.toZIFromDate(new Date())})
            }
            const url = 'post/list';

            const message = {
                list: widgetContext.getContext({extendedOptions: Object.assign(
                    {},
                    listOptions,
                    options
                    )}
                )
            };

            // TMP: compatibility between widget_id and widget_ids:
            if (message.list.widget_id) {
                message.list.widget_ids = [message.list.widget_id];
            }

            const callOptions: any = {apiid: 'b2bapi'};
            if (widgetContext.before !== dates.toZIFromDate(dates.today.to)) {
              callOptions.cache = true;
            }
            return this.api.call(url, message, callOptions).map(res => {
                const postIds = tools.unduplicate((res.json().list.posts || []).map(p => p.id));
                const getMore = res.json().list.cursor ?
                    this.list(widgetContext, options, Object.assign(listOptions, {cursor: res.json().list.cursor})).map(r => r.posts) : undefined;

                // Add posts to localCache
                this.parent.localCache.addPostList(res.json().list);

                return {
                  posts: new ObjectsList(
                    postIds.map(pid => this.parent.localCache.postObject(pid, objectFormat.S)),
                    getMore,
                    {unduplicateFunction:(p, ps) => ps.findIndex(o => o.id === p.id) < 0}
                  ),
                  total: res.json().list.total
                };

            });

            /*
            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap(res => {
                // tmp : missing profiles
                const userIds = res.json().list.posts.map(p => p.authorid);
                if (userIds.length === 0) return Observable.from([new ObjectsList([])]);

                const getMore = res.json().list.cursor ?
                    this.list(widgetContext, options, Object.assign(listOptions, {cursor: res.json().list.cursor})) : undefined;

                // Add posts to localCache
                this.parent.localCache.addObjects(res.json().list.posts, 'post');

                // get profiles
                return this.parent.profiles.getMultiple(userIds)
                // now we can get the posts from cache
                    .flatMap(() => this.parent.localCache.getPosts(res.json().list.posts.map(p => p.id)))
                    // and create the postsList
                    .map( posts => new ObjectsList(posts, getMore));
            });
            // */
        },
        markers: function(
            widgetContext: WidgetContext,
            options: {authorId?: string, source?: string, tag?: string} = {},
            listOptions: {limit?: number, cursor?: string} = { limit: 5}
        ): Observable<{posts: ObjectsList<Post>, total: number}> {

            /*const url = 'post/list';

            const message = {
                list: widgetContext.getContext({extendedOptions: Object.assign(
                    {},
                    listOptions,
                    {author_id: options.authorId, source: options.source}
                    )}
                )
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().list.posts;
            });*/
            return this.list(widgetContext, options, listOptions);
        },
        /**
         *
         * @param ids
         * @param publicAPI whether or not to use public api for profiles
         * @returns {any}
         */
        getMultiple: function(ids: string[], format?: number): Observable<Post[]> {
            if (!ids || ids.length === 0) return Observable.from([[]]);
            return this.parent.localCache.getPosts(ids, format);
        },

        /**
         * only adds posts to LocalCache, does not return anything
         * !uses coldApi and not b2bApi!
         *
         * @param ids
         * @returns {Observable<boolean>}
         */
            loadToCache(ids: string[], format?: number): Observable<boolean> {
            const url = '/posts';
            const message = {
                post_get_multiple: {
                    post_format: 'large',
                    profile_format: 'large',
                    ids: ids
                }
            };

            const retrunFn = this.api.call(url, message, {cache: true});
            if (format !== undefined && format < objectFormat.L) {
                return retrunFn.map(res => {
                    const resPosts = res.json().post_get_multiple.ok[0].posts || [];
                    resPosts.forEach((jp: any) => {
                        jp.auhtorid = jp.auhtorid || jp.author_id; // bug format change between b2b and rive apis
                        this.parent.localCache.addObject(jp, 'post'); // there is not format managment for posts, it is for userInPost
                    });

                    const resProfiles = res.json().post_get_multiple.ok[0].profiles || [];
                    resProfiles.forEach((jp: any) => {
                        jp.isPublic = true;
                        this.parent.localCache.addObject(jp, 'profile', objectFormat.S);
                    });

                    return true;
                });
            } else {
                return  retrunFn.flatMap(res => {
                    // insert post in errors in cach to prevent future errors
                    const errors: string[] = res.json().post_get_multiple.err.map((p: any) => {
                        p.isError = true;
                        this.parent.localCache.addObject(p, 'post');
                    });



                    const resPosts = res.json().post_get_multiple.ok[0].posts || [];

                    // I need to get the post profiles before adding the posts in the cache from the server because
                    // api '/posts' does not provide them in the right format ;
                    return this.parent.localCache.getProfiles(resPosts.map(jp => {
                        jp.isError = false;
                        this.parent.localCache.addObject(jp, 'post');
                        jp.auhtorid = jp.auhtorid || jp.author_id; // bug format change between b2b and rive apis
                        return jp.auhtorid;
                    })).map( () => {
                        return true;
                    });
                });
            }
        },
        getSentiment(ids: string[]): Observable<{positive: number, negative: number, neutral: number}> {
            const url = '/post/sentiment/get_from_watson';
            const message = {
                post_ids: ids
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                const sentiments = res.json().sentiments;
                const sentScore = {positive:0, negative: 0, neutral: 0};
                sentiments.forEach(s => {
                    sentScore[s.label] += s.score;
                })

                return sentScore;
            })
        },
        setSentiment(id: string, sentiment: number, widgetId?: string): Observable<boolean> {

                      //*
            const url = '/post/classify';
            const message = {
                partner_post_classification_query: {
                    post_id: id,
                    value: sentiment
                }
            }

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map( () => {
                // update local cache

                this.parent.localCache.addObject({id: id, content: {sentiment_score: sentiment}}, 'post')
                return true;
            });
            //*/
        }
    };

    profiles = {
        api: this.api,
        parent: <ZupApiService>this,
        /**
         *
         * @param profileId
         * @param widgetId
         * @param {boolean} ban
         * @returns {Observable<R>}
         */
            exclude(profileId: string | string[], widgetIds: string[] = undefined, ban: boolean = true, banFromPartner: boolean = true): Observable<boolean> {
            const url = 'widget/filters/update';
            const todos = [];

            const profileIds = typeof profileId === "string" ? [<string>profileId] : <string[]>profileId;

            if (widgetIds && widgetIds.length > 0){
                widgetIds.map((wid: string) => {
                    const message: any = {
                        filter: {
                            widget_id: wid,
                            apply_to_partner: banFromPartner
                        }

                    };
                    if (ban) {
                        message.filter.filter_profiles = profileIds;
                    } else {
                        message.filter.unfilter_profiles = profileIds;
                    }

                    todos.push(this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}));
                });
            } else {
                const message: any = {
                    filter: {
                        apply_to_partner: true
                    }
                };
                if (ban) {
                    message.filter.filter_profiles = profileIds;
                } else {
                    message.filter.unfilter_profiles = profileIds;
                }

                    todos.push(this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}));
            }


            return Observable.forkJoin(...todos).map(() => true);

        },
        exportFilter(
            listOptions: {limit?: number, cursor?: string} = {},
            widgetContext: WidgetContext
        ): Observable<ObjectsListLC<Profile>> {
            let url = '/profiles/filter';
            let message: any = {
                filter_profiles: widgetContext.getContext({extendedOptions: listOptions, timeline: false})
            };


              return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                  const loadDetail = (profilesIds: string[]): Observable<Profile[]> => {

                    const widgetId = widgetContext && widgetContext.widgetId;

                    return this.getMultiple(profilesIds, undefined, {widgetId: widgetId});

                  }

                  const getMore = res.json().profiles_filtered.cursor && res.json().profiles_filtered.cursor !== '0'
                      ? this.exportFilter(
                          {limit: listOptions.limit, cursor: res.json().profiles_filtered.cursor},
                            widgetContext
                        )
                      : undefined

                  return new ObjectsListLC(res.json().profiles_filtered.profiles, 100, loadDetail, getMore);
              });
        },
        filter(
            listOptions: {limit?: number, cursor?: string} = {limit: 20},
            widgetContext: WidgetContext
        ): Observable<{list: ObjectsList<Profile>, total: number}> {

            let url = '/profiles/filter';
            let message: any = {
                filter_profiles: widgetContext.getContext({extendedOptions: listOptions, timeline: false})
            };

            // TMP: compatibility between widget_id and widget_ids:
            if (message.filter_profiles.widget_id) {
                message.filter_profiles.widget_ids = [message.filter_profiles.widget_id];
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true})
                .flatMap( res => {
                    const widgetId = widgetContext && widgetContext.widgetId;
                    const i4uData = widgetContext && widgetContext.getContext().on_i4u_data;

                    return this.getMultiple(res.json().profiles_filtered.profiles, undefined, {widgetId: widgetId, onI4U: i4uData}).map(resProfiles => {
                        const getMore =
                            res.json().profiles_filtered.cursor && res.json().profiles_filtered.cursor !== '0' ?
                                this.filter(
                                    {limit: listOptions.limit, cursor: res.json().profiles_filtered.cursor},
                                    widgetContext
                                ).map(res2 => res2.list) : undefined;

                        return {
                            list: new ObjectsList<Profile>(resProfiles, getMore),
                            total: res.json().profiles_filtered.total
                        };
                    });

                }).catch(err => {
                    return Observable.throw(err);
                });
        },
        get: function(id: string, useCache?: boolean): Observable<Profile>{
            return this.parent.localCache.getProfile(id, useCache);
        },
        getMultiple: function(ids: string[], format?: number, options?: {widgetId?: string, showExclude?: boolean, onI4U?: boolean}): Observable<Profile[]> {
            if (!ids || ids.length === 0) return Observable.from([[]]);
            // Do not use cache anymore (TODO: redo it) :

            if (format && format < objectFormat.L) { // use public api
                const url = 'public/profiles/get_multi';
                const message = {
                    'profile_getmulti': {
                        ids: ids,
                        // format: 'small',
                        // on_i4u_data: options && options.onI4U
                    }
                };
                return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                    return res.json().profile_getmulti.ok.map(p => new Profile(p));
                });
            } else {
                const url = '/profiles/get_multi';
                const message = {
                    'profile_getmulti': {
                        ids: ids,
                        widget_id:options && options.widgetId,
                        on_i4u_data: options && options.onI4U
                    }
                };


                return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map( res => {
                    let profiles = res.json().profile_getmulti.ok.filter (p => !p.blocked || (options && options.showExclude));

                    this.parent.localCache.addObjects(res.json().profile_getmulti.labels.map(l => {delete l.count; return l;}), 'label');
                    const labels = this.parent.localCache.labels;
                    return profiles.map(p => new Profile(p, labels));
                });
            };
        },
        listExcluded(widgetId?: string): Observable<{widgetId: string, profile: Profile}[]> {
            const url = 'widget/filters';
            const message = {
                filter_get: {
                    widget_id: widgetId
                }
            }
            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap( res => {
                    let profileIds = []
                    const filters = (res.json().filter_get.filters || []);
                    filters.forEach((f: any) => {
                        if (f.profiles) profileIds = profileIds.concat(f.profiles);
                    })
                    if (profileIds.length > 0) {
                        const excluded = [];
                        return this.getMultiple(profileIds, objectFormat.S).map(profiles => {
                            filters.forEach(f => {
                                if (f.profiles) {
                                    f.profiles.forEach(pid => {
                                        const p = profiles.find(p => p.id === pid);
                                        if (p) excluded.push({widgetId: f.widget_id, profile: p})
                                    });
                                }
                            });
                            return excluded;
                        })
                        /*
                        return this.parent.profiles.loadToCache(profileIds)
                            .flatMap(() => {
                                const todos = []
                                filters.forEach((f: any) => {
                                    if (f.profiles) {
                                        f.profiles.forEach((pid: string) => {
                                            todos.push(
                                                this.parent.localCache.getProfile(pid)
                                                    .map((profile: Profile) => {
                                                        return {widgetId: f.widget_id, profile: profile};
                                                    })
                                            );
                                        });
                                    };
                                });

                                return Observable.forkJoin(...todos);
                            });*/
                    } else {
                        return Observable.from([[]]);
                    };
                }
            );
        },
        listExcludedGlobal(): Observable<ObjectsListLC<Profile>> {
            const url = 'widget/filters';
            const message = {
                filter_get: {
                }
            }
            return this.api.call(url, message, {apiid: 'b2bapi'}).map( res => {
                    let profileIds = []
                    const filters = (res.json().filter_get.widget_filters || []);
                    filters.push(res.json().filter_get.partner_filter || []);
                    filters.forEach((f: any) => {
                        if (f.profiles) profileIds = profileIds.concat(f.profiles);
                    })

                    const loadDetail = (ids: string[]): Observable<Profile[]> => this.getMultiple(ids, objectFormat.S);
                    return new ObjectsListLC<Profile>(tools.unduplicate(profileIds), 20, loadDetail);
                }
            );
        },
        listEngaged(
            widgetId: string = undefined,
            filterOptions: any, //daterange
            listOptions: {limit?: number, cursor?: string} = {}
        ): Observable<ObjectsList<Profile>> {
            const url = 'history_log/list';
            const message = {
                list: {
                    limit: listOptions.limit || 20,
                    cursor: listOptions.cursor,
                    widget_id: widgetId,
                    after: (filterOptions && filterOptions.dateRange && dates.toZIFromDate(filterOptions.dateRange.from)) || 1,
                    before: (filterOptions && filterOptions.dateRange && dates.toZIFromDate(filterOptions.dateRange.to)) || dates.toZIFromDate(new Date()),
                    type: 'PUBLISH'
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                const getMore = res.json().list.cursor ?
                    this.listEngaged(widgetId, filterOptions, Object.assign(listOptions, {cursor:  res.json().list.cursor})) :
                    undefined;


                const profilesMap = new Map<string, any>();
                const lastEngagment = new Map<string, any>();

                res.json().list.profiles.forEach(p => {
                  profilesMap.set(p.id, p);
                });


                const profilesIds = tools.unduplicate(
                    res.json().list.history_log_entries
                        .map(l => {
                          if (!lastEngagment.has(l.profile_id)) lastEngagment.set(l.profile_id, l);
                          return l.profile_id;
                        })
                        .filter(id => !!id)
                );

                const profiles = profilesIds
                    .map(id => profilesMap.get(id)).filter(p => !!p)
                    .map(p => {
                        const pr = new Profile(p)
                        const le = lastEngagment.get(p.id);
                        if (le) {
                          pr.customValue =toActionLog(le, this.parent.localCache.profileObject(le.member_id)); //todo add act date
                        } else {
                          console.log("NO ENGAGEMENT FOUND for " + p.id);
                        }

                        return pr
                    });

                return new ObjectsList(
                    profiles, //res.json().list.profiles.map(p => new Profile(p)),
                    getMore,
                    {
                        unduplicateFunction: (p, ps) => {
                            return ps.findIndex(op => op.id === p.id) < 0;
                        }
                    });


                /*
                const profilesIds = tools.unduplicate(
                    res.json().list.history_log_entries.filter(l => l.type === 'PUBLISH')
                        .map(l => l.profile_id)
                        .filter(id => !!id)
                );


                return this.getMultiple(profilesIds, objectFormat.S).map(profiles => {
                    return new ObjectsList(
                        profiles, //res.json().list.profiles.map(p => new Profile(p)),
                        getMore,
                        {
                            unduplicateFunction: (p, ps) => {
                                return ps.findIndex(op => op.id === p.id) < 0;
                            }
                        });
                });
                */
            });

        },
        listInterests(): Observable<string[]> {
            const url = '/interests/get_categories';

            const message = {}

            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => <string[]>res.json().sort());
        },

        /**
         * Only adds user into localCache, does not return anything
         * @param {string[]} userIds
         * @returns {Observable<boolean>}
         */
        loadToCache(userIds: string[], format?: number): Observable<boolean> {
          console.log('STILL USING profiles.loadToCache')
            if (userIds.length === 0) {
                return Observable.from([true]);
            }

            if (format && format < objectFormat.L) { // use publoic api
                const url = '/profiles';
                const message = {
                    'profile_getmulti': {
                        'ids': userIds,
                        format: 'small'
                    }
                };
                return this.api.call(url, message, {cache: true}).map(res => {
                    this.parent.localCache.addObjects(
                        res.json().profile_getmulti.ok.map(p => { p.isPublic = true; return p; }),
                        'profile',
                        format
                    );

                    return true;
                });
            } else {
                const url = '/profiles/get_multi';
                const message = {
                    'profile_getmulti': {
                        'ids': userIds,
                    }
                };
                return this.api.call(url, message, {apiid: 'b2bapi'}).map( res => {
                    this.parent.localCache.addObjects(res.json().profile_getmulti.labels, 'label');
                    this.parent.localCache.addObjects(
                        res.json().profile_getmulti.ok.map(p => { p.isError = false; return p; }),
                        'profile',
                        objectFormat.L
                    );
                    return true;
                });
            };
        },
        searchAll(query: string, limit: number = 50, cursor?: number): Observable<ObjectsList<Profile>> {
            const url = 'profiles/find';
            const message = {
                profile_find: {
                    name: query,
                    limit: limit,
                    profile_format: 'small',
                    cursor: cursor
                }
            };

            return this.api.call(url, message, {cache: true}).map(res => {
                let getMore;
                if (res.json().profile_find.cursor) {
                    getMore = this.searchAll(query, limit, res.json().profile_find.cursor);
                }

                const profiles = res.json().profile_find.profiles || [];

                return new ObjectsList(
                    profiles
                        .filter(p => p.kind !== 'member')
                        .map(p => new Profile(p)),
                    getMore
                );
            });
        },
        setGender(ids: string[], gender: string): Observable<boolean> {
            const url = '/profiles/force_gender';
            const message = {
                "force_gender": ids.map(id=> { return {profile_id: id, gender: gender};})
            }

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['admin']}).map(() => true);
        }

    };

    tasks = {
        api: this.api,
        parent: <ZupApiService>this,
        assignToPost: function(
            postId: string,
            assignTo: string,
            assignmentMessage: string,
            screen?: string,
            widgetId?: string
        ): Observable<boolean> {
            const url = '/task/assign_to_post';
            const message = {
                assign: {
                    post_id: postId,
                    screen: screen,
                    assign_to: assignTo,
                    assignment_message: assignmentMessage,
                    widget_id: widgetId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( () => true);
        },
        assignToUser: function(
            userId: string,
            assignTo: string,
            assignmentMessage: string,
            screen?: string,
            widgetId?: string
        ): Observable<boolean> {
            const url = '/task/assign_to_profile';
            const message = {
                assign: {
                    profile_id: userId,
                    screen: screen,
                    assign_to: assignTo,
                    assignment_message: assignmentMessage,
                    widget_id: widgetId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( () => true);
        },
        close: function(taskId: string): Observable<boolean> {
            const url = '/task/close';
            const message = {
                close: {
                    task_id: taskId,
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( () => true);
        },
        getDetails: function(taskIds: string[]): Observable<Task[]> {
            const url = 'task/get';
            const message = {
                get: {
                    task_ids: taskIds
                }
            }

            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap(res => {
                const jsonTasks = res.json().get;
                if (jsonTasks.tasks.length === 0) return Observable.from([]);


                // add tasks to localCache :
                this.parent.localCache.addObjects(jsonTasks.tasks, 'task')

                // add notifications to localCache :
                this.parent.localCache.addObjects(jsonTasks.notifications, 'notification')

                // add posts to localCache:
                jsonTasks.posts.map(p => { this.parent.localCache.addObject(p, 'post'); });

                // add members profiles to localCache :
                this.parent.localCache.addObjects(jsonTasks.profiles, 'profile', objectFormat.S);

                return this.parent.localCache.getTasks(jsonTasks.tasks.map(t => t.id));
            });
        },
        getMultiple: function(taskIds: string[], format?: number): Observable<Task[]> {
            const jsonTasks = this.parent.localCache.getJsonObjects(taskIds, 'task');

            const profileIds = tools.unduplicate(
                jsonTasks.map((j: any) => j.profile_id)
                    .concat(jsonTasks.map((j: any) => j.assigned_by))
                    .concat(jsonTasks.map((j: any) => j.assigned_to))
                    .filter((id: string) => !!id)
            );

            const postIds = tools.unduplicate(
                jsonTasks.map((j: any) => j.post_id).filter((id: string) => !!id)
            )

            /*
             const notificationIds = tools.unduplicate(
             jsonTasks.map((j: any) => j.notificationId).filter((id: string) => !!id)
             )*/

            return Observable.forkJoin(
                this.parent.profiles.getMultiple(profileIds, format),
                this.parent.posts.getMultiple(postIds, format)
            ).flatMap(() =>  { return <Observable<Task[]>>(this.parent.localCache.getTasks(taskIds, format)); });

        },
        list: function(
            listOptions: {limit?: number, cursor?: string}= {},
            options?: {widgetId?: string, widgetType?: string, after?: number, before?: number, assignedTo?: string, closedTasks?: boolean, openedTasks?: boolean}
        ): Observable<ObjectsList<Task>> {
            const url = '/task/list';
            const message: any = {
                list: {
                    limit: listOptions.limit || 5,
                    cursor: listOptions.cursor,
                    after: options && options.after,
                    before: options && options.before,
                    widget_type: options && options.widgetType,
                    widget_id: options && options.widgetId,
                    assigned_to: (options && options.assignedTo),
                    hide_opened_tasks: (options && options.openedTasks === false) ?  true : false,
                    hide_closed_tasks: (options && options.closedTasks === true) ? false : true,
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi' }).flatMap(resTask => {
                const jsonTask = resTask.json().list;
                if (jsonTask.tasks.length === 0) return Observable.from([new ObjectsList([])]);


                // add tasks to localCache :
                this.parent.localCache.addObjects(jsonTask.tasks, 'task', objectFormat.S)

                // add notifications to localCache :
                this.parent.localCache.addObjects(jsonTask.notifications, 'notification', objectFormat.S)

                // add posts to localCache:
                jsonTask.posts.map(p => { this.parent.localCache.addObject(p, 'post', objectFormat.S); });

                // add members profiles to localCache :
                this.parent.localCache.addObjects(jsonTask.profiles, 'profile', objectFormat.S);


                const getMore = jsonTask.cursor ?
                    this.list({limit: listOptions.limit, cursor: jsonTask.cursor}, options) : undefined;

                /*/ get post authors (that are not sent in profiles)
                let usersId = jsonTask.profiles.filter(p => p.kind !== 'member').map(p => p.id);
                if (jsonTask.posts) usersId = usersId.concat(jsonTask.posts.map(p =>  p.authorid ));
                const getUsers = this.parent.profiles.getMultiple(usersId)
                //*/

                // get notifications post (that are not sent with tasks)
                const postIds = jsonTask.notifications.map( n => n.post);
                const getPosts = this.parent.posts.getMultiple(postIds);

                return Observable.forkJoin( getPosts)
                    .flatMap(() => this.parent.localCache.getTasks(jsonTask.tasks.map(t => t.id), objectFormat.S))
                    .map((res: Task[]) => { return new ObjectsList(res, getMore); });
            });
        },
        updateEvent: function(taskEvent: TaskEvent): Observable<TaskEvent> {
            const url = ''
            const message = {

            }
            return this.api.call(url, message, {apiid: 'b2bapi',  rights: ['write'] }).map(res => {
                return TaskEvent;
            })
        },
        createEvent: function(
            taskEvent: TaskEvent,
            screen: string = screenTypes.UNCKNOWN): Observable<TaskEvent> {

            const tmp = 1;if (tmp === (0+1)) {taskEvent.id=Math.random().toString();return Observable.of(taskEvent)};
            const url = '';
            const message = {
                assign: {
                    profile_id: taskEvent.profileId,
                    screen: screen,
                    assign_to: taskEvent.assignedTo,
                    assignment_message: taskEvent.message,
                    widget_id: taskEvent.widgetId,
                    starts_at: taskEvent.start,
                    ends_at: taskEvent.end,
                    title: taskEvent.title
                }
            }

            return this.api.call(url, message, {apiid: 'b2bapi',  rights: ['write'] }).map(res => {
                return new TaskEvent(res.json().task);
            })
        }, listEvents: function(options: {from: number, to: number, profile_id?: string, widget_id?: string}): Observable<TaskEvent[]> {
            // todo
            return Observable.of([]);
        }
    };

    view = {
        api: this.api,
        parent: <ZupApiService>this,
        create: function(view: View): Observable<View> {
            // test if parentWidget (redondant?)
            if (!view.parentWidgetId) throw 'MISSING_PARENT()';
            if (view.id) throw 'ALREADY_EXISTS()';

            return this.parent.widget.create(view).map((widget) => {
                view.id = widget.id;
                return view;
            });
        },
        delete: function(view: View): Observable<string> {
            return this.parent.widget.delete(view);
        },
        update: function(view: View, originalView: View): Observable<View> {
            if (!view.parentWidgetId) throw 'MISSING_PARENT()';
            if (!view.id) throw 'DOES_NOT_EXISTS()';

            return this.parent.widget.update(view, originalView);
            /*
            // if queries changed, delete and recreate the view
            if (JSON.stringify(view.queries) !== JSON.stringify(originalView.queries)) {
                const newView = view.clone(true);
                return this.delete(originalView).flatMap(() => this.create(newView));
            } else {
                return this.parent.widget.updateExcludeTerms(view, originalView)
                    .flatMap(() => this.parent.widget.updateWidgetInfo(view, originalView))
                    .flatMap(() => this.parent.widget.updateExcludedProfiles(view, originalView))
                    .map(() => view);
            };
            */

        }
    };

    wall = {
        api: this.api,
        parent: <ZupApiService>this,
        addWidget: function(widgetId: string, wallId: string): Observable<boolean> {
            const url = 'wall/widget/add';
            const message = {
                widget_add: {
                    wall_id: wallId,
                    widget_id: widgetId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
        },
        /**
         *
         * @param wall
         * @param partnerName needed to test if name is unique in partner
         * @returns {Observable<R>}
         */
        create: function (wall: Wall, partnerName: string): Observable<Wall> {
            // test if name exists
            return this.search(wall.name, partnerName).flatMap( exists => {
                if (exists) return Observable.throw(`NAME_EXISTS(${wall.name})`);

                wall.version = 0;

                const url = 'wall/create';
                const message = {
                    add: wall.toJson()
                }

                const createWall = this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']})


                return createWall.map(resW => new Wall(resW.json().add.wall));
            });


        },
        delete: function (id: string): Observable<boolean> {

            const url = 'wall/delete';
            const message = {
                remove: {
                    wall_id: id
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
        },
        get: function (id: string): Observable<Wall> {
            const url = 'wall/get';
            const message = {
                get: {
                    wall_id: id
                }
            };

            return this.api.call(url, message, {cache: false}).map((res) => new Wall(res.json().get.wall))
                .catch(err => Observable.throw(err));
        },
        list: function(): Observable<Wall[]>{
            const url = 'wall/list';
            const message = {
                list: {
                    limit: 101
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi'}).map((res) => res.json().list.walls.map(w => {
                return new Wall(w);
            })).catch(err => {
                if (err.toString() === 'Partner has no right for this functionnality') {
                    return Observable.from([[]]);
                } else {
                    return Observable.throw(err);
                }
            });
        },
        posts: function(
            wallId: string,
            dateRange: {from: Date, to: Date} = {from: new Date(2010, 1, 1), to: new Date()},
            listOptions: {limit: number, cursor?: string} = {limit: 20},
            objectListOptions?: any
        ): Observable<ObjectsList<wallJsonPost>>{
            const url = 'wall/post/list';
            const message = {
                list: {
                    wall_id: wallId,
                    after: dates.toZIFromDate(dateRange.from),
                    before: dates.toZIFromDate(dateRange.to),
                    cursor: listOptions.cursor,
                    limit: listOptions.limit
                }
            };

            return this.api.call(url, message, {apiid: 'coldapi'}).map((res) => {
                const jres = res.json();
                const getMore = jres.list.cursor ?
                    this.posts(wallId, dateRange, {limit: listOptions.limit, cursor : jres.list.cursor}, objectListOptions) :
                    undefined;

                return new ObjectsList(this.parent.localCache.postList2Posts(jres.list).map((p: Post) => p.wallJsonPost), getMore, objectListOptions);
            });
        },
        removeWidget: function(widgetId: string, wallId: string): Observable<boolean> {
            const url = 'wall/widget/delete';
            const message = {
                widget_remove: {
                    wall_id: wallId,
                    widget_id: widgetId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
        },
        search: function(wallName: string, partnerName: string): Observable<Wall> {
            const url = 'wall/search';
            const message = {
                search: {
                    wall_name: wallName,
                    partner_name: partnerName
                }
            };

            return this.api.call(url, message, {apiid: 'coldapi'}).catch(err => {
                if (err.toString() === 'Cannot get widget: ERR_NOT_FOUND()') {
                    return Observable.from([false]);
                } /*else {
                 return Observable.throw(err);
                 }*/
            }).map((res) => {
                if (res) {
                    const w = new Wall(res.json().search.wall)
                    // ad pinned posts to localcahce
                    w.pinnedPosts.forEach(p => {
                        const myProfile = {
                            id: p.user.id,
                            screen_name: p.user.screenName,
                            thumbnail: p.user.thumbnail,
                            deletable: false
                        };
                        const myPost = {
                            id: p.id,
                            authorid: myProfile.id,
                            content: {
                                text: p.text,
                                thumbnail: p.thumbnail,
                                medias: [p.thumbnail],
                                title: p.title,
                                deletable: false
                            },
                            position: {}
                        };
                        this.parent.localCache.addObjectWithFormat(myProfile, 'profile', objectFormat.S);
                        this.parent.localCache.addObjectWithFormat(myPost, 'post', objectFormat.S);


                    })

                    return w;
                }
            });
        },
        /**
         *
         * @param {Wall} wall
         * @returns {Observable<R>}
         */
        update: function (wall: Wall): Observable<boolean> {
            wall.version = (wall.version || 0) + 1;

            //todo check name
            const url = 'wall/update';

            const message = {
                update: wall.toJson()
            };


            return this.get(wall.id).flatMap(w => {
                if (w.version >= wall.version) {
                    return Observable.throw('WALL_OBSOLETE()');
                } else {
                    return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
                }

            })

        },
        nbActions: function(wallId: string, dtFrom: number, dtTo: number)
            : Observable<{likes: number, comments: number, shares: number}> {
            const url = 'wall/total_count';
            const message = {
                total_count: {
                    wall_id: wallId,
                    after: dtFrom || 1,
                    before: dtTo || dates.toZIFromDate(new Date())
                }
            };

            return this.api.call(url, message).map(res => {
                return {
                    likes: res.json().total_count.count_likes,
                    comments: res.json().total_count.count_comments,
                    shares: res.json().total_count.count_shares
                };
            });
        },

        // //////////// ANALUYTICS ////////////////
        nbPosts: function(wallId: string, dtFrom?: number, dtTo?: number, options?: any): Observable<number> {
            const url = 'wall/agg_post';
            const message = {
                agg_post: {
                    wall_id: wallId,
                    after: dtFrom || 1,
                    before: dtTo || dates.toZIFromDate(new Date())
                }
            };

            return this.api.call(url, message, {cache: tools.deepGet(options, 'cache') || true}).map(res => res.json().agg_post.count);
        },
        topTags: function(
            wallId: string,
            nb: number = 20,
            dtFrom?: number,
            dtTo?: number
        ): Observable<{payload: string, total: number}[]> {
            const url = '/wall/top_tags';
            const message = {
                top_tags: {
                    wall_id: wallId,
                    after: dtFrom,
                    before: dtTo,
                    limit: nb
                }
            };

            return this.api.call(url, message).map(res => {
                return res.json().top_tags.tags || [];
            });
        },
        topUsers: function(wallId: string, nb: number = 20, dtFrom?: number, dtTo?: number): Observable<Profile[]> {
            const url = '/wall/top_users';
            const message = {
                top_users: {
                    wall_id: wallId,
                    after: dtFrom || 1,
                    before: dtTo || dates.toZIFromDate(new Date()),
                    limit: nb
                }
            };

            return this.api.call(url, message).flatMap(resTop => {
                const userMap = new Map<string, number>();

                const ids = resTop.json().top_users.users.map(u => {
                    userMap.set(u.payload, u.total);
                    return u.payload;
                });


                if (ids.length === 0) return Observable.from([[]]);

                const urlp = '/profiles';
                const messagep = {
                    'profile_getmulti': {
                        'ids': ids,
                        format: 'small'
                    }
                };
                return this.api.call(urlp, messagep).map(res => {
                    return res.json().profile_getmulti.ok.map(p => {
                        p.total_published = userMap.get(p.id);
                        return new Profile(p);
                    });
                });

                // return this.parent.profiles.getMultiple(ids, true);
            });
        }
    };

    widget = {
        api: this.api,
        parent: <ZupApiService>this,
        addPublishAction(publishAction: PublishAction): Observable<boolean> {
            console.log('DEPRECATED: addPublishAction');
            return Observable.from([false]);
        },
        create: function(widget: Widget, isI4U?: boolean): Observable<Widget> {

            if (widget.id) return Observable.throw('WIDGET_ALREADY_EXISTS()');
            if (!widget.name || widget.name.length === 0) return Observable.throw('MISSING_FIELDS(name)');


            const url = !isI4U ? '/widget/create' : '/i4u/binfluence/widget_create';
            const message = {
                create: {
                    view: widget.view,
                    metas: widget.metas,
                    category: widget.category,
                    name: widget.name,
                    type: widget.type,
                    user_daily_threshold: widget.userDailyThreshold,
                    influencer_threshold: widget.influencerThreshold,
                    customer_lead_hreshold: widget.customerLeadThreshold,
                    loyalty_threshold: widget.loyaltyNbThreshold ?
                        [widget.loyaltyNbThreshold, widget.loyaltyDaysThreshold] : undefined,
                    widget_parent: widget.parentWidgetId,
                    filters: {
                        profiles: widget.excludedProfiles || [],
                        places: widget.excludedPlaces || []
                    },
                    without_geo_missions: widget.noMission
                }
            };

            if (widget.options) {
                Object.assign(message.create, widget.options);
            }
            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] })
                .map( res => {
                    //widget.id =  res.json().create.widget_id; return widget;
                    return new Widget(res.json().create.widget);
                });

        },
        delete: function(widget: Widget): Observable<string> {
            const widgetId = widget.id;
            if (!widgetId) return Observable.throw('WIDGET_DOSE_NOT_EXIST()');

            const url = '/widget/delete';
            const message = {
                'delete': {
                    widget_id: widgetId
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write'] }).map( res => {return widgetId; });
        },
        deletePublishAction(publishAction: PublishAction): Observable<boolean> {
            console.log('DEPRECATED: deletePublishAction');
            return Observable.from([false]);
        },
        get: function(wid: string): Observable<Widget> {
            const todos = [];

            // generic info
            const page = '/widget/get';
            const message = {
                get: {
                    widget_id: wid
                }
            };
            todos.push(this.api.call(page, message, {apiid: 'b2bapi'}));
            todos.push(this.getExcluded(wid));


            return Observable.forkJoin(todos).map((res: any[]) => {
                let w = new Widget(res[0].json().get.widget)
                if (w.parentWidgetId) w = new View(res[0].json().get.widget);
                w.excludedProfiles = res[1].profiles || [];
                w.excludedPlaces = res[1].places || [];

                return w;
            });
        },
        filterShares(shares: boolean, widgetId: string): Observable<boolean> {
          const url = '/widget/filters/update';
          return this.getExcluded(widgetId).flatMap((res: {places: string[], profiles: string[]}) => {
            const message = {
              filter: {
                filter_profiles:res.profiles,
                filter_places: res.places,
                filter_shares: !shares
              }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                return true;
            });
          })

        },
        getExcluded(widgetId: string): Observable<{places: string[], profiles: string[]}> {
            const url = 'widget/filters';
            const message = {
                filter_get: {
                    widget_id: widgetId
                }
            };
            return this.api.call(url, message, {apiid: 'b2bapi'}).map(res => {
                return (res.json().filter_get.filters || res.json().filter_get.widget_filters)[0] || {places: [], profiles: []};
            }).catch(err => {
                return Observable.of([]);
            });
        },
        list(limit?: number): Observable<Widget[]> {
            const url = '/widget/list';
            const message = {
                list: {limit: limit }
            };
            // TODO: manage the 101 widgets limit
            return this.api.call(url, message, {apiid: 'b2bapi' }).map(res => {
                const allWidgets = res.json().list.widgets.map(widget => new Widget(widget));
                // split views from widget
                const widgets: Map<string, Widget> = new Map<string, Widget>();
                const views = [];
                allWidgets.forEach((w: Widget) => {
                    if (w.parentWidgetId) {
                        views.push(new View(w.jsonObject));
                    } else {
                        widgets.set(w.id, w);
                    }
                })
                views.forEach((w: View) => {
                    // kill orphan views :
                    if (!widgets.get(w.parentWidgetId)) {
                        console.log(`DELETING_ORPHAN_VIEW(${w.name}, ${w.id})`);
                        this.delete(w).subscribe();
                        return;
                    }

                    widgets.get(w.parentWidgetId).widgetViews.push(w);
                });

                return Array.from(widgets.values());
            });
        },
        listProfiles(widget: Widget): Observable<Profile[]> {
            if (!widget.isProfileListWidget) return Observable.of([]);

            const labelId = widget.labelId;
            return this.parent.labels.profilesList(labelId, {limit: 200}, {simpleProfiles: true}).map(res => {
                return res.list;
            });
        },
        pause(widget: Widget, pause: boolean): Observable<Widget> {

            // generic info

            return this.get(widget.id)
                .flatMap(widget2 => {

                  const pauseDt = !pause ? 0 : (dates.toZIFromDate(new Date()) - 1);
                  let todos = [];
                  /*if (widget.widgetViews.length > 0) {
                    todos = widget.widgetViews.map((v: View) =>  {
                      v.endsAt = pauseDt;
                      return this.updateView(v);
                    });
                  }*/

                widget.endsAt = widget2.endsAt = pauseDt;


                todos.push(this.updateView(widget2));
                return tools.forkJoin2FlatMap(todos).map(res => widget2);
            });


        },
        preview(widget: Widget, fortest?: boolean): Observable<{posts: Post[], total: number, ok?: boolean}> {
            if (
                (!widget.view.zone || JSON.stringify(widget.view.zone) === JSON.stringify({n: 0, e: 0, s: 0, w: 0}))
                && widget.tags.length === 0
            ) {
                return Observable.throw('WRONG_FORMAT()');
            }

            const url = '/widget/preview';
            const message = {
                preview: {
                    view: widget.view
                }
            }


            const flatMap = (res): any => {
                const jsonRes = res.json().preview;

                this.parent.localCache.addObjects(jsonRes.posts, 'post');
                this.parent.localCache.addObjects(jsonRes.profiles, 'profile');

                return this.parent.localCache.getPosts(jsonRes.posts.map(p => p.id))
                    .map((posts: Post[]) => {
                        return {ok: true, total: jsonRes.total, posts: posts};
                    });
            }

            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap(flatMap);
        },
        update: function(widget: Widget, originalWidget?: Widget): Observable<Widget> {

            if (!widget.id) return Observable.throw('WIDGET_DOSE_NOT_EXIST()');
            if (!widget.name || widget.name.length < 3) return Observable.throw('INCORRECT_FORMAT(name)');


            return this.updateAlerts(widget,  originalWidget)
                .flatMap(() => this.updateExcludeTerms(widget,  originalWidget))
                .flatMap(() => this.updateWidgetInfo(widget,  originalWidget))
                .flatMap(() => this.updateExcludedProfiles(widget,  originalWidget))
                //.flatMap(() => this.updatePublishActions(widget,  originalWidget))
                .flatMap(() => this.updateView(widget, originalWidget))
                .map( () => widget);


        },
        updateCategory(widgetId: string, category: number): Observable<boolean>{
            const url = 'widget/update_category'

            const message = {
                update: {
                    id: widgetId,
                    new_category: category
                }
            }

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map( () => true);
        },

        updateExcludedProfiles(widget, originalWidget?: Widget): Observable<boolean> {

            const url = 'widget/filters/update';
            const message: any = {
                filter: {
                    widget_id: widget.id
                }
            }

            // I need to find witch to exclude or reinclude
            return this.getExcluded(widget.id).flatMap((excludedIds: {places: string[], profiles: string[]}) => {
                if (
                    (widget.excludedPlaces || []).sort().toString() === (excludedIds.places || []).sort().toString()
                    && (widget.excludedProfiles || []).sort().toString() === (excludedIds.profiles || []).sort().toString()
                ) return Observable.from([true]);


                const placesDelta = tools.arrayDelta(
                    excludedIds.places,
                    widget.excludedPlaces
                );
                const profilesDelta = tools.arrayDelta(
                    excludedIds.profiles,
                    widget.excludedProfiles
                );

                message.filter.unfilter_places = placesDelta[0];
                message.filter.filter_places = placesDelta[2];
                message.filter.unfilter_profiles = profilesDelta[0];
                message.filter.filter_profiles = profilesDelta[2];


                return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => true);
            });


        },
        updateExcludeTerms(widget: Widget, originalWidget?: Widget): Observable<boolean> {
            const url = 'widget/update_exclude_terms';

            const message = function(w: Widget) {
                if (!w) return;
                return {
                    update_exclude_terms: {
                        id: w.id,
                        exclude_terms: w.minusTags
                    }
                };
            };

            if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                return Observable.from([true]);
            };


            return this.api.call(url, message(widget), {apiid: 'b2bapi', rights: ['write']}).map( () => true);
        },
        updateAlerts(widget: Widget, originalWidget?: Widget): Observable<boolean> {
            const url = 'widget/update_alert';

            const message = function(w: Widget) {
                if (!w) return;
                return {
                    update_alert: {
                        id: widget.id,
                        user_daily_threshold: w.userDailyThreshold,
                        influencer_threshold: w.influencerThreshold,
                        customer_lead_threshold: w.customerLeadThreshold,
                        loyalty_threshold: w.loyaltyNbThreshold ? [w.loyaltyNbThreshold, w.loyaltyDaysThreshold] : null
                    }
                };
            };

            if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                return Observable.from([true]);
            };


            return this.api.call(url, message(widget), {apiid: 'b2bapi', rights: ['write']})
                .map( () => true)
                .catch(err => {
                    if (err.toString() === 'Nothing to update') {
                        return <Observable<any>>Observable.from([true]);
                    } else {
                        return <Observable<any>>Observable.throw(err);
                    }
                });
        },
        updatePublishActions(widget: Widget,  originalWidget?: Widget): Observable<boolean> {
            console.log('DEPRECTAED: updatePublishActions');
            return Observable.from([false]);

        },

        updateView(widget: Widget, originalWidget?: Widget): Observable<Widget> {

            if (
                (originalWidget && JSON.stringify(widget.view) === JSON.stringify(originalWidget.view))
                &&
                (originalWidget && !!originalWidget.noMission === !!widget.noMission)
            )
                return Observable.from([widget]);

            const url = 'widget/update_view';

            const message = {
                update_view: {
                    widget_id: widget.id,
                    view: widget.view,
                    without_geo_missions: widget.noMission
                }
            };

            return this.api.call(url, message, {apiid: 'b2bapi', rights: ['write']}).map(() => widget);

        },
        updateWidgetInfo(widget: Widget,  originalWidget?: Widget): Observable<boolean> {
            const url = 'widget/update';

            const message = function(w: Widget) {
                if (!w) return;
                return {
                    update: {
                        id: w.id,
                        metas: w.metas,
                        // category: widget.category, // we cannot update category or type
                        name: w.name,
                        // type: widget.type,
                    }
                };
            };

            if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                return Observable.from([true]);
            };

            return this.api.call(url, message(widget), {apiid: 'b2bapi', rights: ['write']})
                .map( () => true)
                .catch(err => {
                    if (err.toString() === 'Nothing to update') {
                        return <Observable<any>>Observable.from([true]);
                    } else {
                        return <Observable<any>>Observable.throw(err);
                    }
                });
        }
    };

    widgetAnalytics = {
        parent: <ZupApiService>this,
        api: this.api,
        /*customerLead: function(widgetContext: WidgetContext, limit?: number): Observable<ObjectsList<Profile>> {
            // tmp : utiliser les notifications en attendant
            const options = {
                widgetId: widgetContext.widgetId,
                type: alertTypes.CUSTOMER_LEAD,
                after: widgetContext.after,
                before: widgetContext.before,
            };

            const parent = this.parent;

            function getUsers(notList) {

                const getMore = notList.getMoreObservable ? notList.getMoreObservable.flatMap(getUsers) : undefined;
                return parent.profiles.getMultiple(notList.list.map(n => n.user.id)).map(users => {
                    return new ObjectsList<Profile>(users, getMore);
                });
            }
            return this.parent.notifications.list({limit: limit}, options).flatMap(getUsers);
        },*/
        countByProfile: function(widgetContext: WidgetContext, limit?: number, cursor?: string): Observable<ObjectsList<{profile, posts, likes, shares, comments}>> {
            const url = '/influence/count_by_profile';
            const message = {
                count_by_profile: widgetContext.getContext({extendedOptions: {limit: limit || 20, cursor: cursor}})
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).flatMap(res => {
                const resAnalytics = res.json().counts;
                const getMore = res.json().cursor ?
                    this.countByProfile(widgetContext, limit, res.json().cursor) : undefined;

                return this.parent.profiles.getMultiple(resAnalytics.map(r => r.profile_id)).map(profiles => {
                    const profilesWithAnalytis = resAnalytics.map(a => {
                        a.profile = profiles.find(p => p.id === a.profile_id);
                        return a;
                    }).filter(a => !!a.profile);

                    return new ObjectsList(profilesWithAnalytis, getMore);
                })


            })


        },
        customerLead: function(widgetContext: WidgetContext, limit?: number): Observable<ObjectsList<Profile>> {
            const WC = widgetContext.clone();
            WC.setFilterRule('order_by', '-leadscore', {dates: false, widgets: false, silent: true})

            return this.parent.profiles.filter(
                {
                    limit: limit
                },
               WC
            ).map(res => res.list);
        },
        distinctReach: function(widgetContext: WidgetContext): Observable<number> {
            const url = '/influence/sum_followers'
            const message = {
                sum_followers: widgetContext.getContext(),
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => res.json().total);
        },
        engagements: function(widgetContext: WidgetContext): Observable<{posts, likes, comments, shares}> {
            const url = '/influence/sum_engagements'
            const message = {
                sum_engagements: widgetContext.getContext(),
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => res.json());
        },
        engagementsTimeline: function(widgetContext: WidgetContext): Observable<any> {
            const url = '/analytics/hist_engagements'
            const message = {
                hist_engagements: widgetContext.getContext(),
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().hist_engagements.sources;
            });
        },
        genderSoV: function(widgetContext: WidgetContext): Observable<{name: string, count: number}[]> {
          const url = '/analytics/agg_sov_gender';
          const message = {
              agg_sov_gender: widgetContext.getContext()
          };

          return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
            return (res.json().agg_sov_gender.count || []).map(s => {
                return {
                    name: s.payload,
                    count: s.total
                };
            });

          });
        },
        influencers: function(widgetContext: WidgetContext, limit?: number): Observable<ObjectsList<Profile>> {
            const WC = widgetContext.clone();
            WC.setFilterRule('order_by', '-follower', {dates: false, widgets: false, silent: true})

            return this.parent.profiles.filter(
                {
                    limit: limit
                },
                WC
            ).map(res => res.list);

        },
        insterestsSoV: function(widgetContext: WidgetContext): Observable<{name: string, count: number}[]> {
          const url = '/analytics/agg_sov_interests';
          const message = {
              agg_sov_interests: widgetContext.getContext()
          };

          return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
            return (res.json().agg_sov_interests.count || []).map(s => {
                return {
                    name: s.payload,
                    count: s.total
                };
            });

          });
        },
        lexicalsCount: function(widgetContext: WidgetContext, filterOptions?: {lexical_id?: string, profile_id?: string}) {
            const url = '/analytics/agg_lexical';
            const message = {
                lexical_query: widgetContext.getContext({extendedOptions: filterOptions})
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().lexical_analytics;
            });
        },
        lexicalsTimeline: function(widgetContext: WidgetContext, filterOptions?: {lexical_id?: string, profile_id?: string}) {
            const url = '/analytics/hist_lexical';
            const message = {
                lexical_query: widgetContext.getContext({extendedOptions: filterOptions})
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().lexical_historics;
            });
        },
        nbPosts: function(widgetContext: WidgetContext): Observable<number> {
            const url = '/analytics/agg_post';
            const message = {
                agg_post: widgetContext.getContext()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().agg_post.count;
            });
        },
        payloadToUsers: function(payloads: any[], scoreName: string = 'score'): Observable<Profile[]> {
            if (payloads.length === 0 ) return Observable.from([]);

            const usersScores = { };
            const usersId = payloads.map(
                tu => {usersScores[tu.payload] = tu.total; return tu.payload; }
            );

            return this.parent.profiles.getMultiple(usersId.slice(0, 50)).map(
                res => res.map((p: Profile) => {p[scoreName] = usersScores[p.id]; return p;})
            );
        },
        postsTimeline: function(widgetContext: WidgetContext): Observable<any> {
            const url = '/analytics/hist_post';
            const message = {
                hist_post: widgetContext.getContext()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().hist_post.data || [];
            });
        },
        profileNbPosts: function(profileId: string, widgetContext: WidgetContext): Observable<{widget_id, comments, likes, post_count, shares, total_engagements}[]> {
            const url = '/analytics/profile_count_posts';
            const message = {
                profile_count_posts: widgetContext.getContext({extendedOptions: {profile_id: profileId}})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().count_by_widget || [];
            });
        },
        reach: function(widgetContext: WidgetContext): Observable<number> {
            const url = 'analytics/agg_reach';
            const message = {
                agg_reach: widgetContext.getContext()
            }
            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return (res.json().agg_reach.sources || []).reduce((a, b) => a + b.total, 0);
            });
        },
        reachTimeline: function(widgetContext: WidgetContext): Observable<any> {
            const url = '/analytics/hist_reach'
            const message = {
                hist_reach: widgetContext.getContext(),
            }

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().hist_reach.sources;
            });
        },
        sentiment(widgetContext: WidgetContext) {
            const url = '/analytics/agg_sentiment_analysis';

            const message = {
                agg_sentiment_analysis: widgetContext.getContext()
            }

            return this.api.call(url, message, {apiid:'b2bapi', cache: true}).map(res => {
                const sentiments: any = {};

                for (let s of res.json().agg_sentiment_analysis.count) {
                    sentiments[s.payload] = s.total;
                };
                return sentiments;
            });
        },
        shareOfVoice: (widgetContext: WidgetContext): Observable<{id?: string, count: number}[]> => {
            const url = '/analytics/agg_share_of_voice';
            const message = {
                agg_share_of_voice: widgetContext.getContext()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return (res.json().agg_share_of_voice.count || []).map(s => {
                    return {
                        id: s.payload,
                        count: s.total
                    };
                });

            });
        },
        sourcesTimeline: function(widgetContext: WidgetContext): Observable<any> {
            const url = '/analytics/hist_sources';
            const message = {
                hist_sources: widgetContext.getContext()
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().hist_sources.sources;
            });
        },
        tagsTimeline: function( widgetContext: WidgetContext, tags?: string[], limit?: number): Observable<any> {
            const url = '/analytics/hist_tags';
            const message = {
                hist_tags: widgetContext.getContext({extendedOptions: {limit: limit, tags: tags}})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().hist_tags.tags;
            });
        },
        topLoyalty: function(widgetContext: WidgetContext, limit?: number): Observable<Profile[]> {
            // there is no cursor, so we query the api with a big limit,
            // the limit is used for the detail (through the ObjectListLC)
            const getDetail = ( payloads => { return this.payloadToUsers(payloads, 'nbVisits'); }).bind(this);

            const url = '/analytics/top_loyalty';
            const message = {
                top_loyalty: widgetContext.getContext({extendedOptions: {limit: 50}})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return new ObjectsListLC(res.json().top_loyalty.loyalty, limit, getDetail);
            });
        },
        topPosts: function(
            widgetContext: WidgetContext,
            optionsList: {limit?: number, cursor?: number} = {limit: 5}
        ): Observable<ObjectsList<Post>> {
            const url = '/widget/top_posts';
            const message = {
                top_posts: widgetContext.getContext({
                    extendedOptions: {limit: optionsList.limit, cursor: optionsList.cursor }
                })
            };
            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).flatMap(res => {

                const getMore = res.json().top_posts.cursor ?
                    this.topPosts(widgetContext, {limit: optionsList.limit, cursor: res.json().top_posts.cursor }) : undefined

                const myPosts = res.json().top_posts.posts || [];
                // add posts to localCache :
                //this.parent.localCache.addObjects(myPosts, 'post')
                this.parent.localCache.addPostList(res.json().top_posts);

                // get profiles detail :
                return this.parent.profiles.getMultiple(myPosts.map((p: any) => p.authorid))
                // now we can get the Post[] from the cache
                    .flatMap(() => this.parent.localCache.getPosts(myPosts.map(p => p.id)))
                    // and return a PostList object (to be changed to ObjectsList)
                    .map( posts => new ObjectsList(posts, getMore));

            });
        },
        topSources: function(widgetContext: WidgetContext, limit?: number): Observable<any> {
            const url = '/analytics/top_sources';
            const message = {
                top_sources: widgetContext.getContext({extendedOptions: {limit: limit }})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().top_sources.sources;
            });
        },
        topTags: function(widgetContext: WidgetContext, limit?: number): Observable<any> {
            const url = '/analytics/top_tags';
            const message = {
                top_tags: widgetContext.getContext( {extendedOptions: {limit: limit }})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                return res.json().top_tags.tags || [];
            });
        },
        topUsers: function(
          widgetContext: WidgetContext,
          listOptions: {limit?: number, cursor?: string | number}
        ): Observable<ObjectsList<Profile>> {
            // there is no cursor, so we query the api with a big limit, the limit is used for the detail (through the ObjectListLC)
            const getDetail = ( payloads => { return this.payloadToUsers(payloads, 'nbPosts'); }).bind(this);

            const url = '/analytics/top_users';
            const message = {
                top_users: widgetContext.getContext( {extendedOptions: {limit: (listOptions && listOptions.limit) || 10 }})
            };

            return this.api.call(url, message, {apiid: 'b2bapi', cache: true})
            .flatMap( res => {
              const widgetId = widgetContext.widget && widgetContext.widget.id;

              return this.payloadToUsers(res.json().top_users.users, 'nbPosts').map(resProfiles => {
                  const getMore =
                      res.json().top_users.cursor && res.json().top_users.cursor !== '0' ?
                          this.topUsers(
                              widgetContext,
                              {limit: listOptions.limit, cursor: res.json().top_users.cursor}
                          ).map(res2 => res2.list) : undefined;

                  return new ObjectsList<Profile>(resProfiles, getMore);
              });

            })

        }
    };
}
