import { Injectable, Optional, EventEmitter } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable, Subscriber } from 'rxjs';

import { ApiConfig } from './api.config';


const sha256 = require('crypto-js/sha256');
const hash = (s) => sha256(s).toString().substring(0, 64).toLowerCase();


@Injectable()
export class ApiAuthService {
    RO: boolean = false;
    isAdmin: boolean = false;

    private sessionId: string;
    private sessionSalt: string;
    private userId: string;
    private userPK: string;
    private userSalt: string;
    private apiUrls: any = new ApiConfig().apiUrls;
    private apiStatus: string;
    private hasConnection: boolean = true;
    private onLogin: EventEmitter<boolean> = new EventEmitter<boolean>();
    private loginInformation: any;
    private cache = new APICache();
    private cnxStatus: string // OFFLINE / OFFLINE;

    private urlParams: any;

    constructor(private http: Http, @Optional() config: ApiConfig, ) {
        if (config) {
            if (typeof config === 'function') {
                config = (<() => any>config)();
            }
            this.apiUrls = config.apiUrls;
        }
        this.urlParams = window.location.search.substring(1);
    }

    login(username?: string, password?: string, rememberMe?: boolean): Observable<any> {
        // const trackerId = getCookie('ruuid') || undefined;
        //const protocol = window.location.protocol || 'https';
        const url = `https://${this.apiUrls.coldapi}/sessions/create`;
        const message = {
            session_create: {
                device : 'device_web',
                local: navigator.language,
                // uuid: trackerId,
                login: username
            }
        };

        const observable = this.http.post(url, JSON.stringify(message)).map((res: any) => {
            const body = res.json();
            this.sessionId = body.session_create.session_id;
            this.sessionSalt = body.session_create.session_salt;
            this.userId = body.session_create.member_id || undefined;
            this.userSalt = body.session_create.member_salt || undefined;
            this.userPK = this.userId ? hash(password + this.userSalt) : undefined;
            if (rememberMe === true && this.userId) {
                // currently, only for the session (see how to use jwt for storing loginInformations on computer)
                const loginInformations = {rive: {login: username, userkey: this.userPK}};
                //document.cookie = `currentLogin=${JSON.stringify(loginInformations) || ''};path=/;expires=${new Date(new Date().valueOf() + 30 * 24 * 3600 * 1000)}`;
                document.cookie = `currentLogin=${JSON.stringify(loginInformations) || ''};path=/;`;
            }
            this.loginInformation = {rive: {login: username, userkey: this.userPK}};
            return res;
        }).flatMap(res => {
            const urlLogin = '/sessions/login';
            const messageLogin = {
                session_login: {
                    device: 'device_web',
                    session_id: this.sessionId,
                    sign: hash(this.sessionSalt + (this.userPK || ''))
                }
            };

            return this.call(urlLogin, messageLogin, {bypassRetry: true}).map(resLogin => {
                const jres = resLogin.json();
                this.apiStatus = 'ONLINE';
                return {id: this.userId, partnerId: jres.partner_id};
            });
        }).catch(error => {
            if (error.json) {
                error = new InError(error.json().reason, error.json());
            }

            return Observable.throw(error);
        });

        return observable;
    }

    logout(): Observable<any> {
        return this.login();
    }

    autoLog(loginInformation: any): Observable<any> {
        if (!loginInformation.hasOwnProperty('rive')) return Observable.throw('UNEXPECTED_FORMAT()');
        const url = `https://${this.apiUrls.coldapi}/sessions/create`;
        const message = {
            session_create: {
                device : 'device_web',
                local: navigator.language,
                // uuid: trackerId,
                login: loginInformation.rive.login
            }
        };

        const observable = this.http.post(url, JSON.stringify(message)).map((res: any) => {
            const body = res.json();
            this.sessionId = body.session_create.session_id;
            this.sessionSalt = body.session_create.session_salt;
            this.userId = body.session_create.member_id || undefined;
            this.userSalt = body.session_create.member_salt || undefined;
            this.userPK = loginInformation.rive.userkey;

            return res;
        }).flatMap(res => {
            const urlLogin = '/sessions/login';
            const messageLogin = {
                session_login: {
                    device: 'device_web',
                    session_id: this.sessionId,
                    sign: hash(this.sessionSalt + this.userPK)
                }
            };

            return this.call(urlLogin, messageLogin, {bypassRetry: true}).map( resLogin => {
                const jres = resLogin.json();
                this.apiStatus = 'ONLINE';
                this.loginInformation = loginInformation
                return {id: this.userId, partnerId: jres.partner_id};
            });
        }).catch(error => {
            if (error.json) {
                error = new InError(error.json().reason, error.json());
            }
            /*if (error.toString() === 'MSG_BAD_AUTH()') {
                //this.router.navigate(['/login']);
                return Observable.throw(error)
                // return Observable.empty(); // prevent triggering the error
            }*/
            return Observable.throw(error);
        });

        return observable;
    }

    relog() {
        if (this.hasConnection) {
            this.autoLog(this.loginInformation).subscribe(() => {
                this.onLogin.emit(true);
            });
        }
    }

    private getEndpointUrl(url: string, message: string, options: {urlParameters?: string, session?}): string {
        if (options && options.session === false) return url;

        const urlParameters = options && options.urlParameters;
        const sign = hash(this.sessionSalt + (this.userPK || '') + message);
        const params = urlParameters ? '&'.concat(urlParameters) : '';

        return `${url}?auth=${this.sessionId}/${sign}${params}&${this.urlParams}`;
    }

    call(url: string, message: any = {}, options: any = {}): Observable<any> {
        if (this.RO && (options.rights || []).indexOf('write') >= 0) {
            return Observable.throw('ACTION_NOT_ALLOWED()');
        }
        if (!this.isAdmin && (options.rights || []).indexOf('admin') >= 0) {
            return Observable.throw('ACTION_NOT_ALLOWED()');
        }

         // TODO tester si connection ok (pour utilisation sur mobile device)
        if (this.apiStatus === 'NOAUTH' && !options.bypassRetry) {
            return Observable.fromPromise(this.retryCall(url, message, options));
        }

        let apiUrl;
        if (options.apiid && this.apiUrls.hasOwnProperty(options.apiid)) {
            apiUrl = this.apiUrls[options.apiid];
        } else {
            apiUrl = this.apiUrls.coldapi;
        }

        if (url.indexOf('/') === 0) url = url.substr(1);

        const callUrl = `https://${apiUrl}/${url}`;

        const data = JSON.stringify(message);

        const endPointUrl =  this.getEndpointUrl(callUrl, data, options);
        // is it cached ?
        if (options.cache && (options.rights || []).indexOf('write') < 0 && this.cache.has(endPointUrl)) {
            return this.cache.get(endPointUrl);
        }

        const myCall = this.http.post(endPointUrl, data/*, new Headers({'Content-Type': 'application/json'})*/)
            .map(res => {
                this.cnxStatus = 'ONLINE';
                if ((options.rights || []).indexOf('write') < 0) this.cache.add(endPointUrl, Observable.from([res]));
                return res;
            })
            .catch(err => {
                this.cache.del(endPointUrl);
                if (err.status === 0) { // net::ERR_NETWORK_IO_SUSPENDED
                    this.cnxStatus = 'OFFLINE';
                    return Observable.throw('CONNECTION_LOST()');
                    // TODO: test if connection recovers
                }
                if (err.status === 200) {
                    console.log(`Error 200 in "${callUrl}" for:`, data);
                    return Observable.throw('INTERNAL_SERVER_ERROR()');
                }
                if (err.json) {
                    const errMsg = err.json();
                    if (errMsg.ok === false) {
                        if (errMsg.reason === 'MSG_AUTH_EXPIRED()'  || errMsg.reason === 'Session Expired') {
                            if (this.apiStatus === 'ONLINE') {
                                this.apiStatus = 'NOAUTH';
                                this.relog();
                            }
                            return Observable.fromPromise(this.retryCall(url, message, options));
                        }
                        console.log(`Error: ${errMsg.reason} in "${callUrl}" for`, data);
                        return Observable.throw(new InError(errMsg.reason, errMsg));
                    }
                }

                return Observable.throw('INTERNAL_SERVER_ERROR()');
            });

        if ((options.rights || []).indexOf('write') < 0) this.cache.add(endPointUrl, myCall);
        return myCall;
    }

    retryCall(url: string, message: any = {}, options: any = {}): Promise<any> {
        options.nbRetry = (options.nbRetry || 0) + 1;
        if (options.nbRetry >= 3) {
            this.hasConnection = false;
            throw 'TOO_MANY_RETRIES()';
            // return Promise.reject('TOO_MANY_RETRIES()');
        }
        let sub: Subscriber<any>;
        const promise = new Promise((resolve, reject) => {
            sub = this.onLogin.subscribe(
                () => {
                    sub.unsubscribe();
                    this.call(url, message, options)
                        .catch(err => {
                            reject(err);
                            return Observable.from([]);
                        })
                        .subscribe(res => {
                            resolve(res);
                        });
                },
                (err) => { reject(err); }
            );
        })

        return promise;
    }
}

export class InError {

    constructor(public errorMessage, public options: {noParse?: boolean, noShow?: boolean} = {}, public originalResponse?){}

    toString() {
        return this.errorMessage;
    }

    get noParse(): boolean {
        return !!this.options.noParse;
    }

    get noShow(): boolean {
        return !!this.options.noShow;
    }
}

class APICache {
    private maxSize: number = 200;
    private minSize: number = 100;
    private cache: Map<string, any> = new Map<string, any>();
    private cacheAge: Map<string, Date> = new Map<string, Date>();

    has(id: string): boolean {
        return this.cache.has(id);
    }

    add(id, value): void {
        this.cache.set(id, value);
        this.cacheAge.set(id, new Date());
        this.clean();
    }

    get(id): any {
        if (this.cache.has(id)) {
            this.cacheAge.set(id, new Date());
            return this.cache.get(id);
        }
    }

    del(k: string) {
        this.cache.delete(k);
        this.cacheAge.delete(k);
    }

    private clean() {
        if (this.cache.size > this.maxSize) {
            const oldest = Array.from(this.cacheAge.entries())
                .sort((a: any, b: any) => b[1] - a[1])
                .slice(this.minSize)
                .map(e => e[0]);

            oldest.forEach(k => {
                this.cache.delete(k);
                this.cacheAge.delete(k);
            })
        }
    }
}