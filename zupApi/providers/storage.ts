import { Observable } from 'rxjs';

import { ZupApiService } from './zupApi.service';

import  { Post, Task, Notification, Profile } from '../models';
import { tools } from "../misc/utils";

export const objectFormat = {
    XS: 0,
    S: 1,
    L: 2,
    XL: 3
}

export class StorageError {
    type: string;
    isError: boolean = true;
    postId?: string;
    userId?: string | string[];
    notificationId?: string;
    taskId?: string
    constructor(values: any = {}) {
        for (let p in values) {
            this[p] = values[p];
        }
    }
}

export class InStorage {
    // We only store jsons elements, not entities


    private data: {
        profile?: Map<string, any>,
        post?: Map<string, any>,
        notification?: Map<string, any>,
        task?: Map<string, any>,
        label?: Map<string, any>,
    } = {
        profile: new Map<string, any>(),
        post: new Map<string, any>(),
        notification: new Map<string, any>(),
        task: new Map<string, any>(),
        label: new Map<string, any>(),
    };

    private objectTypes = ['profile', 'post', 'notification', 'task', 'label'];


    constructor(private api: ZupApiService) {
        /*  this.detailFunctions.user  = this.api.getUsersList;
         this.detailFunctions.post  = this.api.getPostsDetail;
         this.detailFunctions.notification  = this.api.notifications.get;*/
    }

    addObjectWithFormat(jsonObject: any, type: string, format?: number) {
        if (this.objectTypes.indexOf(type) < 0) throw `OBJECT_TYPE_UNKNOWN(${type})`;
        jsonObject.format = format
        if (this.data[type].has(jsonObject.id)) {
            const o = this.data[type].get(jsonObject.id);
            if (o.format >= format) {
                return;
            } else {
                Object.assign(o, jsonObject);
            }

        } else {
            this.data[type].set(jsonObject.id, jsonObject);
        }
    }

    addObject(jsonObject: any, type: string) {
        if (this.objectTypes.indexOf(type) < 0) throw `OBJECT_TYPE_UNKNOWN(${type})`;

        // shall we replace or update exisiting Object ?
        // should we check jsonFormat for each objectType (be sure to have all fields)
        if (this.data[type].has(jsonObject.id)) {
            const o = this.data[type].get(jsonObject.id);
            o.isPublic = o.isPublic && jsonObject.isPublic; // une seule gestion des formats (public = format rive)
            tools.deepAssign(o, jsonObject);
        } else {
            this.data[type].set(jsonObject.id, jsonObject);
        }
    }

    addObjects(jsonObjects: any[], type: string, format?: number) {
        if (!jsonObjects) return;
        if (format) {
            jsonObjects.map(o => this.addObjectWithFormat(o, type, format));
        } else {
            jsonObjects.map(o => this.addObject(o, type));
        }

    }

    addPostList(list: {posts: any[], posts_classified_negative?: string[], posts_classified_positive?: string[], profiles?: any[]}) {
      if (list.profiles) {
        this.addObjects(list.profiles, 'profile', objectFormat.S);
      }

      const classification = new Map<string, number>();
      if (list.posts_classified_positive) list.posts_classified_positive.forEach(c => classification.set(c, 1))
      if (list.posts_classified_negative) list.posts_classified_negative.forEach(c => classification.set(c, -1))

      if (list.posts) {
        list.posts.forEach(p => p.content.sentiment_score = classification.get(p.id));

        this.addObjects(list.posts, 'post', objectFormat.S);
      }

    }

    getJsonObjects(ids: string[], type: string): any[] {
        if (this.objectTypes.indexOf(type) < 0) throw `OBJECT_TYPE_UNKNOWN(${type})`;
        return ids.map((id: string) => this.data[type].get(id)).filter((o: any) => !!o);
    }

    /**
     *
     * @param id
     * @param {boolean} useCache if true, force user from cache, if false, ask api, if undefined check cache then api
     * @returns {Observable<R>}
     */
    getProfile(id: string, useCache?: boolean, format?: number): Observable<Profile | StorageError> {

        return (useCache || (this.data.profile.has(id) && useCache !== false)) ?
            Observable.from([this.profileObject(id, format)]) :
            this.api.profiles.loadToCache([id]).map(() => {
                const u = this.profileObject(id);
                if (!u || u.isError) {
                    const err = new StorageError();
                    err.type = 'user';
                    err.userId = id;
                    console.log(err);
                    return err;
                }
                return u;
            });
    }

    /**
     *
     * @param {string[]} ids : list of profils ids
     * @param { boolean} useCache : if true: only search cache, if false: only search remote, undefined: search both
     * @returns {Observable<Profile[]>}
     */
    getProfiles(ids: string[], useCache?: boolean, format?: number): Observable<(Profile)[]> {
        let users = [];
        const missingUsers = [];
        ids.map( id => {
            const u = this.profileObject(id, format);
            if (u && !u.isError  && useCache !== false) {
                users.push(u);
            } else {
                missingUsers.push(id);
            }
        });

        if (missingUsers.length > 0 && useCache !== true) {
            return  this.api.profiles.loadToCache(missingUsers, format).map(() => {
                users = users.concat(missingUsers.map(uid => {
                    const u = this.profileObject(uid);
                    if (!u || u.isError) {
                        const err = new StorageError({type: 'userInUsers', userId: uid});
                        console.log(err);
                        return err;
                    }
                    return u;
                }));
                return <Profile[]>users.filter((p: Profile | StorageError) => !p.isError);
            });
        } else {
            return Observable.from([<Profile[]>users]);
        }
    }

    getPosts(ids: string[], format?: number): Observable<Post[]> {
        let posts = [];
        const missingPosts = [];

        ids.map(id => {
            if (!id) return;
            const p = this.postObject(id, format);
            if (p && !p.isError) {
                posts.push(p);
            } else {
                missingPosts.push(id);
            };
        });

        if (missingPosts.length === 0) {
            return Observable.from([posts]);
        } else {
            return this.api.posts.loadToCache(missingPosts, format)
                .map(() => {
                    posts = posts.concat(missingPosts.map(pid => {
                            const p = this.postObject(pid, format);

                            if (!p) {
                                const err = new StorageError({type: 'postInPosts', postId: pid});
                                console.log(err);
                                return err;
                            }
                            return p;
                        })
                    );
                    return <Post[]>posts.filter(p => !p.isError);
                });
        }
    }

    getNotifications(ids: string[], format?: number): Observable<Notification[]> {

        return Observable.from([<Notification[]>ids.map( nid => {
            const t = this.notificationObject(nid, format);
            if (!t) {
                const err = new StorageError({type: 'notificationsInNotifications', notificationId: nid});
                console.log(err);
                return err;
            }
            return t;
        }).filter((n: any) => !n.isError)]);
    }

    getTasks(ids: string[], format?: number): Observable<Task[]> {
        return Observable.from([<Task[]>ids.map( tid => {
            const t = this.taskObject(tid, format);
            if (!t) {
                const err = new StorageError({type: 'taskInTasks', taskId: tid});
                console.log(err);
                return err;
            }
            return t;
        }).filter((t: any) => !t.isError)]);
    }

    has(id: string, type: string): boolean {
        return this.data[type].has(id);
    }

    get labels() {
      return Array.from(this.data.label.values());
    }

    postList2Posts(list: {posts: any[], profiles: any[]}): Post[] {
        const profileIds = list.profiles.map(p => p.id);

        this.addObjects(list.profiles, 'profile');
        this.addObjects(list.posts.filter(p => profileIds.indexOf(p.author_id || p.authorid) >= 0), 'post');

        return <Post[]>list.posts.filter(p => profileIds.indexOf(p.author_id || p.authorid) >= 0)
            .map(p => this.postObject(p.id));
    }

    profileObject(id: string, format?: number): Profile | StorageError {
        if (id && this.data.profile.has(id)) {
            if (format && (this.data.profile.get(id).format || 0)  < format) return;
            return new Profile(this.data.profile.get(id), Array.from(this.data.label.values()));
        } ;
    }

    postObject(id: string, format?: number): Post | StorageError {
        if (this.data.post.has(id)) {
            const jp = this.data.post.get(id);

            const u = this.profileObject(jp.authorid || jp.author_id, format);
            if (u && u instanceof Profile) {
                return new Post(jp, u);
            } else {
                const err = new StorageError({type: 'userInPost', userId: jp.authorid || jp.author_id, postId: id});
                console.log(err);
                return err;
            };
        };
    }

    notificationObject(id: string, format?: number): Notification | StorageError {
        if (this.data.notification.has(id)) {
            const jn = this.data.notification.get(id);
            const u = this.profileObject(jn.post_author);
            let p: any;

            if (jn.post) {
                p = this.postObject(jn.post, format);
                if (!p || p instanceof StorageError) {
                    const err = new StorageError({type: 'postInNotification', postId: jn.post, notificationId: id});
                    console.log(err);
                    return err;
                }
            }
            if (!u || u instanceof StorageError) {
                const err = new StorageError({type: 'userInNotification', userId: jn.post_author, notificationId: id});
                console.log(err);
                return err;
            }
            return new Notification(jn, <Profile>u, p);
        }
    }

    taskObject(id: string, format?: number): Task | StorageError {
        if (id && this.data.task.has(id)) {
            const jt = this.data.task.get(id);
            let p: Post | StorageError;
            let n: Notification | StorageError;
            let u: Profile | StorageError;

            const ub = this.profileObject(jt.assigned_by, objectFormat.XS);
            const ut = this.profileObject(jt.assigned_to, objectFormat.XS);

            if (!ub || !ut || ub instanceof StorageError || ut instanceof StorageError) {
                const err = new StorageError({
                    type: 'userInTask',
                    userId: [jt.assigned_by, jt.assigned_to],
                    taskId: id
                });
                console.log(err);
                return err;
            }

            if (jt.post_id) {
                p = this.postObject(jt.post_id, format);
                if (!p || p instanceof StorageError) {
                    const err = new StorageError({type: 'postInTask', postId: jt.post_id});
                    console.log(err);
                    return err;
                }
            }

            if (jt.notification_id) {
                n = this.notificationObject(jt.notification_id, format);
                if (!n || n instanceof StorageError) {
                    const err = new StorageError({
                        type: 'notificationInTask',
                        notificationId: jt.notification_id,
                        taskId: id
                    });
                    console.log(err);
                    return err;
                }
            }

            if (jt.profile_id) {
                u = this.profileObject(jt.profile_id, format);
                if (!u || u instanceof StorageError) {
                    const err = new StorageError({type: 'profileInTask', userId: jt.profile_id});
                    console.log(err);
                    return err;
                }
            }

            return new Task(jt, <Notification>n || <Post>p || <Profile>u, <Profile>ub, <Profile>ut);

        }
    }
}
