import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';

import { ZupApiService } from './zupApi.service';
import { HistoLogsService } from './histologs.service';
import { LexicalService } from "./lexical.service";
import {
    Campaign,
    FilterRules,
    Label,
    Lexical,
    lexicalCategories,
    lexicalServices,
    Member,
    NewLexical,
    ObjectsList,
    Partner,
    Profile,
    PublishAction,
    toPublishAction,
    View,
    widgetCategories,
    Widget,
    WidgetContext,
    Wall
} from '../models';

import { ids, tools } from '../misc/utils';
import { _attributionTypes } from '../listOfValues';
import {widgetTypes} from "../models/widget";




export enum STATUS {
    NONE = 0,
    INITATING = 1,
    INITED = 2
}

@Injectable()
export class PartnerService {

  member: Member;
  roles: string[] = [];
  partner: Partner;
  _labels: ObjectsList<Label>;
  _walls: any[];
    status: number = STATUS.NONE;

  public widgetsListChange: Subject<Widget[]> = new Subject<Widget[]>();
  public labelsListChange: Subject<Label[]> = new Subject<Label[]>();
  public wallsListChange: Subject<any[]> = new Subject<any[]>();
  public ICListChange: Subject<any[]> = new Subject<any[]>();
  public mainWidgetContext: WidgetContext;

  private defaultSA: any;
  private _onInit: Subject<boolean> = new Subject<boolean>();

  constructor(private api: ZupApiService, private histoLogs: HistoLogsService, private lexicals: LexicalService) {}


  /**
   * Tests if the member loggued is the main account for this partner
   * @returns {boolean}
   */
  get isAdmin(): boolean {
    return this.member && this.member.id === this.partner.id;
  }

  /**
   * Tests if Partner information (widgets, thresholds, lables, ...) are loaded
   * @returns {boolean}
   */
  get isInited(): boolean {
      return this.status === STATUS.INITED;
  }


  get isWallOnly(): boolean {
      return (this.partner.woUsers || []).indexOf(this.member.id) >= 0;
  }

  /**
   * Returns the labels created for this partner
   * @returns {any[]}
   */
  get labels() {
    return this._labels.list.filter(l => l.name.indexOf('ICampaign_') !== 0);
  }

  get myName() {
    return this.member && this.member.name;
  }

  get widgetList(): Widget[] {
    return (this.partner && this.partner.widgets) || [];
  }

  get walls(): Wall[] {
    return this._walls;
  }
  addCampaign(campaign: Campaign): Observable<Campaign> {
    return this.api.campaigns.add(campaign);
  }

  addFacebookToken(profiles: Profile[]) {
    this.partner.socialAccounts = this.partner.socialAccounts
        .filter(p => p.attributionKind !== _attributionTypes.FACEBOOK)
        .concat(profiles);
    this.partner.fbProfilesLoaded = true;
  }

  addRight(right: string) {
    if (this.partner.rights.indexOf(right) < 0) this.partner.rights.push(right);
  }

  addView(view: View) {
    this.getWidget(view.parentWidgetId).widgetViews.push(view);
  }

  /*
  addWidget(widget: Widget) {
    this.partner.widgets.push(widget);
    this.widgetsListChange.next(this.partner.widgets);
  }*/

  createInfluenceCampaign() {
      this.ICListChange.next( this.getInfluenceCampaigns());
  }

  createWidget(w, isI4U?): Observable<Widget> {
    return this.api.widget.create(w, isI4U).map((wnew) => {
      this.partner.widgets.push(wnew);
      this.partner.widgetAndViews.set(wnew.id, wnew);
      this.widgetsListChange.next(this.partner.widgets);
      return wnew;
    })
  }

  deleteWidget(w: Widget): Observable<boolean> {
      return this.api.widget.delete(w).map(() => {
        const i = this.partner.widgets.findIndex(w2 => w2.id === w.id);
          if (i >= 0) {this.partner.widgets.splice(i, 1);}
          this.partner.widgetAndViews.delete(w.id);
          this.widgetsListChange.next(this.partner.widgets);
          return true;
      });
  }

  deleteInfluenceCampaign(w: Widget): Observable<boolean> {
    return this.deleteWidget(w).map(() => {
        this.ICListChange.next( this.getInfluenceCampaigns());
        return true;
    });
  }

  updateInfluenceCampaign() {

      this.ICListChange.next( this.getInfluenceCampaigns());
  }


  getInfluenceCampaign(wid: string): Widget {
    return this.getWidget(wid);
  }

  getInfluenceCampaigns(): Widget[] {
    return this.getWidgets({category: widgetCategories.PROFILE_CAMPAIGN})
  }

  private _influenceBrands;
  getInfluenceBrands(): Observable<{widget: Widget, me: boolean, profile: Profile }[]> {
    //if (!this.isInited) return this.onInit().flatMap(() => this.getInfluenceBrands());
    if (this._influenceBrands) return Observable.of(this._influenceBrands);
    this._influenceBrands = [];
    const todos = [];
    this.getWidgets({type: 4}).forEach((w: Widget) => {
      todos.push(
          this.api.labels.profilesList(w.labelId, {limit:1},{simpleProfiles: true}).map(res => {
            if (w.category === widgetCategories.PROFILE_MY_BRAND) {
              return {widget: w, me: true, profile: res.list[0]};
            } else if (w.category === widgetCategories.PROFILE_COMPETITOR) {
              return {widget: w, me: false, profile: res.list[0]};
            }
          })
      )
    });


    return Observable.forkJoin(...todos).map(res => {
      res.forEach(r => {
        if (r) {
          this._influenceBrands.push(r);
        }
      });
      return this._influenceBrands;
    });
  }

  getInfluenceBrandWidgets(): Widget[] {
    return this.getWidgets({type: 4}).filter(w => w.category === widgetCategories.PROFILE_MY_BRAND || w.category === widgetCategories.PROFILE_COMPETITOR);
  }


  decodeId(id: string): string {
    return ids.fromSafe(id);
  }

  delCampaign(campaign: Campaign): Observable<boolean> {
    return this.api.campaigns.delete(campaign);
  }

  delView(view: View | string) {
    const myView = typeof  view === 'string' ?  this.getWidget(<string>view) : <View>view;

    const index = this.getWidget(myView.parentWidgetId).widgetViews.findIndex((v: View) => v.id === myView.id)
    if (index >= 0) this.getWidget(myView.parentWidgetId).widgetViews.splice(index, 1);
  }

  delWidget(widgetId: string) {
    return this.api.widget.delete(this.getWidget(widgetId)).map(
        () => {
          const index = this.partner.widgets.findIndex( w => { return w.id === widgetId; });
          this.partner.widgets.splice(index, 1);
          this.widgetsListChange.next(this.partner.widgets);
        }
    );
  }

  getCampaigns(): Observable<Campaign[]> {
    return this.api.campaigns.list().map(res => {
      const campaigns: Campaign[] = [];
      res.forEach((pa: any) => {
        let myCampaign = campaigns.find(ca => ca.label.id === pa.profiles_label_id)
        if (!myCampaign) {
          myCampaign = {
            label: this.labels.find(l => l.id === pa.profiles_label_id),
            publishActions: []
          }

          campaigns.push(myCampaign);
        }
/*
        myCampaign.publishActions.push({
          follow: pa.follow,
          comment: pa.comment,
          like: pa.like,
          mention: pa.mention,
          tokenId: pa.token_xid,
          id: pa.id,
          message: pa.message,
          mentionMessage: pa.mention_message,
          fileUrls: pa.media_urls,
          mentionFileUrls: pa.mention_media_urls
        })*/

        myCampaign.publishActions.push(toPublishAction(pa));
      });

      // check if campaigns from label without TOKENS :
      Array.from(this._labels).forEach(l => {
        if (l.isCampaign && campaigns.findIndex(c => c.label.id === l.id) < 0) {
          campaigns.push(<Campaign>{label: l, publishActions: []});
        }
      })

      return campaigns;
    });
  }

  getLabel(id: string): Label {
    id = decodeURIComponent(id);
    return this._labels.list.find(l => l.id === id);
  }

  getMember( memberId: string) {
    return this.partner.members.find( (m) => m.id === memberId);
  }

  /**
   * Returns the thredsholds for the type of widget provided and the word for "Word Alerts"
   * @param {number} widgetId
   * @returns {Observable<R>}
   */
  getNotificationsLimit(widgetId?: string) {
    if (widgetId) {
      const limits: any = {
      };
      const widgets = this.getWidgets({id: widgetId});

      if (widgets.length > 0) {
        limits.userDailyThreshold = widgets[0].userDailyThreshold || 0;
        limits.influencerThreshold = widgets[0].influencerThreshold || 0;
        limits.customerLeadThreshold = widgets[0].customerLeadThreshold || 0;
        limits.loyaltyNbThreshold =
            widgets[0].loyaltyNbThreshold ? widgets[0].loyaltyNbThreshold : 0;
        limits.loyaltyDaysThreshold =
            widgets[0].loyaltyDaysThreshold ? widgets[0].loyaltyDaysThreshold : 0;
        limits.useDefaultThresholds = widgets[0].useDefaultThresholds || false;
      }

      return limits;
    } else {
      return Object.assign({useDefaultThresholds: true}, this.partner.defaultLimits);
    }

  }

  getDefaultDetectedWords(): string[] {
    return this.partner.defaultWordsAlert;
  }

  getSocialAccounts(kind: string): Observable<Profile> {
    return this.getDefaultSocialAccounts().map(res => {
      if (res[kind]) {
        return res[kind];
      } else {
        const accounts = this.partner.socialAccounts.filter(u => { return u.kind === kind; });
        if (accounts.length === 1) { // only one account => set it as default
          return accounts[0];
        }
      }
    })
  }

  getDefaultSocialAccounts(): Observable<any> {
    if (!this.defaultSA) {

      return this.api.misc.mstores.get('b2b_default_social_accounts').flatMap((resDSA: any) => {
        const profiles = [];
        for (let p in resDSA) {
          profiles.push(resDSA[p]);
        }


        return this.api.profiles.getMultiple(profiles).map((resProfiles: Profile[]) => {
          const DSA: any = {};

          for (let p in resDSA) {
            const dp = resProfiles.find(profile => profile.id === resDSA[p]);
            DSA[p] = dp;
          }
          this.defaultSA = DSA;
          return DSA;
        });
      });
    } else {
      return Observable.from([this.defaultSA]);
    }

  }

  getDetectedWords(): Observable<{widgetId: string, words: string}[]> {
    const lexicals: any[] = this.widgetList.map(w => {
      return {name: 'B2B_word_alert_lexical_name', widgetId: w.id};
    })

    return this.api.lexical.list(lexicalCategories.ALERT).catch( err => {
      return Observable.from([[]]);
    }).map( (res: Lexical[]) => {
      const defaultWords =  this.getDefaultDetectedWords();
      const widgetWords: any[] = res.map(l => {
        const words = l.values.filter((v: any) => defaultWords.indexOf(v) < 0);
        return {widgetId: l.widgetId, words: words.toString()};
      });
      widgetWords.push({words: defaultWords.toString()});
      return widgetWords;
    });
  }

  getProfileICampaigns(profile: Profile): Widget[] {
    const campaigns = this.getInfluenceCampaigns();
    const profileCampaigns: Widget[] = [];

    campaigns.forEach(c => {
      const lid = c.labelId;
      if (profile.labels.map(l => l.id).indexOf(lid) >= 0) {
        profileCampaigns.push(c);
      }
    });

    return profileCampaigns;
  }

  getRecordedMessages(): {attribution: string, message: string, name?: string}[] {
    return this.partner.recordedMessages;
  }

  getSources() {
    let sources = [];
    this.partner.widgets.forEach(w => sources = sources.concat(w.sources));

    return tools.unduplicate(sources);
  }

  getWall(id: string): Wall {
    const wall = this._walls.find(w => w.id === id);
    if (wall) return wall;
  }

  getWidget(id: string): Widget {
    // ///////////////////////////
    if (!this.partner.widgets) return ;

    if (this.partner.widgetAndViews.has(id)) {
      return this.partner.widgetAndViews.get(id);
    } else {
      const w = new Widget()
      w.name = 'Unknown Place';
      return w;
    };
  }


  getWidgetDetail(id: string): Observable<Widget> {
    if (!this.partner.widgets) return Observable.from([]);

    return this.api.widget.get(id).map((w: Widget) => {
      //keep views :
      const views = this.getWidget(id).widgetViews;
      w.widgetViews = views;
      this.partner.widgetAndViews.set(w.id, w.clone())
      return w;
    });
  }

  // TODO: remove main WidgetContext
  /**
   *
   * @param {{id?: string, type?: number, category?: number, main?: boolean, defaultPeriode?: string}} options
   *          main = true, saves the context to mainWidgetContext
   * @returns {WidgetContext}
   *//*
  getWidgetContext(options?: {id?: string, type?: number, category?: number, main?: boolean}) {
    let wc: WidgetContext;
    if (!options) {
      wc = new WidgetContext();
    } else if (options.id) {
      const w = this.getWidget(this.decodeId(options.id));
      wc =  new WidgetContext(w, {type: w.type});
    } else if (options.category) {
      wc = new WidgetContext().cat(options.category);
    }

    if (options.main) {
      this.mainWidgetContext = wc;
    }

    return wc;
  }*/

  getWidgetName(id: string): string {
    if (!this.partner.widgets) return 'Unknown Place';

    if (this.partner.widgetAndViews.has(id)) {
      const w = this.partner.widgetAndViews.get(id)
      if (w.parentWidgetId) {
        const parent = this.partner.widgetAndViews.get(w.parentWidgetId);
        if (!parent) {
          console.log(`PARENT_WIDGET_NOT_FOUND(${id},${w.parentWidgetId})`);
          return 'Unknown Place';
        }
        return parent.name.concat(' : ', w.name);
      }
      return this.partner.widgetAndViews.get(id).name;
    } else {
      return 'Unknown Place';
    };
  }

  getWidgetRuleName(w: {type, id}, options?: {customCats?}): string {
    if (!w) return 'Unknown'
    let cat, type;
    switch (w.type) {
      case 'widget':
        return this.getWidgetName(w.id);
      case 'category':
        const customCat = (tools.deepGet(options, 'customCats')||[]).find(cc => cc.value.id === w.id && cc.value.type === 'category');
        if (customCat) {
          return customCat.text;
        }
          cat = widgetCategories.detail(w.id);
          type = widgetTypes.detail(cat.type)
          return `${type.label} : ${cat.label}`;


      case 'type':
        const customType = (tools.deepGet(options, 'customCats')||[]).find(cc => cc.value.id === w.id && cc.value.type === 'type');
        if (customType) {
          return customType.text;
        }

        type = widgetTypes.detail(w.id)
        return `${type.label}`;
      default :
        return 'Unknown';
    }
  }

  getWidgets(options?: any): Widget[] {
    if (!options) return this.partner.widgets;

    return this.partner.widgets.filter( w => {
      for (let o in options) {
        if (o === 'types' && options[o]) {
          if (options.types.indexOf(w.type) < 0) return false;
        }
        else if (o === 'categories' && options[o]) {
          if (options.categories.indexOf(w.category) < 0) return false;
        }
        else {
          if (options[o] !== undefined && options[o] !== w[o]) return false;
        }
      };
      return true;
    });
  }

  hasRightsFor(rights: string[]): boolean {
    for (let r of rights) {
      //one right can be like "right1,right2" in this case, it means right1 OR right2
      const rs = r.split(',');

      if (rs.length === 1 && rs[0] === 'Admin') {
        return this.isAdmin;
      } else {
        let isOK = false;
        rs.forEach(r => {isOK = isOK || this.partner.rights.indexOf(r) >= 0;})
        //if (this.partner.rights.indexOf(r) < 0) return false;
        if (!isOK) return false;
      }
    }
    return true;
  }

  isInfluencerList(lid: string) {
    if (!lid) return false;
    const wl: Widget[] = this.widgetList;
    for (let i = 0; i < wl.length; i++) {
      if (wl[i].isProfileListWidget && wl[i].labelId === lid) {
        return true;
      }
    }
    return false;
  }
  /**
   * Returns an Observable that returns true when partner information are loaded
   * @returns {Observable<boolean>}
   *
   * ###exemple :
   * ```
   * partnerService.onInit().subscribe( ok => {
   *    // Do something
   * })
   * ```
   */
  onInit(): Observable<boolean> {
    if (this.status === STATUS.INITED) return Observable.from([true]);
    return this.widgetsListChange.first().map(() => { return true; });
  }

  onInit2(callback: () => void) {
    if (this.status === STATUS.INITED) {
      callback();
    } else {
      this._onInit.take(1).subscribe(() => callback());
    }

  }

  refreshLabels(): Observable<any[]> {
    return this.api.labels.listAll().map(res => {
      this._labels = res;
      this.labelsListChange.next(this.labels);
      return this.labels;
    });
  }

  refreshWallList() {
    return this.api.wall.list().map(res => {
      this._walls = res;
      this.wallsListChange.next(this._walls);
    });
  }

  removeMember(id: string) {
    return this.api.members.remove(id).map(res => {
      this.partner.removeMember(id);
    });
  }
/*
  saveQueries(queries: {name: string, query: any}[]): Observable<boolean> {
    this.partner.advancedQueries = queries;
    return this.updatePartner();
  }
*/
  getQueries(type: string = 'profile'):  {name: string, query: any, type?: string}[] {
    return this.partner.advancedQueries.filter(q => (q.type || 'profile') === type); // <= 'profile' for retro-compatibility
  }


  addQuery(query:  {name: string, query: any, type?: string}):  {name: string, query: any, type?: string}[] {
    this.partner.advancedQueries.push(query);
    this.updatePartner().subscribe();
    return this.getQueries(query.type);
  }

  deleteQuery(queries: {name: string, query: any, type?: string}[] ):  {name: string, query: any, type?: string}[] {
    queries.forEach(query => {
      const i = this.partner.advancedQueries.findIndex(q => q.name === query.name);
      if (i >= 0) {
        this.partner.advancedQueries.splice(i, 1);
      }
    })
    this.updatePartner().subscribe();
    return this.getQueries(queries[0].type);
  }
  /**
   *
   * @param limits modifications in the thresholds and words for the alerts
   * @param widgetId
   * @returns {any}
   */
  setNotificationsLimit(limits: any, widgetId: string) {
    const todos = [];

    this.getWidgets({id: widgetId}).forEach( w => {
      for (let p in limits) {
        if (p !== 'useDefaultThresholds') {
          w[p] =  limits[p] ? parseInt(limits[p], 10) : undefined;
        } else {
          w[p] = !!limits[p];
        }
      }
      todos.push(this.api.widget.updateAlerts(w).flatMap(() => this.api.widget.updateWidgetInfo(w)).map(() => this.updatePartnerWidget(w)));
    });

    return Observable.forkJoin(...todos);
  }

  setDefaultLimits(limits: any): Observable<boolean> {
    const initDefault = JSON.stringify(this.partner.defaultLimits);

    for (var t in limits) {
      switch (typeof limits[t]) {
        case 'string' :
          this.partner.defaultLimits[t] = limits[t].length > 0 ? parseInt(limits[t], 10) : undefined;
          break;
        case 'number' :
          this.partner.defaultLimits[t] = limits[t];
              break;
        default :
          this.partner.defaultLimits[t] = undefined;
      }

    }

    if (typeof this.partner.defaultLimits.loyaltyNbThreshold !== typeof this.partner.defaultLimits.loyaltyDaysThreshold) {
      this.partner.defaultLimits = JSON.parse(initDefault);
      return Observable.throw('WRONG_VALUES()');
    }

    const todos = [];
    todos.push(this.api.partner.update(this.partner));

    this.getWidgets().forEach( w => {
      if (!w.useDefaultThresholds) return; // do not update customs values

      Object.assign(w, this.partner.defaultLimits);
      todos.push(this.api.widget.updateAlerts(w).map(() => this.updatePartnerWidget(w)));
    });

    return Observable.forkJoin(todos).map(() => true);
  }

  setDefaultSocialAccount(accountKind: string, profile?: Profile) {
    if (profile) {
      this.defaultSA[accountKind] = profile;
    } else {
      delete this.defaultSA[accountKind];
    }

    const DSA = {};

    for (let p in this.defaultSA) {
      if (this.defaultSA[p]) {
        DSA[p] = this.defaultSA[p].id;
      }
    }

    this.api.misc.mstores.put('b2b_default_social_accounts', DSA).subscribe();
  }

  /**
   *
   * @param {{widgetId?: string, words?: string}[]} words list of lexicals to update
   * @returns {Observable<boolean>}
   */
  setDetectedWords(words: {widgetId?: string, words?: string}[]): Observable<boolean> {
    const todos = [];

    // check if default has change :
    const defaultLexical: any = words.filter(w => !w.widgetId);
    const newDefaultWordsString = defaultLexical.length > 0 ? defaultLexical[0].words : this.getDefaultDetectedWords().toString();
    const newDefaultWords = this.cleanWords(newDefaultWordsString.length > 0 ? newDefaultWordsString.split(/\s*,\s*/) : []);


    if (defaultLexical) {
      this.partner.defaultWordsAlert = newDefaultWords;
      todos.push(this.api.partner.update(this.partner));
    }

    return this.api.lexical.list(lexicalCategories.ALERT).flatMap((res: Lexical[]) => {
      // update lexicals per widget :
      const toDelete = [];
      const toUpdate = [];
      const toAdd = [];
      words.forEach(w => {
        const newWords = this.cleanWords(w.words.length > 0 ? w.words.split(/\s*,\s*/) : []);
        if (newWords.length > 0 || newDefaultWords.length > 0) {
          if (w.widgetId) {
            const wa = res.find(l => l.widgetId === w.widgetId)
            if (wa) {
              wa.values = tools.unduplicate(newDefaultWords.concat(newWords));
              toUpdate.push(wa);
            } else {
              toAdd.push(<NewLexical> {
                displayName: 'word alert',
                widgetId: w.widgetId,
                values: tools.unduplicate(newDefaultWords.concat(newWords)),
                service: lexicalServices.WORD_ALERT
              })
            }
          }
        } else if (w.widgetId) {
          const wa = res.find(l => l.widgetId === w.widgetId);
          if (wa) toDelete.push(wa.id);
        }
      });
      if (toUpdate.length > 0 ) todos.push(this.api.lexical.update(toUpdate));
      if (toDelete.length > 0 ) todos.push(this.api.lexical.delete_multiple(toDelete));
      if (toAdd.length > 0 ) todos.push(this.api.lexical.set(toAdd));
      return Observable.forkJoin(todos).map(() => true);
    });


  }

  setRecordedMessages(messages: {attribution: string, message: string, name?: string}[]) {
    this.partner.recordedMessages = messages;
    this.api.partner.update(this.partner).subscribe();
  }

  private cleanWords(w: string[]): string[] {
    return w.map((s: string) => s.trim()).filter((s: string) => s.length > 0);
  }

  /**
   * replace setUser : load Partner information for this user
   *
   * @param userId
   */
  initPartner(userId?: string): Observable<Partner> {
    if (!userId) {// logout
      this.member = undefined;
        this.status = STATUS.NONE;
      return;
    }

    this.status = STATUS.INITATING;
    this.histoLogs.init();
    this.lexicals.init();
    return Observable.forkJoin(
        this.api.partner.get(),
        this.api.partner.widgetsList(101),
        this.api.members.list(),
        this.api.labels.listAll(),
        this.api.wall.list(),
        this.api.partner.listTokens()
    ).map(res => {
      this.partner = new Partner(res[0], res[1], res[2]);
      this.member = res[2].find(m => m.id === userId);

      // manage special rigts for Admin
      if (this.isAdmin) {
        this.addRight('Wall');
        this.addRight('Insuite');
        this.addRight('Influence'); //TODO : remove that on prod !!!
        this.addRight('I4U');
      }
      if (this.partner.hasInsuite && !this.hasRightsFor(['PartnerBInfluence'])) {
        this.addRight('Insuite');
      }

        if (this.hasRightsFor(['PartnerInfluence'])) {
            this.addRight('Influence');
        }

      /* not true anymore because I4U has this rights...
      if (this.hasRightsFor(['PartnerInfluence'])){
        this.addRight('Influence');
      }
      TODO :
       if (this.partner.hasInfluence) {
       this.addRight('Influence');
       }
      */

      if (this.hasRightsFor(['PartnerBInfluence'])){
        this.addRight('I4U');
      }

      this._labels = res[3];
      this._walls = res[4]//[]// res[4];
      this.partner.socialAccounts = res[5]//res[4]// res[5];
      this.status = STATUS.INITED;
      this.widgetsListChange.next(this.widgetList); // intitate
      this.labelsListChange.next(this.labels);
      this.wallsListChange.next(this._walls);
      this.ICListChange.next(this.getInfluenceCampaigns());

      this._onInit.next(true);
      return this.partner;
    });
  }

  updatePartner() {
    const sub = this.api.partner.update(this.partner);
    return sub;
  }

  updateWidget(widget: Widget): Observable<Widget> {
    return this.api.widget.update(widget, this.getWidget(widget.id)).map(() => {
      this.updatePartnerWidget(widget);
      return widget;
    });
  }

  updateView(view: View): Observable<View> {
    const originalView = this.getWidget(view.parentWidgetId).getView(view.id);
    return this.api.view.update(view, originalView).map((updatedView: View) => {
      if (view.id !== updatedView.id) {
        this.getWidget(view.parentWidgetId).delView(view.id);
      }
      this.getWidget(view.parentWidgetId).setView(updatedView)
      return updatedView;
    });
  }

  private updatePartnerWidget(widget: Widget) {
    const index = this.partner.widgets.findIndex(w => { return w.id === widget.id; });
    this.partner.widgets[index] = widget;
    this.partner.widgetAndViews.set(widget.id, widget);
    this.widgetsListChange.next(this.partner.widgets);
  }
/*
  // todo : remove mainContext, it is the appState service that should manage that
  getOrCreateMainWidgetContext(options: {id?: string, type?: number, category?: number, main?: boolean} = {}) {
    const currentWC = this.mainWidgetContext;
    if (!currentWC) return this.getWidgetContext(Object.assign({main: true}, options));

    if (options.id && ids.fromSafe(options.id) !== currentWC.widgetId) {
      return this.getWidgetContext(Object.assign({main: true}, options));
    }

    if (options.type && options.type !== currentWC.type) {
      return this.getWidgetContext(Object.assign({main: true}, options));
    }

    if (options.category && options.category !== currentWC.catId) {
      return this.getWidgetContext(Object.assign({main: true}, options));
    }

    return currentWC;
  }
*/

}
