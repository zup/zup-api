import { Injectable } from "@angular/core";
import { ZupApiService } from './zupApi.service';
import { Lexical, lexicalServices } from '../models/lexical';
import { Observable, BehaviorSubject } from 'rxjs';
import { InError } from './apiAuth.service';
import { tools } from '../misc/utils';

const enum STATUS {
    NONE = 0,
    INITED = 1
}

@Injectable()
export class LexicalService {
    private status = STATUS.NONE;
    private lexicals: Map<string, Lexical> = new Map<string, Lexical>();
    private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);


    constructor(private api: ZupApiService){}

    init() {
        this.loadLexicals().subscribe();
    }

    getLexicalName(id: string): string {
        if (this.status === STATUS.INITED) {
            return this.lexicals.get(id).displayName;
        }
    }

    getLexical(id: string): Observable<Lexical> {
        if (this.status === STATUS.INITED) {
            return Observable.of(this.lexicals.get(id));
        } else {
            return this.loading.map(() => {
                return this.lexicals.get(id);
            })
        };
    }

    getLexicals(...categories: string[]): Observable<Lexical[]> {

        if (this.status === STATUS.INITED) {
            let lexicals = [];
            if (categories.length ===0) {
                lexicals = this.getLexicalsArray();
            } else {
                categories.forEach(category => {
                    lexicals = lexicals.concat(this.getLexicalsArray(category));
                })
            }
            return Observable.of(lexicals);
        } else {
            return this.loading.map(() => {
                let lexicals = [];
                if (categories.length ===0) {
                    lexicals = this.getLexicalsArray();
                } else {
                    categories.forEach(category => {
                        lexicals = lexicals.concat(this.getLexicalsArray(category));
                    })
                };
                return lexicals;
            })
        }
    }

    getLexicals2(options: {categories?: string[], services?: string[]}): Observable<Lexical[]> {
        let lexicals = [];
        const filterLexical = (v, k) => {
            let isInFiter = true;
            if ((tools.deepGet(options,'categories') || []).length > 0 && tools.deepGet(options,'categories').indexOf(v.category) < 0 ) {
                isInFiter = false;
            }
            if ((tools.deepGet(options,'services') || []).length > 0 && tools.deepGet(options,'services').indexOf(v.service) < 0 ) {
                isInFiter = false;
            }
            if (isInFiter) {
                lexicals.push(v);
            }
        }

        if (this.status === STATUS.INITED) {
            if (!tools.deepGet(options,'categories','length') && !tools.deepGet(options,'services','length')) {
                lexicals = Array.from(this.lexicals.values());
            } else {
                this.lexicals.forEach(filterLexical);
            }
            return Observable.of(lexicals);
        } else {
            return this.loading.map(() => {
                if (!tools.deepGet(options,'categories','length') && !tools.deepGet(options,'services','length')) {
                    lexicals = Array.from(this.lexicals.values());
                } else {
                    this.lexicals.forEach(filterLexical);
                }
                return lexicals;
            })
        }
    }

    loadLexicals(category?: string) {
        this.status = STATUS.NONE;
        this.deleteLexicals(category);
        return this.api.lexical.list(category).map(res => {
            res.forEach(l => this.lexicals.set(l.id, l));
            this.status = STATUS.INITED;
            this.loading.next(true);
        });
    }

    createLexical(kwds: string[], name: string, service: string = lexicalServices.STANDARD, mult?: number): Observable<Lexical> {
        return this.api.lexical.create({displayName: name, values: kwds, service: service, mult: mult}).map(l => {this.lexicals.set(l.id, l); return l});
    }

    updateLexical(lexical: Lexical | string, name?: string, values?: string[]): Observable<Lexical> {

        if (typeof lexical === "string") {
            if (!name || !values || values.length === 0) return Observable.throw(new InError('MISSING_FIELD(name or values)'));
            return this.getLexical(<string> lexical).flatMap(l => {
                l.displayName = name;
                l.values = values;
                return this.api.lexical.update([l]).map(() => {this.lexicals.set(l.id, l); return l});
            })
        } else {
            return this.api.lexical.update([<Lexical>lexical]).map(() => {this.lexicals.set((<Lexical>lexical).id, <Lexical>lexical); return <Lexical>lexical;})
        }

    }

    private deleteLexicals(category?: string) {
        if (!category) {
            this.lexicals.clear()
        } else {
            this.lexicals.forEach((v, k) => {
                if (v.category === category) {
                    this.lexicals.delete(k);
                }
            })
        }
    }

    private getLexicalsArray(category?: string): Lexical[] {
        const lexicalsArray = [];
        this.lexicals.forEach((v, k) => {
            if (!category || v.category === category) lexicalsArray.push(v);
        })

        return lexicalsArray;
    }

}
