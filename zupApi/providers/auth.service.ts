import { Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs';
//import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ApiAuthService } from './apiAuth.service';
import { PartnerService, STATUS } from './partner.service';
import { ApiConfig } from './api.config';
import { Partner } from '../models';

/**
 * AuthService manages access to views (it implements "CanActivate") and login
 *
 * If access is refused, the user is redirected to ApiConfig.loginRoute
 * Anonymous access are allowed to routes with a data.anonymous = true
 *
 * ```
 * route = {
 *    path:'',
 *    component: WallComponent,
 *    canActivate: [AuthService],
 *    data: {
 *      anonymous: true
 *    }
 *  }
 * ```
 */
@Injectable()
//export class AuthService implements CanActivate {
export class AuthService {
  userId: string = undefined;

  isAnonymous: boolean = false;

  private _application: any;

  constructor(
      private apiAuthService: ApiAuthService,
      private partner: PartnerService,
      //private router: Router,
      @Optional() config: ApiConfig
  ) {
    if (config) {
      if (typeof config === 'function') {
        config = (<() => any>config)();
      }


      // TEST I4U :
      this._application = config.application

    }


  }

  canActivate(data?: {anonymous?: boolean, rights?: string[]}): Observable<boolean> {
    data = data || {}
    const anonymous = data.anonymous;
    const rights = data.rights;

    if (this.partner.status === STATUS.INITATING) {
      return this.partner.widgetsListChange.first().map(() => {
        return (!rights || (rights && this.partner.hasRightsFor(rights)));
      });
    }

    if (!this.isLoggedIn(anonymous, rights)) {

      if (this._application) {
        switch (this._application.id) {
          case 'I4U' :
            return this.checkI4U()
                .flatMap((loginInfo: {rive: {login: string, userkey:string}}) => this.autoLog(loginInfo, this._application.rights))
                .catch(err => {
                  return Observable.of(false);
                })
        }

      }

      const currentLogin = getCookie('currentLogin');

      if (currentLogin) { // .length !== 0
        return this.autoLog(JSON.parse(currentLogin), rights, anonymous);
      } else if (anonymous) {
        return this.apiAuthService.logout().map(() => {
          this.isAnonymous = true;
          return true;
        });
      } else {
        return Observable.from([false]);
      }

    } else if (rights) {
      return this.partner.onInit().map(() => {
        return (!rights || (rights && this.partner.hasRightsFor(rights)));
        /*
         this.parentRouter.navigate(['Deny']); // FIXME this route doesn't exist...
         */
      });
    }

    return this.partner.onInit().map(() => true);
  }

  checkI4U(): Observable<{rive: {login: string, userkey:string}}> {
    const clientIdMatch = (/[?&]client_id=([^&]*)/).exec(window.location.search);
    let clientId;
    if (clientIdMatch) clientId = clientIdMatch[1];

    if (clientId) {
      const url = '/i4u/binfluence/connect';
      const message = {
        client_id: clientId
      }

      return this.apiAuthService.logout().flatMap(() => {
        return this.apiAuthService.call(url, message, {apiid: 'b2bapi'}).map(res => {

          return {"rive": {"login": res.json().member_name, "userkey": res.json().member_hash}}
        });
      });

    } else {
      return Observable.of({"rive":{"login":"","userkey":""}});
    }

  }

  isLoggedIn(anonymous?: boolean, rights?: string[]): boolean {
    if (rights && rights.length > 0) {
      if (this.userId !== undefined) {
        return this.partner.hasRightsFor(rights);
      }
      return false;
    }
    return this.userId !== undefined || (anonymous && this.isAnonymous === anonymous);
  }

  hasRole(roles: string[]): boolean {
    return true;
  }

  login(username: string, password: string, rememberMe?: boolean): Observable<string> {
    return this.apiAuthService.login(username, password, rememberMe).flatMap((userInformations: any) => {
      if (!userInformations.partnerId) throw('MEMBER_NOT_PARTNER()');
      return this._setUser(userInformations);
    });
  }

  logout(): Observable<void> {
    return this.apiAuthService.logout().map(res => {
      document.cookie = 'currentLogin=;path=/';
      document.location.reload();
      // return this._setUser({id : undefined});
    });
  }

  autoLog(loginInformation: any, rights: any, anonymous: boolean = false): Observable<boolean> {
    return this.apiAuthService.autoLog(loginInformation)
        .flatMap((userInformations: any) => {
      return this._setUser(userInformations);
      })
        .flatMap(() => {
          return this.partner.onInit().map(() => {
            return (!rights || (rights && this.partner.hasRightsFor(rights)));
          });
        })
        .catch(err => {
          if (anonymous) {
            return this.apiAuthService.logout().map(() => {
              this.isAnonymous = true;
              return true;
            });
          }

          return Observable.from([false]);
        });
  }

  private _setUser(userInformations: any): Observable<string> {
    this.userId = userInformations.id;
    return this.partner.initPartner(this.userId).map((p: Partner) => {
      if (p.roUsers.indexOf(this.userId) >= 0) {
        this.apiAuthService.RO = true;
      } else {
        this.partner.addRight('write');        
      }

      this.apiAuthService.isAdmin = this.partner.isAdmin;

      return this.userId;
    });
  }
}


// TODO move to the right place
// https://github.com/salemdar/angular2-cookie
// https://github.com/BCJTI/ng2-cookies
function getCookie(cname: string): string {
  const name = cname + '=';
  const ca = document.cookie.split(';');

  for (let i = 0, j = ca.length; i < j; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1);
    if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
  }
  return '';
}
