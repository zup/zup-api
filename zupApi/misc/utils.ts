import { attributionTypes } from '../listOfValues';
import { Observable } from 'rxjs';

export const bigbang = Date.UTC(2010, 0, 1); // new Date(2010, 0, 1);

export type dateRange = {from: Date, to: Date}


export const colors: any = {
    toRGBA: function(color: string, transparency: number): string {
        if (color.indexOf('#') === 0) color =color.substr(1);
        const r = parseInt(color.substr(0,2), 16);
        const v = parseInt(color.substr(2,2), 16);
        const b = parseInt(color.substr(4,2), 16);
        return  `rgba(${r},${v},${b},${transparency})`;
    },
    list : {
        "purple-darker": "#1f1326",
        "purple-dark": "#331a38",
        "purple": "#5a3972",
        "purple-light": "#a288a6",
        "purple-lighter": "#bb9bb0",

        "red": "#912f56",
        "red-light": "#f45b69",

        "pink": "#f83c7b",

        "orange": "#e9724c",
        "orange-light": "#ec9b64",

        "yellow": "#fcde9c",

        "green": "#09b27c",
        "green-light": "#14cc91",

        "blue": "#255f85",
        "blue-light": "#527ff5",

        "grey-darker": "#666666",
        "grey-dark": "#999999",
        "grey": "#b7b7b7",
        "grey-light": "#d6d6d6",
        "grey-lighter": "#f5f5f5",

        "attribution": "#ec6911",
        "facebook": "#3b5998",
        "twitter": "#55acee",
        "google": "#dd4b39",
        "linkedin": "#0177b5",
        "github": "#6b6b6b",
        "stackoverflow": "#2f96e8",
        "dribble": "#f26798",
        "behace": "#0093fa",
        "instagram": "#5a3972",
        "yammer": "#2273c4",
        "youtube": "#dd4b39",
        "60mc": "#ffde00",
        "ufc": "#00ffff",

        "insuite": "#209e91",
        "insuite-dark": "#1b867b",
    }
};

export const applicationLanguages: {value: string, text: string}[] = [
    {value: 'fr', text: 'French'},
    {value: 'en', text: 'English'},
    {value: 'ge', text: 'German'},
    {value: 'es', text: 'Spanish'},
    {value: 'pt', text: 'Portuguese'},
    {value: 'it', text: 'Italian'},
    {value: 'cn', text: 'Chinese'},
    {value: 'kr', text: 'Korean'},
    {value: 'ja', text: 'Japanese'},
].sort((a, b) => a.text.localeCompare(b.text));
/**
 *  function to manage with dates :
 *   Dates: regular Date Object
 *   ZI dates: Rive format = nb of s since bigbang, used in all exchanges with api endpoints
 *   ElapsedTime: time elapsed from now (until the date provided), ex: 5 mins, 2 days,....
 *   TODO: add smartDateTime : 'Today 12:15', 'Yesterday 16:12' or '01/27/2016 09:30 pm'
 *   TODO: use user local format
 *
 * @type {{
 * toElapsedTimeFromDate: ((dt:Date)=>string);
 * toElapsedTimeFromDelta: ((delta:number)=>string);
 * toElapsedTimeFromZIDate: ((zdate:number)=>string);
 * toZIFromDate: ((date:Date)=>number);
     * toDateFromZI: ((zi:number)=>Date);
 * today: dateRange;
 * yesterday: dateRange;
 * last7Days: dateRange;
 * isToday: ((dtRange:any)=>boolean);
 * isYesterday: ((dtRange:any)=>boolean);
 * isLast7Days: ((dtRange:any)=>boolean)
 * }}
 */
export const dates: any = {
    dayToZIRange(d: Date): {after: number, before: number} {
        const day = this.toDay(d);
        return {
            after: this.toZIFromDate(day),
            before: this.toZIFromDate(day) + 24 * 3600 -1
        }
    },
    isSameDay(d1: Date, d2: Date): boolean {
        return d1 && d2 && this.toDay(d1).valueOf() === this.toDay(d2).valueOf();
    },
    lastNDays(n: number): dateRange {
        const today = this.dayToZIRange(new Date());
        const end = today.before;
        const start = today.after - n * 24 * 2600;
        return {
            from: this.toDateFromZI(start),
            to: this.toDateFromZI(end)
        }
    },
    toHour(date: Date | number): Date {
        if (typeof date === "number") date = this.toDateFromZI(date);
        return new Date((<Date>date).getFullYear(), (<Date>date).getMonth(), (<Date>date).getDate(), (<Date>date).getHours());
    },

    toDay(date: Date | number, end?: boolean): Date {
        if (typeof date === "number") date = this.toDateFromZI(date);
        if (!end) {
            return new Date((<Date>date).getFullYear(), (<Date>date).getMonth(), (<Date>date).getDate());
        } else {
            return new Date((<Date>date).getFullYear(), (<Date>date).getMonth(), (<Date>date).getDate(), 23, 59, 59);
        }

    },

    toWeek(date: Date | number): Date {
        if (typeof date === "number") date = this.toDateFromZI(date);
        const weekDay = (<Date>date).getDay();
        return new Date((<Date>date).getFullYear(), (<Date>date).getMonth(), (<Date>date).getDate() - weekDay);
    },

    toMonth(date: Date | number): Date {
        if (typeof date === "number") date = this.toDateFromZI(date);
        return new Date((<Date>date).getFullYear(), (<Date>date).getMonth(), 1);
    },
    previousPeriode(range: dateRange): dateRange {
        const a = this.toZIFromDate(range.from);
        const b = this.toZIFromDate(range.to);
        const r = b - a;

        return {
            from: this.toDateFromZI(a - r - 1),
            to: this.toDateFromZI( a - 1)
        };
    },

    toElapsedTimeFromDate(dt: Date): string {
        const now = new Date();
        const init = dt || now;

        const timespan: number = Math.floor((now.valueOf() - init.valueOf()) / 1000);
        if (timespan < 60) return 'now';

        let time: number;
        let timeUnit: string;

        if (timespan < 3600) { // 60*60
            time = Math.floor(timespan / 60);
            timeUnit = 'min';
        } else if (timespan < 86400) { // 60*60*24
            time = Math.floor(timespan / 3600);
            timeUnit = 'hour';
        } else if (timespan < 2592000) { // 60*60*24*30
            time = Math.floor(timespan / 86400);
            timeUnit = 'day';
        } else if (timespan < 31536000) { // 60*60*24*365
            time = Math.floor(timespan / 2592000);
            timeUnit = 'month';
        } else {
            time = Math.floor(timespan / 31536000);
            timeUnit = 'year';
        }

        return `${ time } ${ timeUnit }${ time > 1 ? 's' : '' } ago`;
    },

    toElapsedTimeFromDelta(delta: number): string { // delta : nb secondes from 2010 01 01
        const init = new Date(new Date(2010, 0, 1).getTime() + (parseInt(delta.toString(), 16) || 0) * 1000);

        return this.toElapsedTimeFromDate(init);
    },

    toElapsedTimeFromZIDate(zdate: number): string {
        return this.toElapsedTimeFromDate(this.toDateFromZI(zdate));
    },

    toZIFromDate(date: Date): number { // nombre de secondes depuis le BB
        if (!date) return;
        return Math.floor((date.valueOf() - bigbang) / 1000);
    },

    toDateFromZI(zi: number): Date {
        if (!zi) return;
        return new Date(bigbang + zi * 1000);
    },

    toRange(ab: {after: number, before: number}) {
        return {from: dates.toDateFromZI(ab.after), to: dates.toDateFromZI(ab.before)};
    },

    rangesEqual(r1, r2){
        return dates.toZIFromDate(r1.from) === dates.toZIFromDate(r2.from)
            && dates.toZIFromDate(r1.to) === dates.toZIFromDate(r2.to)
    },
    // To trick the IDE so he proposes autocompletion
    nowZI: undefined,
    today: undefined,
    yesterday: undefined,
    last7Days: undefined,
    last30Days: undefined,
    currentWeek: undefined,
    lastWeek: undefined,
    all: undefined,

    isToday: (dtRange: any): boolean => {
        const dt = dates.today;
        return (dtRange.from.valueOf() === dt.from.valueOf() && dtRange.to.valueOf() === dt.to.valueOf());
    },

    isYesterday: (dtRange: any): boolean => {
        const dt = dates.yesterday;
        return (dtRange.from.valueOf() === dt.from.valueOf() && dtRange.to.valueOf() === dt.to.valueOf());
    },

    isLast7Days: (dtRange: any): boolean => {
        const dt = dates.last7Days;
        return (dtRange.from.valueOf() === dt.from.valueOf() && dtRange.to.valueOf() === dt.to.valueOf());
    },

    isAll: (dtRange: dateRange): boolean => {
        const dt = dates.all;
        return (dtRange.from.valueOf() === dt.from.valueOf() && dtRange.to.valueOf() === dt.to.valueOf());
    },

    min: (...dates: Date[]): Date => {
        return new Date(Math.min(...dates.map(d => d.valueOf())));
    },

    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
};

Object.defineProperties(dates,
    {
        'all': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(2010, 0, 1),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1).getTime() - 1000)
                };
            }
        },
        'nowZI': {
            get: (): number => {
                return dates.toZIFromDate(new Date());
            }
        },
        'today': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                    // -1000 because I need to have full seconds
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1).getTime() - 1000)
                };
            }
        },
        'yesterday': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime() - 1000)
                };
            }
        },
        'last7Days': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime() - 1000)
                };
            }
        },
        'last30Days': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 30),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime() - 1000)
                };
            }
        },
        'currentWeek': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate() - now.getDay() + 1),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1).getTime() - 1000)
                };
            }
        },
        'lastWeek': {
            get: (): dateRange => {
                const now = new Date();
                return {
                    from: new Date(now.getFullYear(), now.getMonth(), now.getDate() - now.getDay() - 6),
                    to: new Date(new Date(now.getFullYear(), now.getMonth(), now.getDate() - now.getDay() + 1).getTime() - 1000)
                };
            }
        }
    }
)



export const objectFormatTools = {
    getImageUrl: function(inUrl: any, medias?: any) {
        //" { "bkey": "AMIfv97QQ3HAXwmC8HzAsOUtMQvRdfkUweKbHdDfjQLAuPWRRnO8rSoqtL2MG2MoveztrU_nONiBkrzRn11hiEodZrAJkg8r1_R22WhaT-nWAM5d-GSnVK2sEOTWCQJ6_1p3CNgPhj3Yyp5Xq0jvpA0au-BWP1aUhdJe_C5UV0PXsRTMxuhOCqA","url": "http: //lh3.googleusercontent.com/BZGCb-ReDua9y4gIXXhzIaVIa7sjol1qWF6cRvOZghaieGjgqxEG3Soy-4a8JeJMVQGI1-ej5EUpavFxZoU","created": 167422124,"format": "png" }"

        if (medias && medias.length > 0) {
            if (medias[0].substring(0, 1) === '{') { // tableau de jsons
                const json = JSON.parse(medias[0])
                if (json.type === 'image') {
                    const img = json.variants.find( (i) => i.size === 'l' ||  i.size === 'xl');
                    if (img) return img.url;
                };
            } else { // tableau d'urls, on prend la dernier (a priori la meilleur def)
                return medias[medias.length - 1];
            }

        }

        if (!inUrl) return undefined;

        let imgUrl;
        if (typeof inUrl !== 'string') return inUrl.url;
        let parts = inUrl.split('\\0');
        if (parts.length === 1) {
            if (inUrl.indexOf('{') === 0 ) {
                imgUrl = JSON.parse(inUrl).url;
            } else {
                imgUrl = parts[0];
            }
        } else {
            imgUrl = parts[1];
        }

        if (imgUrl.indexOf('http: ') === 0) imgUrl = imgUrl.substr(5); // supprimer le protocol pour gerer le https
        return imgUrl;
    },
    parseMedia: function(medias: any[]): {type?: string, imgUrl?: string, videoUrl?: string, videoType?: string}{
        const media: any = {}
        if (!medias || medias.length === 0) return media;
        if (medias[0].substring(0, 1) !== '{') {// tableau d'urls, on prend la dernier (a priori la meilleur def)
            media.type = 'image';
            media.imgUrl = medias[medias.length - 1];

        } else {
            medias = medias.map(m => JSON.parse(m));
            let primary = medias.find(m => m.primary === true);
            if (!primary) {
                primary = medias[0];
            }
            // image rive
            if (primary.url) {
                return {
                    type: 'image',
                    imgUrl: primary.url
                };
            }

            if (primary.type === 'image') {
                media.imgUrl = primary.variants[primary.variants.length - 1].url;
            } else if (primary.type === 'video' && primary.variants) {
                const imgs = primary.variants.filter(v => v.type === 'image');
                if (imgs.length > 0) {
                    media.imgUrl = imgs[imgs.length - 1].url;
                } else {
                    media.imgUrl = primary.variants[primary.variants.length - 1].url;
                }
            } else if (primary.variants) {
                media.imgUrl = primary.variants[primary.variants.length - 1].url;
            }

            const video = medias.find(m => (m.type === 'video' || m.type === 'stream'))
            if (video && video.variants) {
                const vids = video.variants.filter(v => (v.type === 'video' || v.type === 'stream'));
                if (vids.length > 0) {
                    media.videoUrl = vids[vids.length - 1].url;
                    media.type = 'video';
                    if (media.videoUrl.indexOf('youtube') >= 0) {
                        media.videoType = 'Y';
                    }
                } else {
                    media.type = 'image';
                }
            } else {
                media.type = 'image';
            }
        };

        // Sanitize img urls (bug twitter)
        if (media.imgUrl) media.imgUrl = media.imgUrl.replace(/\.(jpg|png|jpeg)[^.\?]+$/, '.$1');

        return media;
    },
    getAttributionTypeFromId: function(id: string) {
        if (id.indexOf('(p)') >= 0) return attributionTypes.RIVE;
        if (id.indexOf('(tw)') === 0) return attributionTypes.TWITTER;
        if (id.indexOf('(ig)') === 0) return attributionTypes.INSTAGRAM;
        if (id.indexOf('(fb)') === 0) return attributionTypes.FACEBOOK;
        if (id.indexOf('(px)') === 0) return attributionTypes.PX500;
        if (id.indexOf('(yt)') === 0) return attributionTypes.YOUTUBE;
        if (id.indexOf('(fw)') === 0) return attributionTypes.FACEBOOKWORKPLACE;
        if (id.indexOf('(f6)') === 0) return attributionTypes.F60MC;
        if (id.indexOf('(yt)') === 0) return attributionTypes.YAMMER;
        console.log(`AttributionType unknown for ${ id }`);
    },
    getAttributionTypeFromAttribution: function(attr: string) {
        if (attr.length === 0) return attributionTypes.RIVE;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*twitter.com\//) === 0) return attributionTypes.TWITTER;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*instagram.com\//) === 0) return attributionTypes.INSTAGRAM;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*facebook.com\//) === 0) return attributionTypes.FACEBOOK;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*youtube.com\//) === 0) return attributionTypes.YOUTUBE;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*yammer.com\//) === 0) return attributionTypes.YAMMER;
        if (regexIndexOf(attr, /https?:\/\/[^\/]*500px.org\//) === 0) return attributionTypes.PX500;
    }
};


export const ids = {
    toSafe: function(id: string): string {
        if (!id) return;
        return id.replace(/\(/g, '%28').replace(/\)/g, '%29');
        //return encodeURIComponent(id).replace('(', '%28').replace(')', '%29');
    },
    fromSafe: function(id: string): string {
        if (!id) return;
        return id.replace(/%28/g, '(').replace(/%29/g, ')');
        //return decodeURIComponent(id).replace(/%28/g, '(').replace(/%29/g, ')');
    }
}

function regexIndexOf (s: string, regex: RegExp, startpos: number = 0) {
    let indexOf = s.substring(startpos).search(regex);
    return (indexOf >= 0) ? (indexOf + (startpos)) : indexOf;
};

/**
 * Recursive assignment of objects
 * @param {any} o1
 * @param {any[]} objects
 * @returns {any}
 */
export function deepAssign(o1: any, ...objects: any[]): any {
    if (!o1) {
        return o1;
    }
    objects.forEach(o2 => {
        for (let prop in o2) {
            if (typeof o1[prop] === typeof o2[prop] && typeof o1[prop] === 'object') {
                deepAssign(o1[prop], o2[prop]);
            } else {
                o1[prop] = o2[prop];
            }
        };

    });

    return o1;
};

export const tools = {
    /**
     *
     * @param {any[]} objects
     * @param {(any) => any} getObjectId get the object Id that will be used for unduplicating
     * @returns {any[]}
     */
    unduplicate: function(objects: any[], getObjectId?: (any) => any) {
        const mySet =  new Set<any>();
        const transformSet = new Set<any>();
        objects.forEach(o => {
            if (getObjectId) {
                if (!transformSet.has(getObjectId(o))) {
                    mySet.add(o);
                    transformSet.add(getObjectId(o));
                }
            } else {
                if (!mySet.has(o)) mySet.add(o);
            }
        });
        return Array.from(mySet);
    },
    /**
     *
     * @param {any[]} a1
     * @param {any[]} a2
     * @param {(any) => any} compareFn
     * @returns {any[][]} [a12, a1a2, a21] = [in a1 not in a2, in a1 and a2, in a2 not in a1]
     */
    arrayDelta: function(a1: any[], a2: any[], compareFn: (any) => any = (o: any) => o): any[][] {
        if (!a1 && a2) return [[], [], a2];
        if (a1 && !a2) return [[1], [], []];
        if (!a1 || ! a2) return [[], [], []];
        const a12 = [];
        const a1a2 = [];
        const a21 = [];

        a1.forEach(a => {
            if (a2.findIndex((o: any) => compareFn(o) === compareFn(a)) >= 0) {
                a1a2.push(a);
            } else {
                a12.push(a);
            }
        });

        a2.forEach(a => {
            if (a1a2.findIndex((o: any) => compareFn(o) === compareFn(a)) < 0) {
                a21.push(a);
            }
        });

        return [a12, a1a2, a21];

    },
    clone: function(o: any): any {
        return JSON.parse(JSON.stringify(o));
    },
    capitalizeFstLetter: function(txt: string) {
        return txt.charAt(0).toUpperCase() + txt.slice(1);
    },
    enumToArray: function(Enum: any): any[]{
        return Object.keys(Enum).map(k => Enum[k]);
    },
    toBase64(file: File): Promise<string> {
        if (file.size / 1024 /1024 > 5) return Promise.reject('TOO_BIG(5M)');
        const promise = new Promise<string>((resolve, reject) => {
            const reader =  new FileReader();
            reader.onloadend = () => {
                resolve(reader.result);
            };
            reader.readAsDataURL(file)

            setTimeout(() => reject(), 2000);
        });
        return promise;
    },
    objectFilter(o: any, callback: (any) => boolean): any {
        const filtered = {};
        Object.keys(o).forEach(k => {
            if (callback(o[k])) filtered[k] = o[k];
        })

        return filtered
    },
    objectToMap: function(o) {
        const map: Map<string, any> = new Map<string, any>();

        for (let k in o) {
            map.set(k, o[k]);
        }

        return map;
    },
    objectMap(object: any, properties: string[]): any[] {
        if (!object) return [];
        return properties.map(k => object[k]);
    },
    deepAssign: function(o1: any, ...objects: any[]): any {
        objects.forEach(o2 => {
            o2 = o2 || {};
            for (let prop in o2) {
                if (typeof o1[prop] === typeof o2[prop] && typeof o1[prop] === 'object') {
                    deepAssign(o1[prop], o2[prop]);
                } else {
                    o1[prop] = o2[prop];
                }
            };

        });

        return o1;
    },
    //should be used instead of previous
    deepAssign2: function(o1: any, ...objects: any[]): any {
        objects.forEach(o2 => {
            o2 = o2 || {};
            for (let prop in o2) {
                if (o2[prop] && typeof o2[prop] === 'object' && !(o2[prop] instanceof Array)) {
                    if (typeof o1[prop] !== 'object' || (o2[prop] instanceof Array)) {
                        o1[prop] = {}; //o2 should erase o1
                    }
                    tools.deepAssign2(o1[prop], o2[prop]);
                } else {
                    o1[prop] = o2[prop];
                }
            };
        });

        return o1;
    },
    deepGet: function(o: any, ...nodes: string[]) {
        if (!o) return;
        if (!nodes || nodes.length === 0) return o;
        let beg = o;
        nodes.forEach((node: string) => {
            if (beg) beg = beg[node];
        });

        return beg;
    },
    deepCreate: function(o: any, ...nodes: string[]) {
        if (!nodes || nodes.length === 0) return o;
        const beg = {};
        let newNode = beg;
        nodes.forEach((node: string, i: number) => {
            newNode[node] = (i < nodes.length - 1) ? {} : o;
            newNode = newNode[node];
        });

        return beg;
    },
    deepInsert: function(v: any, o: any, ...nodes: string[]) {
        if (!o) return;
        if (!nodes || nodes.length === 0) return ;
        let beg = o;
        nodes.forEach((node: string, i: number) => {
            if (i === nodes.length - 1) {
                beg[node] = v;
            } else if (beg.hasOwnProperty(node)) {
                beg = beg[node];
            } else {
                beg[node] = {};
                beg = beg[node];
            }
        });

        return o;
    },
    formatkwd:(kwd: string, options?: {case: boolean}) : string => {
        if (!kwd) return;
        let formatedKwd = kwd.trim();

        if (formatedKwd.length === 0) return;

        if (options && options.case) {
            formatedKwd = formatedKwd.toLowerCase();
        }

        if (formatedKwd.substr(0, 1) !== '*') {
            formatedKwd = ' '.concat(formatedKwd);
        }
        if (formatedKwd.substr(-1) !== '*') {
            formatedKwd = formatedKwd.concat(' ');
        }
        if (formatedKwd !== '*') {
            formatedKwd = formatedKwd.replace(/(^\*|\*$)/g, '');
        } else {
            formatedKwd = ' * ';
        }

        //formatedKwd = formatedKwd.replace(/(^"|"$)/g, ' ');
        //formatedKwd = formatedKwd.replace(/(^«|«$)/g, ' ');

        return formatedKwd;
    },
    unformatKwd: (kwd: string): string => {
        if (!kwd || kwd.length === 0) return;

        if (kwd.substr(0, 1) !== ' ') kwd = '*'.concat(kwd);
        if (kwd.substr(-1) !== ' ') kwd = kwd.concat('*');

        return kwd.trim();
        //return kwd.replace(/(^\s|\s$)/g, '"');
    },
    toKwdList(txt: string): string[] {
        const kwds = txt.split(/\s*,\s*/g);
        return kwds.map(k => this.formatkwd(k));
    },
    formatNumber: function(n: number): string {
        const sn = n.toString(10);
        if (sn.length <= 3) return sn;
        return sn.replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    },
    forkJoin2FlatMap: function(forkJoin: Observable<any>[]): Observable<any> {
        if (!forkJoin || forkJoin.length === 0) return Observable.from([undefined]);
        const res = [];
        let flatMap = forkJoin[0].map(r => {res.push(r); return res;});


        for (let i = 1; i < forkJoin.length; i++) {
            flatMap = flatMap.flatMap(() => forkJoin[i].map(r => {res.push(r); return res;}))
        }

        return flatMap;
    },
    objectValues: function(o: any): any[] {
        if (!o) return  [];
        const v = [];
        Object.keys(o).forEach(k => v.push(o[k]));

        return v;
    },
    objectEntries: function(o: any): [string | number, any][] {
        if (!o) return  [];
        const e = [];
        Object.keys(o).forEach(k => e.push([k, o[k]]));

        return e;
    },
    objectsEquals(...objects): boolean {
        if (! objects || objects.length < 2) return true;
        let previous = objects[0];
        for (let i = 1; i < objects.length; i++) {
            if (JSON.stringify(previous) !== JSON.stringify(objects[i])) return false;
            previous = objects[i];
        }
        return true
    },
    tweetLength(text: string): number {
        if (!text) return 0;
        return text.replace(/(^|\s)https?:\/\/[^\s]+/, `$1${'a'.repeat(23)}`).length;
    }
}

// to del
export function toStringAssign(o1, o2) {
    for (let prop in o2) {
        if (['object', 'function'].indexOf(typeof o2[prop]) >= 0) {
            throw 'BAD_TYPE()';
        }
        if (o2[prop] === undefined) {
            o1[prop] = '';
        } else {
            o1[prop] = o2[prop].toString();
        }
    }

    return o1;
};



/*

 export const utils = {
 dates: dates,
 objectFormatTools: objectFormatTools,
 deepAssign: deepAssign,
 toStringAssign: toStringAssign
 }*/
