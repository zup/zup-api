import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

const regExHashAndCite = /([#@][^\s#@]*?)(\s|#|$|@)/gm;
const linkRegExp = /(https?:\/\/[^\s"]*)/gm


const toStyles = (o) =>  {
  let s = ''
  for (let p in o) {
    s = s.concat(`${p}:${o[p]}; `);
  }
  return s;
}

@Pipe({name: 'decoratedText'})
export class DecoratedText implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(text, hgStyles?): SafeHtml {
    if (!text) return '';
    let higlight = `class="highlight"`
    if (hgStyles) higlight = `style="${toStyles(hgStyles)}"`

    text = text.replace(linkRegExp, ` <a href="$1" target="_blank" ${higlight}>$1</a> `)
    text = text.replace(regExHashAndCite, ` <span ${higlight}>$1</span> `);

    return this.sanitizer.bypassSecurityTrustHtml(text);
  }

}

