import { Pipe, PipeTransform } from '@angular/core';

import { alertTypes } from '../models/notifications';

@Pipe({name: 'alertsTitle'})
export class AlertsTitle implements PipeTransform {
  constructor() {}

  transform(payload, type) {
    if (!type) return '<span></span>';
    const atype = alertTypes.detail(type)
    return atype.title.replace(/\$0/, `<span class="highlight">${payload[atype.variable]}</span>`);
  }
}
