import { Pipe, PipeTransform } from '@angular/core';

import { dates } from '../misc/utils';

@Pipe({name: 'inDate'})
export class InDate implements PipeTransform {
  constructor() {}

  transform(date, value) {
    const d = dates.toDateFromZI(date);
    if (!value) return d;
    if (value === 'local') return this.toLocalDate(d);
    if (value === 'local-text') return this.toLocalDate(d, true);
    let formatedDt = value;
    formatedDt = formatedDt.replace('dd', '0'.concat(d.getDate().toString()).substr(-2));
    formatedDt = formatedDt.replace('MM', '0'.concat((d.getMonth() + 1).toString()).substr(-2));
    formatedDt = formatedDt.replace('yyyy', d.getFullYear().toString());
    formatedDt = formatedDt.replace('yy', '0'.concat(d.getFullYear().toString()).substr(-2));
    formatedDt = formatedDt.replace('hh', '0'.concat(d.getHours().toString()).substr(-2));
    formatedDt = formatedDt.replace('mm', '0'.concat(d.getMinutes().toString()).substr(-2));

    formatedDt = formatedDt.replace('ago', dates.toElapsedTimeFromZIDate(date));
    return formatedDt;
  }

  toLocalDate(date: Date, isText?: boolean): string {
    let dtOptions;
    if (!isText) {
      dtOptions ={month: '2-digit', day: '2-digit', hour:'2-digit', minute: '2-digit'};
    } else {
      dtOptions ={month: 'long', day: '2-digit', hour:'2-digit', minute: '2-digit'};
    }


    return date.toLocaleString(undefined, dtOptions);
  }
}
