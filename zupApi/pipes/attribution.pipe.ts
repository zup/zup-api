import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { _attributionTypes as attributionTypes} from '../listOfValues';
import { tools } from '../misc/utils';


@Pipe({
  name: 'attributionIcon'
})
export class AttributionIcon implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(attribution, link, options?: any) {
    if (!attribution) return;
    const icon = `<i class="${tools.deepGet(attributionTypes.detail(attribution), 'icon') || 'fa fa-question-circle'}" 
                  style="${options && options.style || ''}"></i>`;

    const returnObject = link ? `<a href="${link}" target="_new">${icon}</a>` : icon;
    if (options && options.noSanitize) {
      return returnObject;
    } else {
      return this.sanitizer.bypassSecurityTrustHtml(returnObject);
    }

  }
}

@Pipe({
  name: 'attributionText'
})
export class AttributionText implements PipeTransform {
  transform(attribution) {
    const a = attributionTypes.detail(attribution)
    if (a) {
      return a.text;
    } else {
      console.log(`UNKNOWN_ATTR(${attribution})`)
      return 'Unknown';
    }
  }
}
