import { ActionSource, ActionOrigin, ActionLogText } from './action.pipe';
import { AttributionIcon, AttributionText } from './attribution.pipe';
import { DecoratedText } from './decoratedText.pipe';
import { EvolutionsClass } from './evolutions.pipe';
import { InDate } from './inDate.pipe';
import { AlertsTitle } from './alertsTitle.pipe';
import { SafePipe } from './safe.pipe';

export const PIPES = [
    ActionLogText,
    ActionOrigin,
    ActionSource,
    AlertsTitle,
    AttributionIcon,
    AttributionText,
    DecoratedText,
    EvolutionsClass,
    InDate,
    SafePipe
];