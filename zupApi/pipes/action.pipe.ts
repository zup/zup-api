import { Pipe, PipeTransform } from '@angular/core';

import { ActionLogDetail, ActionLog, actionTypes, widgetTypes, screenTypes } from '../models';


@Pipe({
  name: 'actionOrigin'
})
export class ActionOrigin implements PipeTransform {

  transform(origin) {
    if (!origin) return;
    const detail = screenTypes.detail(origin);
    if (!detail) return;
    return `<span class="color-${detail.color}">${detail.text}</span>`;
  }
}

@Pipe({
  name: 'actionSource'
})
export class ActionSource implements PipeTransform {
  getColor(cat) {
    switch (cat) {
      case widgetTypes.WATCH.value:
        return `blue`;
      case widgetTypes.MY_PLACE.value:
        return 'purple';
      case widgetTypes.COMPETITOR.value:
        return 'red';
    }
  }

  transform(widget) {
    return `<span class="color-${this.getColor(widget.category)}">{{widget.name}}</span>`;
  }
}

@Pipe({
  name: 'actionLogText'
})
export class ActionLogText implements PipeTransform {

  transform(action: ActionLogDetail) {
    let adetail: any;
    if (action.type === 'PUBLISH') {
      adetail = actionTypes.detail(actionTypes[action.publishType]);
    } else {
      adetail = actionTypes.detail(actionTypes[action.type]);
    }
    if (action.profile) {
      return (adetail.actionHtml || adetail.actionText).replace('$0', action.profile.screenName);
    } else {
      return  adetail.actionText;
    }
  }
};
