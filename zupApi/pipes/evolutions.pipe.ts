import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'evolutionsClass' })
export class EvolutionsClass implements PipeTransform {
  constructor() {};

  transform(evol: number) {
      if (evol < -5) {
        return 'evol-negative';
      } else if (evol > 5) {
        return 'evol-positive';
      } else {
        return 'evol-neutral';
      }
    }


};
