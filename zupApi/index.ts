import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApiAuthService } from './providers/apiAuth.service';
import { AuthService } from './providers/auth.service';
import { HistoLogsService } from './providers/histologs.service';
import { LexicalService } from './providers/lexical.service';
import { PartnerService } from './providers/partner.service';
import { NotificationsService } from './providers/notifications.service';
import { WallService } from './providers/wall.service';
import { ZupApiService } from './providers/zupApi.service';

import { ApiConfig, jsonConfig } from './providers/api.config';

import { PIPES } from './pipes';

/*
import {
    ActionOrigin,
    ActionSource,
    AlertsTitle,
    AttributionIcon,
    AttributionText,
    DecoratedText,
    EvolutionsClass,
    InDate
} from './pipes';
export const PIPES = [
    ActionOrigin,
    ActionSource,
    AlertsTitle,
    AttributionIcon,
    AttributionText,
    DecoratedText,
    EvolutionsClass,
    InDate
]*/

/**
 * @module
 * @description
 * This module provides methods to access api endpoints for Rive and Insuite
 *
 * By default, this module points on the int endpoints. You can provide an ApiConfig to change that
 * while using it in your project :
 *
 * ```
 * import { NgModule } from '@angular/core';
 * import { ZApiModule } from 'zup-api';
 *
 * @NgModule({
 * imports: [
 *    RouterModule.forChild(routes),
 *    ZApiModule.forRoot({
 *      apiUrls: {
 *        coldapi: 'default-dot-prod-lkl-0.appspot.com',
 *        hotapi: 'hotapi-dot-prod-lkl-0.appspot.com',
 *        b2bapi: 'b2b-dot-prod-lkl-0.appspot.com'
 *      }
 *    }),
 *   ],
 *   declarations: [
 *   ]
 * })
 *  export class TestModule {}
 * ```
 */
@NgModule({
    imports: [

    ],
    declarations: [
    ],
    providers: [
        ApiAuthService,
        AuthService,
        HistoLogsService,
        LexicalService,
        PartnerService,
        NotificationsService,
        WallService,
        ZupApiService
    ],
    exports: [
    ]
})
export class ZApiModule {

    static forRoot(config: any): ModuleWithProviders {
        return {
            ngModule: ZApiModule,
            providers: [
                {provide: ApiConfig, useValue: config }
            ]
        };
    }
}

@NgModule({
    imports: [
      CommonModule
    ],
    declarations: [
        ...PIPES
    ],
    providers: [
    ],
    exports: [
      CommonModule,
        ...PIPES
    ]
})
export class ZDisplayModule {}


export * from './models';
export * from './listOfValues';
export * from './misc/utils'
export { ApiConfig } from './providers/api.config';


export { objectFormat } from './providers/storage';
export { ZupApiService, INFLUENCER_ANALYTICS } from './providers/zupApi.service';
export { HistoLogsService } from './providers/histologs.service';
export { PartnerService } from './providers/partner.service';
export { AuthService } from './providers/auth.service';
export { LexicalService } from './providers/lexical.service';
export { NotificationsService } from './providers/notifications.service';
export { WallService } from './providers/wall.service';

export { InError } from './providers/apiAuth.service';

console.log('zapi V 3.1.4');
