import { Observable } from 'rxjs';
export declare const bigbang: number;
export declare type dateRange = {
    from: Date;
    to: Date;
};
export declare const colors: any;
export declare const applicationLanguages: {
    value: string;
    text: string;
}[];
/**
 *  function to manage with dates :
 *   Dates: regular Date Object
 *   ZI dates: Rive format = nb of s since bigbang, used in all exchanges with api endpoints
 *   ElapsedTime: time elapsed from now (until the date provided), ex: 5 mins, 2 days,....
 *   TODO: add smartDateTime : 'Today 12:15', 'Yesterday 16:12' or '01/27/2016 09:30 pm'
 *   TODO: use user local format
 *
 * @type {{
 * toElapsedTimeFromDate: ((dt:Date)=>string);
 * toElapsedTimeFromDelta: ((delta:number)=>string);
 * toElapsedTimeFromZIDate: ((zdate:number)=>string);
 * toZIFromDate: ((date:Date)=>number);
     * toDateFromZI: ((zi:number)=>Date);
 * today: dateRange;
 * yesterday: dateRange;
 * last7Days: dateRange;
 * isToday: ((dtRange:any)=>boolean);
 * isYesterday: ((dtRange:any)=>boolean);
 * isLast7Days: ((dtRange:any)=>boolean)
 * }}
 */
export declare const dates: any;
export declare const objectFormatTools: {
    getImageUrl: (inUrl: any, medias?: any) => any;
    parseMedia: (medias: any[]) => {
        type?: string;
        imgUrl?: string;
        videoUrl?: string;
        videoType?: string;
    };
    getAttributionTypeFromId: (id: string) => any;
    getAttributionTypeFromAttribution: (attr: string) => any;
};
export declare const ids: {
    toSafe: (id: string) => string;
    fromSafe: (id: string) => string;
};
/**
 * Recursive assignment of objects
 * @param {any} o1
 * @param {any[]} objects
 * @returns {any}
 */
export declare function deepAssign(o1: any, ...objects: any[]): any;
export declare const tools: {
    unduplicate: (objects: any[], getObjectId?: (any: any) => any) => any[];
    arrayDelta: (a1: any[], a2: any[], compareFn?: (any: any) => any) => any[][];
    clone: (o: any) => any;
    capitalizeFstLetter: (txt: string) => string;
    enumToArray: (Enum: any) => any[];
    toBase64(file: File): Promise<string>;
    objectFilter(o: any, callback: (any: any) => boolean): any;
    objectToMap: (o: any) => Map<string, any>;
    objectMap(object: any, properties: string[]): any[];
    deepAssign: (o1: any, ...objects: any[]) => any;
    deepAssign2: (o1: any, ...objects: any[]) => any;
    deepGet: (o: any, ...nodes: string[]) => any;
    deepCreate: (o: any, ...nodes: string[]) => any;
    deepInsert: (v: any, o: any, ...nodes: string[]) => any;
    formatkwd: (kwd: string, options?: {
        case: boolean;
    }) => string;
    unformatKwd: (kwd: string) => string;
    toKwdList(txt: string): string[];
    formatNumber: (n: number) => string;
    forkJoin2FlatMap: (forkJoin: Observable<any>[]) => Observable<any>;
    objectValues: (o: any) => any[];
    objectEntries: (o: any) => [string | number, any][];
    objectsEquals(...objects: any[]): boolean;
    tweetLength(text: string): number;
};
export declare function toStringAssign(o1: any, o2: any): any;
