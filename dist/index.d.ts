import { ModuleWithProviders } from '@angular/core';
/**
 * @module
 * @description
 * This module provides methods to access api endpoints for Rive and Insuite
 *
 * By default, this module points on the int endpoints. You can provide an ApiConfig to change that
 * while using it in your project :
 *
 * ```
 * import { NgModule } from '@angular/core';
 * import { ZApiModule } from 'zup-api';
 *
 * @NgModule({
 * imports: [
 *    RouterModule.forChild(routes),
 *    ZApiModule.forRoot({
 *      apiUrls: {
 *        coldapi: 'default-dot-prod-lkl-0.appspot.com',
 *        hotapi: 'hotapi-dot-prod-lkl-0.appspot.com',
 *        b2bapi: 'b2b-dot-prod-lkl-0.appspot.com'
 *      }
 *    }),
 *   ],
 *   declarations: [
 *   ]
 * })
 *  export class TestModule {}
 * ```
 */
export declare class ZApiModule {
    static forRoot(config: any): ModuleWithProviders;
}
export declare class ZDisplayModule {
}
export * from './models';
export * from './listOfValues';
export * from './misc/utils';
export { ApiConfig } from './providers/api.config';
export { objectFormat } from './providers/storage';
export { ZupApiService, INFLUENCER_ANALYTICS } from './providers/zupApi.service';
export { HistoLogsService } from './providers/histologs.service';
export { PartnerService } from './providers/partner.service';
export { AuthService } from './providers/auth.service';
export { LexicalService } from './providers/lexical.service';
export { NotificationsService } from './providers/notifications.service';
export { WallService } from './providers/wall.service';
export { InError } from './providers/apiAuth.service';
