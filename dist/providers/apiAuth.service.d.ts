import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiConfig } from './api.config';
export declare class ApiAuthService {
    private http;
    RO: boolean;
    isAdmin: boolean;
    private sessionId;
    private sessionSalt;
    private userId;
    private userPK;
    private userSalt;
    private apiUrls;
    private apiStatus;
    private hasConnection;
    private onLogin;
    private loginInformation;
    private cache;
    private cnxStatus;
    private urlParams;
    constructor(http: Http, config: ApiConfig);
    login(username?: string, password?: string, rememberMe?: boolean): Observable<any>;
    logout(): Observable<any>;
    autoLog(loginInformation: any): Observable<any>;
    relog(): void;
    private getEndpointUrl(url, message, options);
    call(url: string, message?: any, options?: any): Observable<any>;
    retryCall(url: string, message?: any, options?: any): Promise<any>;
}
export declare class InError {
    errorMessage: any;
    options: {
        noParse?: boolean;
        noShow?: boolean;
    };
    originalResponse: any;
    constructor(errorMessage: any, options?: {
        noParse?: boolean;
        noShow?: boolean;
    }, originalResponse?: any);
    toString(): any;
    readonly noParse: boolean;
    readonly noShow: boolean;
}
