import { EventEmitter } from '@angular/core';
import { PartnerService } from './partner.service';
import { ZupApiService } from './zupApi.service';
import { Wall, wallJsonPost, SimpleProfile, Profile, ObjectsList } from '../models';
import { Observable } from 'rxjs';
import { wallPage, wallPageConf, wallConf } from "../models/wall";
export declare class WallService {
    private partner;
    private api;
    displayedPosts: Set<string>;
    wallFrontPage: string;
    private wallName;
    private wallPartnerName;
    private wallId;
    confId: string;
    private _wall;
    private _currentWallConf;
    private dateRange;
    private _posts;
    private _postStack;
    private intervals;
    private pinnedPostTick;
    private sponsoredPostTick;
    private dateInitConfig;
    private lastSentPP;
    serviceError: EventEmitter<boolean>;
    /**
     * Emits a post every every [wall.delays.mainPosts] seconds
     * @type {EventEmitter<wallJsonPost>}
     *
     *
     * current algo to emit new Post:
     * 1 - try use stack (fst-in/fst-out)
     * 2 - ask for new
     * 3 - take a random in the list (older)
     */
    newPost: EventEmitter<{
        post: wallJsonPost;
        isRecycled?: boolean;
    }>;
    /**
     * Emits a pinnedPost every every [wall.delays.pinnedPosts] seconds (if any)
     * @type {EventEmitter<wallJsonPost>}
     */
    /**
     * Emits a sponsoredPost every every [wall.delays.sponsoredPosts] seconds (if any)
     * @type {EventEmitter<wallJsonPost>}
     */
    /**
     * Emits true when wall has changed, the display should reload everything (general info, posts (moderation might have changed),...)
     * @type {EventEmitter<boolean>}
     */
    configChanged: EventEmitter<boolean>;
    analytics: {
        nbPosts: (dtFrom?: number, dtTo?: number) => Observable<number>;
        topHashTags: (nb?: number, dtFrom?: number, dtTo?: number) => Observable<{
            payload: string;
            total: number;
        }[]>;
        topUsers: (nb?: number, dtFrom?: number, dtTo?: number) => Observable<Profile[]>;
        nbActions: (dtFrom?: number, dtTo?: number) => Observable<{
            likes: number;
            comments: number;
            shares: number;
        }>;
    };
    readonly currentWallConf: wallConf;
    readonly confName: string;
    constructor(partner: PartnerService, api: ZupApiService);
    /**
     * Returns custom styles to be used in the wall
     * @returns {any}
     */
    getCustomStyles(): any;
    /**
     * Returns the new post according to the "newPost" algorithm
     * @returns {wallJsonPost}
     */
    getNewPost(): Observable<{
        post: wallJsonPost;
        isRecycled?: boolean;
    }>;
    getRandomPost(): {
        post: wallJsonPost;
        isRecycled: boolean;
    };
    /**
     * Returns the next pinnedPost to display
     * @returns {wallJsonPost}
     */
    getPinnedPost(force?: boolean): Observable<wallJsonPost>;
    /**
     * Returns the nb ljsast posts of the wall
     * @param {number} [nb] default = 20
     * @returns {Observable<wallJsonPost[]>}
     */
    private retry;
    getPosts(nb?: number, min?: number, pageId?: wallPage | string): Observable<wallJsonPost[]>;
    getPostDetail(id: string): wallJsonPost;
    getPostsDetail(ids: string[]): Observable<wallJsonPost[]>;
    getProfiles(nb?: number): Observable<SimpleProfile[]>;
    getPostObjectList(nb: number): Observable<ObjectsList<wallJsonPost>>;
    /**
     * Loads wall configuration (delays, pinned and sponsored posts, moderated posts and users,...)
     * @param {string} wid wall id
     * @returns {Observable<void>}
     */
    init(name: string, partnerName: string, confName?: string): Observable<boolean>;
    getPages(): wallPageConf[];
    getWallStyles(): any;
    getPageConfig(pageId: string | wallPage): wallPageConf;
    getPageStyles(pageId: string | wallPage): {
        pageStyles?;
        postsStyles?;
    };
    getOption(option: string, pageOrDefault?: wallPage | string | boolean): any;
    readonly wall: Wall;
    private _getCurrentConf();
    /**
     * removes moderated posts from input list
     * @param {} posts
     * @returns {any[]}
     */
    private moderate(posts);
    private loadWall(name, partnerName);
    private getConfig();
    private setIntervals();
    private sendNewPost();
}
