import { EventEmitter } from '@angular/core';
import { ZupApiService } from './zupApi.service';
import { PartnerService } from './partner.service';
import { Notification, ObjectsList } from '../models';
import { Observable } from 'rxjs';
export declare class NotificationsService {
    private zupApi;
    private partner;
    counters: any[];
    countChange: EventEmitter<string>;
    private _list;
    private maxNot;
    constructor(zupApi: ZupApiService, partner: PartnerService);
    /**
     *
     * @param count can be a number or an object
     * @returns
     */
    private toMax(count);
    readonly count: string;
    readonly countByWidget: any;
    countByWidgetAndType(type: string): any;
    assign(notif: Notification, memberId: string, message?: string, options?: {
        widgetId?: string;
        profileId?: string;
        postId?: string;
    }): Observable<void>;
    close(id: any): Observable<void>;
    countByType(widgetId?: string, toMax?: boolean): {};
    countByFilter(filter: {
        widgetId?: string;
        widgetType?: number;
        alertType?: string;
    }, toMax?: boolean): string | number;
    getNotifications(limit?: number, options?: any): Observable<ObjectsList<Notification>>;
    private _getNotifications();
    private _countNotifications();
}
