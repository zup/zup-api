import { Injectable, Optional } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { ApiAuthService } from './apiAuth.service';
import { ApiConfig } from './api.config';
import { ActionLog, ActionLogDetail, alertTypes, Campaign, FilterRules, fromLexical, Label, Lexical, Influencer, Member, NewLexical, Notification, ObjectsList, ObjectsListLC, Partner, Post, Profile, PublishAction, screenTypes, SimpleProfile, Task, TaskEvent, toActionLog, toLexical, 
//toPublishAction,
View, Wall, wallJsonPost, Widget, WidgetContext } from '../models';
import { InStorage, objectFormat } from './storage';
import { _attributionTypes } from '../listOfValues';
import { objectFormatTools, dates, tools } from '../misc/utils';
export var INFLUENCER_ANALYTICS;
(function (INFLUENCER_ANALYTICS) {
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["NB_PUB"] = 1] = "NB_PUB";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["FOLLOWERS"] = 2] = "FOLLOWERS";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["FOLLOWINGS"] = 3] = "FOLLOWINGS";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["GENGER"] = 4] = "GENGER";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["INTERESTS"] = 5] = "INTERESTS";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["AGE"] = 6] = "AGE";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["LIVESIN"] = 7] = "LIVESIN";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["LANGUAGE"] = 8] = "LANGUAGE";
    INFLUENCER_ANALYTICS[INFLUENCER_ANALYTICS["ENGAGEMENT_RATE"] = 9] = "ENGAGEMENT_RATE";
})(INFLUENCER_ANALYTICS || (INFLUENCER_ANALYTICS = {}));
var ZupApiService = /** @class */ (function () {
    function ZupApiService(api, config) {
        var _this = this;
        this.api = api;
        this.localCache = new InStorage(this);
        this.actions = {
            parent: this,
            api: this.api,
            likePost: function (postId, like, options) {
                if (like === void 0) { like = true; }
                if (options === void 0) { options = {}; }
                var provider = objectFormatTools.getAttributionTypeFromId(postId);
                var url;
                switch (provider) {
                    case _attributionTypes.TWITTER:
                        url = '/publish/twitter/like';
                        break;
                    case _attributionTypes.INSTAGRAM:
                        url = '/publish/instagram/like';
                        break;
                    case _attributionTypes.FACEBOOK:
                        url = '/publish/facebook/like';
                        break;
                    default:
                        return Observable.throw("SOURCE_TYPE_UNINTERACTABLE(" + objectFormatTools.getAttributionTypeFromId(postId) + ")");
                }
                var message = {
                    like: {
                        post_id: postId,
                        like: like,
                        notification_id: options.notificationId,
                        task_id: options.taskId,
                        screen: options.screen,
                        widget_id: options.widgetId,
                        author_id: options.userId,
                        xid: options.socialAccount
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                    .map(function (res) { return true; })
                    .catch(function (err) {
                    if (err.toString() === 'Not found Token for partner')
                        return Observable.throw("NO_TOKEN(" + _attributionTypes.detail(provider).text + ")");
                    return Observable.throw(err);
                });
            },
            commentPost: function (postId, comment, options) {
                var _this = this;
                if (options === void 0) { options = {}; }
                var provider = objectFormatTools.getAttributionTypeFromId(postId);
                var url;
                var message;
                switch (provider) {
                    case _attributionTypes.TWITTER:
                        if (!this.tweetIsOk({ text: comment, files: options.files }))
                            return Observable.throw('TWEET_TOO_LONG()');
                        if (!options.profileName)
                            return Observable.throw('MISSING_FIELD(profile name)');
                        url = '/publish/twitter/create';
                        message = {
                            create: {
                                post_id: postId,
                                message: comment,
                                notification_id: options.notificationId,
                                task_id: options.taskId,
                                screen: options.screen,
                                widget_id: options.widgetId,
                                author_id: options.userId,
                                xid: options.socialAccount,
                                name: options.profileName
                            }
                        };
                        break;
                    case _attributionTypes.INSTAGRAM:
                        url = '/publish/instagram/comment';
                        message = {
                            comment: {
                                post_id: postId,
                                comment: comment,
                                notification_id: options.notificationId,
                                task_id: options.taskId,
                                screen: options.screen,
                                widget_id: options.widgetId,
                                author_id: options.userId,
                                xid: options.socialAccount
                            }
                        };
                        break;
                    case _attributionTypes.FACEBOOK:
                        url = '/publish/facebook/comment';
                        message = {
                            comment: {
                                post_id: postId,
                                comment: comment,
                                notification_id: options.notificationId,
                                task_id: options.taskId,
                                screen: options.screen,
                                widget_id: options.widgetId,
                                author_id: options.userId,
                                xid: options.socialAccount
                            }
                        };
                        break;
                    default:
                        return Observable.throw("SOURCE_TYPE_UNINTERACTABLE(" + objectFormatTools.getAttributionTypeFromId(postId) + ")");
                }
                return this.parent.loadFiles(options.files).flatMap(function (res) {
                    if (message.create)
                        message.create.media_ids = res.map(function (f) { return f.id; }); // twitter only
                    return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                        .map(function (res) { return true; })
                        .catch(function (err) {
                        return Observable.throw(err);
                    });
                });
            },
            retweetPost: function (postId, options) {
                if (options === void 0) { options = {}; }
                var url = '/publish/twitter/repost';
                var message = {
                    repost: {
                        post_id: postId,
                        repost: true,
                        notification_id: options.notificationId,
                        task_id: options.taskId,
                        screen: options.screen,
                        widget_id: options.widgetId,
                        xid: options.socialAccount
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            },
            tweet: function (tweet, options) {
                var _this = this;
                if (options === void 0) { options = {}; }
                if (!this.tweetIsOk({ text: tweet, files: options.files }))
                    return Observable.throw('TWEET_TOO_LONG()');
                // TODO: chect tweet length (if url or without)
                var url = '/publish/twitter/create';
                var message = {
                    create: {
                        message: tweet,
                        notification_id: options.notificationId,
                        task_id: options.taskId,
                        screen: options.screen,
                        widget_id: options.widgetId,
                        xid: options.socialAccount,
                        author_id: options.userId
                    }
                };
                return this.parent.loadFiles(options.files).flatMap(function (res) {
                    message.create.media_ids = res.map(function (f) { return f.id; });
                    return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                        return true;
                    });
                });
            },
            followUser: function (userId, follow, options) {
                if (follow === void 0) { follow = true; }
                if (options === void 0) { options = {}; }
                var url;
                switch (objectFormatTools.getAttributionTypeFromId(userId)) {
                    case _attributionTypes.TWITTER:
                        url = '/publish/twitter/follow';
                        break;
                    case _attributionTypes.INSTAGRAM:
                        url = '/publish/instagram/follow';
                        break;
                    default:
                        return Observable.throw("SOURCE_TYPE_UNINTERACTABLE(" + objectFormatTools.getAttributionTypeFromId(userId) + ")");
                }
                var message = {
                    follow: {
                        profile_id: userId,
                        follow: follow,
                        notification_id: options.notificationId,
                        task_id: options.taskId,
                        screen: options.screen,
                        widget_id: options.widgetId,
                        post_id: options.postId,
                        xid: options.socialAccount
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            },
            clearAlert: function (alertId) {
                return this.parent.notifications.close(alertId);
            },
            tweetIsOk: function (message) {
                //replace links with 23 characters:
                if (message.text)
                    return tools.tweetLength(message.text) <= 280;
                return !!message.files && message.files.length > 0;
            },
            mention: function (tweet, profileName, options) {
                var _this = this;
                if (options === void 0) { options = {}; }
                if (!this.tweetIsOk({ text: tweet, files: options.files }))
                    return Observable.throw('TWEET_TOO_LONG()');
                if (!profileName)
                    return Observable.throw('MISSING_FIELD(profile name)');
                // TODO: chect tweet length (if url or without)
                var url = '/publish/twitter/create';
                var message = {
                    create: {
                        message: tweet,
                        notification_id: options.notificationId,
                        task_id: options.taskId,
                        screen: options.screen,
                        widget_id: options.widgetId,
                        xid: options.socialAccount,
                        author_id: options.userId,
                        name: profileName,
                    }
                };
                return this.parent.loadFiles(options.files).flatMap(function (res) {
                    message.create.media_ids = res.map(function (f) { return f.id; });
                    return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                        return true;
                    });
                });
            },
            multiple: function (publishes) {
                var _this = this;
                var url = '/publish/multipublish';
                var todo = [];
                publishes.forEach(function (p) {
                    var message = {
                        multi_publish: {
                            like: p.like,
                            comment: p.comment,
                            follow: p.follow,
                            token_xid: p.tokenId,
                            message: p.message,
                            profile_ids: p.profileIds,
                            profile_names: p.profileNames,
                            medias: p.files ? p.files.map(function (f) { return f.substring(f.indexOf(',') + 1); }) : undefined
                        }
                    };
                    todo.push(_this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }));
                });
                return Observable.forkJoin.apply(Observable, todo).map(function () { return true; });
            }
            /*list: function(filters: any): Observable<Action[]>{
             // fake actions
             const fakeActions = [
    
             ];
    
             return Observable.from(fakeActions);
             }*/
        };
        this.campaigns = {
            api: this.api,
            parent: this,
            add: function (campaign) {
                var todos = [];
                campaign.publishActions.forEach(function (p) {
                    var url = '/tokens/autopublish/add';
                    var message = {
                        add: {
                            like: p.like,
                            comment: p.comment,
                            follow: p.follow,
                            mention: p.mention,
                            token_xid: p.tokenId,
                            message: p.message,
                            mention_message: p.mentionMessage,
                            profiles_label_id: campaign.label.id,
                            since: (p.since || 0) * 60,
                            once_for_parter: p.once,
                            rate_limit: p.rateLimit
                        }
                    };
                    var mediaObservable = [];
                    if (p.files && p.files.length > 0) {
                        var mediaUrl = '/tokens/medias/add';
                        var mediaIds_1 = p.files.filter(function (f) { return !!f.id; }).map(function (f) { return f.id; });
                        var files = p.files.filter(function (f) { return !f.id; }).map(function (f) { return f.url.substring(f.url.indexOf(',') + 1); });
                        if (files.length > 0) {
                            var mediaMessage = {
                                add: {
                                    medias: files
                                }
                            };
                            mediaObservable.push(_this.api.call(mediaUrl, mediaMessage, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                                message.add.media_ids = mediaIds_1.concat(res.json().media_ids);
                                return '';
                            }));
                        }
                    }
                    if (p.mentionFiles && p.mentionFiles.length > 0) {
                        var mediaUrl = '/tokens/medias/add';
                        var mediaIds_2 = p.mentionFiles.filter(function (f) { return !!f.id; }).map(function (f) { return f.id; });
                        var files = p.mentionFiles.filter(function (f) { return !f.id; }).map(function (f) { return f.url.substring(f.url.indexOf(',') + 1); });
                        if (files.length > 0) {
                            var mediaMessage = {
                                add: {
                                    medias: files
                                }
                            };
                            mediaObservable.push(_this.api.call(mediaUrl, mediaMessage, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                                message.add.mention_media_ids = mediaIds_2.concat(res.json().media_ids);
                                return '';
                            }));
                        }
                    }
                    if (mediaObservable.length === 0)
                        mediaObservable.push(Observable.of(''));
                    todos.push(Observable.forkJoin.apply(Observable, mediaObservable).flatMap(function () {
                        return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                            .map(function (res) {
                            return {
                                id: res.json().widget_auto_publish.id,
                                files: (res.json().widget_auto_publish.media_urls || []).map(function (u, i) {
                                    return { id: res.json().widget_auto_publish.media_ids[i], url: u };
                                }),
                                mentionFiles: (res.json().widget_auto_publish.mention_media_urls || []).map(function (u, i) {
                                    return { id: res.json().widget_auto_publish.mention_media_ids[i], url: u };
                                })
                            };
                        });
                    }));
                });
                return Observable.forkJoin.apply(Observable, todos).map(function (res) {
                    campaign.publishActions.forEach(function (p, i) {
                        p.id = res[i].id;
                        p.files = res[i].files;
                        p.mentionFiles = res[i].mentionFiles;
                    });
                    return campaign;
                });
            },
            delete: function (campaign, deleteLabel) {
                if (deleteLabel === void 0) { deleteLabel = true; }
                var todos = [];
                campaign.publishActions.forEach(function (p) {
                    if (!p.id)
                        return; // when updating a campaign, som publishAction can be new
                    var url = '/tokens/autopublish/delete';
                    var message = {
                        delete: { id: p.id }
                    };
                    todos.push(_this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }));
                });
                if (deleteLabel)
                    todos.push(_this.labels.delete(campaign.label.id));
                if (todos.length > 0) {
                    return Observable.forkJoin.apply(Observable, todos).map(function (res) { return true; });
                }
                else {
                    return Observable.from([true]);
                }
            },
            list: function () {
                var url = '/tokens/autopublish/list';
                var message = {
                    list: {}
                };
                return _this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    return (res.json().items || []);
                });
            },
            listProfiles: function (campaign, options) {
                if (options === void 0) { options = {}; }
                var wc = new WidgetContext();
                wc.allRange();
                if (options.after)
                    wc.after = options.after;
                if (options.before)
                    wc.before = options.before;
                var todos = campaign.publishActions.map(function (p) {
                    return _this.logs.list(wc, { campaignId: p.id }, { limit: 1000 });
                });
                return Observable.forkJoin.apply(Observable, todos).map(function (res) {
                    //*
                    //let profilesCreated: any[] = [];
                    var profile2Log = new Map();
                    res.forEach(function (ol) {
                        //profilesCreated = profilesCreated.concat(ol.list.map(l => {return {id: l.profileId, created: l.created}}));
                        ol.list.forEach(function (l) {
                            if (l.profileId && !profile2Log.has(l.profileId))
                                profile2Log.set(l.profileId, l);
                        });
                    });
                    //profilesCreated = tools.unduplicate(profilesCreated, (o) => o.id);
                    var profileIds = Array.from(profile2Log.keys());
                    return new ObjectsListLC(profileIds, 20, function (ids) {
                        return _this.profiles.getMultiple(ids).map(function (profiles) {
                            profiles.forEach(function (p) { return p.customValue = profile2Log.get(p.id); });
                            return profiles;
                        });
                    });
                    //*/
                });
                /*
                                .flatMap((res: ObjectsList<ActionLog>[]) => {
                                const profile2Log = new Map<string, ActionLog>();
                
                
                                res.forEach((ol: ObjectsList<ActionLog>) => {
                                    ol.list.forEach( l => {
                                        if (!profile2Log.has(l.profileId)) profile2Log.set(l.profileId, l);
                                    });
                                })
                
                                return this.profiles.getMultiple(Array.from(profile2Log.keys())).map(profiles => {
                                    profiles.forEach(p => p.customValue = profile2Log.get(p.id));
                
                                    return new ObjectsListLC(
                                        profiles,
                                        20,
                                        (profiles) => Observable.of(profiles)
                                    )
                
                                })
                            });
                            //*/
            },
            update: function (campaign, partnerId) {
                var todos = [];
                campaign.publishActions.forEach(function (p) {
                    var url = '/tokens/autopublish/update';
                    var message = {
                        update: {
                            id: p.id,
                            like: p.like,
                            comment: p.comment,
                            follow: p.follow,
                            mention: p.mention,
                            token_xid: p.tokenId,
                            message: p.message,
                            mention_message: p.mentionMessage,
                            profiles_label_id: campaign.label.id,
                            since: (p.since || 0) * 60,
                            once_for_parter: p.once,
                            rate_limit: p.rateLimit,
                            partner_id: partnerId
                        }
                    };
                    var mediaObservable = [];
                    mediaObservable.push(_this.loadFiles(p.files).map(function (res) {
                        message.update.media_ids = res.map(function (f) { return f.id; });
                        message.update.media_urls = res.map(function (f) { return f.url; });
                    }));
                    mediaObservable.push(_this.loadFiles(p.mentionFiles).map(function (res) {
                        message.update.mention_media_ids = res.map(function (f) { return f.id; });
                        message.update.mention_media_urls = res.map(function (f) { return f.url; });
                    }));
                    /*
                                    if ( p.files && p.files.length > 0) {
                                        const mediaUrl = '/tokens/medias/add';
                    
                                        let  mediaIds =  p.files.filter(f => !!f.id).map(f => f.id);
                    
                                        const files = p.files.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));
                    
                                        if (files.length > 0) {
                                            const mediaMessage = {
                                                add: {
                                                    medias:  files
                                                }
                                            }
                    
                    
                                            mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                                                    message.add.media_ids = mediaIds.concat(res.json().media_ids);
                                                    return '';
                                                })
                                            )
                                        }
                    
                                    }
                    */
                    /*
                                    if ( p.mentionFiles && p.mentionFiles.length > 0) {
                                        const mediaUrl = '/tokens/medias/add';
                    
                                        let  mediaIds =  p.mentionFiles.filter(f => !!f.id).map(f => f.id);
                    
                                        const files = p.mentionFiles.filter(f => !f.id).map(f => f.url.substring(f.url.indexOf(',') + 1));
                    
                                        if (files.length > 0) {
                                            const mediaMessage = {
                                                add: {
                                                    medias:  files
                                                }
                                            }
                    
                    
                                            mediaObservable.push(this.api.call(mediaUrl, mediaMessage, {apiid: 'b2bapi', rights: ['write']}).map(res => {
                                                    message.add.mention_media_ids = mediaIds.concat(res.json().media_ids);
                                                    return '';
                                                })
                                            )
                                        }
                    
                                    }
                                    */
                    if (mediaObservable.length === 0)
                        mediaObservable.push(Observable.of(''));
                    todos.push(Observable.forkJoin.apply(Observable, mediaObservable).flatMap(function () {
                        return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                            .map(function (res) {
                            return {
                                id: res.json().widget_auto_publish.id,
                                files: (res.json().widget_auto_publish.media_urls || []).map(function (u, i) {
                                    return { id: res.json().widget_auto_publish.media_ids[i], url: u };
                                }),
                                mentionFiles: (res.json().widget_auto_publish.mention_media_urls || []).map(function (u, i) {
                                    return { id: res.json().widget_auto_publish.mention_media_ids[i], url: u };
                                })
                            };
                        });
                    }));
                });
                return Observable.forkJoin.apply(Observable, todos).map(function (res) {
                    campaign.publishActions.forEach(function (p, i) {
                        p.id = res[i].id;
                        p.files = res[i].files;
                        p.mentionFiles = res[i].mentionFiles;
                    });
                    return campaign;
                });
            }
        };
        this.influencers = {
            api: this.api,
            parent: this,
            analytics: function (iid) {
                var url = '/influence/influencer_last_version';
                var message = {
                    influencer_id: iid,
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    var analytics = res.json().analytics;
                    if (!analytics)
                        return {};
                    var influencerAnalytics = {};
                    // gender :
                    var g = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.GENGER; });
                    if (g)
                        influencerAnalytics.gender = g.analytics.map(function (a) { return { name: a.payload, count: a.score }; });
                    /*if (g) influencerAnalytics.gender = [
                                        {name: 'F', count: (g.analytics[0].score) * 1000},
                                        {name: 'M', count : (1 - g.analytics[0].score) * 1000},
                                        {name: 'O', count: 0}
                                    ];*/
                    // interests :
                    var i = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.INTERESTS; });
                    if (i)
                        influencerAnalytics.interests = i.analytics.map(function (a) { return { name: a.payload, count: Math.floor(a.score * 1000) }; });
                    //followings:
                    var f = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.FOLLOWINGS; });
                    if (f)
                        influencerAnalytics.followings = f.analytics[0].score;
                    //engagement:
                    var e = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.ENGAGEMENT_RATE; });
                    if (e)
                        influencerAnalytics.engagements = tools.deepGet(e.analytics.find(function (a) { return a.payload === 'total_engagements'; }), 'score') || 0;
                    if (e)
                        influencerAnalytics.engagementRate = tools.deepGet(e.analytics.find(function (a) { return a.payload === 'engagement_rate'; }), 'score') || 0;
                    if (e)
                        influencerAnalytics.nbPosts = tools.deepGet(e.analytics.find(function (a) { return a.payload === 'total_posts'; }), 'score') || 0;
                    //age:
                    var age = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.AGE; });
                    if (age)
                        influencerAnalytics.age = age.analytics.map(function (a) { return { name: a.payload, count: Math.floor(a.score * 1000) }; });
                    // transform desc :
                    //language
                    var la = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.LANGUAGE; });
                    if (la)
                        influencerAnalytics.language = la.analytics.map(function (a) { return { name: a.payload, count: Math.floor(a.score * 1000) }; });
                    //linvesin:
                    var l = analytics.find(function (a) { return a.analytic_type === INFLUENCER_ANALYTICS.LIVESIN; });
                    if (l)
                        influencerAnalytics.livesin = l.analytics.map(function (a) { return { name: a.payload, count: Math.floor(a.score * 1000) }; });
                    return influencerAnalytics;
                });
            },
            analyticHistory: function (iid, widgetContext, analyticType) {
                var url = '/influence/analytic_historic';
                var message = {
                    analytic_historic: widgetContext.getContext({ extendedOptions: {
                            influencer_id: iid,
                            analytic_type: analyticType
                        } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    var ha = [];
                    var histo = res.json().historic;
                    histo.forEach(function (h) {
                        ha.push({ date: h.version, analytics: h.analytics });
                    });
                    return ha.sort(function (a, b) { return a.date - b.date; });
                });
            },
            get: function (iid, onI4U) {
                //TODO: check in storage if we already have him
                var todos = [
                    this.parent.profiles.getMultiple([iid], objectFormat.XL, { onI4U: onI4U }),
                    this.analytics(iid)
                ];
                if (onI4U) {
                    todos.push(this.parent.profiles.getMultiple([this._profileId]).map(function (res) { return res[0]; }));
                }
                return Observable.forkJoin.apply(Observable, todos).map(function (res) {
                    var I = res[0][0].toInfluencer();
                    I.communityGender = res[1].gender;
                    I.communityInterests = res[1].interests;
                    I.communityAge = res[1].age;
                    I.communityLanguage = res[1].language;
                    I.communityLivesIn = res[1].livesin;
                    I.followings = res[1].followings;
                    I.engagementRate = res[1].engagementRate;
                    I.engagements = res[1].engagements;
                    I.nbPosts = res[1].nbPosts;
                    if (res[2]) {
                        I.labels = res[2].labels;
                        I.leadScore = res[2].leadScore; // TODO: replace by influencer score
                    }
                    return I;
                });
            },
            multiProfiles: function (iid) {
                var _this = this;
                var url = '/influence/influencer_multi_profiles';
                var message = {
                    profile_id: iid
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).flatMap(function (res) {
                    return _this.parent.profiles.getMultiple(res.json().profiles_id);
                }).catch(function (err) {
                    return _this.parent.profiles.getMultiple([iid]);
                });
            }
        };
        /**
             * Exposes methodes to access and manage labels
             *
             * @Method add
             *
             *
             */
        this.labels = {
            api: this.api,
            parent: this,
            add: function (label) {
                var url = 'label/add';
                var message = {
                    add: label.forExport()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return new Label(res.json().add);
                });
            },
            addProfileToLabel: function (userId, labelId, options) {
                var _this = this;
                if (options === void 0) { options = {}; }
                var url = 'label/profile/include';
                var message = {
                    include: {
                        label_id: labelId,
                        widget_id: options.widgetId,
                        screen: options.screen,
                        postId: options.postId
                    }
                };
                if (typeof userId === 'string') {
                    message.include.profile_id = userId;
                }
                else {
                    message.include.profile_ids = userId;
                }
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    var label = _this.parent.localCache.getJsonObjects([labelId], 'label')[0];
                    if (label)
                        label.count++;
                    var profile = _this.parent.localCache.getJsonObjects([userId], 'profile')[0];
                    if (profile) {
                        profile.profile_label_ids = profile.profile_label_ids || [];
                        profile.profile_label_ids.push(labelId);
                    }
                });
            },
            addProfilesToLabel: function (profilIds, labelId) {
                return this.addProfileToLabel(profilIds, labelId);
            },
            delete: function (id) {
                return this.remove(id);
            },
            get: function (id) {
                var ls = this.parent.localCache.getJsonObjects([id], 'label');
                if (ls.length === 0) {
                    return Observable.throw({ message: 'Unckown Label', noParse: true });
                }
                else {
                    return Observable.of(new Label(ls[0]));
                }
            },
            list: function (listOptions) {
                var _this = this;
                if (listOptions === void 0) { listOptions = {}; }
                var url = 'label/list';
                var message = {
                    list: {
                        limit: listOptions.limit || 100,
                        cursor: listOptions.cursor
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    _this.parent.localCache.addObjects(res.json().list.labels, 'label');
                    return new ObjectsList(res.json().list.labels.map(function (l) { return new Label(l); }), res.json().list.cursor
                        ? _this.list({ limit: listOptions.limit, cursor: res.json().label.cursor })
                        : undefined);
                });
            },
            // to be improved
            listAll: function () {
                var _this = this;
                var url = 'label/list';
                var message = {
                    list: {
                        limit: 1000
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    _this.parent.localCache.addObjects(res.json().list.labels, 'label');
                    return new ObjectsList(res.json().list.labels.map(function (l) { return new Label(l); }));
                }).catch(function (err) {
                    console.log(err.toString());
                    return Observable.of(new ObjectsList([]));
                });
            },
            profileLabels: function (userId) {
                var url = 'label/profile/list';
                var message = {
                    list: {
                        profile_id: userId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' })
                    .map(function (res) { return res.json().list.profile_labels.map(function (l) { return new Label(l); }); });
            },
            profilesList: function (labelId, limitOptions, options) {
                var _this = this;
                if (limitOptions === void 0) { limitOptions = { limit: 50 }; }
                if (options === void 0) { options = {}; }
                var url = '/label/profiles';
                var message = {
                    list: {
                        limit: limitOptions.limit,
                        cursor: limitOptions.cursor,
                        label_id: labelId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', noCache: true }).map(function (res) {
                    var jsonRes = res.json();
                    var getMore = jsonRes.list.cursor ?
                        _this.profilesList(labelId, { limit: limitOptions.limit, cursor: jsonRes.list.cursor }, options) : undefined;
                    var profiles = tools.unduplicate(jsonRes.list.profiles, function (p) { return p.id; });
                    var profilesIds = profiles.map(function (p) { return p.id; });
                    if (options.onlyIds) {
                        return new ObjectsList(profilesIds, getMore, { unduplicateFunction: function (id, ids) { return ids.indexOf(id) < 0; } });
                    }
                    else if (options.simpleProfiles) {
                        return new ObjectsList(profiles.map(function (p) { return new Profile(p); }), getMore, { unduplicateFunction: function (p, ps) { return ps.findIndex(function (u) { return u.id === p.id; }) < 0; } });
                    }
                    else {
                        return new ObjectsListLC(profilesIds, 10, 
                        // if there is a limitoption.cursor, it is a "getMore" call, no need to look for details now
                        limitOptions.cursor ? null : function (ids) { return _this.parent.profiles.getMultiple(ids, objectFormat.L, { onI4U: options.onI4U }); }, getMore, { unduplicateFunction: function (id, ids) { return ids.indexOf(id) < 0; } });
                    }
                });
            },
            remove: function (id) {
                var url = 'label/remove';
                var message = {
                    remove: {
                        label_id: id
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            },
            removeProfileFromLabel: function (userId, labelId) {
                var _this = this;
                var url = 'label/profile/exclude';
                var message = {
                    remove: {
                        profile_id: userId,
                        label_id: labelId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    var label = _this.parent.localCache.getJsonObjects([labelId], 'label')[0];
                    if (label)
                        label.count--;
                    var profile = _this.parent.localCache.getJsonObjects([userId], 'profile')[0];
                    if (profile) {
                        profile.profile_label_ids = profile.profile_label_ids || [];
                        profile.profile_label_ids.splice(profile.profile_label_ids.indexOf(labelId), 1);
                    }
                });
            },
            removeProfilesFromLabel: function (userIds, labelId) {
                var _this = this;
                var todos = [];
                userIds.forEach(function (uid) {
                    todos.push(_this.removeProfileFromLabel(uid, labelId, {}));
                });
                return tools.forkJoin2FlatMap(todos).map(function (res) { return true; });
            },
            update: function (label) {
                var url = 'label/update';
                var message = {
                    update: label.forExport()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            }
        };
        this.lexical = {
            api: this.api,
            self: this,
            delete: function (id) {
                return this.delete_multiple([id]);
            },
            delete_multiple: function (ids) {
                var url = '/lexical/delete';
                var message = {
                    lexicals: ids
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            },
            get: function (ids) {
                var url = '/lexical/get';
                var message = {
                    lexicals: ids
                };
                return this.api.call(url, message, { apiid: 'b2bapi', noCache: true }).map(function (res) {
                    // todo : handle errors
                    return res.json().lexicals.ok;
                });
            },
            list: function (category) {
                var url = '/lexical/list';
                var message = {
                    lexical_category: category
                };
                return this.api.call(url, message, { apiid: 'b2bapi', noCache: true }).map(function (res) {
                    var lexicals = res.json().lexicals.lexicals;
                    return lexicals.map(function (l) { return toLexical(l); });
                });
            },
            set: function (lexicals) {
                var url = 'lexical/create';
                var message = {
                    lexicals: lexicals.map(function (l) { return fromLexical(l); })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return res.json().lexicals.map(function (l) { return toLexical(l); });
                });
            },
            create: function (lexical) {
                return this.set([lexical]).map(function (res) { return res[0]; });
            },
            update: function (lexicals) {
                var url = '/lexical/update';
                var message = {
                    lexicals: lexicals.map(function (l) { return fromLexical(l); })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            }
        };
        this.logs = {
            api: this.api,
            parent: this,
            get: function (actionLog) {
                var pids = [
                    actionLog.publishAccountId,
                    actionLog.memberId,
                    actionLog.profileId
                ].filter(function (s) { return !!s; });
                var returnLog = Object.assign({}, actionLog);
                return this.parent.profiles.getMultiple(pids).map(function (profiles) {
                    var P = new Map();
                    profiles.forEach(function (p) { return P.set(p.id, p); });
                    if (actionLog.publishAccountId) {
                        returnLog.publishAccount = P.get(actionLog.publishAccountId);
                    }
                    if (actionLog.memberId) {
                        returnLog.member = P.get(actionLog.memberId);
                    }
                    if (actionLog.profileId) {
                        returnLog.profile = P.get(actionLog.profileId);
                    }
                    return returnLog;
                });
            },
            list: function (widgetContext, filters, listOptions) {
                var _this = this;
                if (filters === void 0) { filters = {}; }
                if (listOptions === void 0) { listOptions = {}; }
                var url = 'history_log/list';
                var message = {
                    list: widgetContext.getContext({
                        extendedOptions: {
                            limit: listOptions.limit,
                            cursor: listOptions.cursor,
                            by_member: filters.memberId,
                            by_token: filters.token,
                            by_campaign_id: filters.campaignId,
                            on_profile: filters.profileId,
                            //on_post: filters.postId,
                            type: filters.publishType
                        }
                    })
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    /*
                                     {
                                     ..ApiResponse
                                     ..list: {
                                     ....history_log_entries:[]HistoryLogEntries
                                     ....tasksk:[]Task;
                                     ....notifications:[]Notification;
                                     ....profiles:[]Profile
                                     ..}
                                     }
                                     */
                    /*
                     {
                     ..ApiResponse
                     ..list: {
                     ....history_log_entries:[]HistoryLogEntries
                     ....tasksk:[]Task;
                     ....notifications:[]Notification;
                     ....profiles:[]Profile
                     ..}
                     }
                     */
                    _this.parent.localCache.addObjects(res.json().list.profiles.filter(function (p) { return !!p.id; }), 'profile', objectFormat.S);
                    _this.parent.localCache.addObjects(res.json().list.posts, 'post', objectFormat.S);
                    _this.parent.localCache.addObjects(res.json().list.notifications, 'notification', objectFormat.S);
                    _this.parent.localCache.addObjects(res.json().list.tasks, 'task', objectFormat.S);
                    var getMore = res.json().list.cursor ?
                        _this.list(widgetContext, filters, Object.assign(listOptions, { cursor: res.json().list.cursor })) :
                        undefined;
                    var list = res.json().list.history_log_entries.map(function (l) { return toActionLog(l); });
                    if (filters.postId) {
                        list = list.filter(function (l) { return !l.postId || l.postId === filters.postId; });
                    }
                    return new ObjectsList(list, getMore);
                });
            },
            listDetail: function (widgetContext, options, listOptions) {
                var _this = this;
                if (options === void 0) { options = {}; }
                if (listOptions === void 0) { listOptions = {}; }
                var url = 'history_log/list';
                var message = {
                    list: widgetContext.getContext({
                        extendedOptions: {
                            limit: listOptions.limit,
                            cursor: listOptions.cursor,
                            by_member: options.memberId,
                            on_profile: options.profileId,
                            on_post: options.postId,
                        }
                    })
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (res) {
                    /*
                                     {
                                     ..ApiResponse
                                     ..list: {
                                     ....history_log_entries:[]HistoryLogEntries
                                     ....tasksk:[]Task;
                                     ....notifications:[]Notification;
                                     ....profiles:[]Profile
                                     ..}
                                     }
                                     */
                    /*
                     {
                     ..ApiResponse
                     ..list: {
                     ....history_log_entries:[]HistoryLogEntries
                     ....tasksk:[]Task;
                     ....notifications:[]Notification;
                     ....profiles:[]Profile
                     ..}
                     }
                     */
                    _this.parent.localCache.addObjects(res.json().list.profiles, 'profile');
                    _this.parent.localCache.addObjects(res.json().list.posts, 'post');
                    _this.parent.localCache.addObjects(res.json().list.notifications, 'notification');
                    _this.parent.localCache.addObjects(res.json().list.tasks, 'task');
                    var profileIds = tools.unduplicate((res.json().list.posts || []).map(function (p) { return p.profile_id; })
                        .concat((res.json().list.history_log_entries || []).map(function (h) { return h.member_id; }).filter(function (m) { return m !== 'auto'; }))
                        .concat((res.json().list.history_log_entries || []).map(function (h) { return h.by_token; }))
                        .concat((res.json().list.history_log_entries || []).map(function (h) { return h.profile_id; }))).filter(function (id) { return !!id; });
                    return _this.parent.profiles
                        .getMultiple(profileIds, objectFormat.XS)
                        .map(function () {
                        var getMore = res.json().list.cursor ?
                            _this.listDetail(widgetContext, options, Object.assign(listOptions, { cursor: res.json().list.cursor })) :
                            undefined;
                        return new ObjectsList(res.json().list.history_log_entries.map(function (l) {
                            var al = toActionLog(l);
                            al.member = al.memberId && _this.parent.localCache.profileObject(al.memberId);
                            al.profile = al.profileId && _this.parent.localCache.profileObject(al.profileId);
                            al.publishAccount = al.publishAccountId && _this.parent.localCache.profileObject(al.publishAccountId);
                            al.task = al.taskId && _this.parent.localCache.taskObject(al.taskId);
                            return al;
                        }), getMore);
                    });
                });
            }
        };
        /**
             *
             * Exposes member functions : invite, list, login, logout, remove
             */
        this.members = {
            api: this.api,
            parent: this,
            invite: function (mail) {
                var url = '/member/add';
                var message = {
                    add: {
                        member_id: '0aL4mg(m)216'
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            },
            list: function () {
                var _this = this;
                var url = '/member/list';
                var message = {
                    list: {
                        limit: 50
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    _this.parent.localCache.addObjects(res.json().list.members, 'profile');
                    return res.json().list.members;
                });
            },
            /*
             login(username?: string, password?: string, rememberMe: boolean = false) {
             return this.api.login(username, password, rememberMe)
             },
             logout() {
             return this.api.logout();
             },
             */
            remove: /*
                     login(username?: string, password?: string, rememberMe: boolean = false) {
                     return this.api.login(username, password, rememberMe)
                     },
                     logout() {
                     return this.api.logout();
                     },
                     */
            function (id) {
                var url = '/member/remove';
                var message = {
                    remove: {
                        member_id: id
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            }
        };
        this.misc = {
            api: this.api,
            parent: this,
            uploadPhoto: function (data) {
                if (data.indexOf('data:image') !== 0)
                    return Observable.throw('BAD_FORMAT(image)');
                var apiurl = 'photos/upload';
                var message = {
                    photo_upload: {
                        share: true,
                        photo: data.substring(data.indexOf(';base64,') + 8)
                    },
                };
                return this.api.call(apiurl, message, { apiid: 'coldapi', rights: ['write'] }).map(function (res) { return JSON.parse(res.photo_upload.upload_url).url; });
            },
            listPhoto: function () {
                var apiurl = 'photos/list';
                var message = {
                    photo_list: {
                        num_per_page: 5
                    }
                };
                return this.api.call(apiurl, message, { apiid: 'coldapi' }).map(function (res) { return console.log(res.json().photo_list.photos.map(function (p) { return JSON.parse(p.url); })); });
            },
            mstores: {
                api: this.api,
                parent: this,
                list: function () {
                    var url = 'mstores/list';
                    var message = {
                        mstore_list: {}
                    };
                    return this.api.call(url, message, { apiid: 'coldapi' }).map(function (res) {
                        var mstores = {};
                        (res.json().mstore_list.mstore_list || []).forEach(function (m) {
                            mstores[m.key] = JSON.parse(m.value);
                        });
                    });
                },
                get: function (key) {
                    var url = 'mstores/get';
                    var message = {
                        mstore_get: {
                            key: key
                        }
                    };
                    return this.api.call(url, message, { apiid: 'coldapi' }).map(function (res) {
                        return JSON.parse(res.json().mstore_get.value);
                    }).catch(function (err) {
                        if (err.toString() === 'DataNotFound') {
                            return Observable.from([undefined]);
                        }
                        else {
                            return Observable.throw(err);
                        }
                    });
                },
                put: function (key, value) {
                    var url = 'mstores/put';
                    var message = {
                        mstore_put: {
                            key: key,
                            value: JSON.stringify(value)
                        }
                    };
                    return this.api.call(url, message, { apiid: 'coldapi', rights: ['write'] }).map(function (res) { return true; });
                },
                delete: function (key) {
                    var url = 'mstores/delete';
                    var message = {
                        mstore_delete: {
                            key: key
                        }
                    };
                    return this.api.call(url, message, { apiid: 'coldapi', rights: ['write'] }).map(function (res) { return true; });
                }
            }
        };
        this.notifications = {
            api: this.api,
            parent: this,
            assign: function (notification, memberId, assignmentMessage, options) {
                var _this = this;
                var url = '/notifications/assign';
                var message = {
                    assign: {
                        notification_id: notification.id,
                        assign_to: memberId,
                        assignment_message: assignmentMessage,
                        widget_id: options.widgetId,
                        profile_id: options.profileId,
                        post_id: options.postId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).flatMap(function () {
                    if (notification.isClosed)
                        return Observable.from([true]);
                    // we can reassign a closed notification (via tasks workflow)
                    return _this.close(notification.id);
                });
            },
            close: function (id) {
                var url = '/notifications/close';
                var message = {
                    close: {
                        notification_id: id
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return true;
                });
            },
            closeMultiple: function (notificationIds) {
                var url = 'notifications/close_multi';
                if (notificationIds.length === 0) {
                    return Observable.from([true]);
                }
                var message = {
                    close_multi: {
                        ids: notificationIds
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
                /*const message = {
                             close_multi: {
                             ids: notificationIds.slice(0, 10)
                             }
                             };
                             let returnO = this.api.call(url, message, {apiid: 'b2bapi'});
                
                             for (let i = 1; i <= Math.floor(notificationIds.length - 1 / 10); i++) {
                             const tmpIds  = notificationIds.slice(i * 10, (i + 1) * 10);
                             if (tmpIds.length > 0) {
                             const messageSup = {
                             close_multi: {
                             ids: tmpIds
                             }
                             };
                             returnO = returnO.flatMap(() => this.api.call(url, messageSup, {apiid: 'b2bapi'}));
                             };
                             };
                
                             return returnO.map(() => true);*/
            },
            count: function (after) {
                var url = '/notifications/counters';
                var message = {};
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    return res.json().counters.widgets;
                });
            },
            get: function (id) {
                var _this = this;
                var url = '/notifications/get';
                var message = {
                    get: {
                        notification_id: id
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (res) {
                    var notification = res.json().get.notification;
                    var pid = res.json().get.notification.post;
                    var getUser = _this.parent.profiles.get(res.json().get.profile.id);
                    var getPost = _this.parent.posts.getMultiple([pid]);
                    return Observable.forkJoin(getUser, getPost).map(function (resDetail) {
                        return new Notification(notification, resDetail[0], resDetail[1][0]);
                    });
                });
            },
            list: function (listOptions, options) {
                var _this = this;
                var url = '/notifications/list';
                var message = {
                    list: {
                        limit: listOptions.limit || 10,
                        cursor: listOptions.cursor,
                        after: options && options.after,
                        before: options && options.before,
                        type: options && options.type,
                        widget_id: options && options.widgetId,
                        widget_type: options && options.widgetType
                    }
                };
                if (options && options.isClosed) {
                    message.list.hide_opened_notifications = true;
                }
                else {
                    message.list.hide_closed_notifications = true;
                }
                return this.api.call(url, message, { apiid: 'b2bapi' })
                    .flatMap(function (resNotifications) {
                    var postIds = resNotifications.json().list.notifications.map(function (n) { return n.post; });
                    //const profiles = resNotifications.json().list.profiles.map(p => p.id);
                    //const profiles = resNotifications.json().list.profiles.map(p => p.id);
                    _this.parent.localCache.addObjects(resNotifications.json().list.profiles, 'profile', objectFormat.S);
                    var profiles = resNotifications.json().list.profiles.map(function (p) { return _this.parent.localCache.profileObject(p.id, objectFormat.S); });
                    return Observable.forkJoin(
                    //this.parent.profiles.getMultiple(profiles),
                    //this.parent.profiles.getMultiple(profiles),
                    _this.parent.posts.getMultiple(postIds)).map(function (resDetail) {
                        var profilesMap = new Map();
                        profiles.forEach(function (p) { profilesMap.set(p.id, p); });
                        var resPosts = resDetail[0];
                        var postsMap = new Map();
                        resPosts.forEach(function (p) { postsMap.set(p.id, p); });
                        var notifications = [];
                        resNotifications.json().list.notifications.forEach(function (n) {
                            if (!n.post) {
                                console.log('NOTIFICATION WITHOUT POSTID');
                                return;
                            }
                            var post = postsMap.get(n.post);
                            var profile = profilesMap.get(n.post_author);
                            if (!profile && !post) {
                                console.log('notification in error :', n);
                                return;
                            }
                            notifications.push(new Notification(n, profile, post));
                        });
                        return new ObjectsList(notifications, resNotifications.json().list.cursor ?
                            _this.list({ limit: listOptions.limit, cursor: resNotifications.json().list.cursor }, options) : undefined);
                    });
                }).catch(function (err) {
                    console.log(err);
                    return Observable.from([new ObjectsList([], undefined)]);
                });
            },
            setLimits: function (limits) {
                var setOrDelete;
                if (limits.lexical.length > 0) {
                    setOrDelete = this.parent.lexical.set({
                        name: 'word_alert_lexical_name',
                        values: limits.lexical.toLowerCase().split(/\s*,\s*/),
                        mult: 1500
                    });
                }
                else {
                    setOrDelete = this.parent.lexical.delete('word_alert_lexical_name');
                }
                return Observable.forkJoin(setOrDelete).map(function () {
                    return true;
                });
            },
            // //////////////////////////////////////////////////// //
            // ///////////       SUBSCRIPTIONS      /////////////// //
            // //////////////////////////////////////////////////// //
            addSubscribtion: function (emailOrPhone, type, owner) {
                if (type === void 0) { type = 'M'; }
                var url = '/notifications/tokens/add';
                var message = {
                    notification_token: {
                        type: type,
                        token: emailOrPhone,
                        owner: owner
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            },
            listSubscriptions: function () {
                var url = '/notifications/tokens/list';
                return this.api.call(url, {}, { apiid: 'b2bapi' }).map(function (res) { return res.json().list.tokens || []; })
                    .catch(function (err) { return Observable.from([[]]); });
            },
            toggleNotification: function (emailOrPhone, notificationType, enable) {
                if (enable === void 0) { enable = true; }
                var url = '/notifications/tokens/toggle';
                var message = {
                    toggle: {
                        token: emailOrPhone,
                        notification_type: notificationType,
                        enable: enable
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            }
        };
        this.partner = {
            api: this.api,
            parent: this,
            addInstagramToken: function (token) {
                // TODO : tester si pas deja un compte twitter
                var url = '/tokens/insert_ig';
                var message = {
                    insert: {
                        access_token: token,
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                    .map(function (res) {
                    if (res.json().insert.profile) {
                        return new Profile(res.json().insert.profile);
                    }
                    else if (res.json().insert.profiles) {
                        return new Profile(res.json().insert.profiles[0]);
                    }
                });
            },
            addOauthToken: function (token, verifier, provider) {
                var url = 'tokens/insert_oauth';
                var message = {
                    insert: {
                        oauth_token: token,
                        oauth_verifier: verifier,
                        provider: provider
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    if (res.json().insert.profile) {
                        return new Profile(res.json().insert.profile);
                    }
                    else if (res.json().insert.profiles) {
                        return new Profile(res.json().insert.profiles[0]);
                    }
                });
            },
            addTwitterToken: function (token, secret) {
                // TODO : tester si pas deja un compte twitter
                var url = '/tokens/insert_tw';
                var message = {
                    insert: {
                        access_token: token,
                        access_secret: secret
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    if (res.json().insert.profile) {
                        return new Profile(res.json().insert.profile);
                    }
                    else if (res.json().insert.profiles) {
                        return new Profile(res.json().insert.profiles[0]);
                    }
                });
            },
            addFacebookToken: function (token) {
                // TODO : tester si pas deja un compte twitter
                var url = '/tokens/insert_fb';
                var message = {
                    insert: {
                        access_token: token
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return res.json().insert.profiles.map(function (p) { return new Profile(p); });
                }).catch(function (err) {
                    if (err.toString() === 'No Any Page Found') {
                        return Observable.throw('FACEBOOK_PAGE_NEEDED()');
                    }
                    else {
                        return Observable.throw(err);
                    }
                });
            },
            deleteToken: function (tokenUserId) {
                var url = '/tokens/delete';
                var message = {
                    delete: {
                        xid: tokenUserId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            },
            get: function () {
                var url = '/partner/get';
                var getDetail = this.api.call(url, {}, { apiid: 'b2bapi' }).map(function (res) {
                    return res.json().partner;
                });
                return getDetail;
            },
            getOAuthUrl: function (provider) {
                if (provider === _attributionTypes.INSTAGRAM) {
                    var redirectUrl = encodeURIComponent(window.location.origin + this.parent.oauthKeys.INSTAGRAM.REDIRECT);
                    var IG_CLIENTID = this.parent.oauthKeys.INSTAGRAM.CLIENT_ID;
                    var url_1 = "https://api.instagram.com/oauth/authorize/?client_id=" + IG_CLIENTID + "&redirect_uri=" + redirectUrl + "&response_type=token&scope=public_content+likes+comments+relationships";
                    return Observable.from([url_1]);
                }
                var url = '/auth/oauth/urls', message = {
                    providers: [{
                            provider: provider,
                            callback_url: window.location.origin + '/callback/tw'
                        }]
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    return res.json().auth_url[0].redirect_url;
                });
            },
            listTokens: function () {
                var _this = this;
                var url = '/tokens/list';
                var message = {};
                return this.api.call(url, message, { apiid: 'b2bapi' })
                    .map(function (res) {
                    return res.json().tokens.profiles.map(function (p) {
                        _this.parent.localCache.addObject(p, 'profile');
                        return new Profile(p);
                    }).filter(function (p) { return p.attributionKind !== _attributionTypes.FACEBOOK; });
                })
                    .catch(function (err) { Observable.from([[]]); });
            },
            setAccessToken: function (data) {
                switch (data.provider) {
                    case _attributionTypes.TWITTER:
                        return this.addOauthToken(data.oauth_token, data.oauth_verifier, data.provider);
                    case _attributionTypes.INSTAGRAM:
                        return this.addInstagramToken(data.access_token);
                    default:
                        return Observable.throw('PROVIDER_UNKNOWN()');
                }
            },
            update: function (partner) {
                var url = '/partner/update';
                var message = {
                    partner: {
                        description: partner.description,
                        name: partner.name,
                        metas: partner.metas
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return true; });
            },
            widgetsList: function (limit) {
                return this.parent.widget.list(limit);
            }
        };
        this.places = {
            api: this.api,
            parent: this,
            exploreZone: function (zone) {
                var url = 'places/search';
                var message = {
                    box: zone
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] });
            },
            exclude: function (placeId, widgetId, ban) {
                if (ban === void 0) { ban = true; }
                var url = 'widget/filters/update';
                var message = {
                    filter: {
                        widget_id: widgetId,
                    }
                };
                if (ban) {
                    message.filter.filter_places = [placeId];
                }
                else {
                    message.filter.unfilter_places = [placeId];
                }
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            getMultiple: function (ids) {
                if (ids.length === 0)
                    return Observable.of([]);
                var url = 'places';
                var message = {
                    place_getmulti: {
                        ids: ids
                    }
                };
                return this.api.call(url, message, { urlParameters: 'v=2' }).map(function (res) {
                    return res.json().place_getmulti.ok;
                });
            },
            listExcluded: function (widgetId) {
                var _this = this;
                var url = 'widget/filters';
                var message = {
                    filter_get: {
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (res) {
                    var placeIds = (res.json().filter_get.places || []);
                    return _this.parent.profiles.loadToCache(placeIds)
                        .map(function () {
                        return _this.parent.localStorage.getProfiles(placeIds);
                    });
                }).catch(function (err) {
                    return Observable.of([]);
                });
                ;
            },
            searchInZone: function (zone, listOptions) {
                var _this = this;
                if (listOptions === void 0) { listOptions = { limit: 50 }; }
                var url = 'places';
                var message = {
                    zone: zone,
                    limit: listOptions.limit,
                    skip: listOptions.cursor
                };
                return this.api.call(url, message, { apiid: 'hotapi', urlParameters: 'v=2' }).flatMap(function (res) {
                    _this.parent.localCache.addObjects(res.json().places.map(function (p) { p.isError = false; return p; }), 'profile');
                    var getMore = res.json().cursor ?
                        _this.searchInZone(zone, {
                            limit: listOptions.limit,
                            cursor: res.json().cursor
                        }) : undefined;
                    return _this.parent.localCache.getProfiles(res.json().places.map(function (p) { return p.id; }), true)
                        .map(function (resProfiles) {
                        return new ObjectsList(resProfiles, getMore);
                    });
                });
            }
        };
        this.posts = {
            api: this.api,
            parent: this,
            list: function (widgetContext, options, listOptions) {
                var _this = this;
                if (options === void 0) { options = {}; }
                if (listOptions === void 0) { listOptions = { limit: 5 }; }
                if (widgetContext === null) {
                    widgetContext = new WidgetContext(null, { after: 0, before: dates.toZIFromDate(new Date()) });
                }
                var url = 'post/list';
                var message = {
                    list: widgetContext.getContext({ extendedOptions: Object.assign({}, listOptions, options) })
                };
                // TMP: compatibility between widget_id and widget_ids:
                if (message.list.widget_id) {
                    message.list.widget_ids = [message.list.widget_id];
                }
                var callOptions = { apiid: 'b2bapi' };
                if (widgetContext.before !== dates.toZIFromDate(dates.today.to)) {
                    callOptions.cache = true;
                }
                return this.api.call(url, message, callOptions).map(function (res) {
                    var postIds = tools.unduplicate((res.json().list.posts || []).map(function (p) { return p.id; }));
                    var getMore = res.json().list.cursor ?
                        _this.list(widgetContext, options, Object.assign(listOptions, { cursor: res.json().list.cursor })).map(function (r) { return r.posts; }) : undefined;
                    // Add posts to localCache
                    // Add posts to localCache
                    _this.parent.localCache.addPostList(res.json().list);
                    return {
                        posts: new ObjectsList(postIds.map(function (pid) { return _this.parent.localCache.postObject(pid, objectFormat.S); }), getMore, { unduplicateFunction: function (p, ps) { return ps.findIndex(function (o) { return o.id === p.id; }) < 0; } }),
                        total: res.json().list.total
                    };
                });
                /*
                            return this.api.call(url, message, {apiid: 'b2bapi'}).flatMap(res => {
                                // tmp : missing profiles
                                const userIds = res.json().list.posts.map(p => p.authorid);
                                if (userIds.length === 0) return Observable.from([new ObjectsList([])]);
                
                                const getMore = res.json().list.cursor ?
                                    this.list(widgetContext, options, Object.assign(listOptions, {cursor: res.json().list.cursor})) : undefined;
                
                                // Add posts to localCache
                                this.parent.localCache.addObjects(res.json().list.posts, 'post');
                
                                // get profiles
                                return this.parent.profiles.getMultiple(userIds)
                                // now we can get the posts from cache
                                    .flatMap(() => this.parent.localCache.getPosts(res.json().list.posts.map(p => p.id)))
                                    // and create the postsList
                                    .map( posts => new ObjectsList(posts, getMore));
                            });
                            // */
            },
            markers: function (widgetContext, options, listOptions) {
                if (options === void 0) { options = {}; }
                if (listOptions === void 0) { listOptions = { limit: 5 }; }
                /*const url = 'post/list';
                
                            const message = {
                                list: widgetContext.getContext({extendedOptions: Object.assign(
                                    {},
                                    listOptions,
                                    {author_id: options.authorId, source: options.source}
                                    )}
                                )
                            };
                
                            return this.api.call(url, message, {apiid: 'b2bapi', cache: true}).map(res => {
                                return res.json().list.posts;
                            });*/
                return this.list(widgetContext, options, listOptions);
            },
            /**
                     *
                     * @param ids
                     * @param publicAPI whether or not to use public api for profiles
                     * @returns {any}
                     */
            getMultiple: function (ids, format) {
                if (!ids || ids.length === 0)
                    return Observable.from([[]]);
                return this.parent.localCache.getPosts(ids, format);
            },
            /**
             * only adds posts to LocalCache, does not return anything
             * !uses coldApi and not b2bApi!
             *
             * @param ids
             * @returns {Observable<boolean>}
             */
            loadToCache: /**
                     * only adds posts to LocalCache, does not return anything
                     * !uses coldApi and not b2bApi!
                     *
                     * @param ids
                     * @returns {Observable<boolean>}
                     */
            function (ids, format) {
                var _this = this;
                var url = '/posts';
                var message = {
                    post_get_multiple: {
                        post_format: 'large',
                        profile_format: 'large',
                        ids: ids
                    }
                };
                var retrunFn = this.api.call(url, message, { cache: true });
                if (format !== undefined && format < objectFormat.L) {
                    return retrunFn.map(function (res) {
                        var resPosts = res.json().post_get_multiple.ok[0].posts || [];
                        resPosts.forEach(function (jp) {
                            jp.auhtorid = jp.auhtorid || jp.author_id; // bug format change between b2b and rive apis
                            _this.parent.localCache.addObject(jp, 'post'); // there is not format managment for posts, it is for userInPost
                        });
                        var resProfiles = res.json().post_get_multiple.ok[0].profiles || [];
                        resProfiles.forEach(function (jp) {
                            jp.isPublic = true;
                            _this.parent.localCache.addObject(jp, 'profile', objectFormat.S);
                        });
                        return true;
                    });
                }
                else {
                    return retrunFn.flatMap(function (res) {
                        // insert post in errors in cach to prevent future errors
                        var errors = res.json().post_get_multiple.err.map(function (p) {
                            p.isError = true;
                            _this.parent.localCache.addObject(p, 'post');
                        });
                        var resPosts = res.json().post_get_multiple.ok[0].posts || [];
                        // I need to get the post profiles before adding the posts in the cache from the server because
                        // api '/posts' does not provide them in the right format ;
                        return _this.parent.localCache.getProfiles(resPosts.map(function (jp) {
                            jp.isError = false;
                            _this.parent.localCache.addObject(jp, 'post');
                            jp.auhtorid = jp.auhtorid || jp.author_id; // bug format change between b2b and rive apis
                            return jp.auhtorid;
                        })).map(function () {
                            return true;
                        });
                    });
                }
            },
            getSentiment: function (ids) {
                var url = '/post/sentiment/get_from_watson';
                var message = {
                    post_ids: ids
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    var sentiments = res.json().sentiments;
                    var sentScore = { positive: 0, negative: 0, neutral: 0 };
                    sentiments.forEach(function (s) {
                        sentScore[s.label] += s.score;
                    });
                    return sentScore;
                });
            },
            setSentiment: function (id, sentiment, widgetId) {
                var _this = this;
                //*
                var url = '/post/classify';
                var message = {
                    partner_post_classification_query: {
                        post_id: id,
                        value: sentiment
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () {
                    // update local cache
                    // update local cache
                    _this.parent.localCache.addObject({ id: id, content: { sentiment_score: sentiment } }, 'post');
                    return true;
                });
                //*/
            }
        };
        this.profiles = {
            api: this.api,
            parent: this,
            /**
             *
             * @param profileId
             * @param widgetId
             * @param {boolean} ban
             * @returns {Observable<R>}
             */
            exclude: /**
                     *
                     * @param profileId
                     * @param widgetId
                     * @param {boolean} ban
                     * @returns {Observable<R>}
                     */
            function (profileId, widgetIds, ban, banFromPartner) {
                var _this = this;
                if (widgetIds === void 0) { widgetIds = undefined; }
                if (ban === void 0) { ban = true; }
                if (banFromPartner === void 0) { banFromPartner = true; }
                var url = 'widget/filters/update';
                var todos = [];
                var profileIds = typeof profileId === "string" ? [profileId] : profileId;
                if (widgetIds && widgetIds.length > 0) {
                    widgetIds.map(function (wid) {
                        var message = {
                            filter: {
                                widget_id: wid,
                                apply_to_partner: banFromPartner
                            }
                        };
                        if (ban) {
                            message.filter.filter_profiles = profileIds;
                        }
                        else {
                            message.filter.unfilter_profiles = profileIds;
                        }
                        todos.push(_this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }));
                    });
                }
                else {
                    var message = {
                        filter: {
                            apply_to_partner: true
                        }
                    };
                    if (ban) {
                        message.filter.filter_profiles = profileIds;
                    }
                    else {
                        message.filter.unfilter_profiles = profileIds;
                    }
                    todos.push(this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }));
                }
                return Observable.forkJoin.apply(Observable, todos).map(function () { return true; });
            },
            exportFilter: function (listOptions, widgetContext) {
                var _this = this;
                if (listOptions === void 0) { listOptions = {}; }
                var url = '/profiles/filter';
                var message = {
                    filter_profiles: widgetContext.getContext({ extendedOptions: listOptions, timeline: false })
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    var loadDetail = function (profilesIds) {
                        var widgetId = widgetContext && widgetContext.widgetId;
                        return _this.getMultiple(profilesIds, undefined, { widgetId: widgetId });
                    };
                    var getMore = res.json().profiles_filtered.cursor && res.json().profiles_filtered.cursor !== '0'
                        ? _this.exportFilter({ limit: listOptions.limit, cursor: res.json().profiles_filtered.cursor }, widgetContext)
                        : undefined;
                    return new ObjectsListLC(res.json().profiles_filtered.profiles, 100, loadDetail, getMore);
                });
            },
            filter: function (listOptions, widgetContext) {
                var _this = this;
                if (listOptions === void 0) { listOptions = { limit: 20 }; }
                var url = '/profiles/filter';
                var message = {
                    filter_profiles: widgetContext.getContext({ extendedOptions: listOptions, timeline: false })
                };
                // TMP: compatibility between widget_id and widget_ids:
                if (message.filter_profiles.widget_id) {
                    message.filter_profiles.widget_ids = [message.filter_profiles.widget_id];
                }
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true })
                    .flatMap(function (res) {
                    var widgetId = widgetContext && widgetContext.widgetId;
                    var i4uData = widgetContext && widgetContext.getContext().on_i4u_data;
                    return _this.getMultiple(res.json().profiles_filtered.profiles, undefined, { widgetId: widgetId, onI4U: i4uData }).map(function (resProfiles) {
                        var getMore = res.json().profiles_filtered.cursor && res.json().profiles_filtered.cursor !== '0' ?
                            _this.filter({ limit: listOptions.limit, cursor: res.json().profiles_filtered.cursor }, widgetContext).map(function (res2) { return res2.list; }) : undefined;
                        return {
                            list: new ObjectsList(resProfiles, getMore),
                            total: res.json().profiles_filtered.total
                        };
                    });
                }).catch(function (err) {
                    return Observable.throw(err);
                });
            },
            get: function (id, useCache) {
                return this.parent.localCache.getProfile(id, useCache);
            },
            getMultiple: function (ids, format, options) {
                var _this = this;
                if (!ids || ids.length === 0)
                    return Observable.from([[]]);
                // Do not use cache anymore (TODO: redo it) :
                if (format && format < objectFormat.L) {
                    // use public api
                    var url = 'public/profiles/get_multi';
                    var message = {
                        'profile_getmulti': {
                            ids: ids,
                        }
                    };
                    return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                        return res.json().profile_getmulti.ok.map(function (p) { return new Profile(p); });
                    });
                }
                else {
                    var url = '/profiles/get_multi';
                    var message = {
                        'profile_getmulti': {
                            ids: ids,
                            widget_id: options && options.widgetId,
                            on_i4u_data: options && options.onI4U
                        }
                    };
                    return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                        var profiles = res.json().profile_getmulti.ok.filter(function (p) { return !p.blocked || (options && options.showExclude); });
                        _this.parent.localCache.addObjects(res.json().profile_getmulti.labels.map(function (l) { delete l.count; return l; }), 'label');
                        var labels = _this.parent.localCache.labels;
                        return profiles.map(function (p) { return new Profile(p, labels); });
                    });
                }
                ;
            },
            listExcluded: function (widgetId) {
                var _this = this;
                var url = 'widget/filters';
                var message = {
                    filter_get: {
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (res) {
                    var profileIds = [];
                    var filters = (res.json().filter_get.filters || []);
                    filters.forEach(function (f) {
                        if (f.profiles)
                            profileIds = profileIds.concat(f.profiles);
                    });
                    if (profileIds.length > 0) {
                        var excluded_1 = [];
                        return _this.getMultiple(profileIds, objectFormat.S).map(function (profiles) {
                            filters.forEach(function (f) {
                                if (f.profiles) {
                                    f.profiles.forEach(function (pid) {
                                        var p = profiles.find(function (p) { return p.id === pid; });
                                        if (p)
                                            excluded_1.push({ widgetId: f.widget_id, profile: p });
                                    });
                                }
                            });
                            return excluded_1;
                        });
                        /*
                                                return this.parent.profiles.loadToCache(profileIds)
                                                    .flatMap(() => {
                                                        const todos = []
                                                        filters.forEach((f: any) => {
                                                            if (f.profiles) {
                                                                f.profiles.forEach((pid: string) => {
                                                                    todos.push(
                                                                        this.parent.localCache.getProfile(pid)
                                                                            .map((profile: Profile) => {
                                                                                return {widgetId: f.widget_id, profile: profile};
                                                                            })
                                                                    );
                                                                });
                                                            };
                                                        });
                        
                                                        return Observable.forkJoin(...todos);
                                                    });*/
                    }
                    else {
                        return Observable.from([[]]);
                    }
                    ;
                });
            },
            listExcludedGlobal: function () {
                var _this = this;
                var url = 'widget/filters';
                var message = {
                    filter_get: {}
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    var profileIds = [];
                    var filters = (res.json().filter_get.widget_filters || []);
                    filters.push(res.json().filter_get.partner_filter || []);
                    filters.forEach(function (f) {
                        if (f.profiles)
                            profileIds = profileIds.concat(f.profiles);
                    });
                    var loadDetail = function (ids) { return _this.getMultiple(ids, objectFormat.S); };
                    return new ObjectsListLC(tools.unduplicate(profileIds), 20, loadDetail);
                });
            },
            listEngaged: function (widgetId, filterOptions, //daterange
                //daterange
                listOptions) {
                var _this = this;
                if (widgetId === void 0) { widgetId = undefined; }
                if (listOptions === void 0) { listOptions = {}; }
                var url = 'history_log/list';
                var message = {
                    list: {
                        limit: listOptions.limit || 20,
                        cursor: listOptions.cursor,
                        widget_id: widgetId,
                        after: (filterOptions && filterOptions.dateRange && dates.toZIFromDate(filterOptions.dateRange.from)) || 1,
                        before: (filterOptions && filterOptions.dateRange && dates.toZIFromDate(filterOptions.dateRange.to)) || dates.toZIFromDate(new Date()),
                        type: 'PUBLISH'
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    var getMore = res.json().list.cursor ?
                        _this.listEngaged(widgetId, filterOptions, Object.assign(listOptions, { cursor: res.json().list.cursor })) :
                        undefined;
                    var profilesMap = new Map();
                    var lastEngagment = new Map();
                    res.json().list.profiles.forEach(function (p) {
                        profilesMap.set(p.id, p);
                    });
                    var profilesIds = tools.unduplicate(res.json().list.history_log_entries
                        .map(function (l) {
                        if (!lastEngagment.has(l.profile_id))
                            lastEngagment.set(l.profile_id, l);
                        return l.profile_id;
                    })
                        .filter(function (id) { return !!id; }));
                    var profiles = profilesIds
                        .map(function (id) { return profilesMap.get(id); }).filter(function (p) { return !!p; })
                        .map(function (p) {
                        var pr = new Profile(p);
                        var le = lastEngagment.get(p.id);
                        if (le) {
                            pr.customValue = toActionLog(le, _this.parent.localCache.profileObject(le.member_id)); //todo add act date
                        }
                        else {
                            console.log("NO ENGAGEMENT FOUND for " + p.id);
                        }
                        return pr;
                    });
                    return new ObjectsList(profiles, //res.json().list.profiles.map(p => new Profile(p)),
                    getMore, {
                        unduplicateFunction: function (p, ps) {
                            return ps.findIndex(function (op) { return op.id === p.id; }) < 0;
                        }
                    });
                    /*
                                    const profilesIds = tools.unduplicate(
                                        res.json().list.history_log_entries.filter(l => l.type === 'PUBLISH')
                                            .map(l => l.profile_id)
                                            .filter(id => !!id)
                                    );
                    
                    
                                    return this.getMultiple(profilesIds, objectFormat.S).map(profiles => {
                                        return new ObjectsList(
                                            profiles, //res.json().list.profiles.map(p => new Profile(p)),
                                            getMore,
                                            {
                                                unduplicateFunction: (p, ps) => {
                                                    return ps.findIndex(op => op.id === p.id) < 0;
                                                }
                                            });
                                    });
                                    */
                });
            },
            listInterests: function () {
                var url = '/interests/get_categories';
                var message = {};
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) { return res.json().sort(); });
            },
            /**
             * Only adds user into localCache, does not return anything
             * @param {string[]} userIds
             * @returns {Observable<boolean>}
             */
            loadToCache: /**
                     * Only adds user into localCache, does not return anything
                     * @param {string[]} userIds
                     * @returns {Observable<boolean>}
                     */
            function (userIds, format) {
                var _this = this;
                console.log('STILL USING profiles.loadToCache');
                if (userIds.length === 0) {
                    return Observable.from([true]);
                }
                if (format && format < objectFormat.L) {
                    // use publoic api
                    var url = '/profiles';
                    var message = {
                        'profile_getmulti': {
                            'ids': userIds,
                            format: 'small'
                        }
                    };
                    return this.api.call(url, message, { cache: true }).map(function (res) {
                        _this.parent.localCache.addObjects(res.json().profile_getmulti.ok.map(function (p) { p.isPublic = true; return p; }), 'profile', format);
                        return true;
                    });
                }
                else {
                    var url = '/profiles/get_multi';
                    var message = {
                        'profile_getmulti': {
                            'ids': userIds,
                        }
                    };
                    return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                        _this.parent.localCache.addObjects(res.json().profile_getmulti.labels, 'label');
                        _this.parent.localCache.addObjects(res.json().profile_getmulti.ok.map(function (p) { p.isError = false; return p; }), 'profile', objectFormat.L);
                        return true;
                    });
                }
                ;
            },
            searchAll: function (query, limit, cursor) {
                var _this = this;
                if (limit === void 0) { limit = 50; }
                var url = 'profiles/find';
                var message = {
                    profile_find: {
                        name: query,
                        limit: limit,
                        profile_format: 'small',
                        cursor: cursor
                    }
                };
                return this.api.call(url, message, { cache: true }).map(function (res) {
                    var getMore;
                    if (res.json().profile_find.cursor) {
                        getMore = _this.searchAll(query, limit, res.json().profile_find.cursor);
                    }
                    var profiles = res.json().profile_find.profiles || [];
                    return new ObjectsList(profiles
                        .filter(function (p) { return p.kind !== 'member'; })
                        .map(function (p) { return new Profile(p); }), getMore);
                });
            },
            setGender: function (ids, gender) {
                var url = '/profiles/force_gender';
                var message = {
                    "force_gender": ids.map(function (id) { return { profile_id: id, gender: gender }; })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['admin'] }).map(function () { return true; });
            }
        };
        this.tasks = {
            api: this.api,
            parent: this,
            assignToPost: function (postId, assignTo, assignmentMessage, screen, widgetId) {
                var url = '/task/assign_to_post';
                var message = {
                    assign: {
                        post_id: postId,
                        screen: screen,
                        assign_to: assignTo,
                        assignment_message: assignmentMessage,
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            assignToUser: function (userId, assignTo, assignmentMessage, screen, widgetId) {
                var url = '/task/assign_to_profile';
                var message = {
                    assign: {
                        profile_id: userId,
                        screen: screen,
                        assign_to: assignTo,
                        assignment_message: assignmentMessage,
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            close: function (taskId) {
                var url = '/task/close';
                var message = {
                    close: {
                        task_id: taskId,
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            getDetails: function (taskIds) {
                var _this = this;
                var url = 'task/get';
                var message = {
                    get: {
                        task_ids: taskIds
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (res) {
                    var jsonTasks = res.json().get;
                    if (jsonTasks.tasks.length === 0)
                        return Observable.from([]);
                    // add tasks to localCache :
                    // add tasks to localCache :
                    _this.parent.localCache.addObjects(jsonTasks.tasks, 'task');
                    // add notifications to localCache :
                    // add notifications to localCache :
                    _this.parent.localCache.addObjects(jsonTasks.notifications, 'notification');
                    // add posts to localCache:
                    jsonTasks.posts.map(function (p) { _this.parent.localCache.addObject(p, 'post'); });
                    // add members profiles to localCache :
                    // add members profiles to localCache :
                    _this.parent.localCache.addObjects(jsonTasks.profiles, 'profile', objectFormat.S);
                    return _this.parent.localCache.getTasks(jsonTasks.tasks.map(function (t) { return t.id; }));
                });
            },
            getMultiple: function (taskIds, format) {
                var _this = this;
                var jsonTasks = this.parent.localCache.getJsonObjects(taskIds, 'task');
                var profileIds = tools.unduplicate(jsonTasks.map(function (j) { return j.profile_id; })
                    .concat(jsonTasks.map(function (j) { return j.assigned_by; }))
                    .concat(jsonTasks.map(function (j) { return j.assigned_to; }))
                    .filter(function (id) { return !!id; }));
                var postIds = tools.unduplicate(jsonTasks.map(function (j) { return j.post_id; }).filter(function (id) { return !!id; }));
                /*
                             const notificationIds = tools.unduplicate(
                             jsonTasks.map((j: any) => j.notificationId).filter((id: string) => !!id)
                             )*/
                return Observable.forkJoin(this.parent.profiles.getMultiple(profileIds, format), this.parent.posts.getMultiple(postIds, format)).flatMap(function () { return (_this.parent.localCache.getTasks(taskIds, format)); });
            },
            list: function (listOptions, options) {
                var _this = this;
                if (listOptions === void 0) { listOptions = {}; }
                var url = '/task/list';
                var message = {
                    list: {
                        limit: listOptions.limit || 5,
                        cursor: listOptions.cursor,
                        after: options && options.after,
                        before: options && options.before,
                        widget_type: options && options.widgetType,
                        widget_id: options && options.widgetId,
                        assigned_to: (options && options.assignedTo),
                        hide_opened_tasks: (options && options.openedTasks === false) ? true : false,
                        hide_closed_tasks: (options && options.closedTasks === true) ? false : true,
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(function (resTask) {
                    var jsonTask = resTask.json().list;
                    if (jsonTask.tasks.length === 0)
                        return Observable.from([new ObjectsList([])]);
                    // add tasks to localCache :
                    // add tasks to localCache :
                    _this.parent.localCache.addObjects(jsonTask.tasks, 'task', objectFormat.S);
                    // add notifications to localCache :
                    // add notifications to localCache :
                    _this.parent.localCache.addObjects(jsonTask.notifications, 'notification', objectFormat.S);
                    // add posts to localCache:
                    jsonTask.posts.map(function (p) { _this.parent.localCache.addObject(p, 'post', objectFormat.S); });
                    // add members profiles to localCache :
                    // add members profiles to localCache :
                    _this.parent.localCache.addObjects(jsonTask.profiles, 'profile', objectFormat.S);
                    var getMore = jsonTask.cursor ?
                        _this.list({ limit: listOptions.limit, cursor: jsonTask.cursor }, options) : undefined;
                    /*/ get post authors (that are not sent in profiles)
                                    let usersId = jsonTask.profiles.filter(p => p.kind !== 'member').map(p => p.id);
                                    if (jsonTask.posts) usersId = usersId.concat(jsonTask.posts.map(p =>  p.authorid ));
                                    const getUsers = this.parent.profiles.getMultiple(usersId)
                                    //*/
                    // get notifications post (that are not sent with tasks)
                    var postIds = jsonTask.notifications.map(function (n) { return n.post; });
                    var getPosts = _this.parent.posts.getMultiple(postIds);
                    return Observable.forkJoin(getPosts)
                        .flatMap(function () { return _this.parent.localCache.getTasks(jsonTask.tasks.map(function (t) { return t.id; }), objectFormat.S); })
                        .map(function (res) { return new ObjectsList(res, getMore); });
                });
            },
            updateEvent: function (taskEvent) {
                var url = '';
                var message = {};
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return TaskEvent;
                });
            },
            createEvent: function (taskEvent, screen) {
                if (screen === void 0) { screen = screenTypes.UNCKNOWN; }
                var tmp = 1;
                if (tmp === (0 + 1)) {
                    taskEvent.id = Math.random().toString();
                    return Observable.of(taskEvent);
                }
                ;
                var url = '';
                var message = {
                    assign: {
                        profile_id: taskEvent.profileId,
                        screen: screen,
                        assign_to: taskEvent.assignedTo,
                        assignment_message: taskEvent.message,
                        widget_id: taskEvent.widgetId,
                        starts_at: taskEvent.start,
                        ends_at: taskEvent.end,
                        title: taskEvent.title
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                    return new TaskEvent(res.json().task);
                });
            }, listEvents: function (options) {
                // todo
                return Observable.of([]);
            }
        };
        this.view = {
            api: this.api,
            parent: this,
            create: function (view) {
                // test if parentWidget (redondant?)
                if (!view.parentWidgetId)
                    throw 'MISSING_PARENT()';
                if (view.id)
                    throw 'ALREADY_EXISTS()';
                return this.parent.widget.create(view).map(function (widget) {
                    view.id = widget.id;
                    return view;
                });
            },
            delete: function (view) {
                return this.parent.widget.delete(view);
            },
            update: function (view, originalView) {
                if (!view.parentWidgetId)
                    throw 'MISSING_PARENT()';
                if (!view.id)
                    throw 'DOES_NOT_EXISTS()';
                return this.parent.widget.update(view, originalView);
                /*
                            // if queries changed, delete and recreate the view
                            if (JSON.stringify(view.queries) !== JSON.stringify(originalView.queries)) {
                                const newView = view.clone(true);
                                return this.delete(originalView).flatMap(() => this.create(newView));
                            } else {
                                return this.parent.widget.updateExcludeTerms(view, originalView)
                                    .flatMap(() => this.parent.widget.updateWidgetInfo(view, originalView))
                                    .flatMap(() => this.parent.widget.updateExcludedProfiles(view, originalView))
                                    .map(() => view);
                            };
                            */
            }
        };
        this.wall = {
            api: this.api,
            parent: this,
            addWidget: function (widgetId, wallId) {
                var url = 'wall/widget/add';
                var message = {
                    widget_add: {
                        wall_id: wallId,
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            /**
                     *
                     * @param wall
                     * @param partnerName needed to test if name is unique in partner
                     * @returns {Observable<R>}
                     */
            create: function (wall, partnerName) {
                var _this = this;
                // test if name exists
                return this.search(wall.name, partnerName).flatMap(function (exists) {
                    if (exists)
                        return Observable.throw("NAME_EXISTS(" + wall.name + ")");
                    wall.version = 0;
                    var url = 'wall/create';
                    var message = {
                        add: wall.toJson()
                    };
                    var createWall = _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] });
                    return createWall.map(function (resW) { return new Wall(resW.json().add.wall); });
                });
            },
            delete: function (id) {
                var url = 'wall/delete';
                var message = {
                    remove: {
                        wall_id: id
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            get: function (id) {
                var url = 'wall/get';
                var message = {
                    get: {
                        wall_id: id
                    }
                };
                return this.api.call(url, message, { cache: false }).map(function (res) { return new Wall(res.json().get.wall); })
                    .catch(function (err) { return Observable.throw(err); });
            },
            list: function () {
                var url = 'wall/list';
                var message = {
                    list: {
                        limit: 101
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    return res.json().list.walls.map(function (w) {
                        return new Wall(w);
                    });
                }).catch(function (err) {
                    if (err.toString() === 'Partner has no right for this functionnality') {
                        return Observable.from([[]]);
                    }
                    else {
                        return Observable.throw(err);
                    }
                });
            },
            posts: function (wallId, dateRange, listOptions, objectListOptions) {
                var _this = this;
                if (dateRange === void 0) { dateRange = { from: new Date(2010, 1, 1), to: new Date() }; }
                if (listOptions === void 0) { listOptions = { limit: 20 }; }
                var url = 'wall/post/list';
                var message = {
                    list: {
                        wall_id: wallId,
                        after: dates.toZIFromDate(dateRange.from),
                        before: dates.toZIFromDate(dateRange.to),
                        cursor: listOptions.cursor,
                        limit: listOptions.limit
                    }
                };
                return this.api.call(url, message, { apiid: 'coldapi' }).map(function (res) {
                    var jres = res.json();
                    var getMore = jres.list.cursor ?
                        _this.posts(wallId, dateRange, { limit: listOptions.limit, cursor: jres.list.cursor }, objectListOptions) :
                        undefined;
                    return new ObjectsList(_this.parent.localCache.postList2Posts(jres.list).map(function (p) { return p.wallJsonPost; }), getMore, objectListOptions);
                });
            },
            removeWidget: function (widgetId, wallId) {
                var url = 'wall/widget/delete';
                var message = {
                    widget_remove: {
                        wall_id: wallId,
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            search: function (wallName, partnerName) {
                var _this = this;
                var url = 'wall/search';
                var message = {
                    search: {
                        wall_name: wallName,
                        partner_name: partnerName
                    }
                };
                return this.api.call(url, message, { apiid: 'coldapi' }).catch(function (err) {
                    if (err.toString() === 'Cannot get widget: ERR_NOT_FOUND()') {
                        return Observable.from([false]);
                    } /*else {
                                     return Observable.throw(err);
                                     }*/
                }).map(function (res) {
                    if (res) {
                        var w = new Wall(res.json().search.wall);
                        // ad pinned posts to localcahce
                        w.pinnedPosts.forEach(function (p) {
                            var myProfile = {
                                id: p.user.id,
                                screen_name: p.user.screenName,
                                thumbnail: p.user.thumbnail,
                                deletable: false
                            };
                            var myPost = {
                                id: p.id,
                                authorid: myProfile.id,
                                content: {
                                    text: p.text,
                                    thumbnail: p.thumbnail,
                                    medias: [p.thumbnail],
                                    title: p.title,
                                    deletable: false
                                },
                                position: {}
                            };
                            _this.parent.localCache.addObjectWithFormat(myProfile, 'profile', objectFormat.S);
                            _this.parent.localCache.addObjectWithFormat(myPost, 'post', objectFormat.S);
                        });
                        return w;
                    }
                });
            },
            /**
                     *
                     * @param {Wall} wall
                     * @returns {Observable<R>}
                     */
            update: function (wall) {
                var _this = this;
                wall.version = (wall.version || 0) + 1;
                //todo check name
                var url = 'wall/update';
                var message = {
                    update: wall.toJson()
                };
                return this.get(wall.id).flatMap(function (w) {
                    if (w.version >= wall.version) {
                        return Observable.throw('WALL_OBSOLETE()');
                    }
                    else {
                        return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
                    }
                });
            },
            nbActions: function (wallId, dtFrom, dtTo) {
                var url = 'wall/total_count';
                var message = {
                    total_count: {
                        wall_id: wallId,
                        after: dtFrom || 1,
                        before: dtTo || dates.toZIFromDate(new Date())
                    }
                };
                return this.api.call(url, message).map(function (res) {
                    return {
                        likes: res.json().total_count.count_likes,
                        comments: res.json().total_count.count_comments,
                        shares: res.json().total_count.count_shares
                    };
                });
            },
            // //////////// ANALUYTICS ////////////////
            nbPosts: function (wallId, dtFrom, dtTo, options) {
                var url = 'wall/agg_post';
                var message = {
                    agg_post: {
                        wall_id: wallId,
                        after: dtFrom || 1,
                        before: dtTo || dates.toZIFromDate(new Date())
                    }
                };
                return this.api.call(url, message, { cache: tools.deepGet(options, 'cache') || true }).map(function (res) { return res.json().agg_post.count; });
            },
            topTags: function (wallId, nb, dtFrom, dtTo) {
                if (nb === void 0) { nb = 20; }
                var url = '/wall/top_tags';
                var message = {
                    top_tags: {
                        wall_id: wallId,
                        after: dtFrom,
                        before: dtTo,
                        limit: nb
                    }
                };
                return this.api.call(url, message).map(function (res) {
                    return res.json().top_tags.tags || [];
                });
            },
            topUsers: function (wallId, nb, dtFrom, dtTo) {
                var _this = this;
                if (nb === void 0) { nb = 20; }
                var url = '/wall/top_users';
                var message = {
                    top_users: {
                        wall_id: wallId,
                        after: dtFrom || 1,
                        before: dtTo || dates.toZIFromDate(new Date()),
                        limit: nb
                    }
                };
                return this.api.call(url, message).flatMap(function (resTop) {
                    var userMap = new Map();
                    var ids = resTop.json().top_users.users.map(function (u) {
                        userMap.set(u.payload, u.total);
                        return u.payload;
                    });
                    if (ids.length === 0)
                        return Observable.from([[]]);
                    var urlp = '/profiles';
                    var messagep = {
                        'profile_getmulti': {
                            'ids': ids,
                            format: 'small'
                        }
                    };
                    return _this.api.call(urlp, messagep).map(function (res) {
                        return res.json().profile_getmulti.ok.map(function (p) {
                            p.total_published = userMap.get(p.id);
                            return new Profile(p);
                        });
                    });
                    // return this.parent.profiles.getMultiple(ids, true);
                });
            }
        };
        this.widget = {
            api: this.api,
            parent: this,
            addPublishAction: function (publishAction) {
                console.log('DEPRECATED: addPublishAction');
                return Observable.from([false]);
            },
            create: function (widget, isI4U) {
                if (widget.id)
                    return Observable.throw('WIDGET_ALREADY_EXISTS()');
                if (!widget.name || widget.name.length === 0)
                    return Observable.throw('MISSING_FIELDS(name)');
                var url = !isI4U ? '/widget/create' : '/i4u/binfluence/widget_create';
                var message = {
                    create: {
                        view: widget.view,
                        metas: widget.metas,
                        category: widget.category,
                        name: widget.name,
                        type: widget.type,
                        user_daily_threshold: widget.userDailyThreshold,
                        influencer_threshold: widget.influencerThreshold,
                        customer_lead_hreshold: widget.customerLeadThreshold,
                        loyalty_threshold: widget.loyaltyNbThreshold ?
                            [widget.loyaltyNbThreshold, widget.loyaltyDaysThreshold] : undefined,
                        widget_parent: widget.parentWidgetId,
                        filters: {
                            profiles: widget.excludedProfiles || [],
                            places: widget.excludedPlaces || []
                        },
                        without_geo_missions: widget.noMission
                    }
                };
                if (widget.options) {
                    Object.assign(message.create, widget.options);
                }
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] })
                    .map(function (res) {
                    //widget.id =  res.json().create.widget_id; return widget;
                    return new Widget(res.json().create.widget);
                });
            },
            delete: function (widget) {
                var widgetId = widget.id;
                if (!widgetId)
                    return Observable.throw('WIDGET_DOSE_NOT_EXIST()');
                var url = '/widget/delete';
                var message = {
                    'delete': {
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) { return widgetId; });
            },
            deletePublishAction: function (publishAction) {
                console.log('DEPRECATED: deletePublishAction');
                return Observable.from([false]);
            },
            get: function (wid) {
                var todos = [];
                // generic info
                var page = '/widget/get';
                var message = {
                    get: {
                        widget_id: wid
                    }
                };
                todos.push(this.api.call(page, message, { apiid: 'b2bapi' }));
                todos.push(this.getExcluded(wid));
                return Observable.forkJoin(todos).map(function (res) {
                    var w = new Widget(res[0].json().get.widget);
                    if (w.parentWidgetId)
                        w = new View(res[0].json().get.widget);
                    w.excludedProfiles = res[1].profiles || [];
                    w.excludedPlaces = res[1].places || [];
                    return w;
                });
            },
            filterShares: function (shares, widgetId) {
                var _this = this;
                var url = '/widget/filters/update';
                return this.getExcluded(widgetId).flatMap(function (res) {
                    var message = {
                        filter: {
                            filter_profiles: res.profiles,
                            filter_places: res.places,
                            filter_shares: !shares
                        }
                    };
                    return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                        return true;
                    });
                });
            },
            getExcluded: function (widgetId) {
                var url = 'widget/filters';
                var message = {
                    filter_get: {
                        widget_id: widgetId
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    return (res.json().filter_get.filters || res.json().filter_get.widget_filters)[0] || { places: [], profiles: [] };
                }).catch(function (err) {
                    return Observable.of([]);
                });
            },
            list: function (limit) {
                var _this = this;
                var url = '/widget/list';
                var message = {
                    list: { limit: limit }
                };
                // TODO: manage the 101 widgets limit
                return this.api.call(url, message, { apiid: 'b2bapi' }).map(function (res) {
                    var allWidgets = res.json().list.widgets.map(function (widget) { return new Widget(widget); });
                    // split views from widget
                    var widgets = new Map();
                    var views = [];
                    allWidgets.forEach(function (w) {
                        if (w.parentWidgetId) {
                            views.push(new View(w.jsonObject));
                        }
                        else {
                            widgets.set(w.id, w);
                        }
                    });
                    views.forEach(function (w) {
                        // kill orphan views :
                        if (!widgets.get(w.parentWidgetId)) {
                            console.log("DELETING_ORPHAN_VIEW(" + w.name + ", " + w.id + ")");
                            _this.delete(w).subscribe();
                            return;
                        }
                        widgets.get(w.parentWidgetId).widgetViews.push(w);
                    });
                    return Array.from(widgets.values());
                });
            },
            listProfiles: function (widget) {
                if (!widget.isProfileListWidget)
                    return Observable.of([]);
                var labelId = widget.labelId;
                return this.parent.labels.profilesList(labelId, { limit: 200 }, { simpleProfiles: true }).map(function (res) {
                    return res.list;
                });
            },
            pause: function (widget, pause) {
                var _this = this;
                // generic info
                return this.get(widget.id)
                    .flatMap(function (widget2) {
                    var pauseDt = !pause ? 0 : (dates.toZIFromDate(new Date()) - 1);
                    var todos = [];
                    /*if (widget.widgetViews.length > 0) {
                                        todos = widget.widgetViews.map((v: View) =>  {
                                          v.endsAt = pauseDt;
                                          return this.updateView(v);
                                        });
                                      }*/
                    widget.endsAt = widget2.endsAt = pauseDt;
                    todos.push(_this.updateView(widget2));
                    return tools.forkJoin2FlatMap(todos).map(function (res) { return widget2; });
                });
            },
            preview: function (widget, fortest) {
                var _this = this;
                if ((!widget.view.zone || JSON.stringify(widget.view.zone) === JSON.stringify({ n: 0, e: 0, s: 0, w: 0 }))
                    && widget.tags.length === 0) {
                    return Observable.throw('WRONG_FORMAT()');
                }
                var url = '/widget/preview';
                var message = {
                    preview: {
                        view: widget.view
                    }
                };
                var flatMap = function (res) {
                    var jsonRes = res.json().preview;
                    _this.parent.localCache.addObjects(jsonRes.posts, 'post');
                    _this.parent.localCache.addObjects(jsonRes.profiles, 'profile');
                    return _this.parent.localCache.getPosts(jsonRes.posts.map(function (p) { return p.id; }))
                        .map(function (posts) {
                        return { ok: true, total: jsonRes.total, posts: posts };
                    });
                };
                return this.api.call(url, message, { apiid: 'b2bapi' }).flatMap(flatMap);
            },
            update: function (widget, originalWidget) {
                var _this = this;
                if (!widget.id)
                    return Observable.throw('WIDGET_DOSE_NOT_EXIST()');
                if (!widget.name || widget.name.length < 3)
                    return Observable.throw('INCORRECT_FORMAT(name)');
                return this.updateAlerts(widget, originalWidget)
                    .flatMap(function () { return _this.updateExcludeTerms(widget, originalWidget); })
                    .flatMap(function () { return _this.updateWidgetInfo(widget, originalWidget); })
                    .flatMap(function () { return _this.updateExcludedProfiles(widget, originalWidget); })
                    .flatMap(function () { return _this.updateView(widget, originalWidget); })
                    .map(function () { return widget; });
            },
            updateCategory: function (widgetId, category) {
                var url = 'widget/update_category';
                var message = {
                    update: {
                        id: widgetId,
                        new_category: category
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            updateExcludedProfiles: function (widget, originalWidget) {
                var _this = this;
                var url = 'widget/filters/update';
                var message = {
                    filter: {
                        widget_id: widget.id
                    }
                };
                // I need to find witch to exclude or reinclude
                return this.getExcluded(widget.id).flatMap(function (excludedIds) {
                    if ((widget.excludedPlaces || []).sort().toString() === (excludedIds.places || []).sort().toString()
                        && (widget.excludedProfiles || []).sort().toString() === (excludedIds.profiles || []).sort().toString())
                        return Observable.from([true]);
                    var placesDelta = tools.arrayDelta(excludedIds.places, widget.excludedPlaces);
                    var profilesDelta = tools.arrayDelta(excludedIds.profiles, widget.excludedProfiles);
                    message.filter.unfilter_places = placesDelta[0];
                    message.filter.filter_places = placesDelta[2];
                    message.filter.unfilter_profiles = profilesDelta[0];
                    message.filter.filter_profiles = profilesDelta[2];
                    return _this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
                });
            },
            updateExcludeTerms: function (widget, originalWidget) {
                var url = 'widget/update_exclude_terms';
                var message = function (w) {
                    if (!w)
                        return;
                    return {
                        update_exclude_terms: {
                            id: w.id,
                            exclude_terms: w.minusTags
                        }
                    };
                };
                if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                    return Observable.from([true]);
                }
                ;
                return this.api.call(url, message(widget), { apiid: 'b2bapi', rights: ['write'] }).map(function () { return true; });
            },
            updateAlerts: function (widget, originalWidget) {
                var url = 'widget/update_alert';
                var message = function (w) {
                    if (!w)
                        return;
                    return {
                        update_alert: {
                            id: widget.id,
                            user_daily_threshold: w.userDailyThreshold,
                            influencer_threshold: w.influencerThreshold,
                            customer_lead_threshold: w.customerLeadThreshold,
                            loyalty_threshold: w.loyaltyNbThreshold ? [w.loyaltyNbThreshold, w.loyaltyDaysThreshold] : null
                        }
                    };
                };
                if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                    return Observable.from([true]);
                }
                ;
                return this.api.call(url, message(widget), { apiid: 'b2bapi', rights: ['write'] })
                    .map(function () { return true; })
                    .catch(function (err) {
                    if (err.toString() === 'Nothing to update') {
                        return Observable.from([true]);
                    }
                    else {
                        return Observable.throw(err);
                    }
                });
            },
            updatePublishActions: function (widget, originalWidget) {
                console.log('DEPRECTAED: updatePublishActions');
                return Observable.from([false]);
            },
            updateView: function (widget, originalWidget) {
                if ((originalWidget && JSON.stringify(widget.view) === JSON.stringify(originalWidget.view))
                    &&
                        (originalWidget && !!originalWidget.noMission === !!widget.noMission))
                    return Observable.from([widget]);
                var url = 'widget/update_view';
                var message = {
                    update_view: {
                        widget_id: widget.id,
                        view: widget.view,
                        without_geo_missions: widget.noMission
                    }
                };
                return this.api.call(url, message, { apiid: 'b2bapi', rights: ['write'] }).map(function () { return widget; });
            },
            updateWidgetInfo: function (widget, originalWidget) {
                var url = 'widget/update';
                var message = function (w) {
                    if (!w)
                        return;
                    return {
                        update: {
                            id: w.id,
                            metas: w.metas,
                            // category: widget.category, // we cannot update category or type
                            name: w.name,
                        }
                    };
                };
                if (JSON.stringify(message(widget)) === JSON.stringify(message(originalWidget))) {
                    return Observable.from([true]);
                }
                ;
                return this.api.call(url, message(widget), { apiid: 'b2bapi', rights: ['write'] })
                    .map(function () { return true; })
                    .catch(function (err) {
                    if (err.toString() === 'Nothing to update') {
                        return Observable.from([true]);
                    }
                    else {
                        return Observable.throw(err);
                    }
                });
            }
        };
        this.widgetAnalytics = {
            parent: this,
            api: this.api,
            /*customerLead: function(widgetContext: WidgetContext, limit?: number): Observable<ObjectsList<Profile>> {
                        // tmp : utiliser les notifications en attendant
                        const options = {
                            widgetId: widgetContext.widgetId,
                            type: alertTypes.CUSTOMER_LEAD,
                            after: widgetContext.after,
                            before: widgetContext.before,
                        };
            
                        const parent = this.parent;
            
                        function getUsers(notList) {
            
                            const getMore = notList.getMoreObservable ? notList.getMoreObservable.flatMap(getUsers) : undefined;
                            return parent.profiles.getMultiple(notList.list.map(n => n.user.id)).map(users => {
                                return new ObjectsList<Profile>(users, getMore);
                            });
                        }
                        return this.parent.notifications.list({limit: limit}, options).flatMap(getUsers);
                    },*/
            countByProfile: function (widgetContext, limit, cursor) {
                var _this = this;
                var url = '/influence/count_by_profile';
                var message = {
                    count_by_profile: widgetContext.getContext({ extendedOptions: { limit: limit || 20, cursor: cursor } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).flatMap(function (res) {
                    var resAnalytics = res.json().counts;
                    var getMore = res.json().cursor ?
                        _this.countByProfile(widgetContext, limit, res.json().cursor) : undefined;
                    return _this.parent.profiles.getMultiple(resAnalytics.map(function (r) { return r.profile_id; })).map(function (profiles) {
                        var profilesWithAnalytis = resAnalytics.map(function (a) {
                            a.profile = profiles.find(function (p) { return p.id === a.profile_id; });
                            return a;
                        }).filter(function (a) { return !!a.profile; });
                        return new ObjectsList(profilesWithAnalytis, getMore);
                    });
                });
            },
            customerLead: function (widgetContext, limit) {
                var WC = widgetContext.clone();
                WC.setFilterRule('order_by', '-leadscore', { dates: false, widgets: false, silent: true });
                return this.parent.profiles.filter({
                    limit: limit
                }, WC).map(function (res) { return res.list; });
            },
            distinctReach: function (widgetContext) {
                var url = '/influence/sum_followers';
                var message = {
                    sum_followers: widgetContext.getContext(),
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) { return res.json().total; });
            },
            engagements: function (widgetContext) {
                var url = '/influence/sum_engagements';
                var message = {
                    sum_engagements: widgetContext.getContext(),
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) { return res.json(); });
            },
            engagementsTimeline: function (widgetContext) {
                var url = '/analytics/hist_engagements';
                var message = {
                    hist_engagements: widgetContext.getContext(),
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().hist_engagements.sources;
                });
            },
            genderSoV: function (widgetContext) {
                var url = '/analytics/agg_sov_gender';
                var message = {
                    agg_sov_gender: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return (res.json().agg_sov_gender.count || []).map(function (s) {
                        return {
                            name: s.payload,
                            count: s.total
                        };
                    });
                });
            },
            influencers: function (widgetContext, limit) {
                var WC = widgetContext.clone();
                WC.setFilterRule('order_by', '-follower', { dates: false, widgets: false, silent: true });
                return this.parent.profiles.filter({
                    limit: limit
                }, WC).map(function (res) { return res.list; });
            },
            insterestsSoV: function (widgetContext) {
                var url = '/analytics/agg_sov_interests';
                var message = {
                    agg_sov_interests: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return (res.json().agg_sov_interests.count || []).map(function (s) {
                        return {
                            name: s.payload,
                            count: s.total
                        };
                    });
                });
            },
            lexicalsCount: function (widgetContext, filterOptions) {
                var url = '/analytics/agg_lexical';
                var message = {
                    lexical_query: widgetContext.getContext({ extendedOptions: filterOptions })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().lexical_analytics;
                });
            },
            lexicalsTimeline: function (widgetContext, filterOptions) {
                var url = '/analytics/hist_lexical';
                var message = {
                    lexical_query: widgetContext.getContext({ extendedOptions: filterOptions })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().lexical_historics;
                });
            },
            nbPosts: function (widgetContext) {
                var url = '/analytics/agg_post';
                var message = {
                    agg_post: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().agg_post.count;
                });
            },
            payloadToUsers: function (payloads, scoreName) {
                if (scoreName === void 0) { scoreName = 'score'; }
                if (payloads.length === 0)
                    return Observable.from([]);
                var usersScores = {};
                var usersId = payloads.map(function (tu) { usersScores[tu.payload] = tu.total; return tu.payload; });
                return this.parent.profiles.getMultiple(usersId.slice(0, 50)).map(function (res) { return res.map(function (p) { p[scoreName] = usersScores[p.id]; return p; }); });
            },
            postsTimeline: function (widgetContext) {
                var url = '/analytics/hist_post';
                var message = {
                    hist_post: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().hist_post.data || [];
                });
            },
            profileNbPosts: function (profileId, widgetContext) {
                var url = '/analytics/profile_count_posts';
                var message = {
                    profile_count_posts: widgetContext.getContext({ extendedOptions: { profile_id: profileId } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().count_by_widget || [];
                });
            },
            reach: function (widgetContext) {
                var url = 'analytics/agg_reach';
                var message = {
                    agg_reach: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return (res.json().agg_reach.sources || []).reduce(function (a, b) { return a + b.total; }, 0);
                });
            },
            reachTimeline: function (widgetContext) {
                var url = '/analytics/hist_reach';
                var message = {
                    hist_reach: widgetContext.getContext(),
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().hist_reach.sources;
                });
            },
            sentiment: function (widgetContext) {
                var url = '/analytics/agg_sentiment_analysis';
                var message = {
                    agg_sentiment_analysis: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    var sentiments = {};
                    for (var _i = 0, _a = res.json().agg_sentiment_analysis.count; _i < _a.length; _i++) {
                        var s = _a[_i];
                        sentiments[s.payload] = s.total;
                    }
                    ;
                    return sentiments;
                });
            },
            shareOfVoice: function (widgetContext) {
                var url = '/analytics/agg_share_of_voice';
                var message = {
                    agg_share_of_voice: widgetContext.getContext()
                };
                return _this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return (res.json().agg_share_of_voice.count || []).map(function (s) {
                        return {
                            id: s.payload,
                            count: s.total
                        };
                    });
                });
            },
            sourcesTimeline: function (widgetContext) {
                var url = '/analytics/hist_sources';
                var message = {
                    hist_sources: widgetContext.getContext()
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().hist_sources.sources;
                });
            },
            tagsTimeline: function (widgetContext, tags, limit) {
                var url = '/analytics/hist_tags';
                var message = {
                    hist_tags: widgetContext.getContext({ extendedOptions: { limit: limit, tags: tags } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().hist_tags.tags;
                });
            },
            topLoyalty: function (widgetContext, limit) {
                var _this = this;
                // there is no cursor, so we query the api with a big limit,
                // the limit is used for the detail (through the ObjectListLC)
                var getDetail = (function (payloads) { return _this.payloadToUsers(payloads, 'nbVisits'); }).bind(this);
                var url = '/analytics/top_loyalty';
                var message = {
                    top_loyalty: widgetContext.getContext({ extendedOptions: { limit: 50 } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return new ObjectsListLC(res.json().top_loyalty.loyalty, limit, getDetail);
                });
            },
            topPosts: function (widgetContext, optionsList) {
                var _this = this;
                if (optionsList === void 0) { optionsList = { limit: 5 }; }
                var url = '/widget/top_posts';
                var message = {
                    top_posts: widgetContext.getContext({
                        extendedOptions: { limit: optionsList.limit, cursor: optionsList.cursor }
                    })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).flatMap(function (res) {
                    var getMore = res.json().top_posts.cursor ?
                        _this.topPosts(widgetContext, { limit: optionsList.limit, cursor: res.json().top_posts.cursor }) : undefined;
                    var myPosts = res.json().top_posts.posts || [];
                    // add posts to localCache :
                    //this.parent.localCache.addObjects(myPosts, 'post')
                    // add posts to localCache :
                    //this.parent.localCache.addObjects(myPosts, 'post')
                    _this.parent.localCache.addPostList(res.json().top_posts);
                    // get profiles detail :
                    return _this.parent.profiles.getMultiple(myPosts.map(function (p) { return p.authorid; }))
                        .flatMap(function () { return _this.parent.localCache.getPosts(myPosts.map(function (p) { return p.id; })); })
                        .map(function (posts) { return new ObjectsList(posts, getMore); });
                });
            },
            topSources: function (widgetContext, limit) {
                var url = '/analytics/top_sources';
                var message = {
                    top_sources: widgetContext.getContext({ extendedOptions: { limit: limit } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().top_sources.sources;
                });
            },
            topTags: function (widgetContext, limit) {
                var url = '/analytics/top_tags';
                var message = {
                    top_tags: widgetContext.getContext({ extendedOptions: { limit: limit } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true }).map(function (res) {
                    return res.json().top_tags.tags || [];
                });
            },
            topUsers: function (widgetContext, listOptions) {
                var _this = this;
                // there is no cursor, so we query the api with a big limit, the limit is used for the detail (through the ObjectListLC)
                var getDetail = (function (payloads) { return _this.payloadToUsers(payloads, 'nbPosts'); }).bind(this);
                var url = '/analytics/top_users';
                var message = {
                    top_users: widgetContext.getContext({ extendedOptions: { limit: (listOptions && listOptions.limit) || 10 } })
                };
                return this.api.call(url, message, { apiid: 'b2bapi', cache: true })
                    .flatMap(function (res) {
                    var widgetId = widgetContext.widget && widgetContext.widget.id;
                    return _this.payloadToUsers(res.json().top_users.users, 'nbPosts').map(function (resProfiles) {
                        var getMore = res.json().top_users.cursor && res.json().top_users.cursor !== '0' ?
                            _this.topUsers(widgetContext, { limit: listOptions.limit, cursor: res.json().top_users.cursor }).map(function (res2) { return res2.list; }) : undefined;
                        return new ObjectsList(resProfiles, getMore);
                    });
                });
            }
        };
        if (config) {
            if (typeof config === 'function') {
                config = config();
            }
            this.oauthKeys = config.oauth;
        }
        this._config = config;
    }
    ZupApiService.prototype.getConfig = function (key) {
        if (!this._config)
            return null;
        if (!key)
            return this._config;
        return this._config[key];
    };
    ZupApiService.prototype.getNextPageFunction = function (maxResults, maxQueries) {
        if (maxResults === void 0) { maxResults = Infinity; }
        if (maxQueries === void 0) { maxQueries = 20; }
        var cpt = 0;
        var getNextPage = function (res) {
            if (res.hasMore && res.length < maxResults && cpt++ < maxQueries) {
                return res.getMore().flatMap(function (resTmp) { return getNextPage(res); });
            }
            else {
                return Observable.from([res]);
            }
        };
        return getNextPage;
    };
    ZupApiService.prototype.loadFiles = function (files) {
        if (!files || files.length === 0)
            return Observable.of([]);
        var mediaUrl = '/tokens/medias/add';
        var media = files.filter(function (f) { return !!f.id; });
        var mediaFiles = files.filter(function (f) { return !f.id; }).map(function (f) { return f.url.substring(f.url.indexOf(',') + 1); });
        if (mediaFiles.length > 0) {
            var mediaMessage = {
                add: {
                    medias: mediaFiles
                }
            };
            return this.api.call(mediaUrl, mediaMessage, { apiid: 'b2bapi', rights: ['write'] }).map(function (res) {
                return media.concat(res.json().urls.map(function (u, i) { return { id: res.json().media_ids[i], url: u }; }));
            });
        }
        else {
            return Observable.of(files);
        }
    };
    ZupApiService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ZupApiService.ctorParameters = function () { return [
        { type: ApiAuthService, },
        { type: ApiConfig, decorators: [{ type: Optional },] },
    ]; };
    return ZupApiService;
}());
export { ZupApiService };
