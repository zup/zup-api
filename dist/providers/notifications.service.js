import { Injectable, EventEmitter } from '@angular/core';
import { ZupApiService } from './zupApi.service';
// need to be access directly or is not instanced :
import { PartnerService } from './partner.service';
import { Notification, ObjectsList } from '../models';
import { Observable } from 'rxjs'; // is required for typescript (even if not explicitly used)
var NotificationsService = /** @class */ (function () {
    function NotificationsService(zupApi, partner) {
        var _this = this;
        this.zupApi = zupApi;
        this.partner = partner;
        this.countChange = new EventEmitter();
        this.maxNot = 99;
        partner.onInit().subscribe(function () { return _this._countNotifications(); });
    }
    /**
     *
     * @param count can be a number or an object
     * @returns
     */
    /**
       *
       * @param count can be a number or an object
       * @returns
       */
    NotificationsService.prototype.toMax = /**
       *
       * @param count can be a number or an object
       * @returns
       */
    function (count) {
        if (typeof count === 'number') {
            if (count > this.maxNot) {
                return this.maxNot + "+";
            }
        }
        else {
            var returnObject = {};
            for (var p in count) {
                returnObject[p] = this.toMax(count[p]);
            }
            return returnObject;
        }
    };
    Object.defineProperty(NotificationsService.prototype, "count", {
        get: function () {
            if (!this.counters)
                return;
            return this.toMax(this.counters.reduce(function (previous, current) {
                return previous + current.count;
            }, 0));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NotificationsService.prototype, "countByWidget", {
        get: function () {
            if (!this.counters)
                return {};
            var alerts = {};
            this.counters.forEach(function (c) {
                var wid = c.widget_id;
                alerts[wid] = (alerts[wid] || 0) + c.count;
            });
            return this.toMax(alerts);
        },
        enumerable: true,
        configurable: true
    });
    NotificationsService.prototype.countByWidgetAndType = function (type) {
        if (!this.counters)
            return {};
        var alerts = {};
        this.counters.filter(function (c) { return c.notification_type === type; }).forEach(function (c) {
            var wid = c.widget_id;
            alerts[wid] = (alerts[wid] || 0) + c.count;
        });
        return this.toMax(alerts);
    };
    NotificationsService.prototype.assign = function (notif, memberId, message, options) {
        var _this = this;
        return this.zupApi.notifications.assign(notif, memberId, message, options).map(function () {
            notif.isClosed = true;
            _this._countNotifications();
        });
    };
    NotificationsService.prototype.close = function (id) {
        var _this = this;
        return this.zupApi.notifications.close(id).map(function () {
            _this._countNotifications();
        });
    };
    ;
    NotificationsService.prototype.countByType = function (widgetId, toMax) {
        if (!this.counters)
            return {};
        var types = {};
        this.counters.forEach(function (c) {
            if (widgetId && c.widget_id !== widgetId)
                return;
            var t = c.notification_type;
            types[t] = (types[t] || 0) + c.count;
        });
        return types;
    };
    NotificationsService.prototype.countByFilter = function (filter, toMax) {
        var _this = this;
        var sum = 0;
        if (!this.counters)
            return toMax ? this.toMax(sum) : sum;
        var types = {};
        this.counters.filter(function (c) {
            var w = _this.partner.getWidget(c.widget_id);
            if (!w)
                return false;
            if (filter.widgetId && c.widget_id !== filter.widgetId)
                return false;
            if (filter.widgetType && w.type !== filter.widgetType)
                return false;
            if (filter.alertType && c.notification_type !== filter.alertType)
                return false;
            return true;
        }).forEach(function (c) {
            sum += c.count;
        });
        return toMax ? this.toMax(sum) : sum;
    };
    NotificationsService.prototype.getNotifications = function (limit, options) {
        var _this = this;
        return this.zupApi.notifications.list({ limit: limit }, options).map(function (res) {
            _this._list = res;
            return _this._list;
        });
    };
    NotificationsService.prototype._getNotifications = function () {
        var _this = this;
        this.zupApi.notifications.list({}).subscribe(function (res) {
            _this._list = res;
        });
        this._countNotifications();
    };
    ;
    NotificationsService.prototype._countNotifications = function () {
        var _this = this;
        this.zupApi.notifications.count().subscribe(function (res) {
            _this.counters = res;
            var c = _this.count;
            if (_this.count) {
                _this.countChange.emit(_this.count.toString());
            }
            else {
                _this.countChange.emit(undefined);
            }
        });
    };
    NotificationsService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    NotificationsService.ctorParameters = function () { return [
        { type: ZupApiService, },
        { type: PartnerService, },
    ]; };
    return NotificationsService;
}());
export { NotificationsService };
