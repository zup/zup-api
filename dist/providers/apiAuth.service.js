import { Injectable, Optional, EventEmitter } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable, Subscriber } from 'rxjs';
import { ApiConfig } from './api.config';
var sha256 = require('crypto-js/sha256');
var hash = function (s) { return sha256(s).toString().substring(0, 64).toLowerCase(); };
var ɵ0 = hash;
var ApiAuthService = /** @class */ (function () {
    function ApiAuthService(http, config) {
        this.http = http;
        this.RO = false;
        this.isAdmin = false;
        this.apiUrls = new ApiConfig().apiUrls;
        this.hasConnection = true;
        this.onLogin = new EventEmitter();
        this.cache = new APICache();
        if (config) {
            if (typeof config === 'function') {
                config = config();
            }
            this.apiUrls = config.apiUrls;
        }
        this.urlParams = window.location.search.substring(1);
    }
    ApiAuthService.prototype.login = function (username, password, rememberMe) {
        var _this = this;
        // const trackerId = getCookie('ruuid') || undefined;
        //const protocol = window.location.protocol || 'https';
        var url = "https://" + this.apiUrls.coldapi + "/sessions/create";
        var message = {
            session_create: {
                device: 'device_web',
                local: navigator.language,
                // uuid: trackerId,
                login: username
            }
        };
        var observable = this.http.post(url, JSON.stringify(message)).map(function (res) {
            var body = res.json();
            _this.sessionId = body.session_create.session_id;
            _this.sessionSalt = body.session_create.session_salt;
            _this.userId = body.session_create.member_id || undefined;
            _this.userSalt = body.session_create.member_salt || undefined;
            _this.userPK = _this.userId ? hash(password + _this.userSalt) : undefined;
            if (rememberMe === true && _this.userId) {
                // currently, only for the session (see how to use jwt for storing loginInformations on computer)
                var loginInformations = { rive: { login: username, userkey: _this.userPK } };
                //document.cookie = `currentLogin=${JSON.stringify(loginInformations) || ''};path=/;expires=${new Date(new Date().valueOf() + 30 * 24 * 3600 * 1000)}`;
                document.cookie = "currentLogin=" + (JSON.stringify(loginInformations) || '') + ";path=/;";
            }
            _this.loginInformation = { rive: { login: username, userkey: _this.userPK } };
            return res;
        }).flatMap(function (res) {
            var urlLogin = '/sessions/login';
            var messageLogin = {
                session_login: {
                    device: 'device_web',
                    session_id: _this.sessionId,
                    sign: hash(_this.sessionSalt + (_this.userPK || ''))
                }
            };
            return _this.call(urlLogin, messageLogin, { bypassRetry: true }).map(function (resLogin) {
                var jres = resLogin.json();
                _this.apiStatus = 'ONLINE';
                return { id: _this.userId, partnerId: jres.partner_id };
            });
        }).catch(function (error) {
            if (error.json) {
                error = new InError(error.json().reason, error.json());
            }
            return Observable.throw(error);
        });
        return observable;
    };
    ApiAuthService.prototype.logout = function () {
        return this.login();
    };
    ApiAuthService.prototype.autoLog = function (loginInformation) {
        var _this = this;
        if (!loginInformation.hasOwnProperty('rive'))
            return Observable.throw('UNEXPECTED_FORMAT()');
        var url = "https://" + this.apiUrls.coldapi + "/sessions/create";
        var message = {
            session_create: {
                device: 'device_web',
                local: navigator.language,
                // uuid: trackerId,
                login: loginInformation.rive.login
            }
        };
        var observable = this.http.post(url, JSON.stringify(message)).map(function (res) {
            var body = res.json();
            _this.sessionId = body.session_create.session_id;
            _this.sessionSalt = body.session_create.session_salt;
            _this.userId = body.session_create.member_id || undefined;
            _this.userSalt = body.session_create.member_salt || undefined;
            _this.userPK = loginInformation.rive.userkey;
            return res;
        }).flatMap(function (res) {
            var urlLogin = '/sessions/login';
            var messageLogin = {
                session_login: {
                    device: 'device_web',
                    session_id: _this.sessionId,
                    sign: hash(_this.sessionSalt + _this.userPK)
                }
            };
            return _this.call(urlLogin, messageLogin, { bypassRetry: true }).map(function (resLogin) {
                var jres = resLogin.json();
                _this.apiStatus = 'ONLINE';
                _this.loginInformation = loginInformation;
                return { id: _this.userId, partnerId: jres.partner_id };
            });
        }).catch(function (error) {
            if (error.json) {
                error = new InError(error.json().reason, error.json());
            }
            /*if (error.toString() === 'MSG_BAD_AUTH()') {
                            //this.router.navigate(['/login']);
                            return Observable.throw(error)
                            // return Observable.empty(); // prevent triggering the error
                        }*/
            return Observable.throw(error);
        });
        return observable;
    };
    ApiAuthService.prototype.relog = function () {
        var _this = this;
        if (this.hasConnection) {
            this.autoLog(this.loginInformation).subscribe(function () {
                _this.onLogin.emit(true);
            });
        }
    };
    ApiAuthService.prototype.getEndpointUrl = function (url, message, options) {
        if (options && options.session === false)
            return url;
        var urlParameters = options && options.urlParameters;
        var sign = hash(this.sessionSalt + (this.userPK || '') + message);
        var params = urlParameters ? '&'.concat(urlParameters) : '';
        return url + "?auth=" + this.sessionId + "/" + sign + params + "&" + this.urlParams;
    };
    ApiAuthService.prototype.call = function (url, message, options) {
        var _this = this;
        if (message === void 0) { message = {}; }
        if (options === void 0) { options = {}; }
        if (this.RO && (options.rights || []).indexOf('write') >= 0) {
            return Observable.throw('ACTION_NOT_ALLOWED()');
        }
        if (!this.isAdmin && (options.rights || []).indexOf('admin') >= 0) {
            return Observable.throw('ACTION_NOT_ALLOWED()');
        }
        // TODO tester si connection ok (pour utilisation sur mobile device)
        if (this.apiStatus === 'NOAUTH' && !options.bypassRetry) {
            return Observable.fromPromise(this.retryCall(url, message, options));
        }
        var apiUrl;
        if (options.apiid && this.apiUrls.hasOwnProperty(options.apiid)) {
            apiUrl = this.apiUrls[options.apiid];
        }
        else {
            apiUrl = this.apiUrls.coldapi;
        }
        if (url.indexOf('/') === 0)
            url = url.substr(1);
        var callUrl = "https://" + apiUrl + "/" + url;
        var data = JSON.stringify(message);
        var endPointUrl = this.getEndpointUrl(callUrl, data, options);
        // is it cached ?
        if (options.cache && (options.rights || []).indexOf('write') < 0 && this.cache.has(endPointUrl)) {
            return this.cache.get(endPointUrl);
        }
        var myCall = this.http.post(endPointUrl, data)
            .map(function (res) {
            _this.cnxStatus = 'ONLINE';
            if ((options.rights || []).indexOf('write') < 0)
                _this.cache.add(endPointUrl, Observable.from([res]));
            return res;
        })
            .catch(function (err) {
            _this.cache.del(endPointUrl);
            if (err.status === 0) {
                // net::ERR_NETWORK_IO_SUSPENDED
                _this.cnxStatus = 'OFFLINE';
                return Observable.throw('CONNECTION_LOST()');
                // TODO: test if connection recovers
            }
            if (err.status === 200) {
                console.log("Error 200 in \"" + callUrl + "\" for:", data);
                return Observable.throw('INTERNAL_SERVER_ERROR()');
            }
            if (err.json) {
                var errMsg = err.json();
                if (errMsg.ok === false) {
                    if (errMsg.reason === 'MSG_AUTH_EXPIRED()' || errMsg.reason === 'Session Expired') {
                        if (_this.apiStatus === 'ONLINE') {
                            _this.apiStatus = 'NOAUTH';
                            _this.relog();
                        }
                        return Observable.fromPromise(_this.retryCall(url, message, options));
                    }
                    console.log("Error: " + errMsg.reason + " in \"" + callUrl + "\" for", data);
                    return Observable.throw(new InError(errMsg.reason, errMsg));
                }
            }
            return Observable.throw('INTERNAL_SERVER_ERROR()');
        });
        if ((options.rights || []).indexOf('write') < 0)
            this.cache.add(endPointUrl, myCall);
        return myCall;
    };
    ApiAuthService.prototype.retryCall = function (url, message, options) {
        var _this = this;
        if (message === void 0) { message = {}; }
        if (options === void 0) { options = {}; }
        options.nbRetry = (options.nbRetry || 0) + 1;
        if (options.nbRetry >= 3) {
            this.hasConnection = false;
            throw 'TOO_MANY_RETRIES()';
            // return Promise.reject('TOO_MANY_RETRIES()');
        }
        var sub;
        var promise = new Promise(function (resolve, reject) {
            sub = _this.onLogin.subscribe(function () {
                sub.unsubscribe();
                _this.call(url, message, options)
                    .catch(function (err) {
                    reject(err);
                    return Observable.from([]);
                })
                    .subscribe(function (res) {
                    resolve(res);
                });
            }, function (err) { reject(err); });
        });
        return promise;
    };
    ApiAuthService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiAuthService.ctorParameters = function () { return [
        { type: Http, },
        { type: ApiConfig, decorators: [{ type: Optional },] },
    ]; };
    return ApiAuthService;
}());
export { ApiAuthService };
var InError = /** @class */ (function () {
    function InError(errorMessage, options, originalResponse) {
        if (options === void 0) { options = {}; }
        this.errorMessage = errorMessage;
        this.options = options;
        this.originalResponse = originalResponse;
    }
    InError.prototype.toString = function () {
        return this.errorMessage;
    };
    Object.defineProperty(InError.prototype, "noParse", {
        get: function () {
            return !!this.options.noParse;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InError.prototype, "noShow", {
        get: function () {
            return !!this.options.noShow;
        },
        enumerable: true,
        configurable: true
    });
    return InError;
}());
export { InError };
var APICache = /** @class */ (function () {
    function APICache() {
        this.maxSize = 200;
        this.minSize = 100;
        this.cache = new Map();
        this.cacheAge = new Map();
    }
    APICache.prototype.has = function (id) {
        return this.cache.has(id);
    };
    APICache.prototype.add = function (id, value) {
        this.cache.set(id, value);
        this.cacheAge.set(id, new Date());
        this.clean();
    };
    APICache.prototype.get = function (id) {
        if (this.cache.has(id)) {
            this.cacheAge.set(id, new Date());
            return this.cache.get(id);
        }
    };
    APICache.prototype.del = function (k) {
        this.cache.delete(k);
        this.cacheAge.delete(k);
    };
    APICache.prototype.clean = function () {
        var _this = this;
        if (this.cache.size > this.maxSize) {
            var oldest = Array.from(this.cacheAge.entries())
                .sort(function (a, b) { return b[1] - a[1]; })
                .slice(this.minSize)
                .map(function (e) { return e[0]; });
            oldest.forEach(function (k) {
                _this.cache.delete(k);
                _this.cacheAge.delete(k);
            });
        }
    };
    return APICache;
}());
export { ɵ0 };
