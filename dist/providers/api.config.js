/**
 * Configration of the Api
 *
 *
 */
var /**
 * Configration of the Api
 *
 *
 */
ApiConfig = /** @class */ (function () {
    /**
     *
     * @param {any} apiUrls access url to the different apis (cold, hot, b2b). Default: dev apis
     * @param {any} oauth oAuth information of linked social network for publish functionalities
     * @param {any} loginRoute route for the redirection of unlogged users.
     */
    function ApiConfig(config) {
        if (config === void 0) { config = {}; }
        this.apiUrls = {
            coldapi: 'default-dot-exp-lkl.appspot.com',
            hotapi: 'hotapi-dot-exp-lkl.appspot.com',
            b2bapi: 'b2b-dot-exp-lkl.appspot.com'
        };
        this.oauth = {};
        if (config.apiUrls)
            this.apiUrls = config.apiUrls;
        if (config.oauth)
            this.oauth = config.oauth;
        if (config.loginRoute) {
            this.loginRoute = config.loginRoute;
        }
        if (config.keys)
            this.keys = config.keys;
        if (config.application)
            this.application = config.application;
    }
    return ApiConfig;
}());
/**
 * Configration of the Api
 *
 *
 */
export { ApiConfig };
