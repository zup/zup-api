import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { ZupApiService } from './zupApi.service';
import { HistoLogsService } from './histologs.service';
import { LexicalService } from "./lexical.service";
import { Campaign, FilterRules, Label, Lexical, lexicalCategories, lexicalServices, Member, NewLexical, ObjectsList, Partner, Profile, PublishAction, toPublishAction, View, widgetCategories, Widget, WidgetContext, Wall } from '../models';
import { ids, tools } from '../misc/utils';
import { _attributionTypes } from '../listOfValues';
import { widgetTypes } from "../models/widget";
export var STATUS;
(function (STATUS) {
    STATUS[STATUS["NONE"] = 0] = "NONE";
    STATUS[STATUS["INITATING"] = 1] = "INITATING";
    STATUS[STATUS["INITED"] = 2] = "INITED";
})(STATUS || (STATUS = {}));
var PartnerService = /** @class */ (function () {
    function PartnerService(api, histoLogs, lexicals) {
        this.api = api;
        this.histoLogs = histoLogs;
        this.lexicals = lexicals;
        this.roles = [];
        this.status = STATUS.NONE;
        this.widgetsListChange = new Subject();
        this.labelsListChange = new Subject();
        this.wallsListChange = new Subject();
        this.ICListChange = new Subject();
        this._onInit = new Subject();
    }
    Object.defineProperty(PartnerService.prototype, "isAdmin", {
        /**
         * Tests if the member loggued is the main account for this partner
         * @returns {boolean}
         */
        get: /**
           * Tests if the member loggued is the main account for this partner
           * @returns {boolean}
           */
        function () {
            return this.member && this.member.id === this.partner.id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "isInited", {
        /**
         * Tests if Partner information (widgets, thresholds, lables, ...) are loaded
         * @returns {boolean}
         */
        get: /**
           * Tests if Partner information (widgets, thresholds, lables, ...) are loaded
           * @returns {boolean}
           */
        function () {
            return this.status === STATUS.INITED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "isWallOnly", {
        get: function () {
            return (this.partner.woUsers || []).indexOf(this.member.id) >= 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "labels", {
        /**
         * Returns the labels created for this partner
         * @returns {any[]}
         */
        get: /**
           * Returns the labels created for this partner
           * @returns {any[]}
           */
        function () {
            return this._labels.list.filter(function (l) { return l.name.indexOf('ICampaign_') !== 0; });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "myName", {
        get: function () {
            return this.member && this.member.name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "widgetList", {
        get: function () {
            return (this.partner && this.partner.widgets) || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PartnerService.prototype, "walls", {
        get: function () {
            return this._walls;
        },
        enumerable: true,
        configurable: true
    });
    PartnerService.prototype.addCampaign = function (campaign) {
        return this.api.campaigns.add(campaign);
    };
    PartnerService.prototype.addFacebookToken = function (profiles) {
        this.partner.socialAccounts = this.partner.socialAccounts
            .filter(function (p) { return p.attributionKind !== _attributionTypes.FACEBOOK; })
            .concat(profiles);
        this.partner.fbProfilesLoaded = true;
    };
    PartnerService.prototype.addRight = function (right) {
        if (this.partner.rights.indexOf(right) < 0)
            this.partner.rights.push(right);
    };
    PartnerService.prototype.addView = function (view) {
        this.getWidget(view.parentWidgetId).widgetViews.push(view);
    };
    /*
    addWidget(widget: Widget) {
      this.partner.widgets.push(widget);
      this.widgetsListChange.next(this.partner.widgets);
    }*/
    /*
      addWidget(widget: Widget) {
        this.partner.widgets.push(widget);
        this.widgetsListChange.next(this.partner.widgets);
      }*/
    PartnerService.prototype.createInfluenceCampaign = /*
      addWidget(widget: Widget) {
        this.partner.widgets.push(widget);
        this.widgetsListChange.next(this.partner.widgets);
      }*/
    function () {
        this.ICListChange.next(this.getInfluenceCampaigns());
    };
    PartnerService.prototype.createWidget = function (w, isI4U) {
        var _this = this;
        return this.api.widget.create(w, isI4U).map(function (wnew) {
            _this.partner.widgets.push(wnew);
            _this.partner.widgetAndViews.set(wnew.id, wnew);
            _this.widgetsListChange.next(_this.partner.widgets);
            return wnew;
        });
    };
    PartnerService.prototype.deleteWidget = function (w) {
        var _this = this;
        return this.api.widget.delete(w).map(function () {
            var i = _this.partner.widgets.findIndex(function (w2) { return w2.id === w.id; });
            if (i >= 0) {
                _this.partner.widgets.splice(i, 1);
            }
            _this.partner.widgetAndViews.delete(w.id);
            _this.widgetsListChange.next(_this.partner.widgets);
            return true;
        });
    };
    PartnerService.prototype.deleteInfluenceCampaign = function (w) {
        var _this = this;
        return this.deleteWidget(w).map(function () {
            _this.ICListChange.next(_this.getInfluenceCampaigns());
            return true;
        });
    };
    PartnerService.prototype.updateInfluenceCampaign = function () {
        this.ICListChange.next(this.getInfluenceCampaigns());
    };
    PartnerService.prototype.getInfluenceCampaign = function (wid) {
        return this.getWidget(wid);
    };
    PartnerService.prototype.getInfluenceCampaigns = function () {
        return this.getWidgets({ category: widgetCategories.PROFILE_CAMPAIGN });
    };
    PartnerService.prototype.getInfluenceBrands = function () {
        var _this = this;
        //if (!this.isInited) return this.onInit().flatMap(() => this.getInfluenceBrands());
        if (this._influenceBrands)
            return Observable.of(this._influenceBrands);
        this._influenceBrands = [];
        var todos = [];
        this.getWidgets({ type: 4 }).forEach(function (w) {
            todos.push(_this.api.labels.profilesList(w.labelId, { limit: 1 }, { simpleProfiles: true }).map(function (res) {
                if (w.category === widgetCategories.PROFILE_MY_BRAND) {
                    return { widget: w, me: true, profile: res.list[0] };
                }
                else if (w.category === widgetCategories.PROFILE_COMPETITOR) {
                    return { widget: w, me: false, profile: res.list[0] };
                }
            }));
        });
        return Observable.forkJoin.apply(Observable, todos).map(function (res) {
            res.forEach(function (r) {
                if (r) {
                    _this._influenceBrands.push(r);
                }
            });
            return _this._influenceBrands;
        });
    };
    PartnerService.prototype.getInfluenceBrandWidgets = function () {
        return this.getWidgets({ type: 4 }).filter(function (w) { return w.category === widgetCategories.PROFILE_MY_BRAND || w.category === widgetCategories.PROFILE_COMPETITOR; });
    };
    PartnerService.prototype.decodeId = function (id) {
        return ids.fromSafe(id);
    };
    PartnerService.prototype.delCampaign = function (campaign) {
        return this.api.campaigns.delete(campaign);
    };
    PartnerService.prototype.delView = function (view) {
        var myView = typeof view === 'string' ? this.getWidget(view) : view;
        var index = this.getWidget(myView.parentWidgetId).widgetViews.findIndex(function (v) { return v.id === myView.id; });
        if (index >= 0)
            this.getWidget(myView.parentWidgetId).widgetViews.splice(index, 1);
    };
    PartnerService.prototype.delWidget = function (widgetId) {
        var _this = this;
        return this.api.widget.delete(this.getWidget(widgetId)).map(function () {
            var index = _this.partner.widgets.findIndex(function (w) { return w.id === widgetId; });
            _this.partner.widgets.splice(index, 1);
            _this.widgetsListChange.next(_this.partner.widgets);
        });
    };
    PartnerService.prototype.getCampaigns = function () {
        var _this = this;
        return this.api.campaigns.list().map(function (res) {
            var campaigns = [];
            res.forEach(function (pa) {
                var myCampaign = campaigns.find(function (ca) { return ca.label.id === pa.profiles_label_id; });
                if (!myCampaign) {
                    myCampaign = {
                        label: _this.labels.find(function (l) { return l.id === pa.profiles_label_id; }),
                        publishActions: []
                    };
                    campaigns.push(myCampaign);
                }
                /*
                        myCampaign.publishActions.push({
                          follow: pa.follow,
                          comment: pa.comment,
                          like: pa.like,
                          mention: pa.mention,
                          tokenId: pa.token_xid,
                          id: pa.id,
                          message: pa.message,
                          mentionMessage: pa.mention_message,
                          fileUrls: pa.media_urls,
                          mentionFileUrls: pa.mention_media_urls
                        })*/
                myCampaign.publishActions.push(toPublishAction(pa));
            });
            // check if campaigns from label without TOKENS :
            Array.from(_this._labels).forEach(function (l) {
                if (l.isCampaign && campaigns.findIndex(function (c) { return c.label.id === l.id; }) < 0) {
                    campaigns.push({ label: l, publishActions: [] });
                }
            });
            return campaigns;
        });
    };
    PartnerService.prototype.getLabel = function (id) {
        id = decodeURIComponent(id);
        return this._labels.list.find(function (l) { return l.id === id; });
    };
    PartnerService.prototype.getMember = function (memberId) {
        return this.partner.members.find(function (m) { return m.id === memberId; });
    };
    /**
     * Returns the thredsholds for the type of widget provided and the word for "Word Alerts"
     * @param {number} widgetId
     * @returns {Observable<R>}
     */
    /**
       * Returns the thredsholds for the type of widget provided and the word for "Word Alerts"
       * @param {number} widgetId
       * @returns {Observable<R>}
       */
    PartnerService.prototype.getNotificationsLimit = /**
       * Returns the thredsholds for the type of widget provided and the word for "Word Alerts"
       * @param {number} widgetId
       * @returns {Observable<R>}
       */
    function (widgetId) {
        if (widgetId) {
            var limits = {};
            var widgets = this.getWidgets({ id: widgetId });
            if (widgets.length > 0) {
                limits.userDailyThreshold = widgets[0].userDailyThreshold || 0;
                limits.influencerThreshold = widgets[0].influencerThreshold || 0;
                limits.customerLeadThreshold = widgets[0].customerLeadThreshold || 0;
                limits.loyaltyNbThreshold =
                    widgets[0].loyaltyNbThreshold ? widgets[0].loyaltyNbThreshold : 0;
                limits.loyaltyDaysThreshold =
                    widgets[0].loyaltyDaysThreshold ? widgets[0].loyaltyDaysThreshold : 0;
                limits.useDefaultThresholds = widgets[0].useDefaultThresholds || false;
            }
            return limits;
        }
        else {
            return Object.assign({ useDefaultThresholds: true }, this.partner.defaultLimits);
        }
    };
    PartnerService.prototype.getDefaultDetectedWords = function () {
        return this.partner.defaultWordsAlert;
    };
    PartnerService.prototype.getSocialAccounts = function (kind) {
        var _this = this;
        return this.getDefaultSocialAccounts().map(function (res) {
            if (res[kind]) {
                return res[kind];
            }
            else {
                var accounts = _this.partner.socialAccounts.filter(function (u) { return u.kind === kind; });
                if (accounts.length === 1) {
                    // only one account => set it as default
                    return accounts[0];
                }
            }
        });
    };
    PartnerService.prototype.getDefaultSocialAccounts = function () {
        var _this = this;
        if (!this.defaultSA) {
            return this.api.misc.mstores.get('b2b_default_social_accounts').flatMap(function (resDSA) {
                var profiles = [];
                for (var p in resDSA) {
                    profiles.push(resDSA[p]);
                }
                return _this.api.profiles.getMultiple(profiles).map(function (resProfiles) {
                    var DSA = {};
                    var _loop_1 = function (p) {
                        var dp = resProfiles.find(function (profile) { return profile.id === resDSA[p]; });
                        DSA[p] = dp;
                    };
                    for (var p in resDSA) {
                        _loop_1(p);
                    }
                    _this.defaultSA = DSA;
                    return DSA;
                });
            });
        }
        else {
            return Observable.from([this.defaultSA]);
        }
    };
    PartnerService.prototype.getDetectedWords = function () {
        var _this = this;
        var lexicals = this.widgetList.map(function (w) {
            return { name: 'B2B_word_alert_lexical_name', widgetId: w.id };
        });
        return this.api.lexical.list(lexicalCategories.ALERT).catch(function (err) {
            return Observable.from([[]]);
        }).map(function (res) {
            var defaultWords = _this.getDefaultDetectedWords();
            var widgetWords = res.map(function (l) {
                var words = l.values.filter(function (v) { return defaultWords.indexOf(v) < 0; });
                return { widgetId: l.widgetId, words: words.toString() };
            });
            widgetWords.push({ words: defaultWords.toString() });
            return widgetWords;
        });
    };
    PartnerService.prototype.getProfileICampaigns = function (profile) {
        var campaigns = this.getInfluenceCampaigns();
        var profileCampaigns = [];
        campaigns.forEach(function (c) {
            var lid = c.labelId;
            if (profile.labels.map(function (l) { return l.id; }).indexOf(lid) >= 0) {
                profileCampaigns.push(c);
            }
        });
        return profileCampaigns;
    };
    PartnerService.prototype.getRecordedMessages = function () {
        return this.partner.recordedMessages;
    };
    PartnerService.prototype.getSources = function () {
        var sources = [];
        this.partner.widgets.forEach(function (w) { return sources = sources.concat(w.sources); });
        return tools.unduplicate(sources);
    };
    PartnerService.prototype.getWall = function (id) {
        var wall = this._walls.find(function (w) { return w.id === id; });
        if (wall)
            return wall;
    };
    PartnerService.prototype.getWidget = function (id) {
        // ///////////////////////////
        if (!this.partner.widgets)
            return;
        if (this.partner.widgetAndViews.has(id)) {
            return this.partner.widgetAndViews.get(id);
        }
        else {
            var w = new Widget();
            w.name = 'Unknown Place';
            return w;
        }
        ;
    };
    PartnerService.prototype.getWidgetDetail = function (id) {
        var _this = this;
        if (!this.partner.widgets)
            return Observable.from([]);
        return this.api.widget.get(id).map(function (w) {
            //keep views :
            var views = _this.getWidget(id).widgetViews;
            w.widgetViews = views;
            _this.partner.widgetAndViews.set(w.id, w.clone());
            return w;
        });
    };
    // TODO: remove main WidgetContext
    /**
     *
     * @param {{id?: string, type?: number, category?: number, main?: boolean, defaultPeriode?: string}} options
     *          main = true, saves the context to mainWidgetContext
     * @returns {WidgetContext}
     */ /*
   getWidgetContext(options?: {id?: string, type?: number, category?: number, main?: boolean}) {
     let wc: WidgetContext;
     if (!options) {
       wc = new WidgetContext();
     } else if (options.id) {
       const w = this.getWidget(this.decodeId(options.id));
       wc =  new WidgetContext(w, {type: w.type});
     } else if (options.category) {
       wc = new WidgetContext().cat(options.category);
     }
 
     if (options.main) {
       this.mainWidgetContext = wc;
     }
 
     return wc;
   }*/
    // TODO: remove main WidgetContext
    /**
       *
       * @param {{id?: string, type?: number, category?: number, main?: boolean, defaultPeriode?: string}} options
       *          main = true, saves the context to mainWidgetContext
       * @returns {WidgetContext}
       */ /*
      getWidgetContext(options?: {id?: string, type?: number, category?: number, main?: boolean}) {
        let wc: WidgetContext;
        if (!options) {
          wc = new WidgetContext();
        } else if (options.id) {
          const w = this.getWidget(this.decodeId(options.id));
          wc =  new WidgetContext(w, {type: w.type});
        } else if (options.category) {
          wc = new WidgetContext().cat(options.category);
        }
    
        if (options.main) {
          this.mainWidgetContext = wc;
        }
    
        return wc;
      }*/
    PartnerService.prototype.getWidgetName = 
    // TODO: remove main WidgetContext
    /**
       *
       * @param {{id?: string, type?: number, category?: number, main?: boolean, defaultPeriode?: string}} options
       *          main = true, saves the context to mainWidgetContext
       * @returns {WidgetContext}
       */ /*
      getWidgetContext(options?: {id?: string, type?: number, category?: number, main?: boolean}) {
        let wc: WidgetContext;
        if (!options) {
          wc = new WidgetContext();
        } else if (options.id) {
          const w = this.getWidget(this.decodeId(options.id));
          wc =  new WidgetContext(w, {type: w.type});
        } else if (options.category) {
          wc = new WidgetContext().cat(options.category);
        }
    
        if (options.main) {
          this.mainWidgetContext = wc;
        }
    
        return wc;
      }*/
    function (id) {
        if (!this.partner.widgets)
            return 'Unknown Place';
        if (this.partner.widgetAndViews.has(id)) {
            var w = this.partner.widgetAndViews.get(id);
            if (w.parentWidgetId) {
                var parent_1 = this.partner.widgetAndViews.get(w.parentWidgetId);
                if (!parent_1) {
                    console.log("PARENT_WIDGET_NOT_FOUND(" + id + "," + w.parentWidgetId + ")");
                    return 'Unknown Place';
                }
                return parent_1.name.concat(' : ', w.name);
            }
            return this.partner.widgetAndViews.get(id).name;
        }
        else {
            return 'Unknown Place';
        }
        ;
    };
    PartnerService.prototype.getWidgetRuleName = function (w, options) {
        if (!w)
            return 'Unknown';
        var cat, type;
        switch (w.type) {
            case 'widget':
                return this.getWidgetName(w.id);
            case 'category':
                var customCat = (tools.deepGet(options, 'customCats') || []).find(function (cc) { return cc.value.id === w.id && cc.value.type === 'category'; });
                if (customCat) {
                    return customCat.text;
                }
                cat = widgetCategories.detail(w.id);
                type = widgetTypes.detail(cat.type);
                return type.label + " : " + cat.label;
            case 'type':
                var customType = (tools.deepGet(options, 'customCats') || []).find(function (cc) { return cc.value.id === w.id && cc.value.type === 'type'; });
                if (customType) {
                    return customType.text;
                }
                type = widgetTypes.detail(w.id);
                return "" + type.label;
            default:
                return 'Unknown';
        }
    };
    PartnerService.prototype.getWidgets = function (options) {
        if (!options)
            return this.partner.widgets;
        return this.partner.widgets.filter(function (w) {
            for (var o in options) {
                if (o === 'types' && options[o]) {
                    if (options.types.indexOf(w.type) < 0)
                        return false;
                }
                else if (o === 'categories' && options[o]) {
                    if (options.categories.indexOf(w.category) < 0)
                        return false;
                }
                else {
                    if (options[o] !== undefined && options[o] !== w[o])
                        return false;
                }
            }
            ;
            return true;
        });
    };
    PartnerService.prototype.hasRightsFor = function (rights) {
        var _this = this;
        var _loop_2 = function (r) {
            //one right can be like "right1,right2" in this case, it means right1 OR right2
            var rs = r.split(',');
            if (rs.length === 1 && rs[0] === 'Admin') {
                return { value: this_1.isAdmin };
            }
            else {
                var isOK_1 = false;
                rs.forEach(function (r) { isOK_1 = isOK_1 || _this.partner.rights.indexOf(r) >= 0; });
                //if (this.partner.rights.indexOf(r) < 0) return false;
                if (!isOK_1)
                    return { value: false };
            }
        };
        var this_1 = this;
        for (var _i = 0, rights_1 = rights; _i < rights_1.length; _i++) {
            var r = rights_1[_i];
            var state_1 = _loop_2(r);
            if (typeof state_1 === "object")
                return state_1.value;
        }
        return true;
    };
    PartnerService.prototype.isInfluencerList = function (lid) {
        if (!lid)
            return false;
        var wl = this.widgetList;
        for (var i = 0; i < wl.length; i++) {
            if (wl[i].isProfileListWidget && wl[i].labelId === lid) {
                return true;
            }
        }
        return false;
    };
    /**
     * Returns an Observable that returns true when partner information are loaded
     * @returns {Observable<boolean>}
     *
     * ###exemple :
     * ```
     * partnerService.onInit().subscribe( ok => {
     *    // Do something
     * })
     * ```
     */
    /**
       * Returns an Observable that returns true when partner information are loaded
       * @returns {Observable<boolean>}
       *
       * ###exemple :
       * ```
       * partnerService.onInit().subscribe( ok => {
       *    // Do something
       * })
       * ```
       */
    PartnerService.prototype.onInit = /**
       * Returns an Observable that returns true when partner information are loaded
       * @returns {Observable<boolean>}
       *
       * ###exemple :
       * ```
       * partnerService.onInit().subscribe( ok => {
       *    // Do something
       * })
       * ```
       */
    function () {
        if (this.status === STATUS.INITED)
            return Observable.from([true]);
        return this.widgetsListChange.first().map(function () { return true; });
    };
    PartnerService.prototype.onInit2 = function (callback) {
        if (this.status === STATUS.INITED) {
            callback();
        }
        else {
            this._onInit.take(1).subscribe(function () { return callback(); });
        }
    };
    PartnerService.prototype.refreshLabels = function () {
        var _this = this;
        return this.api.labels.listAll().map(function (res) {
            _this._labels = res;
            _this.labelsListChange.next(_this.labels);
            return _this.labels;
        });
    };
    PartnerService.prototype.refreshWallList = function () {
        var _this = this;
        return this.api.wall.list().map(function (res) {
            _this._walls = res;
            _this.wallsListChange.next(_this._walls);
        });
    };
    PartnerService.prototype.removeMember = function (id) {
        var _this = this;
        return this.api.members.remove(id).map(function (res) {
            _this.partner.removeMember(id);
        });
    };
    /*
      saveQueries(queries: {name: string, query: any}[]): Observable<boolean> {
        this.partner.advancedQueries = queries;
        return this.updatePartner();
      }
    */
    /*
      saveQueries(queries: {name: string, query: any}[]): Observable<boolean> {
        this.partner.advancedQueries = queries;
        return this.updatePartner();
      }
    */
    PartnerService.prototype.getQueries = /*
      saveQueries(queries: {name: string, query: any}[]): Observable<boolean> {
        this.partner.advancedQueries = queries;
        return this.updatePartner();
      }
    */
    function (type) {
        if (type === void 0) { type = 'profile'; }
        return this.partner.advancedQueries.filter(function (q) { return (q.type || 'profile') === type; }); // <= 'profile' for retro-compatibility
    };
    PartnerService.prototype.addQuery = function (query) {
        this.partner.advancedQueries.push(query);
        this.updatePartner().subscribe();
        return this.getQueries(query.type);
    };
    PartnerService.prototype.deleteQuery = function (queries) {
        var _this = this;
        queries.forEach(function (query) {
            var i = _this.partner.advancedQueries.findIndex(function (q) { return q.name === query.name; });
            if (i >= 0) {
                _this.partner.advancedQueries.splice(i, 1);
            }
        });
        this.updatePartner().subscribe();
        return this.getQueries(queries[0].type);
    };
    /**
     *
     * @param limits modifications in the thresholds and words for the alerts
     * @param widgetId
     * @returns {any}
     */
    /**
       *
       * @param limits modifications in the thresholds and words for the alerts
       * @param widgetId
       * @returns {any}
       */
    PartnerService.prototype.setNotificationsLimit = /**
       *
       * @param limits modifications in the thresholds and words for the alerts
       * @param widgetId
       * @returns {any}
       */
    function (limits, widgetId) {
        var _this = this;
        var todos = [];
        this.getWidgets({ id: widgetId }).forEach(function (w) {
            for (var p in limits) {
                if (p !== 'useDefaultThresholds') {
                    w[p] = limits[p] ? parseInt(limits[p], 10) : undefined;
                }
                else {
                    w[p] = !!limits[p];
                }
            }
            todos.push(_this.api.widget.updateAlerts(w).flatMap(function () { return _this.api.widget.updateWidgetInfo(w); }).map(function () { return _this.updatePartnerWidget(w); }));
        });
        return Observable.forkJoin.apply(Observable, todos);
    };
    PartnerService.prototype.setDefaultLimits = function (limits) {
        var _this = this;
        var initDefault = JSON.stringify(this.partner.defaultLimits);
        for (var t in limits) {
            switch (typeof limits[t]) {
                case 'string':
                    this.partner.defaultLimits[t] = limits[t].length > 0 ? parseInt(limits[t], 10) : undefined;
                    break;
                case 'number':
                    this.partner.defaultLimits[t] = limits[t];
                    break;
                default:
                    this.partner.defaultLimits[t] = undefined;
            }
        }
        if (typeof this.partner.defaultLimits.loyaltyNbThreshold !== typeof this.partner.defaultLimits.loyaltyDaysThreshold) {
            this.partner.defaultLimits = JSON.parse(initDefault);
            return Observable.throw('WRONG_VALUES()');
        }
        var todos = [];
        todos.push(this.api.partner.update(this.partner));
        this.getWidgets().forEach(function (w) {
            if (!w.useDefaultThresholds)
                return; // do not update customs values
            Object.assign(w, _this.partner.defaultLimits);
            todos.push(_this.api.widget.updateAlerts(w).map(function () { return _this.updatePartnerWidget(w); }));
        });
        return Observable.forkJoin(todos).map(function () { return true; });
    };
    PartnerService.prototype.setDefaultSocialAccount = function (accountKind, profile) {
        if (profile) {
            this.defaultSA[accountKind] = profile;
        }
        else {
            delete this.defaultSA[accountKind];
        }
        var DSA = {};
        for (var p in this.defaultSA) {
            if (this.defaultSA[p]) {
                DSA[p] = this.defaultSA[p].id;
            }
        }
        this.api.misc.mstores.put('b2b_default_social_accounts', DSA).subscribe();
    };
    /**
     *
     * @param {{widgetId?: string, words?: string}[]} words list of lexicals to update
     * @returns {Observable<boolean>}
     */
    /**
       *
       * @param {{widgetId?: string, words?: string}[]} words list of lexicals to update
       * @returns {Observable<boolean>}
       */
    PartnerService.prototype.setDetectedWords = /**
       *
       * @param {{widgetId?: string, words?: string}[]} words list of lexicals to update
       * @returns {Observable<boolean>}
       */
    function (words) {
        var _this = this;
        var todos = [];
        // check if default has change :
        var defaultLexical = words.filter(function (w) { return !w.widgetId; });
        var newDefaultWordsString = defaultLexical.length > 0 ? defaultLexical[0].words : this.getDefaultDetectedWords().toString();
        var newDefaultWords = this.cleanWords(newDefaultWordsString.length > 0 ? newDefaultWordsString.split(/\s*,\s*/) : []);
        if (defaultLexical) {
            this.partner.defaultWordsAlert = newDefaultWords;
            todos.push(this.api.partner.update(this.partner));
        }
        return this.api.lexical.list(lexicalCategories.ALERT).flatMap(function (res) {
            // update lexicals per widget :
            var toDelete = [];
            var toUpdate = [];
            var toAdd = [];
            words.forEach(function (w) {
                var newWords = _this.cleanWords(w.words.length > 0 ? w.words.split(/\s*,\s*/) : []);
                if (newWords.length > 0 || newDefaultWords.length > 0) {
                    if (w.widgetId) {
                        var wa = res.find(function (l) { return l.widgetId === w.widgetId; });
                        if (wa) {
                            wa.values = tools.unduplicate(newDefaultWords.concat(newWords));
                            toUpdate.push(wa);
                        }
                        else {
                            toAdd.push({
                                displayName: 'word alert',
                                widgetId: w.widgetId,
                                values: tools.unduplicate(newDefaultWords.concat(newWords)),
                                service: lexicalServices.WORD_ALERT
                            });
                        }
                    }
                }
                else if (w.widgetId) {
                    var wa = res.find(function (l) { return l.widgetId === w.widgetId; });
                    if (wa)
                        toDelete.push(wa.id);
                }
            });
            if (toUpdate.length > 0)
                todos.push(_this.api.lexical.update(toUpdate));
            if (toDelete.length > 0)
                todos.push(_this.api.lexical.delete_multiple(toDelete));
            if (toAdd.length > 0)
                todos.push(_this.api.lexical.set(toAdd));
            return Observable.forkJoin(todos).map(function () { return true; });
        });
    };
    PartnerService.prototype.setRecordedMessages = function (messages) {
        this.partner.recordedMessages = messages;
        this.api.partner.update(this.partner).subscribe();
    };
    PartnerService.prototype.cleanWords = function (w) {
        return w.map(function (s) { return s.trim(); }).filter(function (s) { return s.length > 0; });
    };
    /**
     * replace setUser : load Partner information for this user
     *
     * @param userId
     */
    /**
       * replace setUser : load Partner information for this user
       *
       * @param userId
       */
    PartnerService.prototype.initPartner = /**
       * replace setUser : load Partner information for this user
       *
       * @param userId
       */
    function (userId) {
        var _this = this;
        if (!userId) {
            // logout
            this.member = undefined;
            this.status = STATUS.NONE;
            return;
        }
        this.status = STATUS.INITATING;
        this.histoLogs.init();
        this.lexicals.init();
        return Observable.forkJoin(this.api.partner.get(), this.api.partner.widgetsList(101), this.api.members.list(), this.api.labels.listAll(), this.api.wall.list(), this.api.partner.listTokens()).map(function (res) {
            _this.partner = new Partner(res[0], res[1], res[2]);
            _this.member = res[2].find(function (m) { return m.id === userId; });
            // manage special rigts for Admin
            if (_this.isAdmin) {
                _this.addRight('Wall');
                _this.addRight('Insuite');
                _this.addRight('Influence'); //TODO : remove that on prod !!!
                _this.addRight('I4U');
            }
            if (_this.partner.hasInsuite && !_this.hasRightsFor(['PartnerBInfluence'])) {
                _this.addRight('Insuite');
            }
            if (_this.hasRightsFor(['PartnerInfluence'])) {
                _this.addRight('Influence');
            }
            /* not true anymore because I4U has this rights...
                  if (this.hasRightsFor(['PartnerInfluence'])){
                    this.addRight('Influence');
                  }
                  TODO :
                   if (this.partner.hasInfluence) {
                   this.addRight('Influence');
                   }
                  */
            if (_this.hasRightsFor(['PartnerBInfluence'])) {
                _this.addRight('I4U');
            }
            _this._labels = res[3];
            _this._walls = res[4]; //[]// res[4];
            _this.partner.socialAccounts = res[5]; //res[4]// res[5];
            _this.status = STATUS.INITED;
            _this.widgetsListChange.next(_this.widgetList); // intitate
            _this.labelsListChange.next(_this.labels);
            _this.wallsListChange.next(_this._walls);
            _this.ICListChange.next(_this.getInfluenceCampaigns());
            _this._onInit.next(true);
            return _this.partner;
        });
    };
    PartnerService.prototype.updatePartner = function () {
        var sub = this.api.partner.update(this.partner);
        return sub;
    };
    PartnerService.prototype.updateWidget = function (widget) {
        var _this = this;
        return this.api.widget.update(widget, this.getWidget(widget.id)).map(function () {
            _this.updatePartnerWidget(widget);
            return widget;
        });
    };
    PartnerService.prototype.updateView = function (view) {
        var _this = this;
        var originalView = this.getWidget(view.parentWidgetId).getView(view.id);
        return this.api.view.update(view, originalView).map(function (updatedView) {
            if (view.id !== updatedView.id) {
                _this.getWidget(view.parentWidgetId).delView(view.id);
            }
            _this.getWidget(view.parentWidgetId).setView(updatedView);
            return updatedView;
        });
    };
    PartnerService.prototype.updatePartnerWidget = function (widget) {
        var index = this.partner.widgets.findIndex(function (w) { return w.id === widget.id; });
        this.partner.widgets[index] = widget;
        this.partner.widgetAndViews.set(widget.id, widget);
        this.widgetsListChange.next(this.partner.widgets);
    };
    PartnerService.decorators = [
        { type: Injectable },
    ];
    /*
      // todo : remove mainContext, it is the appState service that should manage that
      getOrCreateMainWidgetContext(options: {id?: string, type?: number, category?: number, main?: boolean} = {}) {
        const currentWC = this.mainWidgetContext;
        if (!currentWC) return this.getWidgetContext(Object.assign({main: true}, options));
    
        if (options.id && ids.fromSafe(options.id) !== currentWC.widgetId) {
          return this.getWidgetContext(Object.assign({main: true}, options));
        }
    
        if (options.type && options.type !== currentWC.type) {
          return this.getWidgetContext(Object.assign({main: true}, options));
        }
    
        if (options.category && options.category !== currentWC.catId) {
          return this.getWidgetContext(Object.assign({main: true}, options));
        }
    
        return currentWC;
      }
    */
    /** @nocollapse */
    PartnerService.ctorParameters = function () { return [
        { type: ZupApiService, },
        { type: HistoLogsService, },
        { type: LexicalService, },
    ]; };
    return PartnerService;
}());
export { PartnerService };
