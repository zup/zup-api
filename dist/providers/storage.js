import { Observable } from 'rxjs';
import { ZupApiService } from './zupApi.service';
import { Post, Task, Notification, Profile } from '../models';
import { tools } from "../misc/utils";
export var objectFormat = {
    XS: 0,
    S: 1,
    L: 2,
    XL: 3
};
var StorageError = /** @class */ (function () {
    function StorageError(values) {
        if (values === void 0) { values = {}; }
        this.isError = true;
        for (var p in values) {
            this[p] = values[p];
        }
    }
    return StorageError;
}());
export { StorageError };
var InStorage = /** @class */ (function () {
    function InStorage(api) {
        /*  this.detailFunctions.user  = this.api.getUsersList;
                 this.detailFunctions.post  = this.api.getPostsDetail;
                 this.detailFunctions.notification  = this.api.notifications.get;*/
        this.api = api;
        // We only store jsons elements, not entities
        this.data = {
            profile: new Map(),
            post: new Map(),
            notification: new Map(),
            task: new Map(),
            label: new Map(),
        };
        this.objectTypes = ['profile', 'post', 'notification', 'task', 'label'];
    }
    InStorage.prototype.addObjectWithFormat = function (jsonObject, type, format) {
        if (this.objectTypes.indexOf(type) < 0)
            throw "OBJECT_TYPE_UNKNOWN(" + type + ")";
        jsonObject.format = format;
        if (this.data[type].has(jsonObject.id)) {
            var o = this.data[type].get(jsonObject.id);
            if (o.format >= format) {
                return;
            }
            else {
                Object.assign(o, jsonObject);
            }
        }
        else {
            this.data[type].set(jsonObject.id, jsonObject);
        }
    };
    InStorage.prototype.addObject = function (jsonObject, type) {
        if (this.objectTypes.indexOf(type) < 0)
            throw "OBJECT_TYPE_UNKNOWN(" + type + ")";
        // shall we replace or update exisiting Object ?
        // should we check jsonFormat for each objectType (be sure to have all fields)
        if (this.data[type].has(jsonObject.id)) {
            var o = this.data[type].get(jsonObject.id);
            o.isPublic = o.isPublic && jsonObject.isPublic; // une seule gestion des formats (public = format rive)
            tools.deepAssign(o, jsonObject);
        }
        else {
            this.data[type].set(jsonObject.id, jsonObject);
        }
    };
    InStorage.prototype.addObjects = function (jsonObjects, type, format) {
        var _this = this;
        if (!jsonObjects)
            return;
        if (format) {
            jsonObjects.map(function (o) { return _this.addObjectWithFormat(o, type, format); });
        }
        else {
            jsonObjects.map(function (o) { return _this.addObject(o, type); });
        }
    };
    InStorage.prototype.addPostList = function (list) {
        if (list.profiles) {
            this.addObjects(list.profiles, 'profile', objectFormat.S);
        }
        var classification = new Map();
        if (list.posts_classified_positive)
            list.posts_classified_positive.forEach(function (c) { return classification.set(c, 1); });
        if (list.posts_classified_negative)
            list.posts_classified_negative.forEach(function (c) { return classification.set(c, -1); });
        if (list.posts) {
            list.posts.forEach(function (p) { return p.content.sentiment_score = classification.get(p.id); });
            this.addObjects(list.posts, 'post', objectFormat.S);
        }
    };
    InStorage.prototype.getJsonObjects = function (ids, type) {
        var _this = this;
        if (this.objectTypes.indexOf(type) < 0)
            throw "OBJECT_TYPE_UNKNOWN(" + type + ")";
        return ids.map(function (id) { return _this.data[type].get(id); }).filter(function (o) { return !!o; });
    };
    /**
     *
     * @param id
     * @param {boolean} useCache if true, force user from cache, if false, ask api, if undefined check cache then api
     * @returns {Observable<R>}
     */
    /**
         *
         * @param id
         * @param {boolean} useCache if true, force user from cache, if false, ask api, if undefined check cache then api
         * @returns {Observable<R>}
         */
    InStorage.prototype.getProfile = /**
         *
         * @param id
         * @param {boolean} useCache if true, force user from cache, if false, ask api, if undefined check cache then api
         * @returns {Observable<R>}
         */
    function (id, useCache, format) {
        var _this = this;
        return (useCache || (this.data.profile.has(id) && useCache !== false)) ?
            Observable.from([this.profileObject(id, format)]) :
            this.api.profiles.loadToCache([id]).map(function () {
                var u = _this.profileObject(id);
                if (!u || u.isError) {
                    var err = new StorageError();
                    err.type = 'user';
                    err.userId = id;
                    console.log(err);
                    return err;
                }
                return u;
            });
    };
    /**
     *
     * @param {string[]} ids : list of profils ids
     * @param { boolean} useCache : if true: only search cache, if false: only search remote, undefined: search both
     * @returns {Observable<Profile[]>}
     */
    /**
         *
         * @param {string[]} ids : list of profils ids
         * @param { boolean} useCache : if true: only search cache, if false: only search remote, undefined: search both
         * @returns {Observable<Profile[]>}
         */
    InStorage.prototype.getProfiles = /**
         *
         * @param {string[]} ids : list of profils ids
         * @param { boolean} useCache : if true: only search cache, if false: only search remote, undefined: search both
         * @returns {Observable<Profile[]>}
         */
    function (ids, useCache, format) {
        var _this = this;
        var users = [];
        var missingUsers = [];
        ids.map(function (id) {
            var u = _this.profileObject(id, format);
            if (u && !u.isError && useCache !== false) {
                users.push(u);
            }
            else {
                missingUsers.push(id);
            }
        });
        if (missingUsers.length > 0 && useCache !== true) {
            return this.api.profiles.loadToCache(missingUsers, format).map(function () {
                users = users.concat(missingUsers.map(function (uid) {
                    var u = _this.profileObject(uid);
                    if (!u || u.isError) {
                        var err = new StorageError({ type: 'userInUsers', userId: uid });
                        console.log(err);
                        return err;
                    }
                    return u;
                }));
                return users.filter(function (p) { return !p.isError; });
            });
        }
        else {
            return Observable.from([users]);
        }
    };
    InStorage.prototype.getPosts = function (ids, format) {
        var _this = this;
        var posts = [];
        var missingPosts = [];
        ids.map(function (id) {
            if (!id)
                return;
            var p = _this.postObject(id, format);
            if (p && !p.isError) {
                posts.push(p);
            }
            else {
                missingPosts.push(id);
            }
            ;
        });
        if (missingPosts.length === 0) {
            return Observable.from([posts]);
        }
        else {
            return this.api.posts.loadToCache(missingPosts, format)
                .map(function () {
                posts = posts.concat(missingPosts.map(function (pid) {
                    var p = _this.postObject(pid, format);
                    if (!p) {
                        var err = new StorageError({ type: 'postInPosts', postId: pid });
                        console.log(err);
                        return err;
                    }
                    return p;
                }));
                return posts.filter(function (p) { return !p.isError; });
            });
        }
    };
    InStorage.prototype.getNotifications = function (ids, format) {
        var _this = this;
        return Observable.from([ids.map(function (nid) {
                var t = _this.notificationObject(nid, format);
                if (!t) {
                    var err = new StorageError({ type: 'notificationsInNotifications', notificationId: nid });
                    console.log(err);
                    return err;
                }
                return t;
            }).filter(function (n) { return !n.isError; })]);
    };
    InStorage.prototype.getTasks = function (ids, format) {
        var _this = this;
        return Observable.from([ids.map(function (tid) {
                var t = _this.taskObject(tid, format);
                if (!t) {
                    var err = new StorageError({ type: 'taskInTasks', taskId: tid });
                    console.log(err);
                    return err;
                }
                return t;
            }).filter(function (t) { return !t.isError; })]);
    };
    InStorage.prototype.has = function (id, type) {
        return this.data[type].has(id);
    };
    Object.defineProperty(InStorage.prototype, "labels", {
        get: function () {
            return Array.from(this.data.label.values());
        },
        enumerable: true,
        configurable: true
    });
    InStorage.prototype.postList2Posts = function (list) {
        var _this = this;
        var profileIds = list.profiles.map(function (p) { return p.id; });
        this.addObjects(list.profiles, 'profile');
        this.addObjects(list.posts.filter(function (p) { return profileIds.indexOf(p.author_id || p.authorid) >= 0; }), 'post');
        return list.posts.filter(function (p) { return profileIds.indexOf(p.author_id || p.authorid) >= 0; })
            .map(function (p) { return _this.postObject(p.id); });
    };
    InStorage.prototype.profileObject = function (id, format) {
        if (id && this.data.profile.has(id)) {
            if (format && (this.data.profile.get(id).format || 0) < format)
                return;
            return new Profile(this.data.profile.get(id), Array.from(this.data.label.values()));
        }
        ;
    };
    InStorage.prototype.postObject = function (id, format) {
        if (this.data.post.has(id)) {
            var jp = this.data.post.get(id);
            var u = this.profileObject(jp.authorid || jp.author_id, format);
            if (u && u instanceof Profile) {
                return new Post(jp, u);
            }
            else {
                var err = new StorageError({ type: 'userInPost', userId: jp.authorid || jp.author_id, postId: id });
                console.log(err);
                return err;
            }
            ;
        }
        ;
    };
    InStorage.prototype.notificationObject = function (id, format) {
        if (this.data.notification.has(id)) {
            var jn = this.data.notification.get(id);
            var u = this.profileObject(jn.post_author);
            var p = void 0;
            if (jn.post) {
                p = this.postObject(jn.post, format);
                if (!p || p instanceof StorageError) {
                    var err = new StorageError({ type: 'postInNotification', postId: jn.post, notificationId: id });
                    console.log(err);
                    return err;
                }
            }
            if (!u || u instanceof StorageError) {
                var err = new StorageError({ type: 'userInNotification', userId: jn.post_author, notificationId: id });
                console.log(err);
                return err;
            }
            return new Notification(jn, u, p);
        }
    };
    InStorage.prototype.taskObject = function (id, format) {
        if (id && this.data.task.has(id)) {
            var jt = this.data.task.get(id);
            var p = void 0;
            var n = void 0;
            var u = void 0;
            var ub = this.profileObject(jt.assigned_by, objectFormat.XS);
            var ut = this.profileObject(jt.assigned_to, objectFormat.XS);
            if (!ub || !ut || ub instanceof StorageError || ut instanceof StorageError) {
                var err = new StorageError({
                    type: 'userInTask',
                    userId: [jt.assigned_by, jt.assigned_to],
                    taskId: id
                });
                console.log(err);
                return err;
            }
            if (jt.post_id) {
                p = this.postObject(jt.post_id, format);
                if (!p || p instanceof StorageError) {
                    var err = new StorageError({ type: 'postInTask', postId: jt.post_id });
                    console.log(err);
                    return err;
                }
            }
            if (jt.notification_id) {
                n = this.notificationObject(jt.notification_id, format);
                if (!n || n instanceof StorageError) {
                    var err = new StorageError({
                        type: 'notificationInTask',
                        notificationId: jt.notification_id,
                        taskId: id
                    });
                    console.log(err);
                    return err;
                }
            }
            if (jt.profile_id) {
                u = this.profileObject(jt.profile_id, format);
                if (!u || u instanceof StorageError) {
                    var err = new StorageError({ type: 'profileInTask', userId: jt.profile_id });
                    console.log(err);
                    return err;
                }
            }
            return new Task(jt, n || p || u, ub, ut);
        }
    };
    return InStorage;
}());
export { InStorage };
