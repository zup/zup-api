import { Injectable } from "@angular/core";
import { ZupApiService } from './zupApi.service';
import { Lexical, lexicalServices } from '../models/lexical';
import { Observable, BehaviorSubject } from 'rxjs';
import { InError } from './apiAuth.service';
import { tools } from '../misc/utils';
var LexicalService = /** @class */ (function () {
    function LexicalService(api) {
        this.api = api;
        this.status = 0 /* NONE */;
        this.lexicals = new Map();
        this.loading = new BehaviorSubject(true);
    }
    LexicalService.prototype.init = function () {
        this.loadLexicals().subscribe();
    };
    LexicalService.prototype.getLexicalName = function (id) {
        if (this.status === 1 /* INITED */) {
            return this.lexicals.get(id).displayName;
        }
    };
    LexicalService.prototype.getLexical = function (id) {
        var _this = this;
        if (this.status === 1 /* INITED */) {
            return Observable.of(this.lexicals.get(id));
        }
        else {
            return this.loading.map(function () {
                return _this.lexicals.get(id);
            });
        }
        ;
    };
    LexicalService.prototype.getLexicals = function () {
        var _this = this;
        var categories = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            categories[_i] = arguments[_i];
        }
        if (this.status === 1 /* INITED */) {
            var lexicals_1 = [];
            if (categories.length === 0) {
                lexicals_1 = this.getLexicalsArray();
            }
            else {
                categories.forEach(function (category) {
                    lexicals_1 = lexicals_1.concat(_this.getLexicalsArray(category));
                });
            }
            return Observable.of(lexicals_1);
        }
        else {
            return this.loading.map(function () {
                var lexicals = [];
                if (categories.length === 0) {
                    lexicals = _this.getLexicalsArray();
                }
                else {
                    categories.forEach(function (category) {
                        lexicals = lexicals.concat(_this.getLexicalsArray(category));
                    });
                }
                ;
                return lexicals;
            });
        }
    };
    LexicalService.prototype.getLexicals2 = function (options) {
        var _this = this;
        var lexicals = [];
        var filterLexical = function (v, k) {
            var isInFiter = true;
            if ((tools.deepGet(options, 'categories') || []).length > 0 && tools.deepGet(options, 'categories').indexOf(v.category) < 0) {
                isInFiter = false;
            }
            if ((tools.deepGet(options, 'services') || []).length > 0 && tools.deepGet(options, 'services').indexOf(v.service) < 0) {
                isInFiter = false;
            }
            if (isInFiter) {
                lexicals.push(v);
            }
        };
        if (this.status === 1 /* INITED */) {
            if (!tools.deepGet(options, 'categories', 'length') && !tools.deepGet(options, 'services', 'length')) {
                lexicals = Array.from(this.lexicals.values());
            }
            else {
                this.lexicals.forEach(filterLexical);
            }
            return Observable.of(lexicals);
        }
        else {
            return this.loading.map(function () {
                if (!tools.deepGet(options, 'categories', 'length') && !tools.deepGet(options, 'services', 'length')) {
                    lexicals = Array.from(_this.lexicals.values());
                }
                else {
                    _this.lexicals.forEach(filterLexical);
                }
                return lexicals;
            });
        }
    };
    LexicalService.prototype.loadLexicals = function (category) {
        var _this = this;
        this.status = 0 /* NONE */;
        this.deleteLexicals(category);
        return this.api.lexical.list(category).map(function (res) {
            res.forEach(function (l) { return _this.lexicals.set(l.id, l); });
            _this.status = 1 /* INITED */;
            _this.loading.next(true);
        });
    };
    LexicalService.prototype.createLexical = function (kwds, name, service, mult) {
        var _this = this;
        if (service === void 0) { service = lexicalServices.STANDARD; }
        return this.api.lexical.create({ displayName: name, values: kwds, service: service, mult: mult }).map(function (l) { _this.lexicals.set(l.id, l); return l; });
    };
    LexicalService.prototype.updateLexical = function (lexical, name, values) {
        var _this = this;
        if (typeof lexical === "string") {
            if (!name || !values || values.length === 0)
                return Observable.throw(new InError('MISSING_FIELD(name or values)'));
            return this.getLexical(lexical).flatMap(function (l) {
                l.displayName = name;
                l.values = values;
                return _this.api.lexical.update([l]).map(function () { _this.lexicals.set(l.id, l); return l; });
            });
        }
        else {
            return this.api.lexical.update([lexical]).map(function () { _this.lexicals.set(lexical.id, lexical); return lexical; });
        }
    };
    LexicalService.prototype.deleteLexicals = function (category) {
        var _this = this;
        if (!category) {
            this.lexicals.clear();
        }
        else {
            this.lexicals.forEach(function (v, k) {
                if (v.category === category) {
                    _this.lexicals.delete(k);
                }
            });
        }
    };
    LexicalService.prototype.getLexicalsArray = function (category) {
        var lexicalsArray = [];
        this.lexicals.forEach(function (v, k) {
            if (!category || v.category === category)
                lexicalsArray.push(v);
        });
        return lexicalsArray;
    };
    LexicalService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    LexicalService.ctorParameters = function () { return [
        { type: ZupApiService, },
    ]; };
    return LexicalService;
}());
export { LexicalService };
