import { ZupApiService } from './zupApi.service';
import { Lexical } from '../models/lexical';
import { Observable } from 'rxjs';
export declare class LexicalService {
    private api;
    private status;
    private lexicals;
    private loading;
    constructor(api: ZupApiService);
    init(): void;
    getLexicalName(id: string): string;
    getLexical(id: string): Observable<Lexical>;
    getLexicals(...categories: string[]): Observable<Lexical[]>;
    getLexicals2(options: {
        categories?: string[];
        services?: string[];
    }): Observable<Lexical[]>;
    loadLexicals(category?: string): Observable<void>;
    createLexical(kwds: string[], name: string, service?: string, mult?: number): Observable<Lexical>;
    updateLexical(lexical: Lexical | string, name?: string, values?: string[]): Observable<Lexical>;
    private deleteLexicals(category?);
    private getLexicalsArray(category?);
}
