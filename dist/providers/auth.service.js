import { Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs';
//import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiAuthService } from './apiAuth.service';
import { PartnerService, STATUS } from './partner.service';
import { ApiConfig } from './api.config';
import { Partner } from '../models';
/**
 * AuthService manages access to views (it implements "CanActivate") and login
 *
 * If access is refused, the user is redirected to ApiConfig.loginRoute
 * Anonymous access are allowed to routes with a data.anonymous = true
 *
 * ```
 * route = {
 *    path:'',
 *    component: WallComponent,
 *    canActivate: [AuthService],
 *    data: {
 *      anonymous: true
 *    }
 *  }
 * ```
 */
var AuthService = /** @class */ (function () {
    function AuthService(apiAuthService, partner, 
        //private router: Router,
        config) {
        this.apiAuthService = apiAuthService;
        this.partner = partner;
        this.userId = undefined;
        this.isAnonymous = false;
        if (config) {
            if (typeof config === 'function') {
                config = config();
            }
            // TEST I4U :
            this._application = config.application;
        }
    }
    AuthService.prototype.canActivate = function (data) {
        var _this = this;
        data = data || {};
        var anonymous = data.anonymous;
        var rights = data.rights;
        if (this.partner.status === STATUS.INITATING) {
            return this.partner.widgetsListChange.first().map(function () {
                return (!rights || (rights && _this.partner.hasRightsFor(rights)));
            });
        }
        if (!this.isLoggedIn(anonymous, rights)) {
            if (this._application) {
                switch (this._application.id) {
                    case 'I4U':
                        return this.checkI4U()
                            .flatMap(function (loginInfo) { return _this.autoLog(loginInfo, _this._application.rights); })
                            .catch(function (err) {
                            return Observable.of(false);
                        });
                }
            }
            var currentLogin = getCookie('currentLogin');
            if (currentLogin) {
                // .length !== 0
                return this.autoLog(JSON.parse(currentLogin), rights, anonymous);
            }
            else if (anonymous) {
                return this.apiAuthService.logout().map(function () {
                    _this.isAnonymous = true;
                    return true;
                });
            }
            else {
                return Observable.from([false]);
            }
        }
        else if (rights) {
            return this.partner.onInit().map(function () {
                return (!rights || (rights && _this.partner.hasRightsFor(rights)));
                /*
                         this.parentRouter.navigate(['Deny']); // FIXME this route doesn't exist...
                         */
            });
        }
        return this.partner.onInit().map(function () { return true; });
    };
    AuthService.prototype.checkI4U = function () {
        var _this = this;
        var clientIdMatch = (/[?&]client_id=([^&]*)/).exec(window.location.search);
        var clientId;
        if (clientIdMatch)
            clientId = clientIdMatch[1];
        if (clientId) {
            var url_1 = '/i4u/binfluence/connect';
            var message_1 = {
                client_id: clientId
            };
            return this.apiAuthService.logout().flatMap(function () {
                return _this.apiAuthService.call(url_1, message_1, { apiid: 'b2bapi' }).map(function (res) {
                    return { "rive": { "login": res.json().member_name, "userkey": res.json().member_hash } };
                });
            });
        }
        else {
            return Observable.of({ "rive": { "login": "", "userkey": "" } });
        }
    };
    AuthService.prototype.isLoggedIn = function (anonymous, rights) {
        if (rights && rights.length > 0) {
            if (this.userId !== undefined) {
                return this.partner.hasRightsFor(rights);
            }
            return false;
        }
        return this.userId !== undefined || (anonymous && this.isAnonymous === anonymous);
    };
    AuthService.prototype.hasRole = function (roles) {
        return true;
    };
    AuthService.prototype.login = function (username, password, rememberMe) {
        var _this = this;
        return this.apiAuthService.login(username, password, rememberMe).flatMap(function (userInformations) {
            if (!userInformations.partnerId)
                throw ('MEMBER_NOT_PARTNER()');
            return _this._setUser(userInformations);
        });
    };
    AuthService.prototype.logout = function () {
        return this.apiAuthService.logout().map(function (res) {
            document.cookie = 'currentLogin=;path=/';
            document.location.reload();
            // return this._setUser({id : undefined});
        });
    };
    AuthService.prototype.autoLog = function (loginInformation, rights, anonymous) {
        var _this = this;
        if (anonymous === void 0) { anonymous = false; }
        return this.apiAuthService.autoLog(loginInformation)
            .flatMap(function (userInformations) {
            return _this._setUser(userInformations);
        })
            .flatMap(function () {
            return _this.partner.onInit().map(function () {
                return (!rights || (rights && _this.partner.hasRightsFor(rights)));
            });
        })
            .catch(function (err) {
            if (anonymous) {
                return _this.apiAuthService.logout().map(function () {
                    _this.isAnonymous = true;
                    return true;
                });
            }
            return Observable.from([false]);
        });
    };
    AuthService.prototype._setUser = function (userInformations) {
        var _this = this;
        this.userId = userInformations.id;
        return this.partner.initPartner(this.userId).map(function (p) {
            if (p.roUsers.indexOf(_this.userId) >= 0) {
                _this.apiAuthService.RO = true;
            }
            else {
                _this.partner.addRight('write');
            }
            _this.apiAuthService.isAdmin = _this.partner.isAdmin;
            return _this.userId;
        });
    };
    AuthService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: ApiAuthService, },
        { type: PartnerService, },
        { type: ApiConfig, decorators: [{ type: Optional },] },
    ]; };
    return AuthService;
}());
export { AuthService };
// TODO move to the right place
// https://github.com/salemdar/angular2-cookie
// https://github.com/BCJTI/ng2-cookies
function getCookie(cname) {
    var name = cname + '=';
    var ca = document.cookie.split(';');
    for (var i = 0, j = ca.length; i < j; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1);
        if (c.indexOf(name) === 0)
            return c.substring(name.length, c.length);
    }
    return '';
}
