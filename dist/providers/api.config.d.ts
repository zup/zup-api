export declare type jsonConfig = {
    apiUrls?: any;
    oauth?: any;
    loginRoute?: any;
    keys?: any;
    application?: {
        id;
        location?;
        referrer?;
        rights: string[];
        loginRoute: string[];
    };
};
/**
 * Configration of the Api
 *
 *
 */
export declare class ApiConfig {
    apiUrls: any;
    oauth: any;
    loginRoute: any[];
    keys: any;
    application: {
        id;
        location?;
        referrer?;
        rights: string[];
        loginRoute: string[];
    };
    /**
     *
     * @param {any} apiUrls access url to the different apis (cold, hot, b2b). Default: dev apis
     * @param {any} oauth oAuth information of linked social network for publish functionalities
     * @param {any} loginRoute route for the redirection of unlogged users.
     */
    constructor(config?: jsonConfig);
}
