import { Observable } from 'rxjs';
import { ApiAuthService } from './apiAuth.service';
import { PartnerService } from './partner.service';
import { ApiConfig } from './api.config';
/**
 * AuthService manages access to views (it implements "CanActivate") and login
 *
 * If access is refused, the user is redirected to ApiConfig.loginRoute
 * Anonymous access are allowed to routes with a data.anonymous = true
 *
 * ```
 * route = {
 *    path:'',
 *    component: WallComponent,
 *    canActivate: [AuthService],
 *    data: {
 *      anonymous: true
 *    }
 *  }
 * ```
 */
export declare class AuthService {
    private apiAuthService;
    private partner;
    userId: string;
    isAnonymous: boolean;
    private _application;
    constructor(apiAuthService: ApiAuthService, partner: PartnerService, config: ApiConfig);
    canActivate(data?: {
        anonymous?: boolean;
        rights?: string[];
    }): Observable<boolean>;
    checkI4U(): Observable<{
        rive: {
            login: string;
            userkey: string;
        };
    }>;
    isLoggedIn(anonymous?: boolean, rights?: string[]): boolean;
    hasRole(roles: string[]): boolean;
    login(username: string, password: string, rememberMe?: boolean): Observable<string>;
    logout(): Observable<void>;
    autoLog(loginInformation: any, rights: any, anonymous?: boolean): Observable<boolean>;
    private _setUser(userInformations);
}
