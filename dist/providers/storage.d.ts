import { Observable } from 'rxjs';
import { ZupApiService } from './zupApi.service';
import { Post, Task, Notification, Profile } from '../models';
export declare const objectFormat: {
    XS: number;
    S: number;
    L: number;
    XL: number;
};
export declare class StorageError {
    type: string;
    isError: boolean;
    postId?: string;
    userId?: string | string[];
    notificationId?: string;
    taskId?: string;
    constructor(values?: any);
}
export declare class InStorage {
    private api;
    private data;
    private objectTypes;
    constructor(api: ZupApiService);
    addObjectWithFormat(jsonObject: any, type: string, format?: number): void;
    addObject(jsonObject: any, type: string): void;
    addObjects(jsonObjects: any[], type: string, format?: number): void;
    addPostList(list: {
        posts: any[];
        posts_classified_negative?: string[];
        posts_classified_positive?: string[];
        profiles?: any[];
    }): void;
    getJsonObjects(ids: string[], type: string): any[];
    /**
     *
     * @param id
     * @param {boolean} useCache if true, force user from cache, if false, ask api, if undefined check cache then api
     * @returns {Observable<R>}
     */
    getProfile(id: string, useCache?: boolean, format?: number): Observable<Profile | StorageError>;
    /**
     *
     * @param {string[]} ids : list of profils ids
     * @param { boolean} useCache : if true: only search cache, if false: only search remote, undefined: search both
     * @returns {Observable<Profile[]>}
     */
    getProfiles(ids: string[], useCache?: boolean, format?: number): Observable<(Profile)[]>;
    getPosts(ids: string[], format?: number): Observable<Post[]>;
    getNotifications(ids: string[], format?: number): Observable<Notification[]>;
    getTasks(ids: string[], format?: number): Observable<Task[]>;
    has(id: string, type: string): boolean;
    readonly labels: any[];
    postList2Posts(list: {
        posts: any[];
        profiles: any[];
    }): Post[];
    profileObject(id: string, format?: number): Profile | StorageError;
    postObject(id: string, format?: number): Post | StorageError;
    notificationObject(id: string, format?: number): Notification | StorageError;
    taskObject(id: string, format?: number): Task | StorageError;
}
