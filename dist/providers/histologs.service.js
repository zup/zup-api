import { Injectable } from "@angular/core";
import { ZupApiService } from './zupApi.service';
import { ActionLogDetail, ActionLog, Campaign, ObjectsList, Profile, WidgetContext } from '../models/';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { dates } from '../misc/utils';
var HistoLogsService = /** @class */ (function () {
    function HistoLogsService(api) {
        var _this = this;
        this.api = api;
        this._status = 0 /* NONE */;
        this.isInited = new Subject();
        this.histoLogs = new Map();
        this.created = [];
        this.logsByPost = new Map();
        this.logsByProfile = new Map();
        this.logsByCampaign = new Map();
        this.autoPulishToCampaign = new Map();
        this.loading = new BehaviorSubject(true);
        this.logFrom = Infinity;
        this.logTo = 0;
        this.sortLogs = function (a, b) { return _this.histoLogs.get(a).created - _this.histoLogs.get(b).created; };
    }
    Object.defineProperty(HistoLogsService.prototype, "status", {
        get: function () { return this._status; },
        enumerable: true,
        configurable: true
    });
    ;
    HistoLogsService.prototype.init = function () {
        var _this = this;
        this.api.campaigns.list().subscribe(function (aps) {
            aps.forEach(function (ap) { _this.autoPulishToCampaign.set(ap.id, ap.profiles_label_id); });
            var initDR = dates.last30Days;
            //const initDR = dates.all;
            //const initDR = dates.all;
            _this.loadHistoLogs(dates.toZIFromDate(initDR.from), dates.toZIFromDate(initDR.to)).subscribe(function () {
                _this._status = 1 /* INITED */;
                _this.isInited.next(true);
            });
        });
    };
    HistoLogsService.prototype.onInit = function (callback) {
        if (this._status === 1 /* INITED */) {
            callback();
        }
        else {
            this.isInited.subscribe(function () { return callback(); });
        }
    };
    HistoLogsService.prototype.loadHistoLogs = function (after, before) {
        var _this = this;
        this._status = 0 /* NONE */;
        this.logFrom = Math.min(this.logFrom, after);
        this.logTo = Math.max(this.logTo, before);
        var wc = new WidgetContext(null, { after: after, before: before });
        return this.api.logs.list(wc, {}, { limit: 1000 }).flatMap(this.api.getNextPageFunction()).map(function (res) {
            Array.from(res).forEach(function (l) {
                if (!_this.histoLogs.has(l.id)) {
                    _this.histoLogs.set(l.id, l);
                    _this.created.push({ id: l.id, date: l.created, campaignId: l.campaignId, profileId: l.profileId });
                }
                else {
                    return;
                }
                if (l.postId) {
                    if (!_this.logsByPost.has(l.postId))
                        _this.logsByPost.set(l.postId, []);
                    var logs = _this.logsByPost.get(l.postId);
                    if (logs.indexOf(l.postId) < 0)
                        logs.push(l.id);
                }
                if (l.profileId) {
                    if (!_this.logsByProfile.has(l.profileId))
                        _this.logsByProfile.set(l.profileId, []);
                    var logs = _this.logsByProfile.get(l.profileId);
                    if (logs.indexOf(l.profileId) < 0)
                        logs.push(l.id);
                }
                if (l.campaignId) {
                    if (!_this.logsByCampaign.has(l.campaignId))
                        _this.logsByCampaign.set(l.campaignId, []);
                    var logs = _this.logsByCampaign.get(l.campaignId);
                    if (logs.indexOf(l.campaignId) < 0)
                        logs.push(l.id);
                }
            });
            _this.created.sort(function (a, b) { return b.date - a.date; });
        });
    };
    HistoLogsService.prototype.getCampaignProfiles = function (id) {
        var _this = this;
        /*      let profiles = [];
                const profileEngaged: Map<string, number> = new Map<string, number>()
        
                campaign.autoPublish.forEach(p => {
                    profiles = profiles.concat(this.logsByCampaign.get(p.id))
                })
        */
        if (!this.logsByCampaign.has(id))
            return Observable.of(new ObjectsList([]));
        var profileEngaged = new Map();
        var profileIds = this.logsByCampaign.get(id).sort(this.sortLogs).map(function (lid) {
            var l = _this.histoLogs.get(lid);
            profileEngaged.set(l.profileId, l.created);
            return l.profileId;
        });
        return this.api.profiles.getMultiple(profileIds).map(function (profiles) {
            return new ObjectsList(profiles.map(function (p) {
                p.customValue = profileEngaged.get(p.id);
                return p;
            }));
        });
    };
    HistoLogsService.prototype.isProfileInCampaign = function (pid, campaign) {
        var profileLogs = this.logsByProfile.get(pid);
        if (!profileLogs || profileLogs.length === 0)
            return false;
        var paIds = campaign.publishActions.map(function (pa) { return pa.id; });
        for (var i = 0; i < profileLogs.length; i++) {
            if (paIds.indexOf(this.histoLogs.get(profileLogs[i]).campaignId) >= 0)
                return true;
        }
        return false;
    };
    ;
    HistoLogsService.prototype.getProfileLogs = function (pid, last) {
        var _this = this;
        if (last === void 0) { last = true; }
        if (!this.logsByProfile.has(pid))
            return [];
        var lids = this.logsByProfile.get(pid).sort(this.sortLogs);
        var myIds = last ? lids.slice(0, 1) : lids;
        return myIds.map(function (id) { return _this.histoLogs.get(id); }).filter(function (l) { return !!l; });
    };
    HistoLogsService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    HistoLogsService.ctorParameters = function () { return [
        { type: ZupApiService, },
    ]; };
    return HistoLogsService;
}());
export { HistoLogsService };
