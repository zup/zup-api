import { Observable } from 'rxjs/Rx';
import { ApiAuthService } from './apiAuth.service';
import { ApiConfig } from './api.config';
import { ActionLog, ActionLogDetail, Campaign, Label, Lexical, Influencer, Member, NewLexical, Notification, ObjectsList, ObjectsListLC, Partner, Post, Profile, PublishAction, SimpleProfile, Task, TaskEvent, View, Wall, wallJsonPost, Widget, WidgetContext } from '../models';
import { InStorage } from './storage';
export declare enum INFLUENCER_ANALYTICS {
    NB_PUB = 1,
    FOLLOWERS = 2,
    FOLLOWINGS = 3,
    GENGER = 4,
    INTERESTS = 5,
    AGE = 6,
    LIVESIN = 7,
    LANGUAGE = 8,
    ENGAGEMENT_RATE = 9,
}
export declare class ZupApiService {
    private api;
    private oauthKeys;
    localCache: InStorage;
    private _config;
    constructor(api: ApiAuthService, config: ApiConfig);
    getConfig(key: string): any;
    getNextPageFunction(maxResults?: number, maxQueries?: number): (ol: ObjectsList<any>) => Observable<ObjectsList<any>>;
    loadFiles(files: {
        id?;
        url;
    }[]): Observable<{
        id;
        url;
    }[]>;
    actions: {
        parent: ZupApiService;
        api: ApiAuthService;
        likePost: (postId: string, like?: boolean, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            userId?: string;
            socialAccount?: string;
        }) => Observable<boolean>;
        commentPost: (postId: string, comment: string, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            userId?: string;
            socialAccount?: string;
            profileName?: string;
            files?: any[];
        }) => Observable<boolean>;
        retweetPost: (postId: string, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            userId?: string;
            socialAccount?: string;
        }) => Observable<boolean>;
        tweet: (tweet: string, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            socialAccount?: string;
            userId?: string;
            files?: any[];
        }) => Observable<boolean>;
        followUser: (userId: string, follow?: boolean, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            postId?: string;
            socialAccount?: string;
        }) => Observable<boolean>;
        clearAlert: (alertId: string) => any;
        tweetIsOk(message: {
            text: string;
            files: any[];
        }): boolean;
        mention: (tweet: string, profileName: string, options?: {
            notificationId?: string;
            taskId?: string;
            screen?: string;
            widgetId?: string;
            socialAccount?: string;
            userId?: string;
            files?: any[];
        }) => Observable<boolean>;
        multiple(publishes: any[]): Observable<boolean>;
    };
    campaigns: {
        api: ApiAuthService;
        parent: ZupApiService;
        add: (campaign: Campaign) => Observable<Campaign>;
        delete: (campaign: Campaign, deleteLabel?: boolean) => Observable<boolean>;
        list: () => Observable<any[]>;
        listProfiles: (campaign: Campaign, options?: {
            after?: number;
            before?: number;
        }) => Observable<ObjectsListLC<Profile>>;
        update: (campaign: Campaign, partnerId?: string) => Observable<Campaign>;
    };
    influencers: {
        api: ApiAuthService;
        parent: ZupApiService;
        analytics: (iid: string) => Observable<{
            gender?: any;
            interests?: any;
            followings?: any;
            age?: any;
            language?: any;
            livesin?: any;
            engagementRate?: any;
            engagements?: any;
            nbPosts?: any;
        }>;
        analyticHistory(iid: string, widgetContext: WidgetContext, analyticType: INFLUENCER_ANALYTICS): Observable<{
            date: number;
            analytics: any;
        }[]>;
        get: (iid: string, onI4U?: boolean) => Observable<Influencer>;
        multiProfiles: (iid: string) => Observable<Profile[]>;
    };
    /**
     * Exposes methodes to access and manage labels
     *
     * @Method add
     *
     *
     */
    labels: {
        api: ApiAuthService;
        parent: ZupApiService;
        add: (label: Label) => Observable<Label>;
        addProfileToLabel: (userId: string | string[], labelId: string, options?: {
            widgetId?: string;
            screen?: string;
            postId?: string;
        }) => any;
        addProfilesToLabel: (profilIds: string[], labelId: string) => Observable<boolean>;
        delete: (id: string) => Observable<boolean>;
        get: (id: string) => Observable<Label>;
        list: (listOptions?: {
            limit?: number;
            cursor?: string;
        }) => Observable<ObjectsList<Label>>;
        listAll: () => Observable<ObjectsList<Label>>;
        profileLabels: (userId: string) => Observable<Label[]>;
        profilesList: (labelId: string, limitOptions?: {
            limit?: number;
            cursor?: string;
        }, options?: {
            onlyIds?: boolean;
            simpleProfiles?: boolean;
            onI4U?: boolean;
        }) => Observable<ObjectsList<string | Profile>>;
        remove: (id: string) => Observable<boolean>;
        removeProfileFromLabel: (userId: string, labelId: string) => Observable<boolean>;
        removeProfilesFromLabel: (userIds: string[], labelId: string) => Observable<boolean>;
        update: (label: Label) => Observable<boolean>;
    };
    lexical: {
        api: ApiAuthService;
        self: ZupApiService;
        delete: (id: string) => Observable<boolean>;
        delete_multiple: (ids: string[]) => Observable<boolean>;
        get: (ids: string[]) => Observable<Lexical[]>;
        list: (category?: string) => Observable<Lexical[]>;
        set: (lexicals: NewLexical[]) => Observable<Lexical[]>;
        create: (lexical: NewLexical) => Observable<Lexical>;
        update: (lexicals: Lexical[]) => Observable<boolean>;
    };
    logs: {
        api: ApiAuthService;
        parent: ZupApiService;
        get(actionLog: ActionLog): Observable<ActionLogDetail>;
        list(widgetContext: WidgetContext, filters?: {
            memberId?: string;
            profileId?: string;
            postId?: string;
            campaignId?: string;
            token?: string;
            publishType?: string;
        }, listOptions?: {
            limit?: number;
            cursor?: string;
        }): Observable<ObjectsList<ActionLog>>;
        listDetail(widgetContext: WidgetContext, options?: {
            memberId?: string;
            profileId?: string;
            postId?: string;
        }, listOptions?: {
            limit?: number;
            cursor?: string;
        }): Observable<ObjectsList<ActionLogDetail>>;
    };
    /**
     *
     * Exposes member functions : invite, list, login, logout, remove
     */
    members: {
        api: ApiAuthService;
        parent: ZupApiService;
        invite(mail: string): Observable<boolean>;
        list(): Observable<Member[]>;
        remove(id: string): Observable<boolean>;
    };
    misc: {
        api: ApiAuthService;
        parent: ZupApiService;
        uploadPhoto: (data: string) => Observable<string>;
        listPhoto: () => Observable<any>;
        mstores: {
            api: ApiAuthService;
            parent: ZupApiService;
            list: () => Observable<any>;
            get: (key: string) => Observable<any>;
            put: (key: string, value: any) => Observable<boolean>;
            delete: (key: string) => Observable<boolean>;
        };
    };
    notifications: {
        api: ApiAuthService;
        parent: ZupApiService;
        assign: (notification: Notification, memberId: string, assignmentMessage?: string, options?: {
            widgetId?: string;
            profileId?: string;
            postId?: string;
        }) => Observable<boolean>;
        close: (id: string) => Observable<boolean>;
        closeMultiple: (notificationIds: string[]) => Observable<boolean>;
        count: (after?: number) => Observable<any[]>;
        get: (id: string) => Observable<any>;
        list: (listOptions: {
            limit?: number;
            cursor?: string;
        }, options?: {
            widgetId?: string;
            type?: string;
            after?: number;
            before?: number;
            isClosed?: boolean;
            widgetType?: number;
        }) => Observable<any>;
        setLimits: (limits: any) => Observable<any>;
        addSubscribtion: (emailOrPhone: string, type?: string, owner?: string) => Observable<boolean>;
        listSubscriptions: () => Observable<any[]>;
        toggleNotification: (emailOrPhone: string, notificationType: string, enable?: boolean) => Observable<boolean>;
    };
    partner: {
        api: ApiAuthService;
        parent: ZupApiService;
        addInstagramToken: (token: any) => Observable<Profile>;
        addOauthToken: (token: any, verifier: any, provider: any) => Observable<any>;
        addTwitterToken: (token: any, secret: any) => Observable<Profile>;
        addFacebookToken: (token: any) => Observable<Profile[]>;
        deleteToken: (tokenUserId: string) => any;
        get(): Observable<any>;
        getOAuthUrl(provider: string): Observable<string>;
        listTokens: () => Observable<any[]>;
        setAccessToken(data: any): Observable<Profile>;
        update(partner: Partner): Observable<boolean>;
        widgetsList(limit?: number): Observable<Widget[]>;
    };
    places: {
        api: ApiAuthService;
        parent: ZupApiService;
        exploreZone(zone: {
            e: number;
            w: number;
            s: number;
            n: number;
        }): Observable<boolean>;
        exclude(placeId: string, widgetId: string[], ban?: boolean): Observable<boolean>;
        getMultiple(ids: string[]): Observable<SimpleProfile[]>;
        listExcluded(widgetId: string): Observable<Profile[]>;
        searchInZone: (zone: {
            e: number;
            w: number;
            s: number;
            n: number;
        }, listOptions?: {
            limit?: number;
            cursor?: number;
        }) => Observable<ObjectsList<Profile>>;
    };
    posts: {
        api: ApiAuthService;
        parent: ZupApiService;
        list: (widgetContext: WidgetContext, options?: {
            author_id?: string;
            source?: string;
            tag?: string;
            terms?: string[];
            lang?: string;
            post_label_id?: string;
            engagement_rate_more_than?: number;
            sentiment_value?: number;
            order_by?: string;
            xscore_more_than?: number;
        }, listOptions?: {
            limit?: number;
            cursor?: string;
        }) => Observable<{
            posts: ObjectsList<Post>;
            total: number;
        }>;
        markers: (widgetContext: WidgetContext, options?: {
            authorId?: string;
            source?: string;
            tag?: string;
        }, listOptions?: {
            limit?: number;
            cursor?: string;
        }) => Observable<{
            posts: ObjectsList<Post>;
            total: number;
        }>;
        getMultiple: (ids: string[], format?: number) => Observable<Post[]>;
        loadToCache(ids: string[], format?: number): Observable<boolean>;
        getSentiment(ids: string[]): Observable<{
            positive: number;
            negative: number;
            neutral: number;
        }>;
        setSentiment(id: string, sentiment: number, widgetId?: string): Observable<boolean>;
    };
    profiles: {
        api: ApiAuthService;
        parent: ZupApiService;
        exclude(profileId: string | string[], widgetIds?: string[], ban?: boolean, banFromPartner?: boolean): Observable<boolean>;
        exportFilter(listOptions: {
            limit?: number;
            cursor?: string;
        }, widgetContext: WidgetContext): Observable<ObjectsListLC<Profile>>;
        filter(listOptions: {
            limit?: number;
            cursor?: string;
        }, widgetContext: WidgetContext): Observable<{
            list: ObjectsList<Profile>;
            total: number;
        }>;
        get: (id: string, useCache?: boolean) => Observable<Profile>;
        getMultiple: (ids: string[], format?: number, options?: {
            widgetId?: string;
            showExclude?: boolean;
            onI4U?: boolean;
        }) => Observable<Profile[]>;
        listExcluded(widgetId?: string): Observable<{
            widgetId: string;
            profile: Profile;
        }[]>;
        listExcludedGlobal(): Observable<ObjectsListLC<Profile>>;
        listEngaged(widgetId: string, filterOptions: any, listOptions?: {
            limit?: number;
            cursor?: string;
        }): Observable<ObjectsList<Profile>>;
        listInterests(): Observable<string[]>;
        loadToCache(userIds: string[], format?: number): Observable<boolean>;
        searchAll(query: string, limit?: number, cursor?: number): Observable<ObjectsList<Profile>>;
        setGender(ids: string[], gender: string): Observable<boolean>;
    };
    tasks: {
        api: ApiAuthService;
        parent: ZupApiService;
        assignToPost: (postId: string, assignTo: string, assignmentMessage: string, screen?: string, widgetId?: string) => Observable<boolean>;
        assignToUser: (userId: string, assignTo: string, assignmentMessage: string, screen?: string, widgetId?: string) => Observable<boolean>;
        close: (taskId: string) => Observable<boolean>;
        getDetails: (taskIds: string[]) => Observable<Task[]>;
        getMultiple: (taskIds: string[], format?: number) => Observable<Task[]>;
        list: (listOptions?: {
            limit?: number;
            cursor?: string;
        }, options?: {
            widgetId?: string;
            widgetType?: string;
            after?: number;
            before?: number;
            assignedTo?: string;
            closedTasks?: boolean;
            openedTasks?: boolean;
        }) => Observable<ObjectsList<Task>>;
        updateEvent: (taskEvent: TaskEvent) => Observable<TaskEvent>;
        createEvent: (taskEvent: TaskEvent, screen?: string) => Observable<TaskEvent>;
        listEvents: (options: {
            from: number;
            to: number;
            profile_id?: string;
            widget_id?: string;
        }) => Observable<TaskEvent[]>;
    };
    view: {
        api: ApiAuthService;
        parent: ZupApiService;
        create: (view: View) => Observable<View>;
        delete: (view: View) => Observable<string>;
        update: (view: View, originalView: View) => Observable<View>;
    };
    wall: {
        api: ApiAuthService;
        parent: ZupApiService;
        addWidget: (widgetId: string, wallId: string) => Observable<boolean>;
        create: (wall: Wall, partnerName: string) => Observable<Wall>;
        delete: (id: string) => Observable<boolean>;
        get: (id: string) => Observable<Wall>;
        list: () => Observable<Wall[]>;
        posts: (wallId: string, dateRange?: {
            from: Date;
            to: Date;
        }, listOptions?: {
            limit: number;
            cursor?: string;
        }, objectListOptions?: any) => Observable<ObjectsList<wallJsonPost>>;
        removeWidget: (widgetId: string, wallId: string) => Observable<boolean>;
        search: (wallName: string, partnerName: string) => Observable<Wall>;
        update: (wall: Wall) => Observable<boolean>;
        nbActions: (wallId: string, dtFrom: number, dtTo: number) => Observable<{
            likes: number;
            comments: number;
            shares: number;
        }>;
        nbPosts: (wallId: string, dtFrom?: number, dtTo?: number, options?: any) => Observable<number>;
        topTags: (wallId: string, nb?: number, dtFrom?: number, dtTo?: number) => Observable<{
            payload: string;
            total: number;
        }[]>;
        topUsers: (wallId: string, nb?: number, dtFrom?: number, dtTo?: number) => Observable<Profile[]>;
    };
    widget: {
        api: ApiAuthService;
        parent: ZupApiService;
        addPublishAction(publishAction: PublishAction): Observable<boolean>;
        create: (widget: Widget, isI4U?: boolean) => Observable<Widget>;
        delete: (widget: Widget) => Observable<string>;
        deletePublishAction(publishAction: PublishAction): Observable<boolean>;
        get: (wid: string) => Observable<Widget>;
        filterShares(shares: boolean, widgetId: string): Observable<boolean>;
        getExcluded(widgetId: string): Observable<{
            places: string[];
            profiles: string[];
        }>;
        list(limit?: number): Observable<Widget[]>;
        listProfiles(widget: Widget): Observable<Profile[]>;
        pause(widget: Widget, pause: boolean): Observable<Widget>;
        preview(widget: Widget, fortest?: boolean): Observable<{
            posts: Post[];
            total: number;
            ok?: boolean;
        }>;
        update: (widget: Widget, originalWidget?: Widget) => Observable<Widget>;
        updateCategory(widgetId: string, category: number): Observable<boolean>;
        updateExcludedProfiles(widget: any, originalWidget?: Widget): Observable<boolean>;
        updateExcludeTerms(widget: Widget, originalWidget?: Widget): Observable<boolean>;
        updateAlerts(widget: Widget, originalWidget?: Widget): Observable<boolean>;
        updatePublishActions(widget: Widget, originalWidget?: Widget): Observable<boolean>;
        updateView(widget: Widget, originalWidget?: Widget): Observable<Widget>;
        updateWidgetInfo(widget: Widget, originalWidget?: Widget): Observable<boolean>;
    };
    widgetAnalytics: {
        parent: ZupApiService;
        api: ApiAuthService;
        countByProfile: (widgetContext: WidgetContext, limit?: number, cursor?: string) => Observable<ObjectsList<{
            profile: any;
            posts: any;
            likes: any;
            shares: any;
            comments: any;
        }>>;
        customerLead: (widgetContext: WidgetContext, limit?: number) => Observable<ObjectsList<Profile>>;
        distinctReach: (widgetContext: WidgetContext) => Observable<number>;
        engagements: (widgetContext: WidgetContext) => Observable<{
            posts: any;
            likes: any;
            comments: any;
            shares: any;
        }>;
        engagementsTimeline: (widgetContext: WidgetContext) => Observable<any>;
        genderSoV: (widgetContext: WidgetContext) => Observable<{
            name: string;
            count: number;
        }[]>;
        influencers: (widgetContext: WidgetContext, limit?: number) => Observable<ObjectsList<Profile>>;
        insterestsSoV: (widgetContext: WidgetContext) => Observable<{
            name: string;
            count: number;
        }[]>;
        lexicalsCount: (widgetContext: WidgetContext, filterOptions?: {
            lexical_id?: string;
            profile_id?: string;
        }) => any;
        lexicalsTimeline: (widgetContext: WidgetContext, filterOptions?: {
            lexical_id?: string;
            profile_id?: string;
        }) => any;
        nbPosts: (widgetContext: WidgetContext) => Observable<number>;
        payloadToUsers: (payloads: any[], scoreName?: string) => Observable<Profile[]>;
        postsTimeline: (widgetContext: WidgetContext) => Observable<any>;
        profileNbPosts: (profileId: string, widgetContext: WidgetContext) => Observable<{
            widget_id: any;
            comments: any;
            likes: any;
            post_count: any;
            shares: any;
            total_engagements: any;
        }[]>;
        reach: (widgetContext: WidgetContext) => Observable<number>;
        reachTimeline: (widgetContext: WidgetContext) => Observable<any>;
        sentiment(widgetContext: WidgetContext): any;
        shareOfVoice: (widgetContext: WidgetContext) => Observable<{
            id?: string;
            count: number;
        }[]>;
        sourcesTimeline: (widgetContext: WidgetContext) => Observable<any>;
        tagsTimeline: (widgetContext: WidgetContext, tags?: string[], limit?: number) => Observable<any>;
        topLoyalty: (widgetContext: WidgetContext, limit?: number) => Observable<Profile[]>;
        topPosts: (widgetContext: WidgetContext, optionsList?: {
            limit?: number;
            cursor?: number;
        }) => Observable<ObjectsList<Post>>;
        topSources: (widgetContext: WidgetContext, limit?: number) => Observable<any>;
        topTags: (widgetContext: WidgetContext, limit?: number) => Observable<any>;
        topUsers: (widgetContext: WidgetContext, listOptions: {
            limit?: number;
            cursor?: string | number;
        }) => Observable<ObjectsList<Profile>>;
    };
}
