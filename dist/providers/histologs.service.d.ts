import { ZupApiService } from './zupApi.service';
import { ActionLog, Campaign, ObjectsList, Profile } from '../models/';
import { Observable } from 'rxjs';
export declare const enum STATUS {
    NONE = 0,
    INITED = 1,
}
export declare class HistoLogsService {
    private api;
    private _status;
    private isInited;
    private histoLogs;
    private created;
    private logsByPost;
    private logsByProfile;
    private logsByCampaign;
    private autoPulishToCampaign;
    private loading;
    private logFrom;
    private logTo;
    readonly status: STATUS;
    constructor(api: ZupApiService);
    init(): void;
    onInit(callback: Function): void;
    loadHistoLogs(after?: number, before?: number): Observable<void>;
    getCampaignProfiles(id: string): Observable<ObjectsList<Profile>>;
    isProfileInCampaign(pid: any, campaign: Campaign): boolean;
    getProfileLogs(pid: string, last?: boolean): ActionLog[];
    private sortLogs;
}
