import { Observable, Subject } from 'rxjs/Rx';
import { ZupApiService } from './zupApi.service';
import { HistoLogsService } from './histologs.service';
import { LexicalService } from "./lexical.service";
import { Campaign, Label, Member, ObjectsList, Partner, Profile, View, Widget, WidgetContext, Wall } from '../models';
export declare enum STATUS {
    NONE = 0,
    INITATING = 1,
    INITED = 2,
}
export declare class PartnerService {
    private api;
    private histoLogs;
    private lexicals;
    member: Member;
    roles: string[];
    partner: Partner;
    _labels: ObjectsList<Label>;
    _walls: any[];
    status: number;
    widgetsListChange: Subject<Widget[]>;
    labelsListChange: Subject<Label[]>;
    wallsListChange: Subject<any[]>;
    ICListChange: Subject<any[]>;
    mainWidgetContext: WidgetContext;
    private defaultSA;
    private _onInit;
    constructor(api: ZupApiService, histoLogs: HistoLogsService, lexicals: LexicalService);
    /**
     * Tests if the member loggued is the main account for this partner
     * @returns {boolean}
     */
    readonly isAdmin: boolean;
    /**
     * Tests if Partner information (widgets, thresholds, lables, ...) are loaded
     * @returns {boolean}
     */
    readonly isInited: boolean;
    readonly isWallOnly: boolean;
    /**
     * Returns the labels created for this partner
     * @returns {any[]}
     */
    readonly labels: Label[];
    readonly myName: string;
    readonly widgetList: Widget[];
    readonly walls: Wall[];
    addCampaign(campaign: Campaign): Observable<Campaign>;
    addFacebookToken(profiles: Profile[]): void;
    addRight(right: string): void;
    addView(view: View): void;
    createInfluenceCampaign(): void;
    createWidget(w: any, isI4U?: any): Observable<Widget>;
    deleteWidget(w: Widget): Observable<boolean>;
    deleteInfluenceCampaign(w: Widget): Observable<boolean>;
    updateInfluenceCampaign(): void;
    getInfluenceCampaign(wid: string): Widget;
    getInfluenceCampaigns(): Widget[];
    private _influenceBrands;
    getInfluenceBrands(): Observable<{
        widget: Widget;
        me: boolean;
        profile: Profile;
    }[]>;
    getInfluenceBrandWidgets(): Widget[];
    decodeId(id: string): string;
    delCampaign(campaign: Campaign): Observable<boolean>;
    delView(view: View | string): void;
    delWidget(widgetId: string): Observable<void>;
    getCampaigns(): Observable<Campaign[]>;
    getLabel(id: string): Label;
    getMember(memberId: string): Member;
    /**
     * Returns the thredsholds for the type of widget provided and the word for "Word Alerts"
     * @param {number} widgetId
     * @returns {Observable<R>}
     */
    getNotificationsLimit(widgetId?: string): any;
    getDefaultDetectedWords(): string[];
    getSocialAccounts(kind: string): Observable<Profile>;
    getDefaultSocialAccounts(): Observable<any>;
    getDetectedWords(): Observable<{
        widgetId: string;
        words: string;
    }[]>;
    getProfileICampaigns(profile: Profile): Widget[];
    getRecordedMessages(): {
        attribution: string;
        message: string;
        name?: string;
    }[];
    getSources(): any[];
    getWall(id: string): Wall;
    getWidget(id: string): Widget;
    getWidgetDetail(id: string): Observable<Widget>;
    /**
     *
     * @param {{id?: string, type?: number, category?: number, main?: boolean, defaultPeriode?: string}} options
     *          main = true, saves the context to mainWidgetContext
     * @returns {WidgetContext}
     */ getWidgetName(id: string): string;
    getWidgetRuleName(w: {
        type;
        id;
    }, options?: {
        customCats?;
    }): string;
    getWidgets(options?: any): Widget[];
    hasRightsFor(rights: string[]): boolean;
    isInfluencerList(lid: string): boolean;
    /**
     * Returns an Observable that returns true when partner information are loaded
     * @returns {Observable<boolean>}
     *
     * ###exemple :
     * ```
     * partnerService.onInit().subscribe( ok => {
     *    // Do something
     * })
     * ```
     */
    onInit(): Observable<boolean>;
    onInit2(callback: () => void): void;
    refreshLabels(): Observable<any[]>;
    refreshWallList(): Observable<void>;
    removeMember(id: string): Observable<void>;
    getQueries(type?: string): {
        name: string;
        query: any;
        type?: string;
    }[];
    addQuery(query: {
        name: string;
        query: any;
        type?: string;
    }): {
        name: string;
        query: any;
        type?: string;
    }[];
    deleteQuery(queries: {
        name: string;
        query: any;
        type?: string;
    }[]): {
        name: string;
        query: any;
        type?: string;
    }[];
    /**
     *
     * @param limits modifications in the thresholds and words for the alerts
     * @param widgetId
     * @returns {any}
     */
    setNotificationsLimit(limits: any, widgetId: string): Observable<{}[]>;
    setDefaultLimits(limits: any): Observable<boolean>;
    setDefaultSocialAccount(accountKind: string, profile?: Profile): void;
    /**
     *
     * @param {{widgetId?: string, words?: string}[]} words list of lexicals to update
     * @returns {Observable<boolean>}
     */
    setDetectedWords(words: {
        widgetId?: string;
        words?: string;
    }[]): Observable<boolean>;
    setRecordedMessages(messages: {
        attribution: string;
        message: string;
        name?: string;
    }[]): void;
    private cleanWords(w);
    /**
     * replace setUser : load Partner information for this user
     *
     * @param userId
     */
    initPartner(userId?: string): Observable<Partner>;
    updatePartner(): Observable<boolean>;
    updateWidget(widget: Widget): Observable<Widget>;
    updateView(view: View): Observable<View>;
    private updatePartnerWidget(widget);
}
