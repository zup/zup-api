import { Injectable, EventEmitter } from '@angular/core';
import { PartnerService } from './partner.service';
import { ZupApiService } from './zupApi.service';
import { Wall, wallJsonPost, Post, SimpleProfile, Profile, WidgetContext, wallPages, ObjectsList } from '../models';
import { dates, tools } from '../misc/utils';
import { Observable } from 'rxjs';
import { objectFormat } from './storage';
import { wallPage, wallPageConf, wallConf } from "../models/wall";
var WallService = /** @class */ (function () {
    function WallService(partner, api) {
        var _this = this;
        this.partner = partner;
        this.api = api;
        this.displayedPosts = new Set();
        this._postStack = [];
        this.pinnedPostTick = 0;
        this.sponsoredPostTick = 0;
        this.dateInitConfig = dates.toZIFromDate(new Date());
        this.serviceError = new EventEmitter();
        /**
             * Emits a post every every [wall.delays.mainPosts] seconds
             * @type {EventEmitter<wallJsonPost>}
             *
             *
             * current algo to emit new Post:
             * 1 - try use stack (fst-in/fst-out)
             * 2 - ask for new
             * 3 - take a random in the list (older)
             */
        this.newPost = new EventEmitter();
        /**
             * Emits a pinnedPost every every [wall.delays.pinnedPosts] seconds (if any)
             * @type {EventEmitter<wallJsonPost>}
             */
        //newPinnedPost: EventEmitter<wallJsonPost> = new EventEmitter<wallJsonPost>();
        /**
             * Emits a sponsoredPost every every [wall.delays.sponsoredPosts] seconds (if any)
             * @type {EventEmitter<wallJsonPost>}
             */
        //newSponsoredPost: EventEmitter<wallJsonPost> = new EventEmitter<wallJsonPost>();
        /**
             * Emits true when wall has changed, the display should reload everything (general info, posts (moderation might have changed),...)
             * @type {EventEmitter<boolean>}
             */
        this.configChanged = new EventEmitter();
        // refreshWall: EventEmitter<boolean> = new EventEmitter<boolean>();
        this.analytics = {
            nbPosts: function (dtFrom, dtTo) {
                return _this.api.wall.nbPosts(_this._wall.id, dtFrom, dtTo, { cache: false });
            },
            topHashTags: function (nb, dtFrom, dtTo) {
                //return this.api.wall.topTags(this.wall.widgetIds[0], nb, dtFrom, dtTo);
                return _this.api.wall.topTags(_this._wall.id, nb, dtFrom, dtTo);
            },
            topUsers: function (nb, dtFrom, dtTo) {
                return _this.api.wall.topUsers(_this._wall.id, nb, dtFrom, dtTo);
            },
            nbActions: function (dtFrom, dtTo) {
                return _this.api.wall.nbActions(_this._wall.id, dtFrom, dtTo);
            }
        };
        /**
             * Returns the nb ljsast posts of the wall
             * @param {number} [nb] default = 20
             * @returns {Observable<wallJsonPost[]>}
             */
        this.retry = 0;
    }
    Object.defineProperty(WallService.prototype, "currentWallConf", {
        get: function () {
            return tools.clone(this._currentWallConf);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WallService.prototype, "confName", {
        get: function () {
            return this.confId;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Returns custom styles to be used in the wall
     * @returns {any}
     */
    /**
         * Returns custom styles to be used in the wall
         * @returns {any}
         */
    WallService.prototype.getCustomStyles = /**
         * Returns custom styles to be used in the wall
         * @returns {any}
         */
    function () {
        return this._wall.styles;
    };
    /**
     * Returns the new post according to the "newPost" algorithm
     * @returns {wallJsonPost}
     */
    /**
         * Returns the new post according to the "newPost" algorithm
         * @returns {wallJsonPost}
         */
    WallService.prototype.getNewPost = /**
         * Returns the new post according to the "newPost" algorithm
         * @returns {wallJsonPost}
         */
    function () {
        var _this = this;
        if (!this.dateRange)
            return Observable.of(this.getRandomPost());
        var sandBoxDelay = this.getOption('sandBoxDelay') || 0;
        var now = new Date(new Date().valueOf() - sandBoxDelay * 60 * 1000);
        var nowZI = dates.nowZI - sandBoxDelay * 60;
        if (!this._wall) {
            console.log('NOWALL');
            return Observable.of(undefined);
        }
        var isReplay = this.getOption('replay');
        if (!isReplay && this._postStack.length > 0) {
            while (
            //go to the first post tounger than 2 mins
            this._postStack.length > 0
                && this._posts.unshift(this._postStack.pop())
                && (nowZI - this._posts[0].created >= 120)) { }
            var test = nowZI - this._posts[0].created;
            if (this._postStack.length === 0 && (nowZI - this._posts[0].created >= 120)) {
                //the last is oldest than 2 mins; ask for new ones
                var dtFrom = new Date(now.valueOf() - 15 * 60 * 1000);
                this.dateRange = { from: dtFrom, to: now };
                return this.api.wall.posts(this._wall.id, this.dateRange, { limit: 50 }).map(function (res) {
                    if (res.list.length > 0) {
                        var ids_1 = _this._posts.slice(0, 100).map(function (p) { return p.id; });
                        _this._postStack = _this.moderate(res.list)
                            .filter(function (p) { return ids_1.findIndex(function (id) { return id === p.id; }) < 0; });
                    }
                    if (_this._postStack.length > 0) {
                        while (
                        //go to the first post tounger than 2 mins
                        _this._postStack.length > 0
                            && _this._posts.unshift(_this._postStack.pop())
                            && (dates.nowZI - _this._posts[0].created >= 120)) { }
                    }
                    return { post: _this._posts[0] };
                }, function (err) { console.log(err); return Observable.of({ post: _this._posts[0] }); });
            }
            else {
                return Observable.of({ post: this._posts[0] });
            }
        }
        else if (this._postStack.length > 0 && isReplay) {
            this._posts.unshift(this._postStack.pop());
            return Observable.of({ post: this._posts[0] });
        }
        else if (isReplay) {
            var newFrom = this.dateRange.to; // we add 1h
            var maxTo = dates.min(dates.toDateFromZI(tools.deepGet(this.getOption('replay-timeRange'), 'to')
                || dates.toZIFromDate(new Date())), now);
            var newTo = dates.min(maxTo, new Date(this.dateRange.to.valueOf() + 3600 * 1000));
            if (newFrom.valueOf() + 59 * 1000 >= maxTo.valueOf()) {
                // we go back from the begining
                this._postStack = this._posts;
                this._posts = [];
                this._posts.unshift(this._postStack.pop());
                return Observable.of({ post: this._posts[0] });
            }
            this.dateRange = { from: this.dateRange.to, to: newTo };
            return this.api.wall.posts(this._wall.id, this.dateRange, { limit: 300 }).map(function (res) {
                if (res.list.length > 0) {
                    var ids_2 = _this._posts.map(function (p) { return p.id; });
                    _this._postStack = _this.moderate(res.list)
                        .filter(function (p) { return ids_2.findIndex(function (id) { return id === p.id; }) < 0; });
                }
                if (_this._postStack.length > 0) {
                    _this._posts.unshift(_this._postStack.pop());
                    return { post: _this._posts[0] };
                }
                else {
                    return _this.getRandomPost();
                }
            }, function (err) { console.log(err); return Observable.of(_this.getRandomPost()); });
        }
        else if ((now.valueOf() - this.dateRange.to.valueOf()) > 14 * 1000) {
            // no more than one query each 15s
            var dtFrom = new Date(now.valueOf() - 15 * 60 * 1000);
            this.dateRange = { from: dtFrom, to: now };
            return this.api.wall.posts(this._wall.id, this.dateRange, { limit: 50 }).map(function (res) {
                if (res.list.length > 0) {
                    var ids_3 = _this._posts.slice(0, 100).map(function (p) { return p.id; });
                    _this._postStack = _this.moderate(res.list)
                        .filter(function (p) { return ids_3.findIndex(function (id) { return id === p.id; }) < 0; });
                }
                if (_this._postStack.length > 0) {
                    _this._posts.unshift(_this._postStack.pop());
                    return { post: _this._posts[0] };
                }
                else {
                    return _this.getRandomPost();
                }
            }, function (err) { console.log(err); return Observable.of(_this.getRandomPost()); });
        }
        else {
            return Observable.of(this.getRandomPost());
        }
    };
    WallService.prototype.getRandomPost = function () {
        if (!this._posts)
            return;
        //*
        // pick a random
        var r = Math.floor(Math.random() * this._posts.length);
        if (this.displayedPosts) {
            var cpt = 0;
            while (cpt++ < 5 && this.displayedPosts.has(this._posts[r].id)) {
                r = Math.floor(Math.random() * this._posts.length);
            }
            if (cpt === 6) {
                console.log('Could not find a post not already displayed');
            }
        }
        return { post: this._posts[r], isRecycled: true };
        //*/
    };
    /**
     * Returns the next pinnedPost to display
     * @returns {wallJsonPost}
     */
    /**
         * Returns the next pinnedPost to display
         * @returns {wallJsonPost}
         */
    WallService.prototype.getPinnedPost = /**
         * Returns the next pinnedPost to display
         * @returns {wallJsonPost}
         */
    function (force) {
        var pp;
        if (this._wall.pinnedPosts.length > 0) {
            if (this._wall.pinnedPosts.length === 1 && this.pinnedPostTick > 0 && !force)
                return null;
            pp = this._wall.pinnedPosts[this.pinnedPostTick++ % this._wall.pinnedPosts.length];
        }
        return Observable.of(pp);
    };
    WallService.prototype.getPosts = function (nb, min, pageId) {
        var _this = this;
        if (nb === void 0) { nb = 20; }
        if (min === void 0) { min = nb / 2; }
        var isReplay = false;
        var sandBoxDelay = this.getOption('sandBoxDelay') || 0;
        var now = new Date(new Date().valueOf() - sandBoxDelay * 60 * 1000);
        if (this.getOption('replay')) {
            isReplay = true;
            //const interval = tools.deepGet(this._wall.pagesConfig, <string>wallPage.POSTWALL, 'options', 'replay-interval') || 24
            var interval = this.getOption('replay-timeRange')
                || { from: dates.toZIFromDate(now) - 24 * 3600, to: dates.toZIFromDate(now) }; // default: last 24h
            this.dateRange = {
                from: dates.toDateFromZI(interval.from),
                to: dates.min(dates.toDateFromZI(interval.from + 3600), now)
            };
        }
        else {
            var dtFrom = new Date(new Date().valueOf() - 1000 * 3600 * 24 * 30);
            this.dateRange = { from: dtFrom, to: now };
        }
        this._postStack = [];
        return this.api.wall.posts(this._wall.id, this.dateRange, { limit: nb })
            .flatMap(function (res) {
            var getMore = function () {
                var moderated = _this.moderate(res.list);
                if (moderated.length < min && res.hasMore) {
                    return res.getMore().flatMap(function () {
                        return getMore();
                    });
                }
                else {
                    _this._posts = moderated;
                    if (isReplay) {
                        var nbPosts = Math.min(nb, 20); //if mode == replay I just need the last 20 to init the wall
                        _this._postStack = _this._posts.slice(0, -(nbPosts + 1));
                        var returnPosts = _this._posts.slice(-nbPosts);
                        _this._posts = [];
                        return Observable.from([returnPosts]);
                    }
                    else {
                        return Observable.from([_this._posts]);
                    }
                }
            };
            return getMore();
        })
            .catch(function (err) {
            // if error in post recuperation, warn front and try again for 1 mintues
            if (_this.retry++ >= 4) {
                _this.retry = 0;
                return Observable.throw(err);
            }
            _this.serviceError.emit(true);
            return Observable.create(function (observer) {
                setTimeout(function () {
                    console.log('try again');
                    _this.getPosts(nb).subscribe(function (ps) {
                        _this.serviceError.emit(false);
                        observer.next(ps);
                    });
                }, 15 * 1000);
            });
        });
    };
    WallService.prototype.getPostDetail = function (id) {
        return this._posts.find(function (p) { return p.id === id; });
    };
    WallService.prototype.getPostsDetail = function (ids) {
        return this.api.posts.getMultiple(ids, objectFormat.XS).map(function (posts) { return posts.map(function (p) { return p.wallJsonPost; }); });
    };
    WallService.prototype.getProfiles = function (nb) {
        if (!this._posts || this._posts.length === 0)
            return Observable.from([[]]);
        var profiles = tools.unduplicate(this._posts.map(function (post) { return post.user; }), function (u) { return u.id; });
        if (nb)
            profiles = profiles.slice(0, nb - 1);
        return Observable.from([profiles]);
    };
    WallService.prototype.getPostObjectList = function (nb) {
        var _this = this;
        if (this._currentWallConf)
            throw 'DOIT ETRE MODIFIE';
        var sandBoxDelay = this.getOption('sandBoxDelay') || 0;
        var now = new Date(new Date().valueOf() - sandBoxDelay * 60 * 1000);
        if (this.getOption('replay')) {
            var interval = this.getOption('replay-timeRange')
                || { from: dates.toZIFromDate(now) - 24 * 3600, to: dates.toZIFromDate(now) }; // default: last 24h
            this.dateRange = {
                from: dates.toDateFromZI(interval.from),
                to: dates.min(dates.toDateFromZI(interval.to), now)
            };
        }
        else {
            this.dateRange = { from: new Date('2010/01/01'), to: now };
        }
        return this.api.wall.posts(this._wall.id, this.dateRange, { limit: nb }, { filterFunction: function (p) { return _this._wall.isOkPost(p, _this._currentWallConf); } }).map(function (res) {
            _this._posts = tools.clone(res.list);
            return res;
        });
    };
    /**
     * Loads wall configuration (delays, pinned and sponsored posts, moderated posts and users,...)
     * @param {string} wid wall id
     * @returns {Observable<void>}
     */
    /**
         * Loads wall configuration (delays, pinned and sponsored posts, moderated posts and users,...)
         * @param {string} wid wall id
         * @returns {Observable<void>}
         */
    WallService.prototype.init = /**
         * Loads wall configuration (delays, pinned and sponsored posts, moderated posts and users,...)
         * @param {string} wid wall id
         * @returns {Observable<void>}
         */
    function (name, partnerName, confName) {
        var _this = this;
        if (confName === void 0) { confName = 'default'; }
        this.wallName = name;
        this.wallPartnerName = partnerName;
        this.confId = confName;
        this.intervals = {
            config: setInterval(function () { return _this.getConfig(); }, 15 * 1000)
        };
        return this.loadWall(name, partnerName).map(function (wall) {
            _this._wall = wall;
            var ok = _this._getCurrentConf();
            _this.setIntervals();
            return ok;
        }).catch(function (err) { return Observable.from([false]); });
    };
    WallService.prototype.getPages = function () {
        var _this = this;
        if (!this._currentWallConf)
            return;
        var conf = tools.clone(this._currentWallConf.pages);
        if (this._currentWallConf.frontPage) {
            var p = conf.find(function (p) { return p.pageId === _this._currentWallConf.frontPage; });
            if (p)
                return [p];
        }
        ;
        // remove pages with no duration
        conf = conf.filter(function (p) {
            if (p.id === wallPage.FAVPOSTS) {
                if (_this._wall.favoritePosts.length === 0) {
                    return false;
                }
                else if (!p.duration && !tools.deepGet(p, 'options', 'favInterval')) {
                    return false;
                }
            }
            else if (p.id === wallPage.FULLSCREEN) {
                // if not post, do not display
                if (!p.duration || !tools.deepGet(p, 'options', 'postId')) {
                    return false;
                }
            }
            else {
                if (!p.duration) {
                    return false;
                }
            }
            return true;
        });
        /*
                // remove favorites if no favposts
                if (this._wall.favoritePosts.length === 0) {
                    let i = conf.findIndex(p => p.id === wallPage.FAVPOSTS);
                    while(i  >= 0) {
                        conf.splice(i, 1);
                        i = conf.findIndex(p => p.id === wallPage.FAVPOSTS);
                    }
                }*/
        if (conf.length === 0) {
            conf.push({
                id: wallPage.COVERPAGE,
                pageId: "(wp)404"
            });
        }
        return conf;
    };
    WallService.prototype.getWallStyles = function () {
        if (!this._currentWallConf)
            return;
        // TODO: send the styles for the currentconf
        var styles = tools.clone(this._wall.styles || {});
        return styles.wallStyles;
    };
    WallService.prototype.getPageConfig = function (pageId) {
        if (pageId.indexOf('(wp)') === 0) {
            var conf = this._currentWallConf.pages.find(function (p) { return p.pageId === pageId; });
            if (!conf) {
                console.log('NO CONF FOR ' + pageId);
                return;
            }
            return tools.clone(conf);
        }
        else {
            return tools.clone(this._wall.defaultPageConf.find(function (p) { return p.id === pageId; }));
        }
    };
    WallService.prototype.getPageStyles = function (pageId) {
        var pageConf = this.getPageConfig(pageId);
        var pageStyles = tools.clone(tools.deepGet(pageConf, 'styles') || {});
        var confStyles = tools.clone(tools.deepGet(this._currentWallConf, 'styles') || {});
        var wallStyles = tools.clone(tools.deepGet(this._wall, 'styles') || {});
        wallStyles.pageStyles = wallStyles.wallStyles || {};
        wallStyles.postStyles = wallStyles.postStyles || {};
        delete wallStyles.wallStyles;
        return tools.deepAssign2(wallStyles, confStyles, pageStyles);
    };
    WallService.prototype.getOption = function (option, pageOrDefault) {
        if (typeof pageOrDefault === 'boolean') {
            if (pageOrDefault) {
                return tools.deepGet(this._wall, 'defaultOptions', option);
            }
            else {
                return tools.deepGet(this._currentWallConf, 'options', option) || tools.deepGet(this._wall, 'defaultOptions', option);
            }
        }
        else if (typeof pageOrDefault === 'string') {
            var myConf = this.getPageConfig(pageOrDefault);
            return tools.deepGet(myConf, 'options', option);
        }
        else {
            return tools.deepGet(this._currentWallConf, 'options', option) || tools.deepGet(this._wall, 'defaultOptions', option);
        }
    };
    Object.defineProperty(WallService.prototype, "wall", {
        get: function () {
            return this._wall.clone();
        },
        enumerable: true,
        configurable: true
    });
    WallService.prototype._getCurrentConf = function () {
        var _this = this;
        if (this._wall.wallConfs.length > 0) {
            var i = this._wall.wallConfs.findIndex(function (c) { return c.id === _this.confId; });
            if (i < 0) {
                console.log("Could not find configuration for '" + this.confId + "', using Default configuration");
                i = 0;
            }
            this._currentWallConf = this._wall.wallConfs[i];
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * removes moderated posts from input list
     * @param {} posts
     * @returns {any[]}
     */
    /**
         * removes moderated posts from input list
         * @param {} posts
         * @returns {any[]}
         */
    WallService.prototype.moderate = /**
         * removes moderated posts from input list
         * @param {} posts
         * @returns {any[]}
         */
    function (posts) {
        var _this = this;
        return posts.filter(function (p) { return _this._wall.isOkPost(p, _this._currentWallConf); });
    };
    WallService.prototype.loadWall = function (name, partnerName) {
        var _this = this;
        return this.api.wall.search(name, partnerName).map(function (wall) {
            _this._wall = wall;
            return _this._wall;
        });
    };
    WallService.prototype.getConfig = function () {
        var _this = this;
        // tmp while waiting for wall/get to be on defaultapi
        /*this.api.wall.search(this.wallName, this.wallPartnerName).subscribe(resWall => {
                    const nw =  resWall;
                    if (this._wall.version !== nw.version) {
                        this._wall = nw;
                        this.setIntervals()
                        // TODO: moderate posts
                    };
                    this.configChanged.emit(true);
                });*/
        // TODO when wall/get on default api :
        this.api.wall.get(this._wall.id).subscribe(function (resWall) {
            var nw = resWall;
            console.log('get wall config');
            if (_this._wall.version !== nw.version) {
                var oldWall = _this._wall;
                _this._wall = nw;
                _this._getCurrentConf();
                // Do we have to hard refresh ?
                if (_this._currentWallConf.refreshDt && _this.dateInitConfig < _this._currentWallConf.refreshDt) {
                    window.location.reload(true);
                    return;
                }
                ;
                var doModerate = false;
                if (JSON.stringify(_this._wall.moderated) !== JSON.stringify(oldWall.moderated)) {
                    doModerate = true;
                    _this._wall.needModeration = true;
                }
                if (doModerate) {
                    _this._postStack = _this.moderate(_this._postStack);
                    _this._posts = _this.moderate(_this._posts);
                }
                _this.setIntervals();
                console.log('new config: ', _this._wall);
                _this.configChanged.emit(true);
            }
            ;
        }, function (err) { console.log(err); });
    };
    WallService.prototype.setIntervals = function () {
        var _this = this;
        if (this.intervals.main)
            clearInterval(this.intervals.main);
        var mainDelay = (this.getOption('entryDelay') || 15) * 1000;
        this.intervals.main = setInterval(function () { return _this.sendNewPost(); }, mainDelay);
    };
    WallService.prototype.sendNewPost = function () {
        var _this = this;
        this.getNewPost().subscribe(function (p) {
            _this.newPost.emit(p);
        });
    };
    WallService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    WallService.ctorParameters = function () { return [
        { type: PartnerService, },
        { type: ZupApiService, },
    ]; };
    return WallService;
}());
export { WallService };
