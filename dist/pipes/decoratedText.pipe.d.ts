import { PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
export declare class DecoratedText implements PipeTransform {
    private sanitizer;
    constructor(sanitizer: DomSanitizer);
    transform(text: any, hgStyles?: any): SafeHtml;
}
