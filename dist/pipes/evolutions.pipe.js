import { Pipe, PipeTransform } from '@angular/core';
var EvolutionsClass = /** @class */ (function () {
    function EvolutionsClass() {
    }
    ;
    EvolutionsClass.prototype.transform = function (evol) {
        if (evol < -5) {
            return 'evol-negative';
        }
        else if (evol > 5) {
            return 'evol-positive';
        }
        else {
            return 'evol-neutral';
        }
    };
    EvolutionsClass.decorators = [
        { type: Pipe, args: [{ name: 'evolutionsClass' },] },
    ];
    /** @nocollapse */
    EvolutionsClass.ctorParameters = function () { return []; };
    return EvolutionsClass;
}());
export { EvolutionsClass };
;
