import { PipeTransform } from '@angular/core';
export declare class InDate implements PipeTransform {
    constructor();
    transform(date: any, value: any): any;
    toLocalDate(date: Date, isText?: boolean): string;
}
