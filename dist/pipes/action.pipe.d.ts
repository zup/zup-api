import { PipeTransform } from '@angular/core';
import { ActionLogDetail } from '../models';
export declare class ActionOrigin implements PipeTransform {
    transform(origin: any): string;
}
export declare class ActionSource implements PipeTransform {
    getColor(cat: any): "red" | "purple" | "blue";
    transform(widget: any): string;
}
export declare class ActionLogText implements PipeTransform {
    transform(action: ActionLogDetail): any;
}
