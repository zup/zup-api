import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
var regExHashAndCite = /([#@][^\s#@]*?)(\s|#|$|@)/gm;
var linkRegExp = /(https?:\/\/[^\s"]*)/gm;
var toStyles = function (o) {
    var s = '';
    for (var p in o) {
        s = s.concat(p + ":" + o[p] + "; ");
    }
    return s;
};
var ɵ0 = toStyles;
var DecoratedText = /** @class */ (function () {
    function DecoratedText(sanitizer) {
        this.sanitizer = sanitizer;
    }
    DecoratedText.prototype.transform = function (text, hgStyles) {
        if (!text)
            return '';
        var higlight = "class=\"highlight\"";
        if (hgStyles)
            higlight = "style=\"" + toStyles(hgStyles) + "\"";
        text = text.replace(linkRegExp, " <a href=\"$1\" target=\"_blank\" " + higlight + ">$1</a> ");
        text = text.replace(regExHashAndCite, " <span " + higlight + ">$1</span> ");
        return this.sanitizer.bypassSecurityTrustHtml(text);
    };
    DecoratedText.decorators = [
        { type: Pipe, args: [{ name: 'decoratedText' },] },
    ];
    /** @nocollapse */
    DecoratedText.ctorParameters = function () { return [
        { type: DomSanitizer, },
    ]; };
    return DecoratedText;
}());
export { DecoratedText };
export { ɵ0 };
