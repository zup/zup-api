import { PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
export declare class AttributionIcon implements PipeTransform {
    private sanitizer;
    constructor(sanitizer: DomSanitizer);
    transform(attribution: any, link: any, options?: any): SafeHtml;
}
export declare class AttributionText implements PipeTransform {
    transform(attribution: any): any;
}
