import { PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
export declare class SafePipe implements PipeTransform {
    protected _sanitizer: DomSanitizer;
    constructor(_sanitizer: DomSanitizer);
    transform(value: string, type?: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl;
}
