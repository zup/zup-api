import { AttributionIcon } from './attribution.pipe';
import { DecoratedText } from './decoratedText.pipe';
import { AlertsTitle } from './alertsTitle.pipe';
import { SafePipe } from './safe.pipe';
export declare const PIPES: (typeof AttributionIcon | typeof DecoratedText | typeof AlertsTitle | typeof SafePipe)[];
