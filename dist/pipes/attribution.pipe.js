import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { _attributionTypes as attributionTypes } from '../listOfValues';
import { tools } from '../misc/utils';
var AttributionIcon = /** @class */ (function () {
    function AttributionIcon(sanitizer) {
        this.sanitizer = sanitizer;
    }
    AttributionIcon.prototype.transform = function (attribution, link, options) {
        if (!attribution)
            return;
        var icon = "<i class=\"" + (tools.deepGet(attributionTypes.detail(attribution), 'icon') || 'fa fa-question-circle') + "\" \n                  style=\"" + (options && options.style || '') + "\"></i>";
        var returnObject = link ? "<a href=\"" + link + "\" target=\"_new\">" + icon + "</a>" : icon;
        if (options && options.noSanitize) {
            return returnObject;
        }
        else {
            return this.sanitizer.bypassSecurityTrustHtml(returnObject);
        }
    };
    AttributionIcon.decorators = [
        { type: Pipe, args: [{
                    name: 'attributionIcon'
                },] },
    ];
    /** @nocollapse */
    AttributionIcon.ctorParameters = function () { return [
        { type: DomSanitizer, },
    ]; };
    return AttributionIcon;
}());
export { AttributionIcon };
var AttributionText = /** @class */ (function () {
    function AttributionText() {
    }
    AttributionText.prototype.transform = function (attribution) {
        var a = attributionTypes.detail(attribution);
        if (a) {
            return a.text;
        }
        else {
            console.log("UNKNOWN_ATTR(" + attribution + ")");
            return 'Unknown';
        }
    };
    AttributionText.decorators = [
        { type: Pipe, args: [{
                    name: 'attributionText'
                },] },
    ];
    /** @nocollapse */
    AttributionText.ctorParameters = function () { return []; };
    return AttributionText;
}());
export { AttributionText };
