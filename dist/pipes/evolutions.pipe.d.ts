import { PipeTransform } from '@angular/core';
export declare class EvolutionsClass implements PipeTransform {
    constructor();
    transform(evol: number): "evol-negative" | "evol-positive" | "evol-neutral";
}
