import { Pipe, PipeTransform } from '@angular/core';
import { alertTypes } from '../models/notifications';
var AlertsTitle = /** @class */ (function () {
    function AlertsTitle() {
    }
    AlertsTitle.prototype.transform = function (payload, type) {
        if (!type)
            return '<span></span>';
        var atype = alertTypes.detail(type);
        return atype.title.replace(/\$0/, "<span class=\"highlight\">" + payload[atype.variable] + "</span>");
    };
    AlertsTitle.decorators = [
        { type: Pipe, args: [{ name: 'alertsTitle' },] },
    ];
    /** @nocollapse */
    AlertsTitle.ctorParameters = function () { return []; };
    return AlertsTitle;
}());
export { AlertsTitle };
