import { Pipe, PipeTransform } from '@angular/core';
import { ActionLogDetail, ActionLog, actionTypes, widgetTypes, screenTypes } from '../models';
var ActionOrigin = /** @class */ (function () {
    function ActionOrigin() {
    }
    ActionOrigin.prototype.transform = function (origin) {
        if (!origin)
            return;
        var detail = screenTypes.detail(origin);
        if (!detail)
            return;
        return "<span class=\"color-" + detail.color + "\">" + detail.text + "</span>";
    };
    ActionOrigin.decorators = [
        { type: Pipe, args: [{
                    name: 'actionOrigin'
                },] },
    ];
    /** @nocollapse */
    ActionOrigin.ctorParameters = function () { return []; };
    return ActionOrigin;
}());
export { ActionOrigin };
var ActionSource = /** @class */ (function () {
    function ActionSource() {
    }
    ActionSource.prototype.getColor = function (cat) {
        switch (cat) {
            case widgetTypes.WATCH.value:
                return "blue";
            case widgetTypes.MY_PLACE.value:
                return 'purple';
            case widgetTypes.COMPETITOR.value:
                return 'red';
        }
    };
    ActionSource.prototype.transform = function (widget) {
        return "<span class=\"color-" + this.getColor(widget.category) + "\">{{widget.name}}</span>";
    };
    ActionSource.decorators = [
        { type: Pipe, args: [{
                    name: 'actionSource'
                },] },
    ];
    /** @nocollapse */
    ActionSource.ctorParameters = function () { return []; };
    return ActionSource;
}());
export { ActionSource };
var ActionLogText = /** @class */ (function () {
    function ActionLogText() {
    }
    ActionLogText.prototype.transform = function (action) {
        var adetail;
        if (action.type === 'PUBLISH') {
            adetail = actionTypes.detail(actionTypes[action.publishType]);
        }
        else {
            adetail = actionTypes.detail(actionTypes[action.type]);
        }
        if (action.profile) {
            return (adetail.actionHtml || adetail.actionText).replace('$0', action.profile.screenName);
        }
        else {
            return adetail.actionText;
        }
    };
    ActionLogText.decorators = [
        { type: Pipe, args: [{
                    name: 'actionLogText'
                },] },
    ];
    /** @nocollapse */
    ActionLogText.ctorParameters = function () { return []; };
    return ActionLogText;
}());
export { ActionLogText };
;
