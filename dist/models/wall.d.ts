import { SimpleProfile } from './profile';
export declare enum wallPage {
    EMPTY = "EMPTY",
    POSTWALL = "POSTWALL",
    TOPUSERS = "TOPUSERS",
    TOPHASHTAGS = "TOPHASHTAGS",
    FAVPOSTS = "FAVPOSTS",
    COVERPAGE = "COVERPAGE",
    LIVESTATS = "LIVESTATS",
    FULLSCREEN = "FULLSCREEN",
}
export declare const wallConfOptions: ({
    name: string;
    value: number;
    text: string;
    type: string;
} | {
    name: string;
    text: string;
    value: boolean;
    type: string;
} | {
    name: string;
    text: string;
    parent: string;
    type: string;
})[];
export declare const wallPages: {
    POSTWALL: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            text: string;
            value: boolean;
            type: string;
        } | {
            name: string;
            text: string;
            parent: string;
            value: string;
            type: string;
            default: boolean;
        } | {
            name: string;
            text: string;
            parent: string;
            value: string;
            type: string;
        } | {
            name: string;
            text: string;
            value: number;
            choices: number[];
        } | {
            name: string;
            value: number;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        })[];
    };
    TOPUSERS: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            value: number;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        })[];
    };
    TOPHASHTAGS: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            value: number;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        })[];
    };
    FAVPOSTS: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            value: number;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        })[];
    };
    COVERPAGE: {
        value: string;
        text: string;
        description: string;
        options: {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        }[];
    };
    LIVESTATS: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            value: number;
            text: string;
            type: string;
        } | {
            name: string;
            text: string;
            type: string;
            maxLength: number;
        })[];
    };
    FULLSCREEN: {
        value: string;
        text: string;
        description: string;
        options: ({
            name: string;
            text: string;
            type: string;
            maxLength: number;
        } | {
            name: string;
            type: string;
        })[];
    };
};
export declare const confOptions: ({
    name: string;
    text: string;
    value: boolean;
    type: string;
} | {
    name: string;
    text: string;
    parent: string;
    type: string;
    value: Date;
} | {
    name: string;
    text: string;
    parent: string;
    type: string;
})[];
export declare type wallStyles = {
    postStyles: {
        post: {
            'background-color';
            color;
        };
        postUserScreenName: {
            color;
        };
        postUserName: {
            color;
        };
        highlight: {
            color;
        };
        postText: {
            color;
        };
        postAttribution: {
            'background-color';
        };
    };
    pageStyles: {
        'background-color';
    };
};
export declare type wallConf = {
    id;
    frontPage?: string | boolean;
    refreshDt?: number;
    styles?: any;
    options?: any;
    pages?: wallPageConf[];
};
export declare type wallPageConf = {
    id: wallPage;
    pageId?: string;
    duration?: number;
    styles?: {
        pageStyles?;
        postsStyles?;
    };
    options?: any;
};
export declare type wallJsonPost = {
    id: string;
    isShared: boolean;
    created: number;
    attributionKind: string;
    attributionLink?: string;
    position: {
        geo_name?: string;
    };
    title?: string;
    text: string;
    thumbnail?: string;
    video?: string;
    videoType?: string;
    user: SimpleProfile;
};
export interface jsonWall {
    id?: string;
    name?: string;
    pinnedPosts?: wallJsonPost[];
    sponsoredPosts?: wallJsonPost[];
    favoritePosts?: string[];
    overlayPost?: {
        postId?: string;
        endAt?: number;
    };
    delays?: {
        mainPosts: number;
        pinnedPosts: number;
        sponsoredPosts: number;
        sandbox: number;
    };
    title?: string;
    description?: string;
    thumbnail?: string;
    poweredBy?: string;
    styles?: any;
    moderated?: {
        posts?: string[];
        users?: string[];
    };
    version: number;
    widgetIds?: string[];
    configurations?: any[];
    wallConfs?: wallConf[];
    defaultPageConf?: wallPageConf[];
    defaultOptions?: any;
}
export interface zWall {
    created: number;
    deleted: number;
    id: string;
    metas: string;
    name: string;
    partner_id?: string;
    widgets: string[];
}
export declare class Wall {
    private wall;
    private jsonWall;
    needModeration: boolean;
    constructor(wall?: zWall);
    configurations: wallConf[];
    delays: any;
    description: string;
    favoritePosts: string[];
    readonly id: string;
    moderated: any;
    name: string;
    overlayPost: {
        postId?: string;
        endAt?: number;
    };
    pagesConfig: any;
    wallConfs: wallConf[];
    defaultPageConf: wallPageConf[];
    defaultOptions: any;
    poweredBy: string;
    pinnedPosts: wallJsonPost[];
    sponsoredPosts: wallJsonPost[];
    styles: any;
    thumbnail: string;
    title: string;
    version: number;
    widgetIds: string[];
    addPinnedPost(p: wallJsonPost): boolean;
    isBlocked(post: wallJsonPost): boolean;
    isOkPost(p: wallJsonPost, conf: wallConf): boolean;
    updatePinnedPost(p: wallJsonPost): boolean;
    toString(): string;
    toJson(): any;
    clone(): Wall;
    private initPagesConfig();
}
