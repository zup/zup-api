var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { dates, tools, applicationLanguages } from '../misc/utils';
import { WidgetContext } from './';
import { lexicalCategories } from '../models/lexical';
export var FILTER_RULES_CATEGORIES;
(function (FILTER_RULES_CATEGORIES) {
    FILTER_RULES_CATEGORIES["PROFILE"] = "profile";
    FILTER_RULES_CATEGORIES["CONTENT"] = "content";
    FILTER_RULES_CATEGORIES["AUDIENCE"] = "audience";
    FILTER_RULES_CATEGORIES["CONTEXT"] = "context";
})(FILTER_RULES_CATEGORIES || (FILTER_RULES_CATEGORIES = {}));
export var FILTER_RULES_CATEGORIES_DESC = {
    profile: { name: 'Profile', icon: 'fa fa-user' },
    content: { name: 'Content', icon: 'icon-speech' },
    audience: { name: 'Audience', icon: 'fa fa-handshake-o' },
    context: { name: 'Context', icon: 'fa fa-calendar' }
};
export var FILTER_RULES = {
    order_by: {
        id: 'order_by',
        value: 'order_by',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    author_id: {
        id: 'author_id',
        value: 'author_id',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['post']
    },
    zone: {
        id: 'zone',
        value: 'zone',
        hidden: true,
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['post']
    },
    date: {
        id: 'date',
        text: 'Was detected between',
        datePicker: true,
        //initValue: dates['yesterday'],
        valueToRule: function (v, query) {
            if (!v && query) {
                query.after = query.before = undefined;
                return;
            }
            else if (v && query) {
                query.after = dates.toZIFromDate(v.from);
                query.before = dates.toZIFromDate(v.to);
            }
            else if (v && !query) {
                return {
                    after: dates.toZIFromDate(v.from),
                    before: dates.toZIFromDate(v.to)
                };
            }
            else {
                return {};
            }
        },
        ruleToValue: function (query) {
            if (query.after === 1) {
                return undefined;
            }
            if (query.after || query.before) {
                return {
                    from: dates.toDateFromZI(query.after),
                    to: dates.toDateFromZI(query.before)
                };
            }
        },
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    engagement_rate_more_than: {
        id: 'engagement_rate_more_than',
        text: 'post engagement rate is greater than',
        text2: '%',
        inputSize: 5,
        max: 100,
        defaultValue: 0,
        isInt: true,
        valueToRule: function (v, query) {
            if (query) {
                query.engagement_rate_more_than = v / 100;
            }
            else if (v) {
                return { engagement_rate_more_than: v / 100 };
            }
            else {
                return {};
            }
        },
        ruleToValue: function (query) { return (query.engagement_rate_more_than || 0) * 100; },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    community_engagement_rate_more_than: {
        id: 'community_engagement_rate_more_than',
        text: 'Average author engagement rate is greater than',
        text2: '%',
        inputSize: 5,
        max: 100,
        defaultValue: 0,
        isInt: true,
        valueToRule: function (v, query) {
            if (query) {
                query.community_engagement_rate_more_than = v / 100;
            }
            else if (v) {
                return { community_engagement_rate_more_than: v / 100 };
            }
            else {
                return {};
            }
        },
        ruleToValue: function (query) { return (query.community_engagement_rate_more_than || 0) * 100; },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    filter_shares: {
        id: 'filter_shares',
        text: 'exclude shares ',
        checkbox: true,
        defaultValue: false,
        isValue: true,
        // to bypass 'strict rules' validation
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    filter_reply: {
        id: 'filter_reply',
        text: 'exclude replies ',
        checkbox: true,
        defaultValue: false,
        isValue: true,
        // to bypass 'strict rules' validation
        forLabel: true,
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['label']
    },
    follower_less_than: {
        id: 'follower_less_than',
        text: 'Has less followers than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    follower_more_than: {
        id: 'follower_more_than',
        text: 'Has more followers than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    gender: {
        id: 'gender',
        text: 'Filter by gender',
        choicePicker: true,
        choices: [
            { value: 'F', text: 'Female' },
            { value: 'M', text: 'Male' },
            { value: 'H', text: 'Human' },
            { value: 'O', text: 'Organisation' }
        ],
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    has_interests: {
        id: 'has_interests',
        text: 'Is interested in ',
        interestPicker: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    lang: {
        id: 'lang',
        text: 'Who writes in  ',
        choicePicker: true,
        choices: applicationLanguages,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    lang2: {
        id: 'lang',
        text: 'Who writes in  ',
        choicePicker: true,
        choices: applicationLanguages,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer']
    },
    lead_score_more_than: {
        id: 'lead_score_more_than',
        text: 'Has a Customer Lead Score over ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    not_profile_label_id: {
        id: 'not_profile_label_id',
        text: 'Exclude profiles from ...',
        labelPicker: true,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    name_like: {
        id: 'name_like',
        text: 'Whose name contains ',
        inputSize: 12,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    profile_label_id: {
        id: 'profile_label_id',
        text: 'Is in my list or campaign ',
        labelPicker: true,
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    post_label_id: {
        id: 'post_label_id',
        text: 'Has spoken on brand or theme ',
        sovPicker: true,
        lexicalCategories: [lexicalCategories.STANDARD, lexicalCategories.SOV],
        defaultValue: '',
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    published_more_than: {
        id: 'published_more_than',
        text: 'Has published more than ',
        text2: ' times',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile']
    },
    source: {
        id: 'source',
        text: 'Filter by social network ',
        kindPicker: true,
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    terms: {
        id: 'terms',
        text: 'Has used the words ',
        inputSize: 12,
        //defaultValue: '',
        multiple: true,
        valueToRule: function (ks, query) {
            if (typeof ks === 'string')
                ks = ks.split(/\s*,\s*/);
            if (ks && query) {
                query.terms = ks.map(function (k) { return tools.formatkwd(k); });
            }
            else if (!ks && query) {
                delete query.terms;
            }
            else if (ks && !query) {
                return { terms: ks.map(function (k) { return tools.formatkwd(k); }) };
            }
            else {
                return {};
            }
        },
        ruleToValue: function (query) {
            return query.terms && query.terms.map(function (k) { return tools.unformatKwd(k); });
        },
        category: FILTER_RULES_CATEGORIES.CONTENT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    description_terms: {
        id: 'description_terms',
        text: 'Has the words in his description',
        inputSize: 12,
        //defaultValue: '',
        multiple: true,
        valueToRule: function (ks, query) {
            if (typeof ks === 'string')
                ks = ks.split(/\s*,\s*/);
            if (ks && query) {
                query.description_terms = ks.map(function (k) { return tools.formatkwd(k); });
            }
            else if (!ks && query) {
                delete query.description_terms;
            }
            else if (ks && !query) {
                return { description_terms: ks.map(function (k) { return tools.formatkwd(k); }) };
            }
            else {
                return {};
            }
        },
        ruleToValue: function (query) {
            return query.description_terms && query.description_terms.map(function (k) { return tools.unformatKwd(k); });
        },
        category: FILTER_RULES_CATEGORIES.PROFILE,
        for: ['label']
    },
    xscore_more_than: {
        id: 'xscore_more_than',
        text: 'post has more engagements than ',
        inputSize: 5,
        defaultValue: 0,
        isInt: true,
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer', 'post', 'profile', 'label']
    },
    widgets: {
        id: 'widgets',
        text: 'Was detected in',
        widgetPicker: true,
        ruleToValue: function (query) {
            if (query.widget_id)
                return { type: 'widget', id: query.widget_id };
            if (query.widget_category)
                return { type: 'category', id: query.widget_category };
            if (query.widget_type)
                return { type: 'type', id: query.widget_type };
        },
        valueToRule: function (v, query) {
            if (!v && query) {
                query.widget_id = query.widget_type = query.widget_category = undefined;
                return;
            }
            else if (v) {
                var value = void 0;
                if (typeof v === 'string') {
                    value = { type: 'widget', id: v };
                }
                else {
                    value = v;
                }
                if (query) {
                    switch (value.type) {
                        case 'widget':
                            query.widget_id = value.id;
                            delete query.widget_category;
                            delete query.widget_type;
                            break;
                        case 'category':
                            query.widget_category = value.id;
                            delete query.widget_id;
                            delete query.widget_type;
                            break;
                        case 'type':
                            query.widget_type = value.id;
                            delete query.widget_category;
                            delete query.widget_id;
                            break;
                    }
                }
                else {
                    switch (value.type) {
                        case 'widget':
                            return { widget_id: value.id };
                        case 'category':
                            return { widget_category: value.id };
                        case 'type':
                            return { widget_type: value.id };
                    }
                }
            }
            else {
                return {};
            }
        },
        category: FILTER_RULES_CATEGORIES.CONTEXT,
        for: ['influencer', 'post', 'profile', 'label']
    },
    community_gender: {
        id: 'community_gender',
        text: 'Gender of the community : ',
        payload: {
            type: 'gender',
            default: 0.6
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_interests: {
        id: 'community_interests',
        text: 'Interests of the community :',
        payload: {
            type: 'interests',
            default: 0.2
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_age: {
        id: 'community_age',
        text: 'Age rank of the community :',
        payload: {
            type: 'age',
            default: 0.3
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
    community_language: {
        id: 'community_language',
        text: 'Language of the community :',
        payload: {
            type: 'language',
            default: 0.5
        },
        category: FILTER_RULES_CATEGORIES.AUDIENCE,
        for: ['influencer']
    },
};
var FilterQuery = /** @class */ (function () {
    // profiles: +/-published", "+/-follower", "+/-leadscore", "+/-lastseen
    function FilterQuery(rules, ruleType, ruleOptions) {
        this.ruleOptions = ruleOptions;
        if (!rules)
            return;
        for (var _i = 0, _a = Object.keys(rules); _i < _a.length; _i++) {
            var k = _a[_i];
            if (ruleType && (tools.deepGet(FILTER_RULES, k, 'for') || []).indexOf(ruleType) < 0)
                continue; //will not work on  widget and dates
            if (rules[k]) {
                this[k] = rules[k];
            }
        }
        if (this.widget_id) {
            this.widgets = { type: 'widget', id: this.widget_id };
        }
        else if (this.widget_category) {
            this.widgets = { type: 'category', id: this.widget_category };
        }
        else if (this.widget_type) {
            this.widgets = { type: 'type', id: this.widget_type };
        }
        ;
    }
    FilterQuery.prototype.clear = function (persistent) {
        var _this = this;
        Object.keys(FILTER_RULES).forEach(function (k) {
            if (persistent && persistent.indexOf(k) >= 0)
                return;
            var rule = FILTER_RULES[k];
            if (rule.valueToRule) {
                rule.valueToRule(rule.defaultValue, _this);
            }
            else {
                delete _this[rule.id];
            }
            ;
        });
    };
    FilterQuery.prototype.fromContext = function (widgetContext, ruleOptions) {
        this.ruleOptions = ruleOptions;
        if (widgetContext) {
            var jwc = widgetContext.getContext({ timeline: false });
            Object.assign(this, jwc);
            // test if dates are allRange to delete them
            if (JSON.stringify(widgetContext.dateRange) === JSON.stringify(dates.all)) {
                delete this.after;
                delete this.before;
            }
        }
        return this;
    };
    FilterQuery.prototype.getActiveRules = function (category, withHidden) {
        var _this = this;
        var activeRules = [];
        Object.keys(FILTER_RULES).forEach(function (k) {
            if (category && FILTER_RULES[k].category !== category)
                return;
            if (withHidden === false && FILTER_RULES[k].hidden)
                return;
            var currentVal = _this[k];
            if (FILTER_RULES[k].ruleToValue) {
                currentVal = FILTER_RULES[k].ruleToValue(_this);
            }
            var defaultValue = tools.deepGet(_this.ruleOptions, k, 'defaultValue') || FILTER_RULES[k].defaultValue;
            if (currentVal && JSON.stringify(currentVal) !== JSON.stringify(defaultValue)) {
                activeRules.push(FILTER_RULES[k]);
            }
            ;
        });
        return activeRules;
    };
    FilterQuery.prototype.isActive = function (rid) {
        if (!FILTER_RULES[rid]) {
            console.log("UNKNOW_RULE(" + rid + ")");
            return false;
        }
        var currentVal = this[rid];
        if (FILTER_RULES[rid].ruleToValue) {
            currentVal = FILTER_RULES[rid].ruleToValue(this);
        }
        var defaultValue = tools.deepGet(this.ruleOptions, rid, 'defaultValue') || FILTER_RULES[rid].defaultValue;
        return currentVal && JSON.stringify(currentVal) !== JSON.stringify(defaultValue);
    };
    FilterQuery.prototype.ruleValue = function (rid) {
        if (!FILTER_RULES[rid]) {
            console.log("UNKNOW_RULE(" + rid + ")");
            return;
        }
        var val = this[rid];
        if (!val)
            val = FILTER_RULES[rid].defaultValue;
        if (FILTER_RULES[rid].ruleToValue)
            val = FILTER_RULES[rid].ruleToValue(this);
        if (FILTER_RULES[rid].multiple && val) {
            return val.toString();
        }
        return val;
    };
    FilterQuery.prototype.toJSON = function () {
        var _this = this;
        var activeRules = this.getActiveRules();
        var json = {};
        activeRules.forEach(function (r) {
            var rv = {};
            if (r.valueToRule) {
                rv = r.valueToRule(_this.ruleValue(r.id));
            }
            else {
                rv[r.id] = _this.ruleValue(r.id);
            }
            json = __assign({}, json, rv);
        });
        return json;
    };
    Object.defineProperty(FilterQuery.prototype, "length", {
        get: function () {
            return this.getActiveRules().length;
        },
        enumerable: true,
        configurable: true
    });
    return FilterQuery;
}());
export { FilterQuery };
var FilterRules = /** @class */ (function () {
    function FilterRules(rules) {
        if (!rules)
            return;
        for (var _i = 0, _a = Object.keys(rules); _i < _a.length; _i++) {
            var k = _a[_i];
            if (rules[k]) {
                this[k] = rules[k];
            }
        }
        if (this.widget_id) {
            this.widgets = { type: 'widget', id: this.widget_id };
        }
        else if (this.widget_category) {
            this.widgets = { type: 'category', id: this.widget_category };
        }
        else if (this.widget_type) {
            this.widgets = { type: 'type', id: this.widget_type };
        }
        ;
    }
    return FilterRules;
}());
export { FilterRules };
var PostFilterRules = /** @class */ (function (_super) {
    __extends(PostFilterRules, _super);
    function PostFilterRules(rules) {
        return _super.call(this, rules) || this;
    }
    return PostFilterRules;
}(FilterRules));
export { PostFilterRules };
var ProfileFilterRules = /** @class */ (function (_super) {
    __extends(ProfileFilterRules, _super);
    function ProfileFilterRules(rules) {
        return _super.call(this, rules) || this;
    }
    return ProfileFilterRules;
}(FilterRules));
export { ProfileFilterRules };
