import { Profile, JsonProfile } from './';
import { wallJsonPost } from './';
export declare type Location = {
    lat: number;
    lng: number;
    geo_name?: string;
};
export declare type FullJsonPost = {
    id: string;
    isError?: boolean;
    kind: string;
    authorid: string;
    approximate: boolean;
    content: {
        attribution: string;
        cites: string[];
        is_share: boolean;
        lang: string;
        links: string[];
        medias: string[];
        short_text: string;
        tags: string[];
        text: string;
        thumbnail: string;
        title: string;
        sentiment_fetched: number;
        sentiment_score: number;
    };
    crawled_at: number;
    created: number;
    filtered: string;
    likes: number;
    position: Location;
    updated: number;
    xcomments: number;
    xlikes: number;
    xscore: number;
    xshares: number;
};
export interface PostInterface {
    id: string;
    created: number;
    attributionType: string;
    attributionLink: string;
    authorId: string;
    position: {
        lat: number;
        lng: number;
        geo_name?: string;
    };
    lang?: string;
    tags?: string[];
    isShared?: boolean;
    text: string;
    thumbnail: string;
    user: Profile;
    video?: string;
    videoType?: string;
    xlikes?: number;
    xcomments?: number;
    xshares?: number;
    sentimentScore?: number;
    lexicalIds?: string[];
    engagements?: number;
}
export declare class Post implements PostInterface {
    id: string;
    isError: boolean;
    isShared: boolean;
    created: number;
    tags?: string[];
    text: string;
    thumbnail: string;
    attributionType: string;
    attribution: string;
    attributionLink: string;
    authorId: string;
    user: Profile;
    position: {
        lat: number;
        lng: number;
        geo_name?: string;
    };
    video?: string;
    videoType?: string;
    xlikes?: number;
    xcomments?: number;
    xshares: number;
    sentimentScore?: number;
    lexicalIds?: string[];
    lang: string;
    engagements?: number;
    constructor(post: FullJsonPost | any, user: Profile | JsonProfile);
    readonly elapsedTime: any;
    readonly wallJsonPost: wallJsonPost;
    readonly engagement: number;
}
