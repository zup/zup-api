var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Widget, widgetTypes } from './widget';
import { tools } from '../misc/utils';
var View = /** @class */ (function (_super) {
    __extends(View, _super);
    function View(jsonWidget) {
        var _this = _super.call(this, jsonWidget) || this;
        if (jsonWidget) {
            if (!_this.parentWidgetId)
                throw 'MISSING_PARENT()';
        }
        _this.type = widgetTypes.VIEW;
        return _this;
    }
    View.prototype.setParent = function (widget) {
        if (widget.parentWidgetId)
            throw 'PARENT_ALREADY_A_VIEW()';
        if (this.parentWidgetId && this.parentWidgetId !== widget.id)
            throw 'VIEW_HAS_ANOTHER_PARENT()';
        this.parentWidgetId = widget.id;
        this._parentZone = tools.clone(widget.zone);
        this._parentTags = tools.clone(widget.tags);
        this._parentOrTags = tools.clone(widget.orTags);
        this._parentMinusTags = tools.clone(widget.minusTags);
        this._parentName = widget.name;
        this.buildQueries();
    };
    View.prototype.clone = function (anonymous) {
        if (anonymous === void 0) { anonymous = false; }
        this.buildQueries();
        var myJsonView = JSON.parse(JSON.stringify(this._jsonWidget));
        if (anonymous)
            delete myJsonView.id;
        return new View(myJsonView);
    };
    Object.defineProperty(View.prototype, "jsonObject", {
        get: function () {
            this.buildQueries();
            return JSON.parse(JSON.stringify(this._jsonWidget));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(View.prototype, "parentZone", {
        get: function () {
            return this._parentZone;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(View.prototype, "allExluded", {
        get: function () {
            return this._view.exclude_terms;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(View.prototype, "view", {
        get: function () {
            this._view.zone = this._view.zone || this._parentZone;
            return JSON.parse(JSON.stringify(this._view));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(View.prototype, "zone", {
        get: function () {
            if (!this._view)
                return this._parentZone;
            return this._view.zone || this._parentZone;
        },
        set: function (z) {
            if (tools.objectsEquals(z, { "n": 0, "e": 0, "s": 0, "w": 0 }))
                z = undefined;
            if (!this._view)
                this._view = {};
            this._view.zone = z;
            if (!this._jsonWidget.view)
                this._jsonWidget.view = {};
            this._jsonWidget.view.zone = z;
        },
        enumerable: true,
        configurable: true
    });
    return View;
}(Widget));
export { View };
