import { Observable } from 'rxjs';
import { EventEmitter } from "@angular/core";
export declare type OLOptions = {
    unduplicateFunction?: any;
    filterFunction?: (T) => boolean;
    afterGMFunction?: (o: ObjectsList<any>) => Observable<ObjectsList<any>>;
};
/**
 * ObjectsList is an list of objects (that should be iterable, but it does not work currently)
 * copulated with an API call (getMore methode) to get next page of objects
 */
export declare class ObjectsList<T> {
    isLoading: EventEmitter<boolean>;
    loading: boolean;
    getMoreObservable: Observable<ObjectsList<T>> | Observable<ObjectsListLC<T>>;
    protected objects: T[];
    protected _cursor: string;
    protected filterFunction: (T) => boolean;
    protected options: OLOptions;
    /**
     *
     * @param {T[]} list
     * @param {Observable<ObjectsList<T>>} getMore Observable that when subscribes send the next page of elements
     * (eventually after an API call)
     */
    constructor(list: T[], getMore?: Observable<ObjectsList<T>> | Observable<ObjectsListLC<T>>, options?: OLOptions);
    [Symbol.iterator](): {
        next: () => {
            done: boolean;
            value: T;
        };
    };
    addObject(o: T, index?: number): void;
    cleanList(): void;
    /**
     * Deletes an object of the list identified by is id property
     * @param id
     */
    deleteById(id: string): void;
    /**
     * Deletes an object from the list
     * @param o
     */
    deleteObject(o: T): void;
    /**
     * Returns an object from the list, identified by its id property
     * @param id
     * @returns {T}
     */
    getById(id: string): T;
    getShiftedObject(id: string, shift: number): Observable<T>;
    getObject(o: T): T;
    /**
     * Returns the length of the list
     * @returns {number}
     */
    readonly length: number;
    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(T) => boolean)} rulesFn function to filter the list
     * @param {boolean} replace : replace existing objects
     * @returns {T[]}
     */
    filter(rulesFn: (T) => boolean, replace?: boolean): T[];
    /**
     *
     * @param {any} filters object of type { prop: value,... } to be matched by the posts
     * @returns {T[]}
     */
    filterByProp(filters?: any): T[];
    /**
     *
     * @returns {T[]}
     */
    readonly list: T[];
    /**
     * Returns if a "next page" exists
     * @returns {boolean}
     */
    readonly hasMore: boolean;
    /**
     * Returns an OBservable that when subscribed returns the "next page" of elements and add them to the current list
     * @returns {Observable<T[]>}
     */
    getMore(): Observable<T[]>;
    setOptions(optionName: string, optionValue: any): void;
}
export declare class ObjectsListLC<T> extends ObjectsList<T> {
    private loadDetail;
    private pageLimit;
    private page;
    private _list;
    protected objects: any[];
    constructor(list: any[], limit?: number, loadDetail?: (ids: any[]) => Observable<T[]>, getMore?: Observable<ObjectsListLC<T>>, options?: OLOptions);
    [Symbol.iterator](): {
        next: () => {
            done: boolean;
            value: T;
        };
    };
    readonly cacheLength: number;
    readonly ids: any[];
    readonly length: number;
    readonly list: T[];
    readonly hasMore: boolean;
    addObject(object: T, index?: number, key?: any): void;
    contains(key: any): boolean;
    deleteById(id: string): void;
    deleteObject(key: any): void;
    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(id) => boolean)} rulesFn function to filter the list, will be applied on ids, not on <T>
     * @param {boolean} replace : replace existing objects
     * @returns {id[]}
     */
    filter(rulesFn: (T) => boolean, replace?: boolean): any[];
    getMore(): Observable<T[]>;
    /**
     * Loads in // all the details in cache
     */
    getCacheDetail(): Observable<ObjectsListLC<T>>;
    getShiftedObject(id: string, shift: number): Observable<T>;
    setObjects(objects: T[], keys: any[]): void;
    getCacheObjects(): any[];
    hasInCache(id: any): boolean;
    private loadList();
}
