import { Widget, View, Profile } from './';
import { tools } from '../misc/utils';
/**
 * A partner is a client, he can have several Widgets and Members
 */
var /**
 * A partner is a client, he can have several Widgets and Members
 */
Partner = /** @class */ (function () {
    function Partner(partner, widgets, members) {
        var _this = this;
        this.advancedQueries = [];
        this.widgets = [];
        this.socialAccounts = [];
        this.defaultLimits = {
            userDailyThreshold: undefined,
            influencerThreshold: undefined,
            customerLeadThreshold: undefined,
            loyaltyNbThreshold: undefined,
            loyaltyDaysThreshold: undefined
        };
        this.defaultWordsAlert = [];
        this.recordedMessages = [];
        this.roUsers = [];
        this.woUsers = [];
        this.widgetAndViews = new Map();
        this._metas = {};
        this._id = partner.partner_id;
        this.name = partner.name || 'Noname';
        this._metas = !!partner.metas ? JSON.parse(partner.metas) : {};
        this.email = partner.email;
        this.description = partner.description;
        this.rights = partner.rights;
        // TODO: parse meta while no other info:
        if (this._metas.hasOwnProperty('logo'))
            this.logo = this._metas.logo;
        if (this._metas.hasOwnProperty('defaultLimits'))
            Object.assign(this.defaultLimits, this._metas.defaultLimits);
        if (this._metas.hasOwnProperty('defaultWordsAlert'))
            this.defaultWordsAlert = this._metas.defaultWordsAlert;
        if (this._metas.hasOwnProperty('roUsers'))
            this.roUsers = this._metas.roUsers;
        if (this._metas.hasOwnProperty('woUsers'))
            this.woUsers = this._metas.woUsers;
        if (this._metas.hasOwnProperty('advancedQueries'))
            this.advancedQueries = this._metas.advancedQueries;
        if (this._metas.hasOwnProperty('recordedMessages'))
            this.recordedMessages = this._metas.recordedMessages;
        this.widgets = widgets;
        this.members = members;
        widgets.forEach(function (w) {
            w.widgetViews.forEach(function (v) {
                _this.widgetAndViews.set(v.id, v);
            });
            _this.widgetAndViews.set(w.id, w);
        });
    }
    Object.defineProperty(Partner.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Partner.prototype, "metas", {
        get: function () {
            var metas = {
                logo: this.logo,
                defaultLimits: this.defaultLimits,
                defaultWordsAlert: this.defaultWordsAlert,
                roUsers: this.roUsers,
                woUsers: this.woUsers,
                advancedQueries: this.advancedQueries,
                recordedMessages: this.recordedMessages,
                inSuite: this._metas.inSuite,
                tmpInfo: this._metas.tmpInfo
            };
            return JSON.stringify(metas);
        },
        enumerable: true,
        configurable: true
    });
    Partner.prototype.removeMember = function (id) {
        var i = this.members.findIndex(function (u) { return u.id === id; });
        if (i >= 0) {
            this.members.splice(i, 1);
        }
        ;
    };
    Partner.prototype.setInsuiteRights = function (hasRights) {
        this._metas.inSuite = hasRights;
    };
    Object.defineProperty(Partner.prototype, "hasInsuite", {
        get: function () {
            return this._metas.inSuite !== false;
        },
        enumerable: true,
        configurable: true
    });
    /*
      get sources(): string[] {
        if (!this._metas.sources) this._metas.sources = Object.keys(attributionSources);
        return this._metas.sources;
      }
    
      set sources(sources: string[])  {
        this._metas.sources = sources;
      }*/
    /*
      get sources(): string[] {
        if (!this._metas.sources) this._metas.sources = Object.keys(attributionSources);
        return this._metas.sources;
      }
    
      set sources(sources: string[])  {
        this._metas.sources = sources;
      }*/
    Partner.prototype.getTmpInfo = /*
      get sources(): string[] {
        if (!this._metas.sources) this._metas.sources = Object.keys(attributionSources);
        return this._metas.sources;
      }
    
      set sources(sources: string[])  {
        this._metas.sources = sources;
      }*/
    function (key) {
        return tools.deepGet(this._metas, 'tmpInfo', key);
    };
    Partner.prototype.setTmpInfo = function (key, value) {
        this._metas.tmpInfo = this._metas.tmpInfo || {};
        this._metas.tmpInfo[key] = value;
    };
    return Partner;
}());
/**
 * A partner is a client, he can have several Widgets and Members
 */
export { Partner };
