var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Observable } from 'rxjs';
import { EventEmitter } from "@angular/core";
/**
 * ObjectsList is an list of objects (that should be iterable, but it does not work currently)
 * copulated with an API call (getMore methode) to get next page of objects
 */
var /**
 * ObjectsList is an list of objects (that should be iterable, but it does not work currently)
 * copulated with an API call (getMore methode) to get next page of objects
 */
ObjectsList = /** @class */ (function () {
    // getMore is the functions (observable) that loads the next page
    /**
     *
     * @param {T[]} list
     * @param {Observable<ObjectsList<T>>} getMore Observable that when subscribes send the next page of elements
     * (eventually after an API call)
     */
    function ObjectsList(list, getMore, options) {
        var _this = this;
        this.isLoading = new EventEmitter();
        this.loading = false;
        this.options = {};
        this.getMoreObservable = getMore;
        this.options = options || {};
        var myList = list;
        if (this.options.filterFunction) {
            this.filterFunction = this.options.filterFunction;
            myList = list.filter(function (o) { return _this.filterFunction(o); });
        }
        if (this.options.unduplicateFunction) {
            this.objects = [];
            myList.forEach(function (o) {
                if (_this.options.unduplicateFunction(o, _this.objects))
                    _this.objects.push(o);
            });
        }
        else {
            this.objects = myList;
        }
    }
    ObjectsList.prototype[Symbol.iterator] = function () {
        var _this = this;
        var index = 0;
        return {
            next: function () {
                if (index < _this.objects.length) {
                    index += 1;
                    return { done: false, value: _this.objects[index - 1] };
                }
                return { value: undefined, done: true };
            }
        };
    };
    ObjectsList.prototype.addObject = function (o, index) {
        if (index === void 0) { index = 0; }
        this.objects.splice(index, 0, o);
    };
    ObjectsList.prototype.cleanList = function () {
        var _this = this;
        if (this.filterFunction) {
            this.objects = this.objects.filter(function (o) { return _this.filterFunction(o); });
        }
    };
    /**
     * Deletes an object of the list identified by is id property
     * @param id
     */
    /**
         * Deletes an object of the list identified by is id property
         * @param id
         */
    ObjectsList.prototype.deleteById = /**
         * Deletes an object of the list identified by is id property
         * @param id
         */
    function (id) {
        var i = this.objects.findIndex(function (n) {
            return n['id'] === id;
        });
        if (i < 0) {
            console.log('UNKNOWN ID for ObjectsList.deleteById');
            return;
        }
        this.objects.splice(i, 1);
    };
    /**
     * Deletes an object from the list
     * @param o
     */
    /**
         * Deletes an object from the list
         * @param o
         */
    ObjectsList.prototype.deleteObject = /**
         * Deletes an object from the list
         * @param o
         */
    function (o) {
        var i = this.objects.findIndex(function (n) {
            return n === o;
        });
        if (i < 0) {
            console.log('UNKNOWN Object for ObjectsList.deleteObject');
            return;
        }
        this.objects.splice(i, 1);
    };
    /**
     * Returns an object from the list, identified by its id property
     * @param id
     * @returns {T}
     */
    /**
         * Returns an object from the list, identified by its id property
         * @param id
         * @returns {T}
         */
    ObjectsList.prototype.getById = /**
         * Returns an object from the list, identified by its id property
         * @param id
         * @returns {T}
         */
    function (id) {
        if (!id)
            return;
        return this.objects.find(function (n) {
            return n['id'] === id;
        });
    };
    ObjectsList.prototype.getShiftedObject = function (id, shift) {
        var _this = this;
        var i = this.objects.findIndex(function (n) {
            return n['id'] === id;
        }) + shift;
        if (i < 0) {
            return Observable.of(null);
        }
        else if (i < this.objects.length) {
            return Observable.of(this.objects[i]);
        }
        else if (this.hasMore) {
            var j_1 = i + 1 - this.objects.length;
            var lastId_1 = this.objects[this.objects.length - 1]['id'];
            return this.getMore().flatMap(function () { return _this.getShiftedObject(lastId_1, j_1); });
        }
        else {
            return Observable.of(null);
        }
    };
    ObjectsList.prototype.getObject = function (o) {
        return this.objects.find(function (n) {
            return n === o;
        });
    };
    Object.defineProperty(ObjectsList.prototype, "length", {
        /**
         * Returns the length of the list
         * @returns {number}
         */
        get: /**
             * Returns the length of the list
             * @returns {number}
             */
        function () {
            return (this.objects && this.objects.length) || 0;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(T) => boolean)} rulesFn function to filter the list
     * @param {boolean} replace : replace existing objects
     * @returns {T[]}
     */
    /**
         * Filters the list with the function provided and returns elements that matched the test
         * @param {(T) => boolean)} rulesFn function to filter the list
         * @param {boolean} replace : replace existing objects
         * @returns {T[]}
         */
    ObjectsList.prototype.filter = /**
         * Filters the list with the function provided and returns elements that matched the test
         * @param {(T) => boolean)} rulesFn function to filter the list
         * @param {boolean} replace : replace existing objects
         * @returns {T[]}
         */
    function (rulesFn, replace) {
        // local filter
        var filtered = this.objects.filter(function (o) { return rulesFn(o); });
        if (replace) {
            this.filterFunction = rulesFn;
            this.objects = filtered;
        }
        return filtered;
    };
    /**
     *
     * @param {any} filters object of type { prop: value,... } to be matched by the posts
     * @returns {T[]}
     */
    /**
         *
         * @param {any} filters object of type { prop: value,... } to be matched by the posts
         * @returns {T[]}
         */
    ObjectsList.prototype.filterByProp = /**
         *
         * @param {any} filters object of type { prop: value,... } to be matched by the posts
         * @returns {T[]}
         */
    function (filters) {
        var fFn = function (o) {
            if (!filters)
                return true;
            var ok = true;
            for (var p in filters) {
                ok = ok && o[p] === filters[p];
            }
            return ok;
        };
        return this.filter(fFn);
    };
    Object.defineProperty(ObjectsList.prototype, "list", {
        /**
         *
         * @returns {T[]}
         */
        get: /**
             *
             * @returns {T[]}
             */
        function () {
            return this.objects;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectsList.prototype, "hasMore", {
        /**
         * Returns if a "next page" exists
         * @returns {boolean}
         */
        get: /**
             * Returns if a "next page" exists
             * @returns {boolean}
             */
        function () {
            return !!this.getMoreObservable;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Returns an OBservable that when subscribed returns the "next page" of elements and add them to the current list
     * @returns {Observable<T[]>}
     */
    /**
         * Returns an OBservable that when subscribed returns the "next page" of elements and add them to the current list
         * @returns {Observable<T[]>}
         */
    ObjectsList.prototype.getMore = /**
         * Returns an OBservable that when subscribed returns the "next page" of elements and add them to the current list
         * @returns {Observable<T[]>}
         */
    function () {
        var _this = this;
        if (!this.hasMore)
            return Observable.from([]);
        this.loading = true;
        this.isLoading.emit(this.loading);
        var getMoreObservable = this.getMoreObservable;
        if (this.options.afterGMFunction)
            getMoreObservable = getMoreObservable.flatMap(function (res) { return _this.options.afterGMFunction(res); });
        return getMoreObservable.map(function (res) {
            if (_this.filterFunction) {
                res.filter(_this.filterFunction, true);
            }
            ;
            if (_this.options.unduplicateFunction) {
                res.list.forEach(function (no) {
                    if (_this.options.unduplicateFunction(no, _this.objects))
                        _this.objects.push(no);
                });
            }
            else {
                _this.objects = _this.objects.concat(res.list);
            }
            _this.getMoreObservable = (res.getMoreObservable);
            _this.loading = false;
            _this.isLoading.emit(_this.loading);
            if (_this.filterFunction) {
                return res.list.filter(function (o) { return _this.filterFunction(o); });
            }
            else {
                return res.list;
            }
        });
    };
    ;
    ObjectsList.prototype.setOptions = function (optionName, optionValue) {
        this.options[optionName] = optionValue;
    };
    return ObjectsList;
}());
/**
 * ObjectsList is an list of objects (that should be iterable, but it does not work currently)
 * copulated with an API call (getMore methode) to get next page of objects
 */
export { ObjectsList };
// list with LocalCache (no cursor)
var 
// list with LocalCache (no cursor)
ObjectsListLC = /** @class */ (function (_super) {
    __extends(ObjectsListLC, _super);
    // !list is can be something else than T[] if a load Detail is provided
    function ObjectsListLC(list, limit, loadDetail, getMore, options) {
        var _this = _super.call(this, list, getMore, options) || this;
        _this.loadDetail = loadDetail;
        _this.page = 1;
        _this._list = [];
        _this.pageLimit = limit || 10;
        _this.loadList().subscribe();
        return _this;
    }
    ;
    ObjectsListLC.prototype[Symbol.iterator] = function () {
        var _this = this;
        var index = 0;
        return {
            next: function () {
                if (index < _this._list.length) {
                    index += 1;
                    return { done: false, value: _this._list[index - 1] };
                }
                return { value: undefined, done: true };
            }
        };
    };
    Object.defineProperty(ObjectsListLC.prototype, "cacheLength", {
        get: function () {
            return this.objects.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectsListLC.prototype, "ids", {
        get: function () {
            return this.objects;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectsListLC.prototype, "length", {
        get: function () {
            return (this._list && this._list.length) || 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectsListLC.prototype, "list", {
        get: function () {
            return this._list;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectsListLC.prototype, "hasMore", {
        get: function () {
            return this.objects.length > (this.pageLimit * this.page) || !!this.getMoreObservable;
        },
        enumerable: true,
        configurable: true
    });
    ObjectsListLC.prototype.addObject = function (object, index, key) {
        if (index === void 0) { index = 0; }
        this.objects.splice(index, 0, key);
        this.list.splice(index, 0, object);
    };
    ObjectsListLC.prototype.contains = function (key) {
        return this.objects.indexOf(key) >= 0;
    };
    ObjectsListLC.prototype.deleteById = function (id) {
        var i = this.objects.indexOf(id);
        if (i < 0) {
            console.log('UNKNOWN ID for ObjectsList.deleteById');
            return;
        }
        this.objects.splice(i, 1);
        this._list.splice(i, 1);
    };
    ObjectsListLC.prototype.deleteObject = function (key) {
        var i = this.objects.indexOf(key);
        if (i >= 0) {
            this.objects.splice(i, 1);
            this._list.splice(i, 1); // what if objects and list are not synchronized ?????
        }
    };
    ;
    /**
     * Filters the list with the function provided and returns elements that matched the test
     * @param {(id) => boolean)} rulesFn function to filter the list, will be applied on ids, not on <T>
     * @param {boolean} replace : replace existing objects
     * @returns {id[]}
     */
    /**
         * Filters the list with the function provided and returns elements that matched the test
         * @param {(id) => boolean)} rulesFn function to filter the list, will be applied on ids, not on <T>
         * @param {boolean} replace : replace existing objects
         * @returns {id[]}
         */
    ObjectsListLC.prototype.filter = /**
         * Filters the list with the function provided and returns elements that matched the test
         * @param {(id) => boolean)} rulesFn function to filter the list, will be applied on ids, not on <T>
         * @param {boolean} replace : replace existing objects
         * @returns {id[]}
         */
    function (rulesFn, replace) {
        var _this = this;
        // local filter
        var filtered = this.objects.filter(function (o) { return rulesFn(o); });
        if (replace) {
            this.filterFunction = rulesFn;
            this.objects = filtered;
            this._list = this._list.filter(function (o) { return _this.objects.indexOf(o.id) >= 0; });
        }
        return filtered;
    };
    ObjectsListLC.prototype.getMore = function () {
        var _this = this;
        if (!this.hasMore) {
            return Observable.from([]);
        }
        else if (this.objects.length > (this.pageLimit * (this.page + 1))) {
            // still enough objects in localcache for nextPage
            this.page++;
            this.loading = true;
            this.isLoading.emit(this.loading);
            return this.loadList();
        }
        else if (!!this.getMoreObservable) {
            this.loading = true;
            this.isLoading.emit(this.loading);
            return this.getMoreObservable.flatMap(function (res) {
                if (_this.options.unduplicateFunction) {
                    res.objects.forEach(function (no) {
                        if (_this.options.unduplicateFunction(no, _this.objects))
                            _this.objects.push(no);
                    });
                }
                else {
                    _this.objects = _this.objects.concat(res.list);
                }
                if (_this.filterFunction) {
                    _this.objects = _this.objects.filter(function (o) { return _this.filterFunction(o); });
                }
                _this.getMoreObservable = (res.getMoreObservable);
                return _this.getMore();
            });
        }
        else {
            // no more in back, need to finish loading detail
            this.page++;
            this.loading = true;
            this.isLoading.emit(this.loading);
            return this.loadList();
        }
    };
    /**
     * Loads in // all the details in cache
     */
    /**
         * Loads in // all the details in cache
         */
    ObjectsListLC.prototype.getCacheDetail = /**
         * Loads in // all the details in cache
         */
    function () {
        var _this = this;
        if (!this.loadDetail || this.list.length >= this.objects.length)
            return Observable.from([this]);
        this.loading = true;
        this.isLoading.emit(this.loading);
        var todos = [];
        var allIds = this.objects.slice(this.list.length);
        while (allIds.length > 0) {
            var ids = allIds.slice(0, this.pageLimit);
            allIds = allIds.slice(this.pageLimit);
            todos.push(this.loadDetail(ids));
        }
        ;
        return Observable.forkJoin.apply(Observable, todos).map(function (res) {
            res.forEach(function (details) {
                _this._list = _this._list.concat(details);
            });
            _this.loading = false;
            _this.isLoading.emit(_this.loading);
            return _this;
        });
    };
    ObjectsListLC.prototype.getShiftedObject = function (id, shift) {
        var _this = this;
        var i = this.list.findIndex(function (n) {
            return n['id'] === id;
        }) + shift;
        if (i < 0) {
            return Observable.of(null);
        }
        else if (i < this.list.length) {
            return Observable.of(this.list[i]);
        }
        else if (this.hasMore) {
            var j_2 = i + 1 - this.list.length;
            var lastId_2 = this.list[this.list.length - 1]['id'];
            return this.getMore().flatMap(function () { return _this.getShiftedObject(lastId_2, j_2); });
        }
        else {
            return Observable.of(null);
        }
    };
    ObjectsListLC.prototype.setObjects = function (objects, keys) {
        this._list = objects;
        this.objects = keys;
    };
    ObjectsListLC.prototype.getCacheObjects = function () {
        return this.objects;
    };
    ObjectsListLC.prototype.hasInCache = function (id) {
        return this.list.indexOf(id) >= 0;
    };
    ObjectsListLC.prototype.loadList = function () {
        var _this = this;
        if (!this.objects)
            return Observable.from([[]]);
        this.loading = true;
        this.isLoading.emit(this.loading);
        var l = this.objects.slice(this.pageLimit * (this.page - 1), this.pageLimit * this.page);
        if (this.loadDetail) {
            return this.loadDetail(l).map(function (res) {
                if (_this.filterFunction)
                    res = res.filter(function (t) { return _this.filterFunction(t.id); });
                _this._list = _this._list.concat(res);
                _this.loading = false;
                _this.isLoading.emit(_this.loading);
                return _this._list;
            });
        }
        else {
            this._list = this._list.concat(l);
            this.loading = false;
            this.isLoading.emit(this.loading);
            return Observable.from([this._list]);
        }
    };
    return ObjectsListLC;
}(ObjectsList));
// list with LocalCache (no cursor)
export { ObjectsListLC };
