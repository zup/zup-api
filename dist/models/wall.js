/*
export interface wallJsonPost {
    id: string;
    created: number;
    attributionKind: string;
    position: {geo_name?: string};
    text: string;
    thumbnail?: string;
    video?: string;
    user: {
        id: string,
        name: string,
        screenName: string,
        thumbnail: string
    }
}
*/
import { SimpleProfile } from './profile';
import { toLOV } from '../listOfValues';
import { tools, dates } from '../misc/utils';
var options = [
    { name: 'embedded', text: 'Embed this wall', value: false, type: 'boolean' },
    { name: 'replay', text: 'Use replay mode', value: false, type: 'boolean' },
    { name: 'replay-from', text: 'From: ', parent: 'replay', type: 'date-time', value: new Date(2017, 0, 1) },
    { name: 'replay-to', text: 'To: ', parent: 'replay', type: 'date-time' }
];
export var wallPage;
(function (wallPage) {
    wallPage["EMPTY"] = "EMPTY";
    wallPage["POSTWALL"] = "POSTWALL";
    wallPage["TOPUSERS"] = "TOPUSERS";
    wallPage["TOPHASHTAGS"] = "TOPHASHTAGS";
    wallPage["FAVPOSTS"] = "FAVPOSTS";
    wallPage["COVERPAGE"] = "COVERPAGE";
    wallPage["LIVESTATS"] = "LIVESTATS";
    wallPage["FULLSCREEN"] = "FULLSCREEN";
})(wallPage || (wallPage = {}));
export var wallConfOptions = [
    { name: 'sandBoxDelay', value: 0, text: 'Sandbox delay (delay between a post creation and its apparition in the wall ) : $1 minutes', type: 'int' },
    { name: 'entryDelay', value: 0, text: 'Entry Delay (before the entry of the next post, wait for) : $1 seconds', type: 'int' },
    //{name: 'pinnedPostDelay', value: 0, text: 'PinnedPost frequency : $1 seconds', type: 'int'},
    { name: 'replay', text: 'Use replay mode', value: false, type: 'boolean' },
    { name: 'replay-timeRange', text: 'Replay period', parent: 'replay', type: 'time-range' },
    { name: 'noRT', text: 'Hide shares from the wall', value: true, type: 'boolean' },
    { name: 'onlyMedia', text: 'Only show post with photo or video', value: false, type: 'boolean' },
    { name: 'embedded', text: 'Embed this wall', value: false, type: 'boolean' },
    { name: 'hideAttribution', text: 'Hide attribution icon', value: false, type: 'boolean' }
];
var pages = {
    POSTWALL: {
        value: 'mainWall',
        text: 'Wall of posts',
        description: 'Shows in real time all post done in the place',
        options: [
            //{name: 'sandBoxDelay', value: 0, text: 'Sandbox delay (delay between a post creation and its apparition in the wall ) : $1 minutes', type: 'int', defaultConf: true},
            //{name: 'entryDelay', value: 0, text: 'Entry Delay (before the entry of the next post, wait for) : $1 seconds', type: 'int', defaultConf: true},
            //{name: 'pinnedPostDelay', value: 0, text: 'PinnedPost frequency : $1 seconds', type: 'int', defaultConf: true},
            //{name: 'replay', text: 'Use replay mode', value: false, type: 'boolean', defaultConf: true},
            //{name: 'replay-interval', text: 'Replay the last $1 hours', parent: 'replay', type: 'int', value: 24},
            //{name: 'replay-timeRange', text: 'Replay period', parent: 'replay', type: 'time-range', defaultConf: true},
            { name: 'hideText', text: 'Hide text on posts with image or video', value: false, type: 'boolean' },
            //{name: 'onlyMedia', text: 'Only show post with photo or video', value: false, type: 'boolean', defaultConf: true},
            //{name: 'noRT', text: 'Hide retweets from the wall', value: true, type: 'boolean'},
            { name: 'sideBar', text: 'Display side bar', value: false, type: 'boolean' },
            { name: 'sideBar-option', text: 'Nothing', parent: 'sideBar', value: 'nothing', type: 'radio', default: true },
            { name: 'sideBar-option', text: 'Side bar stats', parent: 'sideBar', value: 'stats', type: 'radio' },
            //{name: 'sideBar-option', text: 'Side bar pinned posts', parent: 'sideBar', value: 'pinnedPosts', type: 'radio'},
            { name: 'bottomBar', text: 'Display bottom bar', value: false, type: 'boolean' },
            { name: 'colsNum', text: 'Number of columns', value: 4, choices: [1, 2, 3, 4, 6] },
            { name: 'avatarBubblesInterval', value: 0, text: 'Display Avatar bubbles every $1 seconds', type: 'int' },
            { name: 'avatarBubblesDuration', value: 0, text: 'Keep Avatar animation during $1 seconds', type: 'int' },
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 }
        ]
    },
    TOPUSERS: {
        value: 'topUsers',
        text: 'Top Users',
        description: 'Shows the top contributors in the last hour',
        options: [
            { name: 'timeInterval', value: 2, text: 'Display top users of the $1 last hours', type: 'int' },
            { name: 'excludedProfiles', text: 'Profiles to Exclude from top users (enter person\'s name or user name without the @)', type: 'freelist' },
            //{name: 'useBackgroundImage', text: 'Use background image', value: true, type: 'boolean'},
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 }
        ]
    },
    TOPHASHTAGS: {
        value: 'topHashtags',
        text: 'Top Hashtags',
        description: 'Top hashtags used',
        options: [
            { name: 'timeInterval', value: 12, text: 'Display top hashtags of the $1 last hours', type: 'int' },
            { name: 'nbTags', value: 40, text: 'Number of tags to display : ', type: 'int' },
            //{name: 'tagsColor', value: 'black', text: 'Color of the tags', type: 'input', size: 10},
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 }
        ]
    },
    /*
        AVATARWALL: {
            value: 'avatarWall',
            text: 'Avatar Wall',
            description: 'Shows the contributors',
            options: [
                {name: 'type', value: 'cube', choices: ['cube', 'sphere', 'bubbles']}
            ]
        },*/
    FAVPOSTS: {
        value: 'favoritePage',
        text: 'Highlights',
        description: 'Animates highlighted posts',
        options: [
            // {name: 'useBackgroundImage', text: 'Use background image', value: true, type: 'boolean'},
            { name: 'favInterval', value: 0, text: 'Display each post for $1 seconds. <br><b>if set to 0</b>, the time used ' +
                    'will be calculated from the "page duration". <br>' +
                    '<b>if NOT set to 0</b>, the "page duration" will be ignored.', type: 'int' },
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 }
        ]
    },
    COVERPAGE: {
        value: 'coverPage',
        text: 'Cover Page',
        description: 'Presentation page',
        options: [
            { name: 'image-url', text: 'Url of a background image', type: 'input', maxLength: 512 },
        ]
    },
    LIVESTATS: {
        value: 'liveStats',
        text: 'Statistics',
        description: 'Shows the evolution of the number of posts',
        options: [
            { name: 'timeInterval', value: 12, text: 'Display post evolution of the $1 last hours', type: 'int' },
            { name: 'startCount', text: 'Start counting from', type: 'time-picker', value: dates.ZInow - 3600 * 24 },
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 }
        ]
    },
    FULLSCREEN: {
        value: 'fullScreen',
        text: 'Full screen post',
        description: 'Display a specific post on stand alone',
        options: [
            { name: 'backgroundImage', text: 'Url of a background image', type: 'input', maxLength: 512 },
            { name: 'postId', type: 'hidden' }
        ]
    }
};
//export const wallPages = toLOV(pages);
export var wallPages = pages;
export var confOptions = options;
var Wall = /** @class */ (function () {
    function Wall(wall) {
        this.wall = wall;
        if (wall) {
            this.jsonWall = wall.metas ? JSON.parse(wall.metas) : {};
            this.jsonWall.id = wall.id;
            this.jsonWall.name = wall.name;
            this.jsonWall.widgetIds = wall.widgets;
        }
        else {
            this.jsonWall = { widgetIds: [], version: 0 };
        }
        this.initPagesConfig();
    }
    Object.defineProperty(Wall.prototype, "configurations", {
        get: function () {
            /*if (!this.jsonWall.configurations) this.jsonWall.configurations = [];
                    return this.jsonWall.configurations;*/
            //if (this.jsonWall.configurations) throw "TODO" //return this.convertConf(this.jsonWall.configurations);
            if (!this.jsonWall.wallConfs)
                this.jsonWall.wallConfs = [];
            return this.jsonWall.wallConfs;
        },
        set: function (pc) {
            //this.jsonWall.configurations = pc;
            this.jsonWall.wallConfs = pc;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "delays", {
        get: function () {
            if (!this.jsonWall.delays) {
                this.jsonWall.delays = { mainPosts: 30, pinnedPosts: 30, sponsoredPosts: 30, sandbox: 0 };
            }
            return this.jsonWall.delays;
        },
        set: function (delays) {
            this.jsonWall.delays = delays;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "description", {
        get: function () { return this.jsonWall.description; },
        set: function (description) {
            this.jsonWall.description = description;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "favoritePosts", {
        get: function () {
            if (!this.jsonWall.favoritePosts) {
                this.jsonWall.favoritePosts = [];
            }
            return this.jsonWall.favoritePosts;
        },
        set: function (fp) {
            this.jsonWall.favoritePosts = fp;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Wall.prototype, "id", {
        get: function () { return this.jsonWall.id; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Wall.prototype, "moderated", {
        get: function () {
            if (!this.jsonWall.moderated) {
                this.jsonWall.moderated = {};
            }
            return this.jsonWall.moderated;
        },
        set: function (moderated) {
            this.jsonWall.moderated = moderated;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "name", {
        get: function () { return this.jsonWall.name; },
        set: function (name) {
            this.jsonWall.name = name;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "overlayPost", {
        get: function () {
            if (!this.jsonWall.moderated) {
                this.jsonWall.overlayPost = {};
            }
            return this.jsonWall.overlayPost;
        },
        set: function (postId) {
            this.jsonWall.overlayPost = postId;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "pagesConfig", {
        get: function () {
            throw "OBSOLET";
            //  return this.jsonWall.pagesConfig;
        },
        set: function (pc) {
            throw "OBSOLET";
            //this.jsonWall.pagesConfig = pc;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "wallConfs", {
        get: function () {
            if (!this.jsonWall.wallConfs)
                this.jsonWall.wallConfs = [];
            return this.jsonWall.wallConfs;
        },
        set: function (conf) {
            this.jsonWall.wallConfs = conf;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Wall.prototype, "defaultPageConf", {
        get: function () {
            if (!this.jsonWall.defaultPageConf)
                this.jsonWall.defaultPageConf = [];
            return this.jsonWall.defaultPageConf;
        },
        set: function (conf) {
            this.jsonWall.defaultPageConf = conf;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Wall.prototype, "defaultOptions", {
        get: function () {
            if (!this.jsonWall.defaultOptions)
                this.jsonWall.defaultOptions = {};
            return this.jsonWall.defaultOptions;
        },
        set: function (option) {
            this.jsonWall.defaultOptions = option;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Wall.prototype, "poweredBy", {
        get: function () {
            return this.jsonWall.poweredBy;
        },
        set: function (img) {
            this.jsonWall.poweredBy = img;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "pinnedPosts", {
        get: function () {
            if (!this.jsonWall.pinnedPosts) {
                this.jsonWall.pinnedPosts = [];
            }
            return this.jsonWall.pinnedPosts;
        },
        set: function (pp) {
            this.jsonWall.pinnedPosts = pp.filter(function (p) { return !!p; });
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Wall.prototype, "sponsoredPosts", {
        get: function () {
            if (!this.jsonWall.sponsoredPosts) {
                this.jsonWall.sponsoredPosts = [];
            }
            return this.jsonWall.sponsoredPosts;
        },
        set: function (sp) {
            this.jsonWall.sponsoredPosts = sp.filter(function (p) { return !!p; });
            ;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "styles", {
        get: function () {
            if (!this.jsonWall.styles) {
                this.jsonWall.styles = {};
            }
            return this.jsonWall.styles;
        },
        set: function (styles) {
            this.jsonWall.styles = styles;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "thumbnail", {
        get: function () {
            return this.jsonWall.thumbnail;
        },
        set: function (img) {
            this.jsonWall.thumbnail = img;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "title", {
        get: function () { return this.jsonWall.title; },
        set: function (title) {
            this.jsonWall.title = title;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "version", {
        /*get version(): string {
            //initialising setters :
            let a: any = this.pinnedPosts;
            a = this.sponsoredPosts;
            a = this.favoritePosts;
            a = this.moderated;
            a = this.delays;
            a = this.styles;
            a = this.overlayPost;
    
            return JSON.stringify(this.jsonWall);
        };*/
        get: /*get version(): string {
                //initialising setters :
                let a: any = this.pinnedPosts;
                a = this.sponsoredPosts;
                a = this.favoritePosts;
                a = this.moderated;
                a = this.delays;
                a = this.styles;
                a = this.overlayPost;
        
                return JSON.stringify(this.jsonWall);
            };*/
        function () { return this.jsonWall.version; },
        set: function (v) { this.jsonWall.version = v; },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(Wall.prototype, "widgetIds", {
        get: function () {
            if (!this.jsonWall.widgetIds)
                this.jsonWall.widgetIds = [];
            return this.jsonWall.widgetIds;
        },
        set: function (ws) {
            this.jsonWall.widgetIds = ws;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Wall.prototype.addPinnedPost = function (p) {
        if (!this.pinnedPosts.find(function (pp) { return pp.id === p.id; })) {
            this.pinnedPosts.push(p);
            return true;
        }
        else {
            return false;
        }
    };
    Wall.prototype.isBlocked = function (post) {
        return (this.moderated.posts || []).findIndex(function (p) { return p === post.id; }) >= 0 ||
            (this.moderated.users || []).findIndex(function (u) { return u === post.user.id; }) >= 0;
    };
    Wall.prototype.isOkPost = function (p, conf) {
        if (!p)
            return false;
        var ok = true;
        if (tools.deepGet(conf, 'options', 'noRT') !== false // default = true!
            && p.isShared)
            return false;
        if (conf &&
            tools.deepGet(conf, 'options', 'onlyMedia')
            && !p.thumbnail)
            return false;
        if (p.attributionKind === 'tw'
            && !p.thumbnail
            && !p.text)
            return false;
        if (this.moderated.posts && this.moderated.posts.length > 0) {
            ok = ok && this.moderated.posts.indexOf(p.id) < 0;
        }
        if (this.moderated.users && this.moderated.users.length > 0) {
            ok = ok && this.moderated.users.indexOf(p.user.id) < 0;
        }
        return ok;
    };
    /*
    moderatePost(id): void {
        if (!this.moderated.posts || this.moderated.posts.indexOf(id) < 0) {
            this.moderated.posts = (this.moderated.posts || []).push(id);
        }
    }
    moderateUser(id): void {
        if (!this.moderated.users || this.moderated.users.indexOf(id) < 0) {
            this.moderated.users = (this.moderated.users || []).push(id);
        }
    }
*/
    /*
        moderatePost(id): void {
            if (!this.moderated.posts || this.moderated.posts.indexOf(id) < 0) {
                this.moderated.posts = (this.moderated.posts || []).push(id);
            }
        }
        moderateUser(id): void {
            if (!this.moderated.users || this.moderated.users.indexOf(id) < 0) {
                this.moderated.users = (this.moderated.users || []).push(id);
            }
        }
    */
    Wall.prototype.updatePinnedPost = /*
        moderatePost(id): void {
            if (!this.moderated.posts || this.moderated.posts.indexOf(id) < 0) {
                this.moderated.posts = (this.moderated.posts || []).push(id);
            }
        }
        moderateUser(id): void {
            if (!this.moderated.users || this.moderated.users.indexOf(id) < 0) {
                this.moderated.users = (this.moderated.users || []).push(id);
            }
        }
    */
    function (p) {
        var i = this.pinnedPosts.findIndex(function (pp) { return pp.id === p.id; });
        if (i < 0) {
            return false;
        }
        else {
            this.pinnedPosts.splice(i, 1, p);
            return true;
        }
    };
    Wall.prototype.toString = function () {
        return JSON.stringify(this.jsonWall);
    };
    Wall.prototype.toJson = function () {
        return {
            wall_id: this.id,
            name: this.name,
            metas: JSON.stringify(this.jsonWall)
        };
    };
    Wall.prototype.clone = function () {
        var jw = this.toJson();
        jw.created = 0;
        jw.deleted = 0;
        jw.id = jw.wall_id;
        jw.widgets = this.jsonWall.widgetIds;
        var clonedWall = new Wall(jw);
        clonedWall.needModeration = this.needModeration;
        return clonedWall;
    };
    Wall.prototype.initPagesConfig = function () {
        var _this = this;
        if (this.wallConfs.length > 0)
            return; // already new format
        if (!this.jsonWall.configurations || this.jsonWall.configurations.length === 0) {
        }
        else {
            var wallPagesEntries_1 = tools.objectEntries(wallPages);
            this.jsonWall.configurations.forEach(function (c, i) {
                //pageName -> {options: any, duration: number, show: boolean}
                var pages = [];
                var _loop_1 = function (p) {
                    var oldConf = c[p];
                    var pageEntry = wallPagesEntries_1.find(function (_a) {
                        var k = _a[0], v = _a[1];
                        return v.value === p;
                    });
                    if (!pageEntry)
                        return "continue";
                    var pageConf = { id: pageEntry[0] };
                    pageConf.duration = oldConf.duration;
                    pageConf.options = oldConf.options;
                    pages.push(pageConf);
                };
                for (var p in c) {
                    _loop_1(p);
                }
                //fix bug for mutliple conf with same name
                if (_this.wallConfs.find(function (co) { return co.id === c.name; }))
                    c.name += Math.floor(Math.random() * 1000);
                _this.wallConfs.push({
                    id: c.name,
                    pages: pages,
                    refreshDt: c.refreshDt,
                    frontPage: c.frontPage
                });
            });
            delete this.jsonWall.configurations;
        }
        if (this.defaultPageConf.length === 0) {
            var defaultConfig_1 = [];
            Object.keys(wallPages).forEach(function (k) { return defaultConfig_1.push({ id: k, duration: 0 }); });
            this.defaultPageConf = defaultConfig_1;
        }
        /*
        
                const defaultConfig = {}
                wallPages.keys.forEach(k => defaultConfig[k] = {duration: 0});
                if (!this.jsonWall.pagesConfig) this.jsonWall.pagesConfig = {};
                this.jsonWall.pagesConfig = tools.deepAssign({}, defaultConfig, this.jsonWall.pagesConfig);
        
                // new config format
                if (!this.jsonWall.configurations || this.jsonWall.configurations.length === 0) {
                    const defaultConf = tools.clone(this.jsonWall.pagesConfig);
                    defaultConf.name = 'default'
                    this.jsonWall.configurations = [defaultConf];
                } else {
                    this.jsonWall.configurations.forEach((c, i) => {
                        this.jsonWall.configurations[i] = tools.deepAssign({}, defaultConfig, c);
                    })
                }
                */
    };
    return Wall;
}());
export { Wall };
