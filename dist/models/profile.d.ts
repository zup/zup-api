import { Label } from './labels';
export declare type JsonProfile = {
    attribution: string;
    b2b_followers: number;
    blocked?: boolean;
    category?: string;
    cover?: string;
    created?: number;
    description?: string;
    email?: string;
    extern?: boolean;
    followers?: number;
    followings?: number;
    format?: number;
    gender?: string;
    id: string;
    interests: string[];
    interests_details?: {
        intensity: number;
        tags: string;
    }[];
    is_profile_engaged?: boolean;
    isError?: boolean;
    kind: string;
    last_seen?: number;
    last_source?: string;
    last_widget_id?: string;
    lead_score?: number;
    live_in?: any;
    name: string;
    profile_label_ids?: string[];
    screen_name: string;
    search_terms?: string[];
    thumbnail?: string;
    total_published?: number;
    updated?: number;
    xscore?: number;
};
export declare type SimpleProfile = {
    id: string;
    name: string;
    screenName: string;
    thumbnail: string;
};
export interface ProfileInterface {
    id: string;
    attributionLink: string;
    attributionKind: string;
    cover?: string;
    customValue?: any;
    description?: string;
    interests?: string[];
    followers?: number;
    format?: number;
    labels?: Label[];
    lastSeen?: number;
    leadScore?: number;
    livesIn?: any;
    name: string;
    nbPosts?: number;
    screenName: string;
    thumbnail: string;
    widgetId?: string;
}
export declare class Profile implements ProfileInterface {
    private juser;
    private allLabels;
    id: string;
    isError: boolean;
    kind: string;
    name: string;
    screenName: string;
    email: string;
    thumbnail: string;
    description: string;
    attributionLink: string;
    attributionKind: string;
    cover: string;
    interests: string[];
    interestsV2: {
        name: string;
        intensity?: number;
        tags?: string[];
    }[];
    followers: number;
    format?: number;
    gender: string;
    leadScore: number;
    lastSeen: number;
    nbPosts: number;
    widgetId: string;
    labels: any[];
    livesIn?: any;
    isBanned: boolean;
    isEngaged: boolean;
    customValue: any;
    constructor(juser: JsonProfile, allLabels?: Label[]);
    toInfluencer(): Influencer;
}
export declare type JsonInfluencer = JsonProfile & {
    engagement_rate?: number;
    followings?: number;
};
export declare class Influencer extends Profile {
    private jinfluencer;
    private allILabels;
    engagementRate: number;
    followings: number;
    campaignRank: number;
    engagements: number;
    communityGender: {
        name;
        count;
    }[];
    communityInterests: {
        name;
        count;
    }[];
    communityAge: {
        name;
        count;
    }[];
    communityLanguage: {
        name;
        count;
    }[];
    communityLivesIn: {
        name;
        count;
    }[];
    constructor(jinfluencer: JsonInfluencer, allILabels?: Label[]);
}
