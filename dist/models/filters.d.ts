import { WidgetContext } from './';
export declare enum FILTER_RULES_CATEGORIES {
    PROFILE = "profile",
    CONTENT = "content",
    AUDIENCE = "audience",
    CONTEXT = "context",
}
export declare const FILTER_RULES_CATEGORIES_DESC: {
    profile: {
        name: string;
        icon: string;
    };
    content: {
        name: string;
        icon: string;
    };
    audience: {
        name: string;
        icon: string;
    };
    context: {
        name: string;
        icon: string;
    };
};
export declare const FILTER_RULES: {
    order_by: {
        id: string;
        value: string;
        hidden: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    author_id: {
        id: string;
        value: string;
        hidden: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    zone: {
        id: string;
        value: string;
        hidden: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    date: {
        id: string;
        text: string;
        datePicker: boolean;
        valueToRule: (v: any, query?: FilterQuery) => {
            after: any;
            before: any;
        } | {};
        ruleToValue: (query: FilterQuery) => {
            from: any;
            to: any;
        };
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    engagement_rate_more_than: {
        id: string;
        text: string;
        text2: string;
        inputSize: number;
        max: number;
        defaultValue: number;
        isInt: boolean;
        valueToRule: (v: number, query: FilterQuery) => {
            engagement_rate_more_than: number;
        } | {};
        ruleToValue: (query: FilterQuery) => number;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    community_engagement_rate_more_than: {
        id: string;
        text: string;
        text2: string;
        inputSize: number;
        max: number;
        defaultValue: number;
        isInt: boolean;
        valueToRule: (v: number, query: FilterQuery) => {
            community_engagement_rate_more_than: number;
        } | {};
        ruleToValue: (query: FilterQuery) => number;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    filter_shares: {
        id: string;
        text: string;
        checkbox: boolean;
        defaultValue: boolean;
        isValue: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    filter_reply: {
        id: string;
        text: string;
        checkbox: boolean;
        defaultValue: boolean;
        isValue: boolean;
        forLabel: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    follower_less_than: {
        id: string;
        text: string;
        inputSize: number;
        defaultValue: number;
        isInt: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    follower_more_than: {
        id: string;
        text: string;
        inputSize: number;
        defaultValue: number;
        isInt: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    gender: {
        id: string;
        text: string;
        choicePicker: boolean;
        choices: {
            value: string;
            text: string;
        }[];
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    has_interests: {
        id: string;
        text: string;
        interestPicker: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    lang: {
        id: string;
        text: string;
        choicePicker: boolean;
        choices: {
            value: string;
            text: string;
        }[];
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    lang2: {
        id: string;
        text: string;
        choicePicker: boolean;
        choices: {
            value: string;
            text: string;
        }[];
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    lead_score_more_than: {
        id: string;
        text: string;
        inputSize: number;
        defaultValue: number;
        isInt: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    not_profile_label_id: {
        id: string;
        text: string;
        labelPicker: boolean;
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    name_like: {
        id: string;
        text: string;
        inputSize: number;
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    profile_label_id: {
        id: string;
        text: string;
        labelPicker: boolean;
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    post_label_id: {
        id: string;
        text: string;
        sovPicker: boolean;
        lexicalCategories: string[];
        defaultValue: string;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    published_more_than: {
        id: string;
        text: string;
        text2: string;
        inputSize: number;
        defaultValue: number;
        isInt: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    source: {
        id: string;
        text: string;
        kindPicker: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    terms: {
        id: string;
        text: string;
        inputSize: number;
        multiple: boolean;
        valueToRule: (ks: any, query?: FilterQuery) => {
            terms: any;
        } | {};
        ruleToValue: (query: FilterQuery) => string[];
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    description_terms: {
        id: string;
        text: string;
        inputSize: number;
        multiple: boolean;
        valueToRule: (ks: any, query?: FilterQuery) => {
            description_terms: any;
        } | {};
        ruleToValue: (query: FilterQuery) => string[];
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    xscore_more_than: {
        id: string;
        text: string;
        inputSize: number;
        defaultValue: number;
        isInt: boolean;
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    widgets: {
        id: string;
        text: string;
        widgetPicker: boolean;
        ruleToValue: (query: FilterQuery) => {
            type: string;
            id: string;
        } | {
            type: string;
            id: number;
        };
        valueToRule: (v: string | {
            type: string;
            id: string | number;
        }, query?: FilterQuery) => {
            widget_id: string | number;
        } | {
            widget_category: string | number;
        } | {
            widget_type: string | number;
        } | {};
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    community_gender: {
        id: string;
        text: string;
        payload: {
            type: string;
            default: number;
        };
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    community_interests: {
        id: string;
        text: string;
        payload: {
            type: string;
            default: number;
        };
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    community_age: {
        id: string;
        text: string;
        payload: {
            type: string;
            default: number;
        };
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
    community_language: {
        id: string;
        text: string;
        payload: {
            type: string;
            default: number;
        };
        category: FILTER_RULES_CATEGORIES;
        for: string[];
    };
};
export declare class FilterQuery {
    private ruleOptions;
    after?: number;
    before?: number;
    widget_id?: string;
    widget_category?: number;
    widget_type?: number;
    widgets?: {
        type: string;
        id: string | number;
    };
    author_id?: string;
    published_by_widget_more_than?: number;
    published_more_than?: number;
    published_by_widget_more_days_than?: number;
    engagement_rate_more_than?: number;
    community_engagement_rate_more_than?: number;
    terms?: string[];
    description_terms?: string[];
    source?: string;
    follower_more_than?: number;
    follower_less_than?: number;
    lead_score_more_than?: number;
    name_like?: string;
    post_content_contains?: string;
    profile_kind?: string;
    has_interest?: string;
    has_interest_detail?: string;
    gender: string;
    lang: string;
    order_by?: string;
    constructor(rules?: any, ruleType?: any, ruleOptions?: any);
    clear(persistent: string[]): void;
    fromContext(widgetContext?: WidgetContext, ruleOptions?: any): FilterQuery;
    getActiveRules(category?: FILTER_RULES_CATEGORIES, withHidden?: boolean): any[];
    isActive(rid: string): boolean;
    ruleValue(rid: string): any;
    toJSON(): any;
    readonly length: number;
}
export declare class FilterRules {
    widget_id?: string;
    widget_category?: number;
    widget_type?: number;
    widgets?: {
        type: string;
        id: string | number;
    };
    constructor(rules?: any);
}
export declare class PostFilterRules extends FilterRules {
    order_by?: string;
    after?: number;
    before?: number;
    tag?: string;
    terms?: string[];
    source?: string;
    lang?: string;
    post_label_id?: string;
    author_id?: string;
    sentiment_value?: number;
    xscore_more_than?: number;
    xlike_more_than?: number;
    xcomment_more_than?: number;
    xshare_more_than?: number;
    engagement_rate_more_than?: number;
    widget_id?: string;
    widget_category?: number;
    widget_type?: number;
    widgets?: {
        type: string;
        id: string | number;
    };
    constructor(rules: any);
}
export declare class ProfileFilterRules extends FilterRules {
    order_by?: string;
    after?: number;
    before?: number;
    published_by_widget_more_than?: number;
    published_more_than?: number;
    published_by_widget_more_days_than?: number;
    terms?: string[];
    source?: string;
    follower_more_than?: number;
    follower_less_than?: number;
    lead_score_more_than?: number;
    name_like?: string;
    post_content_contains?: string;
    profile_kind?: string;
    has_interest?: string;
    has_interest_detail?: string;
    gender: string;
    lang: string;
    profile_label_id: string;
    widget_id?: string;
    widget_category?: number;
    widget_type?: number;
    widgets?: {
        type: string;
        id: string | number;
    };
    constructor(rules: any);
}
