var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import { Subject } from 'rxjs';
import { dates, ids, tools, bigbang } from '../misc/utils';
import { toLOV, attributionSources } from '../listOfValues';
import { View, Profile, PostFilterRules, ProfileFilterRules, PublishAction } from './';
import { FilterQuery } from "./filters";
export var widgetTypes = {
    PLACE: { value: 1, menuIndex: 1, label: 'Places', label2: 'Place', path: 'places', icon: 'icon-map', name: 'place' },
    LISTENING: { value: 2, menuIndex: 2, label: 'Listening', path: 'listening', icon: 'icon-globe', name: 'listening' },
    VIEW: { value: 3, label: 'View', path: 'view' },
    PROFILES: { value: 4, label: 'Profile List', path: 'profile-list' }
};
toLOV(widgetTypes, {
    findFromPath: function (path) {
        var myT = this.values.find(function (t) { return t.path === path; });
        return myT && myT.value;
    }
});
export var widgetCategories = {
    MY_PLACE: {
        value: 1,
        type: widgetTypes.PLACE,
        label: 'My Places',
        icon: 'icon-location-pin',
        description: 'Belongs to my places'
    },
    WATCH: {
        value: 2,
        type: widgetTypes.PLACE,
        label: 'Watch',
        icon: 'icon-chart',
        description: 'Place to watch'
    },
    COMPETITOR: {
        value: 3,
        type: widgetTypes.PLACE,
        label: 'Competitors',
        icon: 'icon-magnifier',
        description: 'Competitor'
    },
    LISTENING_ME: {
        value: 4,
        type: widgetTypes.LISTENING,
        label: 'My Listenings',
        icon: 'icon-globe',
        description: 'Is one of my listenings'
    },
    LISTENING_WATCH: {
        value: 5,
        type: widgetTypes.LISTENING,
        label: 'Watch',
        icon: 'icon-chart',
        description: 'A keyword to watch'
    },
    LISTENING_COMPETITOR: {
        value: 6,
        type: widgetTypes.LISTENING,
        label: 'Competitors',
        icon: 'icon-magnifier',
        description: 'Belongs to my competitors'
    },
    PROFILE_LIST: {
        value: 7,
        type: widgetTypes.PROFILES,
        label: 'List of influencers',
        description: 'Influencers I want to monitor'
    },
    PROFILE_MY_BRAND: {
        value: 8,
        type: widgetTypes.PROFILES,
        label: 'My brand',
        description: 'Social accounts of my brand'
    },
    PROFILE_COMPETITOR: {
        value: 9,
        type: widgetTypes.PROFILES,
        label: 'My competitors',
        description: 'Social accounts of my competitors'
    },
    PROFILE_CAMPAIGN: {
        value: 10,
        type: widgetTypes.PROFILES,
        label: 'Influencer Campaign',
        description: 'Influencers for one of my campaigns'
    }
};
toLOV(widgetCategories, {
    filter: function (type) {
        return this.values.filter(function (c) { return c.type === type; });
    }
});
var Widget = /** @class */ (function () {
    function Widget(widget, type) {
        var _this = this;
        this.widgetViews = [];
        this._metas = {};
        this._view = {};
        this._mandatoryKeywords = [];
        this._associatedKeywords = [];
        this._excludeKeywords = [];
        this._useDefaultThresholds = true;
        this.options = {};
        if (widget) {
            this._jsonWidget = widget;
            if (widget) {
                this.originalQueries = tools.clone(widget.view.queries || []);
            }
            this._view = widget.view;
            this._view.ends_at = this._view.ends_at || 0;
            this._view.starts_at = this._view.starts_at || 0;
            // transform new format to old format:
            // this.view.queries = widget.view.queries;
            if (!widget.name || widget.name.length === 0) {
                throw 'INCORRECT_FORMAT()';
            }
            if (widget.metas) {
                this._metas = JSON.parse(widget.metas);
                // tmp dev parentWidgetId
                this._jsonWidget.widget_parent = this._jsonWidget.widget_parent || this._metas.parent_widget_id;
                // tmp bug du type laisse a 1
                if (this._jsonWidget.widget_parent) {
                    this._jsonWidget.type = 3;
                }
            }
            else {
                throw 'INCORRECT_FORMAT()';
            }
            this._useDefaultThresholds = this._metas.useDefaultThresholds;
            // convert queries to mandatory / associated keywords :
            if (this._view.queries && this._view.queries.length > 0) {
                this.view.queries.forEach(function (q) {
                    if (!q.and_terms)
                        throw "BAD_VIEW_FORMAT('and_terms'," + _this.id + ")";
                    _this._mandatoryKeywords = _this._mandatoryKeywords.concat(q.and_terms);
                    if (q.or_terms && q.or_terms.length > 0 &&
                        _this._associatedKeywords.length > 0 &&
                        q.or_terms.toString() !== _this._associatedKeywords.toString()) {
                        throw "BAD_VIEW_FORMAT('or_terms'," + _this.id + ")";
                    }
                    else if (q.or_terms && q.or_terms.length > 0) {
                        _this._associatedKeywords = q.or_terms;
                    }
                });
                this._mandatoryKeywords = tools.unduplicate(this._mandatoryKeywords);
                this._associatedKeywords = tools.unduplicate(this._associatedKeywords);
            }
            this._excludeKeywords = this._view.exclude_terms || [];
            this.buildQueries(); // for consistance inempty exclude_terms
        }
        else {
            this._metas = { useDefaultThresholds: true };
            this._jsonWidget = {
                metas: JSON.stringify(this._metas),
                view: this._view
            };
        }
        if (type) {
            this.type = type;
            if (this.type === widgetTypes.PROFILES) {
                this.options = {
                    "no_bootstrap": true,
                    "no_auto_search_places": true,
                    "without_mission": true,
                };
            }
        }
    }
    Object.defineProperty(Widget.prototype, "associatedLexical", {
        get: function () {
            return this._metas.associatedLexical;
        },
        set: function (lid) {
            this._metas.associatedLexical = lid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "category", {
        get: function () {
            if (this._jsonWidget.category && typeof this._jsonWidget.category !== 'number') {
                console.log("CATEGORY=", this._jsonWidget.category);
                throw 'INVALID CATEGORY FORMAT';
            }
            return this._jsonWidget.category;
        },
        set: function (category) {
            if (typeof category === 'string')
                category = parseInt(category, 10);
            this._jsonWidget.category = category;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "customerLeadThreshold", {
        get: function () {
            return this._jsonWidget.customer_lead_threshold || undefined;
        },
        set: function (t) {
            this._jsonWidget.customer_lead_threshold = t;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "dateRangeDt", {
        get: function () {
            if (!this._view)
                return {};
            return {
                from: this._view.starts_at && dates.toDateFromZI(this._view.starts_at),
                to: this._view.ends_at && dates.toDateFromZI(this._view.ends_at),
            };
        },
        set: function (r) {
            this.startsAtDt = r.from;
            this.endsAtDt = r.to;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "endsAt", {
        get: function () {
            return this._view.ends_at || 0;
        },
        set: function (e) {
            this._view.ends_at = e || 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "endsAtDt", {
        get: function () {
            if (this._view.ends_at)
                return dates.toDateFromZI(this._view.ends_at);
        },
        set: function (e) {
            this._view.ends_at = dates.toZIFromDate(e);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "excludedPlaces", {
        get: function () {
            if (!this._jsonWidget.excludedPlaces) {
                this._jsonWidget.excludedPlaces = [];
            }
            ;
            return this._jsonWidget.excludedPlaces;
        },
        set: function (profiles) {
            this._jsonWidget.excludedPlaces = profiles;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "excludedProfiles", {
        get: function () {
            if (!this._jsonWidget.excludedProfiles) {
                this._jsonWidget.excludedProfiles = [];
            }
            ;
            return this._jsonWidget.excludedProfiles;
        },
        set: function (profiles) {
            this._jsonWidget.excludedProfiles = profiles;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "id", {
        get: function () {
            return this._jsonWidget.id;
        },
        set: function (id) {
            if (this._jsonWidget.id)
                throw 'ID_ALREADY_EXISTS()';
            this._jsonWidget.id = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "influencerThreshold", {
        get: function () {
            return this._jsonWidget.influencer_threshold || undefined;
        },
        set: function (t) {
            this._jsonWidget.influencer_threshold = t;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "isProfileListWidget", {
        get: function () {
            return !!this._view.profiles_label_id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "brandListeningId", {
        get: function () {
            if (!this.isProfileListWidget)
                return;
            return this._metas.brandListeningId;
        },
        set: function (id) {
            this._metas.brandListeningId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "brandLexicalId", {
        get: function () {
            if (!this.isProfileListWidget)
                return;
            return this._metas.brandLexicalId;
        },
        set: function (id) {
            this._metas.brandLexicalId = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "excludedSources", {
        get: function () {
            return this._view.exclude_sources;
        },
        set: function (s) {
            this._view.exclude_sources = s;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "jsonObject", {
        /**
         * @returns {any} a clone of the _jsonWidget (not the original)
         */
        get: /**
             * @returns {any} a clone of the _jsonWidget (not the original)
             */
        function () {
            return JSON.parse(JSON.stringify(this._jsonWidget));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "labelId", {
        get: function () {
            return this._view.profiles_label_id;
        },
        set: function (id) {
            this._view.profiles_label_id = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "lang", {
        get: function () {
            return this._view && this._view.lang;
        },
        set: function (l) {
            this._view.lang = l || '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "loyaltyDaysThreshold", {
        get: function () {
            if (this._jsonWidget.loyalty_threshold)
                return this._jsonWidget.loyalty_threshold[1];
        },
        set: function (t) {
            if (this._jsonWidget.loyalty_threshold) {
                this._jsonWidget.loyalty_threshold[1] = t;
                if (this._jsonWidget.loyalty_threshold[0] === this._jsonWidget.loyalty_threshold[1] &&
                    this._jsonWidget.loyalty_threshold[0] === undefined) {
                    this._jsonWidget.loyalty_threshold = undefined;
                }
            }
            else {
                this._jsonWidget.loyalty_threshold = [undefined, t];
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "loyaltyNbThreshold", {
        get: function () {
            if (this._jsonWidget.loyalty_threshold)
                return this._jsonWidget.loyalty_threshold[0];
        },
        set: function (t) {
            if (this._jsonWidget.loyalty_threshold) {
                this._jsonWidget.loyalty_threshold[0] = t;
                if (this._jsonWidget.loyalty_threshold[0] === this._jsonWidget.loyalty_threshold[1] &&
                    this._jsonWidget.loyalty_threshold[0] === undefined) {
                    this._jsonWidget.loyalty_threshold = undefined;
                }
            }
            else {
                this._jsonWidget.loyalty_threshold = [t, undefined];
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "mainWidgetId", {
        get: function () {
            return this.parentWidgetId || this.id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "metas", {
        get: function () {
            return JSON.stringify(this._metas);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "minusTags", {
        get: function () {
            return this._excludeKeywords;
        },
        set: function (ts) {
            this._excludeKeywords = ts || [];
            this.buildQueries();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "name", {
        get: function () {
            return this._jsonWidget.name;
        },
        set: function (n) {
            this._jsonWidget.name = n;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "noMission", {
        get: function () {
            return this._jsonWidget.without_geo_missions;
        },
        set: function (b) {
            this._jsonWidget.without_geo_missions = b;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "noReplies", {
        get: function () {
            return !!(this._view && this._view.exclude_reply);
        },
        set: function (s) {
            this._view.exclude_reply = s;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "noShares", {
        get: function () {
            return !!(this._view && this._view.exclude_shares);
        },
        set: function (s) {
            this._view.exclude_shares = s;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "orTags", {
        get: function () {
            return this._associatedKeywords;
        },
        set: function (ts) {
            this._associatedKeywords = ts || [];
            this.buildQueries();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "parentWidgetId", {
        get: function () {
            return this._jsonWidget.widget_parent;
        },
        set: function (id) {
            this._jsonWidget.widget_parent = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "publishActions", {
        get: function () {
            return this._jsonWidget.publishActions || [];
        },
        set: function (publishActions) {
            this._jsonWidget.publishActions = publishActions;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "queryTags", {
        get: function () {
            return { tags: this.tags, orTags: this.orTags, minusTags: this.minusTags };
        },
        set: function (queryTags) {
            this._mandatoryKeywords = queryTags.tags || [];
            this._associatedKeywords = queryTags.orTags || [];
            this._excludeKeywords = queryTags.minusTags || [];
            this.buildQueries();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "startsAt", {
        get: function () {
            return this._view.starts_at || 0;
        },
        set: function (e) {
            this._view.starts_at = e;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "startsAtDt", {
        get: function () {
            if (this._view.starts_at)
                return dates.toDateFromZI(this._view.starts_at);
        },
        set: function (e) {
            this._view.starts_at = dates.toZIFromDate(e);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "sources", {
        get: function () {
            return tools.arrayDelta(tools.objectValues(attributionSources), this._view.exclude_sources || [])[0];
        },
        set: function (sources) {
            this._view.exclude_sources = tools.arrayDelta(tools.objectValues(attributionSources), sources)[0];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "tags", {
        get: function () {
            return this._mandatoryKeywords;
        },
        set: function (ts) {
            this._mandatoryKeywords = ts || [];
            this.buildQueries();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "type", {
        get: function () {
            return this._jsonWidget.type;
        },
        set: function (t) {
            this._jsonWidget.type = t;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "useDefaultThresholds", {
        get: function () {
            return !!this._useDefaultThresholds;
        },
        set: function (useDefault) {
            this._useDefaultThresholds = useDefault;
            this._metas.useDefaultThresholds = this._useDefaultThresholds;
            this._jsonWidget.metas = JSON.stringify(this._metas);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "userDailyThreshold", {
        get: function () {
            return this._jsonWidget.user_daily_threshold || undefined;
        },
        set: function (t) {
            this._jsonWidget.user_daily_threshold = t;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "view", {
        get: function () {
            return JSON.parse(JSON.stringify(this._view));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "zone", {
        get: function () {
            if (!this._view)
                return;
            return this._view.zone;
        },
        set: function (z) {
            // TODO test format
            if (tools.objectsEquals(z, { "n": 0, "e": 0, "s": 0, "w": 0 }))
                z = undefined;
            if (!this._view)
                this._view = {};
            this._view.zone = z;
            if (!this._jsonWidget.view)
                this._jsonWidget.view = {};
            this._jsonWidget.view.zone = z;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "queries", {
        get: function () {
            return this._view.queries;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "search", {
        get: function () {
            return {
                tags: this.tags,
                or_tags: this.orTags
            };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Widget.prototype, "safeId", {
        get: function () {
            return ids.toSafe(this._jsonWidget.id);
            //return encodeURIComponent(this._id).replace('(', '%28').replace(')', '%29');
        },
        enumerable: true,
        configurable: true
    });
    Widget.prototype.clone = function (anonymous) {
        var myJsonWidget = JSON.parse(JSON.stringify(this._jsonWidget));
        if (anonymous)
            delete myJsonWidget.id;
        var w = new Widget(myJsonWidget);
        w.widgetViews = this.widgetViews;
        return w;
    };
    Widget.prototype.getView = function (viewId) {
        return this.widgetViews.find(function (v) { return v.id === viewId; });
    };
    Widget.prototype.delView = function (viewId) {
        var i = this.widgetViews.findIndex(function (v) { return v.id === viewId; });
        if (i >= 0) {
            this.widgetViews.splice(i, 1);
        }
    };
    Widget.prototype.setView = function (view) {
        var i = this.widgetViews.findIndex(function (v) { return v.id === view.id; });
        if (i >= 0) {
            this.widgetViews[i] = view;
        }
        else {
            this.widgetViews.push(view);
        }
    };
    Widget.prototype.toString = function () {
        return JSON.stringify(this._jsonWidget);
    };
    Widget.prototype.buildQueries = function () {
        var _this = this;
        if (!this._view)
            this._view = {};
        // make queries from mandatory and associated kwds :
        var queries = [];
        tools.unduplicate(this._mandatoryKeywords).forEach(function (k) {
            var q = { and_terms: [k] };
            if (_this._associatedKeywords.length > 0) {
                q.or_terms = tools.unduplicate(_this._associatedKeywords);
            }
            ;
            queries.push(q);
        });
        if (!this._view)
            this._view = {};
        this._view.queries = queries;
        this._view.exclude_terms = tools.unduplicate(this._excludeKeywords);
        if (!this._jsonWidget.view)
            this._jsonWidget.view = {};
        this._jsonWidget.view.queries = queries;
        this._jsonWidget.view.exclude_terms = this._excludeKeywords;
    };
    return Widget;
}());
export { Widget };
var WidgetContext = /** @class */ (function () {
    function WidgetContext(widget, options, filter) {
        if (options === void 0) { options = {}; }
        this.options = options;
        this.filter = filter;
        this.catId = undefined;
        this.change = new Subject();
        this._persistantsRules = {};
        if (widget && widget.id)
            this._widget = widget;
        // bug of deleted widgets still in notifications (had to return {name:'uncknown'} )
        this._filterRules = filter || {};
        // default date range = current day
        var today = dates.today;
        if (options.after === 0)
            options.after = 1;
        this._after = options.after || this._filterRules.after || dates.toZIFromDate(today.from);
        this._before = options.before || this._filterRules.before || dates.toZIFromDate(today.to);
        this.catId = options.category || tools.deepGet(this, '_filterRules', 'widget_category');
        this.type = options.type || tools.deepGet(this, '_filterRules', 'widget_type');
        /*
                 if (this._filterRules) {
                 if (!this._widget) {
                 this._widgetId = this._filterRules.widget_id;
                 this.catId = this._filterRules.widget_category;
                 this.type = this._filterRules.widget_type;
                 }
                 }*/
    }
    Object.defineProperty(WidgetContext.prototype, "_iframe", {
        get: function () {
            return;
            /*const delta = (this.before - this.after);
                     if (delta <= 24 * 3600) {
                     return; // default : 1h
                     } else if (delta <= 24 * 3600 * 31) {
                     return 24 * 3600;
                     } else if (delta <= 24 * 3600 * 180) {
                     return 24 * 3600 * 7;
                     }else {
                     return 24 * 3600 * 30;
                     };*/
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "_frame", {
        get: function () {
            var delta = (this.before - this.after);
            if (delta <= 24 * 3600) {
                return 'h'; // default : 1h
            }
            else if (delta <= 24 * 3600 * 31) {
                return 'd';
            }
            else if (delta <= 24 * 3600 * 180) {
                return 'w';
            }
            else {
                return 'm';
            }
            ;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "category", {
        get: function () {
            return this.catId;
        },
        set: function (c) {
            if (c) {
                if (typeof c === 'string')
                    c = parseInt(c, 10);
                this.type = widgetCategories.detail(c).type;
            }
            this.catId = c;
        },
        enumerable: true,
        configurable: true
    });
    /**
     *
     * @param rules
     * @param options: {dates?: boolean, widgets?: boolean, silent?: boolean}
     *          if dates = true, rules.after over rights context.after (or set to context.after to 0)
     *          if widgets, delete widget and set widget (id/category / type) of the rule
     */
    /**
         *
         * @param rules
         * @param options: {dates?: boolean, widgets?: boolean, silent?: boolean}
         *          if dates = true, rules.after over rights context.after (or set to context.after to 0)
         *          if widgets, delete widget and set widget (id/category / type) of the rule
         */
    WidgetContext.prototype.setFilterRules = /**
         *
         * @param rules
         * @param options: {dates?: boolean, widgets?: boolean, silent?: boolean}
         *          if dates = true, rules.after over rights context.after (or set to context.after to 0)
         *          if widgets, delete widget and set widget (id/category / type) of the rule
         */
    function (rules, options) {
        options = Object.assign({}, this.filterRulesOptions, options);
        //if (!options.hasOwnProperty('dates') || !options.hasOwnProperty('widgets')) throw 'FILTER_OPTIONS_REQUIRED()';
        var init = JSON.stringify(this.getContext());
        if (options.persistent) {
            this._persistantsRules = rules;
        }
        if (options.dates) {
            this._after = rules.after; //|| 1;
            this._before = rules.before; //|| dates.toZIFromDate(dates.today.to);
        }
        if (options.widgets) {
            delete this.widget;
            this._widgetId = rules.widget_id;
            this.category = rules.widget_category;
            this.type = rules.widget_type;
        }
        this._filterRules = __assign({}, (rules || {}), this._persistantsRules);
        //
        if (init !== JSON.stringify(this.getContext()) && !options.silent)
            this.triggerChange();
    };
    Object.defineProperty(WidgetContext.prototype, "frame", {
        get: function () {
            return this._frame;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "filterRules", {
        get: function () {
            return this._filterRules;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "after", {
        get: function () {
            return this._after;
        },
        set: function (after) {
            this._after = after;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "before", {
        get: function () {
            return this._before;
        },
        set: function (before) {
            this._before = before;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "widget", {
        get: function () {
            return this._widget;
        },
        set: function (widget) {
            var init = JSON.stringify(this.getContext());
            this._widget = widget;
            if (init !== JSON.stringify(this.getContext()))
                this.triggerChange();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "widgetId", {
        get: function () {
            if (this._widget) {
                return this._widget.id;
            }
            else {
                return this._widgetId;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "zone", {
        get: function () {
            return this._widget && this._widget.zone;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "view", {
        get: function () {
            return this._widget && this._widget.view;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "tags", {
        get: function () {
            return this._widget && this._widget.tags;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "orTags", {
        get: function () {
            return this._widget && this._widget.orTags;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "minusTags", {
        get: function () {
            return this._widget && this._widget.minusTags;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetContext.prototype, "dateRange", {
        get: function () {
            return { from: dates.toDateFromZI(this._after), to: dates.toDateFromZI(this._before) };
        },
        set: function (dts) {
            var init = JSON.stringify(this.getContext());
            if (typeof dts.from === 'string')
                dts.from = new Date(dts.from);
            if (typeof dts.to === 'string')
                dts.to = new Date(dts.to);
            this._after = dates.toZIFromDate(dts.from);
            this._before = dates.toZIFromDate(dts.to);
            if (init !== JSON.stringify(this.getContext()))
                this.triggerChange();
        },
        enumerable: true,
        configurable: true
    });
    /*
     * @parameter Object : options
     *                     extendedOptions: Options to add to the context (assign),
     *                                      ex : limit witch is on the card level and not the page level
     * */
    // TODO : manage category and all partners widget !
    /*
         * @parameter Object : options
         *                     extendedOptions: Options to add to the context (assign),
         *                                      ex : limit witch is on the card level and not the page level
         * */
    // TODO : manage category and all partners widget !
    WidgetContext.prototype.getContext = /*
         * @parameter Object : options
         *                     extendedOptions: Options to add to the context (assign),
         *                                      ex : limit witch is on the card level and not the page level
         * */
    // TODO : manage category and all partners widget !
    function (options) {
        if (options === void 0) { options = {}; }
        var c = {};
        if (this._filterRules && options.filter !== false) {
            Object.assign(c, this._filterRules);
            //CLEAN object with unused fields :
            delete c.widgets;
        }
        c.after = this._after;
        c.before = this._before;
        if (this._widget) {
            c.widget_id = this._widget.id;
        }
        if (this.catId) {
            c.widget_category = this.catId;
        }
        if (this.type) {
            c.widget_type = this.type;
        }
        if (options.timeline !== false) {
            // frame managment :
            var delta = (this.before - this.after) / 3600 / 24;
            var frame = void 0;
            if (delta <= 1) {
                frame = 'h';
            }
            else if (delta <= 31) {
                frame = 'd';
            }
            else if (delta <= 70) {
                frame = 'w';
            }
            else {
                frame = 'm';
            }
            c.frame = frame;
            c.hour_offset = -1 * new Date().getTimezoneOffset() / 60;
        }
        if (options.extendedOptions) {
            Object.assign(c, options.extendedOptions);
        }
        return c;
    };
    Object.defineProperty(WidgetContext.prototype, "isWidget", {
        get: function () {
            return !!this._widget || !!this._widgetId;
        },
        enumerable: true,
        configurable: true
    });
    WidgetContext.prototype.allRange = function () {
        var ar = dates.all;
        this._after = undefined; //dates.toZIFromDate(ar.from);
        this._before = undefined; //dates.toZIFromDate(ar.to);
        return this;
    };
    WidgetContext.prototype.clone = function () {
        var wc = new WidgetContext(this._widget, { category: this.catId, type: this.type }, Object.assign({}, this._filterRules));
        // I need to set before and after after, otherwhise allrange => today
        wc.after = this.after;
        wc.before = this.before;
        return wc;
    };
    WidgetContext.prototype.isInContext = function (o) {
        if (o.created || o.date) {
            if (this.after > (o.created || o.date) || this.before < (o.created || o.date))
                return false;
        }
        if (o.widgetId && this.widgetId && o.widgetId !== this.widgetId)
            return false;
        if (o.catId && this.catId && o.catId !== this.catId)
            return false;
        return true;
    };
    WidgetContext.prototype.setFilterRule = function (ruleName, ruleValue, options) {
        if (options === void 0) { options = {}; }
        if (this._persistantsRules.hasOwnProperty(ruleName) && !options.persistentForce)
            return;
        var currentRules = tools.clone(this._filterRules);
        currentRules[ruleName] = ruleValue;
        this.setFilterRules(currentRules, options); // RISQUE DE GROS BUGS, ce ne sont pas toutes les regles qui sont persistenetes, juste celle la <= tout refaire
    };
    /***
     *
     * Does not trigger change
     * @param dr
     * @returns {WidgetContext}
     */
    /***
         *
         * Does not trigger change
         * @param dr
         * @returns {WidgetContext}
         */
    WidgetContext.prototype.setRange = /***
         *
         * Does not trigger change
         * @param dr
         * @returns {WidgetContext}
         */
    function (dr) {
        this._after = dates.toZIFromDate(dr.from);
        this._before = dates.toZIFromDate(dr.to);
        return this;
    };
    WidgetContext.prototype.triggerChange = function () {
        var _this = this;
        if (!this.beforeChange) {
            this.change.next(true);
        }
        else {
            this.beforeChange().then(function (change) { if (change)
                _this.change.next(true); });
        }
    };
    return WidgetContext;
}());
export { WidgetContext };
