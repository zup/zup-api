import { Profile, Widget, Task } from './';
import { Label } from './';
export declare const actionTypes: any;
export declare type Campaign = {
    label: Label;
    publishActions: PublishAction[];
};
export declare type PublishAction = {
    id?: string;
    like?: boolean;
    comment?: boolean;
    follow?: boolean;
    mention?: boolean;
    tokenId: string;
    message?: string;
    mentionMessage?: string;
    files?: {
        id?: string;
        url: string;
    }[];
    mentionFiles?: {
        id?: string;
        url: string;
    }[];
    since?: number;
    once?: boolean;
    rateLimit?: number[];
};
export declare class Action {
    id: string;
    actionObject: any;
    actionObjectType: string;
    actionType: number;
    attribution: string;
    assignedTo: Profile;
    author: Profile;
    created: number;
    _origin: number;
    text: string;
    title: string;
    widget: Widget;
    constructor(json: any);
    readonly class: string;
}
export declare type ActionLog = {
    id: string;
    created: number;
    auto?: boolean;
    campaignId?: string;
    partnerId: string;
    widgetId: string;
    memberId: string;
    member?: Profile;
    postId: string;
    profileId: string;
    type: string;
    text: string;
    publishType: string;
    publishAccountId?: string;
    notificationId: string;
    taskId?: string;
    screen?: string;
};
export declare type ActionLogDetail = ActionLog & {
    publishAccount?: Profile;
    member?: Profile;
    profile?: Profile;
    task?: Task;
};
export declare function toActionLog(o: any, member?: Profile): ActionLog;
export declare function toPublishAction(o: any): PublishAction;
