import { dates } from '../misc/utils';
var _ICampaign = /** @class */ (function () {
    function _ICampaign(jsonIC) {
        Object.assign(this, jsonIC);
    }
    Object.defineProperty(_ICampaign.prototype, "dates", {
        get: function () {
            return { from: dates.toDateFromZI(this.after), to: dates.toDateFromZI(this.before) };
        },
        set: function (d) {
            this.after = dates.toZIFromDate(d.from);
            this.before = dates.toZIFromDate(d.to);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(_ICampaign.prototype, "OK", {
        get: function () {
            return !!(this.name && this.widget_id && this.before && this.after && this.lexical_id);
        },
        enumerable: true,
        configurable: true
    });
    return _ICampaign;
}());
export { _ICampaign };
