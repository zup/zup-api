import { Profile } from './profile';
import { Notification } from './notifications';
import { Post } from './post';
export declare const screenTypes: any;
export declare type JsonTask = {
    id?: string;
    created_at?: number;
    is_closed?: boolean;
    closed_at?: number;
    partner_id?: string;
    widget_id?: string;
    assigned_by?: string;
    assigned_to?: string;
    notification_id?: string;
    post_id?: string;
    profile_id?: string;
    message?: string;
    screen?: string;
};
export declare type JsonEventTask = JsonTask & {
    starts_at?: number;
    ends_at?: number;
    title?: string;
    allDay?: boolean;
};
export interface TaskInterface {
    id: string;
    createdAt: number;
    isClosed: boolean;
    closedAt: number;
    partnerId: string;
    widgetId: string;
    assignedBy: string;
    assignedByUser: Profile;
    assignedTo: string;
    assignedToUser: Profile;
    notificationId: string;
    postId: string;
    userId: string;
    message: string;
    screen: string;
    object: Notification | Post | Profile;
    objectAttributionType: string;
    objectAttributionLink: string;
}
export declare class Task implements TaskInterface {
    id: string;
    createdAt: number;
    isClosed: boolean;
    closedAt: number;
    partnerId: string;
    widgetId: string;
    assignedBy: string;
    assignedByUser: Profile;
    assignedTo: string;
    assignedToUser: Profile;
    notificationId: string;
    postId: string;
    userId: string;
    message: string;
    screen: string;
    object: Notification | Post | Profile;
    objectAttributionType: string;
    objectAttributionLink: string;
    attribution: string;
    constructor(jsonTask: JsonTask, object: Notification | Post | Profile, assignedBy: Profile, assignedTo: Profile);
}
export declare class TaskEvent {
    id: string;
    isClosed: boolean;
    widgetId: string;
    assignedBy: string;
    assignedTo: string;
    notificationId: string;
    postId: string;
    profileId: string;
    message: string;
    screen: string;
    start: number;
    end: number;
    title: string;
    color: string;
    allDay: boolean;
    profile?: Profile;
    post?: Post;
    notification: Notification;
    constructor(jsonTask: JsonEventTask);
    toCalendarEvent(me: string): {
        title: string;
        start: any;
        end: any;
        color: {
            primary: string;
            secondary: string;
        };
        draggable: boolean;
        resizable: {
            beforeStart: boolean;
            afterEnd: boolean;
        };
        meta: {
            taskId: string;
            profileId: string;
        };
    };
}
