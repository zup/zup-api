import { FilterQuery } from './';
var Label = /** @class */ (function () {
    function Label(label) {
        this.rules = new FilterQuery();
        if (label) {
            this.id = label.id;
            this.name = label.name;
            this.rules = new FilterQuery(label.auto_rules);
            this.auto = label.auto;
            this.count = label.count;
            this.isCampaign = label.is_auto_publish_campaign;
        }
    }
    Label.prototype.forExport = function () {
        return {
            auto: this.auto,
            auto_rules: this.rules,
            id: this.id,
            name: this.name,
            is_auto_publish_campaign: this.isCampaign
        };
    };
    Label.prototype.clone = function () {
        var l = new Label();
        l.id = this.id;
        l.name = this.name;
        l.rules = new FilterQuery(this.rules.toJSON());
        l.auto = this.auto;
        l.count = this.count;
        l.isCampaign = this.isCampaign;
        return l;
    };
    return Label;
}());
export { Label };
//export const tmpLabel = 1; // needed for correct export and compilation (buf typescript ?)
