var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { _attributionTypes } from '../listOfValues';
import * as utils from '../misc/utils';
import { Label } from './labels';
var Profile = /** @class */ (function () {
    function Profile(juser, allLabels) {
        var _this = this;
        this.juser = juser;
        this.allLabels = allLabels;
        this.interests = [];
        this.followers = 0;
        this.leadScore = 0;
        this.lastSeen = utils.dates.toZIFromDate(new Date());
        this.nbPosts = 0;
        this.labels = [];
        if (!juser)
            throw 'BAD_CONSTRUCTOR(User)';
        if (juser.isError) {
            this.id = juser.id;
            this.isError = true;
        }
        this.id = juser.id;
        this.kind = juser.kind;
        this.thumbnail = juser.thumbnail;
        this.description = juser.description;
        this.attributionLink = juser.attribution;
        this.attributionKind = juser.kind === _attributionTypes.PLACE ? _attributionTypes.FACEBOOK : juser.kind;
        this.cover = juser.cover;
        this.isEngaged = juser.is_profile_engaged;
        this.gender = juser.gender;
        this.livesIn = juser.live_in;
        // name vs screen name has to be defined (for me, the name is the id, the screen name is the understandable name:
        //if (this.attributionKind !== _attributionTypes.FACEBOOK) {
        this.screenName = juser.screen_name;
        this.name = juser.name;
        /*} else {
                 this.screenName = juser.name;
                 this.name = juser.screen_name;
                 //} */
        // TODO: check for 500px
        //labels
        if (allLabels && juser.profile_label_ids)
            this.labels = juser.profile_label_ids.map(function (id) { return allLabels.find(function (l) { return l.id === id; }); });
        // tobe decided :
        this.leadScore = juser.lead_score;
        this.widgetId = juser.last_widget_id;
        this.nbPosts = juser.total_published;
        this.lastSeen = juser.last_seen;
        this.followers = juser.xscore || juser.b2b_followers || 0;
        this.format = juser.format;
        this.interests = juser.interests && juser.interests.filter(function (interest) { return !!interest; });
        if (juser.interests_details) {
            this.interestsV2 = [];
            juser.interests.filter(function (interest) { return !!interest; }).forEach(function (name, i) {
                if (i >= juser.interests_details.length)
                    return;
                _this.interestsV2.push({
                    name: name,
                    intensity: juser.interests_details[i].intensity,
                    tags: juser.interests_details[i].tags.split(' ').map(function (t) { return t.indexOf('#') === 0 ? t.substr(1) : t; })
                });
            });
        }
        // correct tw thumbnail
        if (this.kind === _attributionTypes.TWITTER)
            this.thumbnail = this.thumbnail.replace('_normal.', '_200x200.');
    }
    Profile.prototype.toInfluencer = function () {
        var I = new Influencer(this.juser, this.labels);
        if (I.attributionKind === _attributionTypes.TWITTER) {
            I.thumbnail = I.thumbnail.replace(/_200x200\./, '_400x400.');
        }
        else if (I.attributionKind === _attributionTypes.INSTAGRAM) {
            //I.thumbnail = I.thumbnail.replace(/s150x150/, 's640x640')
        }
        return I;
    };
    return Profile;
}());
export { Profile };
var Influencer = /** @class */ (function (_super) {
    __extends(Influencer, _super);
    function Influencer(jinfluencer, allILabels) {
        var _this = _super.call(this, jinfluencer, allILabels) || this;
        _this.jinfluencer = jinfluencer;
        _this.allILabels = allILabels;
        _this.engagementRate = jinfluencer.engagement_rate;
        _this.followings = jinfluencer.followings;
        return _this;
    }
    return Influencer;
}(Profile));
export { Influencer };
