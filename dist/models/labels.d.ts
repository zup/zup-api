import { FilterQuery } from './';
export interface inLabel {
    id?: string;
    auto: boolean;
    auto_rules?: any;
    count?: number;
    exclude_profiles?: string[];
    include_profiles?: string[];
    name: string;
    partner_id?: string;
    is_auto_publish_campaign?: boolean;
}
export declare class Label {
    id: string;
    name: string;
    rules: FilterQuery;
    auto: boolean;
    count: number;
    isCampaign: boolean;
    constructor(label?: inLabel);
    forExport(): {
        auto: boolean;
        auto_rules: FilterQuery;
        id: string;
        name: string;
        is_auto_publish_campaign: boolean;
    };
    clone(): Label;
}
