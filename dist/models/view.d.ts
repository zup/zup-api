import { Widget } from './widget';
export interface ViewInterface {
    id?: string;
    queries?: {
        tags?: string[];
        orTags?: string[];
        minusTags?: string[];
    };
    name?: string;
    parentWidget?: Widget;
}
export declare class View extends Widget {
    private _parentTags;
    private _parentMinusTags;
    private _parentOrTags;
    private _parentName;
    private _parentZone;
    constructor(jsonWidget?: any);
    setParent(widget: Widget): void;
    clone(anonymous?: boolean): View;
    readonly jsonObject: any;
    readonly parentZone: any;
    readonly allExluded: string[];
    readonly view: any;
    zone: any;
}
