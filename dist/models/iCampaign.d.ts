export interface ICampaignInterface {
    id?: string;
    name?: string;
    widget_id?: string;
    after?: number;
    before?: number;
    lexical_id?: string;
}
export declare class _ICampaign {
    id?: string;
    name?: string;
    widget_id?: string;
    after?: number;
    before?: number;
    lexical_id?: string;
    dates: {
        from: Date;
        to: Date;
    };
    readonly OK: boolean;
    constructor(jsonIC?: ICampaignInterface);
}
