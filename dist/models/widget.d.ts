import { Subject } from 'rxjs';
import { View, PostFilterRules, ProfileFilterRules, PublishAction } from './';
import { FilterQuery } from "./filters";
export declare const widgetTypes: any;
export declare const widgetCategories: any;
export declare type WidgetView = {
    ends_at?: number;
    starts_at?: number;
    exclude_sources?: string[];
    zone?: {
        n: number;
        s: number;
        e: number;
        w: number;
    };
    tags?: string[];
    or_tags?: string[];
    queries?: {
        and_terms?: string[];
        or_terms?: string[];
    }[];
    exclude_terms?: string[];
    exclude_shares?: boolean;
    exclude_reply?: boolean;
    lang?: string;
    profiles_label_id?: string;
};
export declare class Widget {
    wordsAlert: string[];
    widgetViews: View[];
    protected _metas: any;
    protected _jsonWidget: any;
    protected _view: WidgetView;
    protected _mandatoryKeywords: string[];
    protected _associatedKeywords: string[];
    protected _excludeKeywords: string[];
    protected originalQueries: any;
    private _useDefaultThresholds;
    options: any;
    constructor(widget?: any, type?: number);
    associatedLexical: string;
    category: number | string;
    customerLeadThreshold: number;
    dateRangeDt: {
        from?: Date;
        to?: Date;
    };
    endsAt: number;
    endsAtDt: Date;
    excludedPlaces: string[];
    excludedProfiles: string[];
    id: string;
    influencerThreshold: number;
    readonly isProfileListWidget: boolean;
    brandListeningId: string;
    brandLexicalId: string;
    excludedSources: string[];
    /**
     * @returns {any} a clone of the _jsonWidget (not the original)
     */
    readonly jsonObject: any;
    labelId: string;
    lang: string;
    loyaltyDaysThreshold: number;
    loyaltyNbThreshold: number;
    readonly mainWidgetId: string;
    readonly metas: string;
    minusTags: string[];
    name: string;
    noMission: boolean;
    noReplies: boolean;
    noShares: boolean;
    orTags: string[];
    parentWidgetId: string;
    publishActions: PublishAction[];
    queryTags: {
        tags?: string[];
        orTags?: string[];
        minusTags?: string[];
    };
    startsAt: number;
    startsAtDt: Date;
    sources: string[];
    tags: string[];
    type: number;
    useDefaultThresholds: boolean;
    userDailyThreshold: number;
    readonly view: any;
    zone: any;
    readonly queries: {
        and_terms?: string[];
        or_terms?: string[];
    }[];
    readonly search: {
        tags: string[];
        or_tags: string[];
    };
    readonly safeId: string;
    clone(anonymous?: boolean): Widget;
    getView(viewId: string): View;
    delView(viewId: string): void;
    setView(view: View): void;
    toString(): string;
    protected buildQueries(): void;
}
export declare class WidgetContext {
    private options;
    private filter;
    catId: number;
    _after: number;
    _before: number;
    type: number;
    change: Subject<boolean>;
    filterRulesOptions: {
        dates: boolean;
        widgets: boolean;
    };
    beforeChange: (any?) => Promise<boolean>;
    private _filterRules;
    private _persistantsRules;
    private _widget;
    private _widgetId;
    constructor(widget?: Widget, options?: {
        after?: number;
        before?: number;
        category?: number;
        type?: number;
    }, filter?: PostFilterRules | ProfileFilterRules);
    private readonly _iframe;
    private readonly _frame;
    category: number | string;
    /**
     *
     * @param rules
     * @param options: {dates?: boolean, widgets?: boolean, silent?: boolean}
     *          if dates = true, rules.after over rights context.after (or set to context.after to 0)
     *          if widgets, delete widget and set widget (id/category / type) of the rule
     */
    setFilterRules(rules: ProfileFilterRules | PostFilterRules | FilterQuery, options?: {
        dates?: boolean;
        widgets?: boolean;
        silent?: boolean;
        persistent?: boolean;
    }): void;
    readonly frame: string;
    readonly filterRules: any;
    after: number;
    before: number;
    widget: Widget;
    readonly widgetId: string;
    readonly zone: {
        n: number;
        s: number;
        e: number;
        w: number;
    };
    readonly view: any;
    readonly tags: string[];
    readonly orTags: string[];
    readonly minusTags: string[];
    dateRange: {
        from: Date;
        to: Date;
    };
    getContext(options?: {
        extendedOptions?: Object;
        filter?: boolean;
        timeline?: boolean;
    }): any;
    readonly isWidget: boolean;
    allRange(): WidgetContext;
    clone(): WidgetContext;
    isInContext(o: any): boolean;
    setFilterRule(ruleName: string, ruleValue: any, options?: {
        dates?: boolean;
        widgets?: boolean;
        silent?: boolean;
        persistent?: boolean;
        persistentForce?: boolean;
    }): void;
    /***
     *
     * Does not trigger change
     * @param dr
     * @returns {WidgetContext}
     */
    setRange(dr: {
        from?: Date;
        to?: Date;
    }): this;
    triggerChange(): void;
}
