export declare const lexicalCategories: {
    ALERT: string;
    CLS: string;
    SOV: string;
    INF: string;
    STANDARD: string;
};
export declare const lexicalServices: {
    STANDARD: string;
    WORD_ALERT: string;
    SOV: string;
    SOV_MYBRAND: string;
    SOV_COMPETITOR: string;
    CLS_INTEREST_HI: string;
    CLS_INTEREST_LO: string;
    CLS_BRAND: string;
    CLS_COMPETITOR: string;
    CLS_LINKS: string;
    INF_CAMPAIGN: string;
};
export declare type NewLexical = {
    displayName: string;
    mult?: number;
    partnerId?: string;
    service: string;
    values: string[];
    widgetId?: string;
};
export declare type Lexical = NewLexical & {
    id: string;
    category: string;
};
export declare const toLexical: (l: any) => Lexical;
export declare const fromLexical: (l: NewLexical | Lexical) => any;
