import { Profile } from './profile';
import { Notification } from './notifications';
import { toLOV } from '../listOfValues';
import { Post } from './post';
import { alertTypes } from './';
import { objectFormatTools as tools, dates } from '../misc/utils';
export var screenTypes = {
    ALERT_WORD: alertTypes.detail(alertTypes.WORD),
    ALERT_CUSTOMER_LEAD: alertTypes.detail(alertTypes.CUSTOMER_LEAD),
    ALERT_INFLUENCER: alertTypes.detail(alertTypes.INFLUENCER),
    ALERT_USER_DAILY: alertTypes.detail(alertTypes.USER_DAILY),
    ALERT_LOYALTY: alertTypes.detail(alertTypes.LOYALTY),
    CARD_TOP_USERS: { text: 'Profile from Top User Analytics', color: 'blue', value: 'top_user_analytics' },
    CARD_TOP_POSTS: { text: 'Post from Top Posts Analytics', color: 'green', value: 'top_posts_analytics' },
    CARD_LAST_POSTS: { text: 'Post from All Posts Analytics', color: 'green', value: 'last_posts' },
    CARD_CUSTOMER_LEAD: { text: 'Profile from Customer Lead Analytics', color: 'purple', value: 'customer_lead_analytics' },
    CARD_TASK_LIST: { text: 'Post from Tasks List', color: 'red', value: 'tasks_list' },
    CARD_PROFILES_LIST: { text: 'Profile from Audience profiles', color: 'red', value: 'profiles_list' },
    CARD_BOOKMARKED_PROFILES: { text: 'Profile from Bookmarked profiles', color: 'red', value: 'bookmarked_profiles' },
    UNKNOWN_4USER: { text: 'Profile from profile detail', color: 'red', value: 'unkown_screen_for_profile' },
    UNKNOWN_4POST: { text: 'Post from post detail', color: 'red', value: 'unkown_screen_for_post' },
    WALL_DETAIL: { text: 'Post seen in a wall view', color: 'red', value: 'wall_detail' },
    MOBILE_DEFAULT: { text: 'Post seen on mobile', color: 'red', value: 'action_on_mobile' },
    UNCKNOWN: { text: 'Element seen on inSuite', color: 'blue', value: 'unknown' }
};
toLOV(screenTypes);
var Task = /** @class */ (function () {
    /*
    * @parametre {object} can be a Notificaiton, a Post or a User
    * */
    function Task(jsonTask, object, assignedBy, assignedTo) {
        this.id = jsonTask.id;
        this.createdAt = jsonTask.created_at;
        this.closedAt = jsonTask.closed_at;
        this.isClosed = jsonTask.is_closed;
        this.partnerId = jsonTask.partner_id;
        this.widgetId = jsonTask.widget_id;
        this.assignedBy = jsonTask.assigned_by;
        this.assignedByUser = assignedBy;
        this.assignedTo = jsonTask.assigned_to;
        this.assignedToUser = assignedTo;
        this.notificationId = jsonTask.notification_id;
        this.postId = jsonTask.post_id;
        this.userId = jsonTask.profile_id;
        this.message = jsonTask.message;
        this.screen = (jsonTask.screen && jsonTask.screen.length > 0) ? jsonTask.screen : undefined;
        this.object = object;
        if (this.postId) {
            this.objectAttributionType = tools.getAttributionTypeFromId(this.postId);
            this.objectAttributionLink = this.object.attributionLink;
            if (this.object instanceof Post) {
                // for post we do not have the widgetID, we use the one of the user (can be false)
                this.widgetId = this.object.user.widgetId;
            }
        }
        else if (this.notificationId) {
            this.objectAttributionType = tools.getAttributionTypeFromId(this.object.postId);
            this.objectAttributionLink = this.object.attributionLink;
        }
        else if (this.userId) {
            this.objectAttributionType = this.object.attributionKind;
            this.objectAttributionLink = this.object.attributionLink;
            this.widgetId = this.object.widgetId;
        }
    }
    return Task;
}());
export { Task };
var TaskEvent = /** @class */ (function () {
    function TaskEvent(jsonTask) {
        this.id = jsonTask.id;
        this.isClosed = jsonTask.is_closed;
        this.widgetId = jsonTask.widget_id;
        this.assignedBy = jsonTask.assigned_by;
        this.assignedTo = jsonTask.assigned_to;
        this.notificationId = jsonTask.notification_id;
        this.postId = jsonTask.post_id;
        this.profileId = jsonTask.profile_id;
        this.message = jsonTask.message;
        this.screen = jsonTask.screen;
        this.start = jsonTask.starts_at;
        this.end = jsonTask.ends_at;
        this.title = jsonTask.title;
        this.allDay = jsonTask.allDay;
    }
    TaskEvent.prototype.toCalendarEvent = function (me) {
        var color = me === this.assignedTo ? { primary: '#ff0000', secondary: 'rgba(255,0,0,0.3)' }
            : { primary: '#0000ff', secondary: 'rgba(0, 0, 255, 0.3)' };
        return {
            title: this.title,
            start: dates.toDateFromZI(this.start),
            end: dates.toDateFromZI(this.end),
            color: color,
            draggable: false,
            resizable: {
                beforeStart: false,
                afterEnd: false
            },
            meta: {
                taskId: this.id,
                profileId: this.profileId
            }
        };
    };
    return TaskEvent;
}());
export { TaskEvent };
