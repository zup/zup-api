import { Profile } from './profile';
import { Post, Location } from './post';
export declare const alertTypes: any;
export declare type JsonNotification = {
    id: string;
    type: string;
    follower: string;
    action: string;
    action_author: string;
    action_type: string;
    action_text: string;
    post: string;
    post_attribution: string;
    post_author: string;
    post_location: Location;
    created: number;
    closed_at: number;
    is_closed: boolean;
    payload: any;
    widget_id: string;
};
export interface NotificationInterface {
    id: string;
    type: string;
    isError?: boolean;
    user?: Profile;
    post?: Post;
    postId: string;
    created: number;
    payload: any;
    widgetId: string;
    text: string;
    attributionType: string;
    attributionLink: string;
    closedAt: number;
    isClosed: boolean;
}
export declare class Notification implements NotificationInterface {
    id: string;
    type: string;
    isError: boolean;
    user?: Profile;
    post?: Post;
    postId: string;
    created: number;
    payload: any;
    widgetId: string;
    text: string;
    attr: string;
    attributionType: string;
    attributionLink: string;
    closedAt: number;
    isClosed: boolean;
    constructor(alert: JsonNotification, user?: Profile, post?: Post);
}
