import { Profile, JsonProfile } from './';
import * as utils from '../misc/utils';
import { wallJsonPost } from './';
var Post = /** @class */ (function () {
    function Post(post, user) {
        if (post.isError || user.isError) {
            this.id = post.id;
            this.isError = true;
            return;
        }
        // manage temporarly 2 formats :
        if (post.author_id) {
            // format rive
            //throw 'BAD_FORMAT(Post)';
            this.id = post.id;
            this.created = post.created;
            this.text = post.text || post.short_text;
            var media = utils.objectFormatTools.parseMedia(post.medias);
            //this.thumbnail = utils.objectFormatTools.getImageUrl(post.thumbnail, post.medias);
            this.thumbnail = media.imgUrl || post.thumbnail;
            this.video = media.videoUrl;
            this.videoType = media.videoType;
            this.attributionType = post.attribution ?
                utils.objectFormatTools.getAttributionTypeFromAttribution(post.attribution) : utils.objectFormatTools.getAttributionTypeFromId(post.id);
            this.authorId = post.author_id;
            this.attribution = post.attribution;
            this.attributionLink = post.attribution;
            this.position = { lat: post.lat, lng: post.lng, geo_name: post.geo_name };
            this.xcomments = post.xcomments;
            this.xlikes = post.xlikes;
            this.xshares = post.xshares;
        }
        else {
            // format B2B
            this.id = post.id;
            this.created = post.created;
            this.text = post.content.text;
            //this.thumbnail = utils.objectFormatTools.getImageUrl(post.content.thumbnail);
            var media = utils.objectFormatTools.parseMedia(post.content.medias);
            this.thumbnail = media.imgUrl || post.content.thumbnail;
            this.video = media.videoUrl;
            this.videoType = media.videoType;
            this.attributionType = post.kind;
            this.authorId = post.authorid;
            this.attribution = post.content.attribution;
            this.attributionLink = post.content.attribution;
            this.tags = post.content.tags;
            this.position = post.position;
            this.xcomments = post.xcomments;
            this.xlikes = post.xlikes;
            this.xshares = post.xshares;
            this.lang = post.content.lang;
            this.sentimentScore = post.content.sentiment_score;
            this.lexicalIds = post.lexical_ids || [];
            this.engagements = post.xscore;
            this.isShared = post.content.is_share;
        }
        if (user instanceof Profile) {
            this.user = user;
        }
        else {
            this.user = new Profile(user);
        }
    }
    Object.defineProperty(Post.prototype, "elapsedTime", {
        get: function () {
            return utils.dates.toElapsedTimeFromZIDate(this.created);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "wallJsonPost", {
        get: function () {
            return {
                id: this.id,
                created: this.created,
                isShared: this.isShared,
                attributionKind: this.attributionType,
                attributionLink: this.attributionLink,
                position: { geo_name: this.position.geo_name },
                text: this.text,
                thumbnail: this.thumbnail,
                video: this.video,
                videoType: this.videoType,
                user: {
                    id: this.user.id,
                    name: this.user.name,
                    screenName: this.user.screenName,
                    thumbnail: this.user.thumbnail
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Post.prototype, "engagement", {
        get: function () {
            if (!utils.tools.deepGet(this.user, 'followers'))
                return;
            return (this.xlikes + this.xcomments) / this.user.followers;
        },
        enumerable: true,
        configurable: true
    });
    return Post;
}());
export { Post };
