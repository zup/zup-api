export var lexicalCategories = {
    ALERT: 'Alert',
    CLS: 'CLS',
    SOV: 'SOV',
    INF: 'INF',
    STANDARD: 'Standard'
};
export var lexicalServices = {
    STANDARD: 'Standard',
    WORD_ALERT: 'B2B_word_alert_lexical_name',
    SOV: 'B2B_SOV',
    SOV_MYBRAND: 'B2B_SOV_MYBRAND',
    SOV_COMPETITOR: 'B2B_SOV_COMPETITOR',
    CLS_INTEREST_HI: 'B2B_customer_lead_interest_high',
    CLS_INTEREST_LO: 'B2B_customer_lead_interest_low',
    CLS_BRAND: 'B2B_customer_lead_brand_screennames',
    CLS_COMPETITOR: 'B2B_customer_lead_concurrent_screennames',
    CLS_LINKS: 'B2B_customer_lead_link',
    INF_CAMPAIGN: 'B2B_SOV_INF_campaign'
};
export var toLexical = function (l) {
    return {
        category: l.category,
        displayName: l.display_name,
        id: l.id,
        mult: l.mult,
        partnerId: l.partner_id,
        service: l.service,
        values: l.values,
        widgetId: l.widget_id
    };
};
export var fromLexical = function (l) {
    return {
        id: l.id,
        display_name: l.displayName,
        mult: l.mult,
        partner_id: l.partnerId,
        service: l.service,
        values: l.values,
        widget_id: l.widgetId
    };
};
