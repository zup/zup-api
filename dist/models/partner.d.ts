import { Widget, Profile } from './';
export declare type JsonPartner = {
    created: number;
    description: string;
    email: string;
    metas: string;
    name: string;
    partner_id: string;
    rights: string[];
    updated: number;
};
export declare type Member = {
    created: number;
    email: string;
    id: string;
    locale: string;
    name: string;
    updated: string;
};
/**
 * A partner is a client, he can have several Widgets and Members
 */
export declare class Partner {
    advancedQueries: {
        name: string;
        query: any;
        type?: string;
    }[];
    logo: string;
    name: string;
    email: string;
    description: string;
    widgets: Widget[];
    members: Member[];
    socialAccounts: Profile[];
    fbProfilesLoaded: boolean;
    rights: string[];
    defaultLimits: any;
    defaultWordsAlert: string[];
    recordedMessages: {
        attribution: string;
        message: string;
        name?: string;
    }[];
    roUsers: string[];
    woUsers: string[];
    widgetAndViews: Map<string, Widget>;
    private _id;
    private _metas;
    constructor(partner?: JsonPartner, widgets?: Widget[], members?: Member[]);
    readonly id: string;
    readonly metas: string;
    removeMember(id: string): void;
    setInsuiteRights(hasRights: boolean): void;
    readonly hasInsuite: boolean;
    getTmpInfo(key: any): any;
    setTmpInfo(key: any, value: any): void;
}
