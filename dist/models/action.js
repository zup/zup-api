import { Profile, Widget, Task, Post, Notification } from './';
import { toLOV } from '../listOfValues';
import { publish } from "rxjs/operator/publish";
import { Label } from './';
export var actionTypes = {
    BOOKMARK: {
        value: 'bookmark',
        name: 'Add to list',
        icon: 'fa fa-bookmark-o',
        icon2: 'fa fa-bookmark',
        description: 'Add this user to your list',
        description2: 'Number of people bookmarked',
        actionText: 'Profile was <b>bookmarked</b>',
        actionHtml: '<b>$0</b> was bookmarked'
    },
    UNBOOKMARK: {
        value: 'unbookmark',
        name: 'Remove from list',
        icon: 'fa fa-times',
        description: 'Remove this user from this list'
    },
    LIKE: {
        value: 'like',
        name: 'Like',
        icon: 'fa fa-heart-o',
        icon2: 'fa fa-heart',
        description: 'Like this post',
        description2: 'Number of likes sent',
        actionText: 'Post was <b>liked</b>',
        actionHtml: 'Post from <b>$0</b> was liked',
        kind: 'publish'
    },
    COMMENT: {
        value: 'comment',
        name: 'Comment',
        icon: 'fa fa-comment-o',
        icon2: 'fa fa-comment',
        description: 'Comment this post',
        description2: 'Number of comments sent',
        actionText: 'A post was <b>commented</b>',
        actionHtml: 'A post of <b>$0</b> was <b>commented</b>',
        kind: 'publish'
    },
    RETWEET: {
        value: 'retweet',
        name: 'Retweet',
        icon: 'fa fa-retweet',
        icon2: 'fa fa-retweet',
        description: 'Retweet this post',
        description2: 'Number of retweets done',
        actionText: 'A post was <b>retweeted</b>',
        actionHtml: 'A post of <b>$0</b> was <b>retweeted</b>',
        kind: 'publish'
    },
    UNRETWEET: {
        value: 'unretweet',
        name: 'Unretweet',
        icon: 'fa fa-retweet',
        icon2: 'fa fa-retweet',
        description: 'unretweet this post',
        description2: 'number of retweet deleted',
        actionText: 'A retweet was deleted',
        kind: 'publish'
    },
    FOLLOW: {
        value: 'follow',
        name: 'Follow user',
        icon: 'fa fa-plus-square-o',
        icon2: 'fa fa-plus-square',
        description: 'Follow this user',
        description2: 'Number of people followed',
        actionText: 'Profile was <b>followed</b>',
        actionHtml: '<b>$0</b> was followed',
        kind: 'publish'
    },
    ASSIGN: {
        value: 'assign',
        name: 'Assign to',
        icon: 'fa fa-send',
        description: function (o) {
            if (o instanceof Notification) {
                return 'Transfer this notification to a member of your team';
            }
            else if (o instanceof Task) {
                return 'Transfer this task to a member of your team';
            }
            else if (o instanceof Post) {
                return 'Create a task on this post for a member of your team';
            }
            else if (o instanceof Profile) {
                return 'Create a task on this profile for a member of your team';
            }
            else {
                return 'Create a task for a member of your team';
            }
        },
        actionText: '<b>Task</b> was created'
    },
    CLEAR: {
        value: 'clear',
        name: 'Archive',
        icon: 'fa fa-check-circle',
        description: 'Flag this notification as archived'
    },
    SEND_MESSAGE: {
        value: 'message',
        name: 'Message',
        icon: 'fa fa-comment-o',
        description: 'Send a message to this user',
        actionText: 'Message was <b>sent</b>',
        actionHtml: 'A message was sent to <b>$0</b>',
        kind: 'publish'
    },
    MENTION: {
        value: 'message',
        name: 'Message',
        icon: 'fa fa-comment-o',
        description: 'Send a message to this user',
        actionText: 'Message was <b>sent</b>',
        actionHtml: 'A message was sent to <b>$0</b>',
        kind: 'publish'
    },
    CLOSE_ALERT: {
        value: 'clear_alert',
        name: 'Archive',
        icon: 'fa fa-check-circle',
        description: 'Archive this notification',
        actionText: 'Notification was archived',
    },
    CLOSE_TASK: {
        value: 'clear_task',
        name: 'Archive',
        icon: 'fa fa-check-circle',
        description: 'Archive this task',
        actionText: 'Task was archived',
    }
};
toLOV(actionTypes);
var Action = /** @class */ (function () {
    function Action(json) {
        this.id = json.id;
        this._origin = json.origin;
    }
    Object.defineProperty(Action.prototype, "class", {
        get: function () {
            return actionTypes.getClass(this.actionType);
        },
        enumerable: true,
        configurable: true
    });
    return Action;
}());
export { Action };
var CONST_TRANSLATION = {
    ASSIGNMENT: 'ASSIGN'
};
export function toActionLog(o, member) {
    // transform close_alert into close_task
    if (o.type === 'CLOSE_ALERT' && !!o.task_id)
        o.type = 'CLOSE_TASK';
    return {
        id: o.id,
        created: o.created_at,
        campaignId: o.auto_publish_campaign_id,
        partnerId: o.partner_id,
        widgetId: o.widget_id,
        memberId: o.member_id !== 'auto' ? o.member_id : null,
        member: member,
        auto: o.member_id === 'auto',
        postId: o.post_id,
        profileId: o.profile_id,
        type: CONST_TRANSLATION[o.type] || o.type,
        text: o.payload ? JSON.parse(o.payload).data : "",
        publishType: o.publish_type,
        publishAccountId: o.by_token || undefined,
        notificationId: o.notification_id,
        taskId: o.task_id,
        screen: o.screen
    };
}
export function toPublishAction(o) {
    return {
        id: o.id,
        widgetId: o.widget_id,
        tokenId: o.token_xid,
        like: o.like,
        follow: o.follow,
        comment: o.comment,
        mention: o.mention,
        message: o.message,
        mentionMessage: o.mention_message,
        files: (o.media_urls || []).map(function (u, i) { return { id: o.media_ids[i], url: u }; }),
        mentionFiles: (o.mention_media_urls || []).map(function (u, i) { return { id: o.mention_media_urls[i], url: u }; }),
        since: ((o.since || 0) / 60) || null,
        once: o.once_for_parter,
        rateLimit: o.rate_limit
    };
}
