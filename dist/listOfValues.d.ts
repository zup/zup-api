export declare const notificationsStatus: {
    PENDING: number;
    CLEARED: number;
    ASSIGN: number;
};
export declare const attributionTypes: any;
export declare const _alertTypes: {
    WORD: string;
    INFLUENCER: string;
    LOYALTY: string;
    USER_DAILY: string;
    CUSTOMER_LEAD: string;
    getType(t: string): any;
};
export declare const actionsType: {
    BOOKMARK: string;
    LIKE: string;
    COMMENT: string;
    FOLLOW: string;
    ASSIGN: string;
    CLEAR: string;
    SEND_MESSAGE: string;
    RETWEET: string;
    getType: (t: string) => {
        name: string;
        icon: string;
        description: string;
    };
};
export declare const _attributionTypes: any;
export declare const attributionSources: any;
export declare const attributionSourcesDetail: any;
export declare function toLOV(values: any, additionalMethods?: any): any;
