export var notificationsStatus = {
    PENDING: 0,
    CLEARED: 2,
    ASSIGN: 1
};
export var attributionTypes = {
    TWITTER: 'tw',
    INSTAGRAM: 'ig',
    FACEBOOK: 'fb',
    PX500: 'px',
    PLACE: 'place',
    YOUTUBE: 'yt',
    FACEBOOKWORKPLACE: 'fw',
    YAMMER: 'ym',
    F60MC: 'f6',
    getType: function (type) {
        switch (type) {
            case this.TWITTER:
                return { id: this.TWITTER, icon: 'fa fa-twitter', text: 'Twitter' };
            case this.INSTAGRAM:
                return { id: this.INSTAGRAM, icon: 'fa fa-instagram', text: 'Instagram' };
            case this.FACEBOOK:
                return { id: this.FACEBOOK, icon: 'fa fa-facebook', text: 'Facebook' };
            case this.PLACE:
                return { id: this.FACEBOOK, icon: 'fa fa-facebook', text: 'Facebook' };
            case this.PX500:
                return { id: this.PX500, icon: 'fa fa-500px', text: '500px' };
        }
    }
};
export var _alertTypes = {
    WORD: 'b2b_word',
    INFLUENCER: 'b2b_influencer',
    LOYALTY: 'b2b_loyalty',
    USER_DAILY: 'b2b_user_daily',
    CUSTOMER_LEAD: 'b2b_customer_lead',
    getType: function (t) {
        switch (t) {
            case this.WORD:
                return {
                    text: 'World Alert',
                    color: 'red',
                    title: 'The sensitive word "$0" was used in a user\'s post',
                    description: 'Be notified when a post containing a specific word is detetced',
                    variable: 'word',
                    value: 'b2b_word'
                };
            case this.INFLUENCER:
                return {
                    text: 'Influencer Alert',
                    color: 'blue-dark',
                    title: 'An influencer with more than $0 followers has posted in this place',
                    description: 'Be notified when an influencer publishes content',
                    variable: 'profile_x_score',
                    value: 'b2b_influencer'
                };
            case this.LOYALTY:
                return {
                    text: 'Loyalty Alert',
                    color: 'purple',
                    title: 'A user has returned $0 times in this place',
                    description: 'Be notified when a user comes back several times in the same place',
                    variable: 'day_count',
                    value: 'b2b_loyalty'
                };
            case this.USER_DAILY:
                return {
                    text: 'Top users',
                    color: 'green',
                    title: 'A user has postedd $0 times in one day',
                    description: 'Be notified when someone publishes content multiple times in the same place',
                    variable: 'score',
                    value: 'b2b_user_daily'
                };
            case this.CUSTOMER_LEAD:
                return {
                    text: 'Customer Lead',
                    color: 'orange',
                    title: 'A strong <span>cutomer lead</span> has been detected in this place ',
                    description: 'Be notified when a strong potential customer is detected',
                    variable: 'score',
                    value: 'b2b_customer_lead'
                };
            default:
                return {
                    text: "Unknown Alert",
                    color: 'red',
                    title: "Unknown Alert: " + t
                };
        }
    }
};
Object.defineProperty(_alertTypes, 'ALLTYPES', {
    get: function () {
        return ['b2b_word', 'b2b_influencer', 'b2b_loyalty', 'b2b_user_daily', 'b2b_customer_lead'];
    }
});
export var actionsType = {
    BOOKMARK: 'bookmark',
    LIKE: 'like',
    COMMENT: 'comment',
    FOLLOW: 'follow',
    ASSIGN: 'assign',
    CLEAR: 'clear',
    SEND_MESSAGE: 'write',
    RETWEET: 'retweet',
    getType: function (t) {
        switch (t) {
            case this.BOOKMARK:
                return { name: 'Bookmark', icon: 'fa fa-bookmark-o', description: 'Bookmark this user' };
            case this.LIKE:
                return { name: 'Like', icon: 'fa fa-heart-o', description: 'Like this post' };
            case this.COMMENT:
                return { name: 'Comment', icon: 'fa fa-comment-o', description: 'Comment this post' };
            case this.SEND_MESSAGE:
                return { name: 'Comment', icon: 'fa fa-comment-o', description: 'Send a message to this user' };
            case this.FOLLOW:
                return { name: 'Follow user', icon: 'fa fa-plus-square-o', description: 'Follow this user' };
            case this.ASSIGN:
                return { name: 'Assign to', icon: 'fa fa-send', description: 'Transfert this notification to a member of your team' };
            case this.CLEAR:
                return { name: 'Archive', icon: 'fa fa-check-circle', description: 'Flag this notification as archived' };
            case this.RETWEET:
                return { name: 'Retweet', icon: 'fa fa-retweet', description: 'Retweet this post' };
        }
    }
};
export var _attributionTypes = {
    TWITTER: { value: 'tw', icon: 'fa fa-twitter', text: 'Twitter' },
    INSTAGRAM: { value: 'ig', icon: 'fa fa-instagram', text: 'Instagram' },
    FACEBOOK: { value: 'fb', icon: 'fa fa-facebook', text: 'Facebook', hidden: true },
    YOUTUBE: { value: 'yt', icon: 'fa fa-youtube', text: 'Youtube' },
    PX500: { value: 'px', icon: 'fa fa-500px', text: '500px', hidden: true },
    PLACE: { value: 'place', icon: 'fa fa-facebook', text: 'Facebook', hidden: true, sys: true },
    FACEBOOKWORKPLACE: { value: 'fw', icon: 'fa fa-facebook', text: 'Facebook Workplace', hidden: true },
    YAMMER: { value: 'ym', icon: 'inl-yammer', text: 'Yammer', hidden: true },
    F60MC: { value: 'f6', icon: 'inl-60mc', text: '60 Millions de consommateurs', hidden: true },
    UFC: { value: 'uf', icon: 'fa fa-quora', text: 'UFC que choisir', hidden: true },
    TRIPADVISOR: { value: 'tr', icon: 'fa fa-tripadvisor', text: 'Trip Advisor', hidden: true }
};
toLOV(_attributionTypes);
//export const attributionSources = _attributionTypes.values.filter((t: any) => !t.hidden).map(s => s.value);
export var attributionSources = {
    TWITTER: 'tw',
    INSTAGRAM: 'ig',
    FACEBOOK: 'fb',
    YOUTUBE: 'yt',
    PX500: 'px',
    PLACE: 'place',
    FACEBOOKWORKPLACE: 'fw',
    YAMMER: 'ym',
    F60MC: 'f6',
    UFC: 'uf',
    TRIPADVISOR: 'tr'
};
export var attributionSourcesDetail = {
    tw: { value: 'tw', icon: 'fa fa-twitter', text: 'Twitter' },
    ig: { value: 'ig', icon: 'fa fa-instagram', text: 'Instagram' },
    fb: { value: 'fb', icon: 'fa fa-facebook', text: 'Facebook' },
    yt: { value: 'yt', icon: 'fa fa-youtube', text: 'Youtube', hidden: true },
    px: { value: 'px', icon: 'fa fa-500px', text: '500px', hidden: true },
    place: { value: 'place', icon: 'fa fa-facebook', text: 'Facebook', hidden: true, sys: true },
    fw: { value: 'fw', icon: 'fa fa-facebook', text: 'Facebook Workplace', hidden: true },
    ym: { value: 'ym', icon: 'inl-yammer', text: 'Yammer', hidden: true },
    f6: { value: 'f6', icon: 'inl-60mc', text: '60 Millions de consommateurs', hidden: true },
    uf: { value: 'uf', icon: 'fa fa-quora', text: 'UFC Que choisir', hidden: true },
    tr: { value: 'tr', icon: 'fa fa-tripadvisor', text: 'Trip Advisor', hidden: true }
};
export function toLOV(values, additionalMethods) {
    var mapProperties = new Map();
    for (var _i = 0, _a = Object.keys(values); _i < _a.length; _i++) {
        var p = _a[_i];
        if (!values[p].hasOwnProperty('value'))
            throw "INCORRECT_VALUE_FORMAT(" + p + ")";
        mapProperties.set(values[p].value, values[p]);
        values[p] = values[p].value;
    }
    ;
    Object.assign(values, {
        detail: function (p) {
            return this.properties.get(p);
        },
        keys: Array.from(mapProperties.keys()),
        values: Array.from(mapProperties.values()),
        properties: mapProperties,
        has: function (k) { return mapProperties.has(k); }
    }, additionalMethods);
    return values;
}
