var gulp = require("gulp");
var ts = require("gulp-typescript");
var inlineNg2Template = require('gulp-inline-ng2-template');
var sass = require('gulp-sass');
var ext_replace = require('gulp-ext-replace');
var del = require('del');

gulp.task("default", ['clean:tmp', 'move:tmp', 'sass', 'change:scss:tmp', 'change:css:scss'],function () {

    return gulp.src('./tmp/**/*.ts')
        .pipe(inlineNg2Template({ useRelativePaths: true }))
        .pipe(gulp.dest("tmp"));

});

gulp.task('move:tmp', ['clean:tmp'], function () {
  return gulp.src('./zupApi/**/*.*')
    .pipe(gulp.dest('./tmp'));
});

gulp.task('sass', ['clean:tmp', 'move:tmp'],function () {
    return gulp.src('./tmp/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./tmp'));
});

gulp.task('change:scss:tmp', ['clean:tmp', 'move:tmp', 'sass'],function() {
  gulp.src('./tmp/**/*.scss')
    .pipe(ext_replace('.tmp'))
    .pipe(gulp.dest('tmp'))
});

gulp.task('change:css:scss', ['clean:tmp', 'move:tmp', 'sass', 'change:scss:tmp'],function() {
  gulp.src('./tmp/**/*.css')
    .pipe(ext_replace('.scss'))
    .pipe(gulp.dest('tmp'))
});




gulp.task('clean:scss',function () {
  return del([
    './tmp/**/*.scss'
  ]);
});

gulp.task('clean:tmp' ,function () {
    return del([
        './tmp'
    ]);
});
